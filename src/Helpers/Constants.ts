export const metaMaskAddy = '0xa3602881b28571500f0CAb8b8f697A973ae87F9b';
export const colorPalette: Record<string, string> = {
  white: 'white',
  op: '#09e701',
  meta: '#64c642',
  good: '#98d85d',
  decent: '#dad72c',
  average: '#dfb708',
  moderate: '#e7862c',
  inferior: '#dd5119',
  trash: '#c20000',
  ruygye: '#e453cc',
  experience: '#2161bf',
  lightBlueGrey: '#7587a6',
  mediumBlueGrey: '#2d3748',
  mediumBlue: '#3779b6',
  alphaMediumGrey: '#2d3748cc',
  darkBlueGrey: '#1a202c',
  title: '#3779b6',
  human: '#e4c47a',
  whiteElf: '#e7e7e7',
  blueElf: '#5d7ad8',
  orc: '#26bd21',
  fairy: '#d634c9',
  skeleton: '#858585',
  cyborg: '#9b4c4c',
  darkElf: '#353535',
  none: '#7587a6',
  common: 'rgb(169, 169, 169)',
  uncommon: 'rgb(0, 128, 0)',
  rare: 'rgb(63, 66, 204)',
  epic: '#A335EE',
  legendary: 'rgb(255, 215, 0)',
  mythic: '#E6CC80',
  heirloom: '#00CCFF',
  limited: '#FFFFFF',
  rune: 'rgb(178, 149, 80)',
  transparent: 'rgba(0, 0, 0, 0)',
  gridElementSelected: 'rgba(116, 116, 116, 0.5)',
  strength: '#8f0c0c',
  agility: '#c8a508',
  intelligence: '#0761b6',
  wisdom: '#dd8460',
  charm: '#d04dd0',
  luck: '#2bbd43',
  offensive: 'rgb(255, 36, 0)',
  defensive: 'rgb(63, 66, 204)',
  utility: 'rgb(255, 215, 0)',
  blackOpaque: '#0d0d0dd1',
};
export const tammyAvatarTriggerLSName: string = 'CRC_tammy_avatar_trigger';
export const inventoryPopUpClassName: string = 'css-1lz89iw';
export const inventoryItemParentsClassName: string = '.css-gcwpim';
export const inventoryClassName: string = '.css-1m7f4zo';
export const inventoryUpdateButtonClassName: string = '.css-7g3iss';
export const itemNameFieldClassName: string = '.css-1t6b1jw';
export const itemFooterFieldClassName: string = '.css-1uyazt3';
export const raiderInfosClassName: string = '.css-1w86ha4';
export const raiderInventoryButtonClassName: string = '.css-1uc6km0';
export const raiderDivClassName: string = 'css-7zm89b';

export const questIcon: string = 'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/basicQuest.png';
export const clockIcon: string = 'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/clock.png';
export const herbalismIcon: string =
  'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/herbalism.png';
export const alchemyIcon: string = 'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/emptyVial.png';
export const sporeBarkIcon: string =
  'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/sporebark.png';
export const grimWeedIcon: string = 'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/grimweed.png';
export const newtIcon: string = 'https://storage.googleapis.com/crypto-raiders-assets/web/currencies/eye_of_newt.png';
export const levelSocketIcon: string = 'https://econ.cryptoraiders.xyz/levelSocket.png';

export const notQuesting = 1;
export const inQuest = 2;
export const comingBack = 3;
export const collectReward = 4;

export const humanizeBaseConfig = {
  language: 'shortEn',
  languages: {
    shortEn: {
      y: () => 'y',
      mo: () => 'mo',
      w: () => 'w',
      d: () => 'd',
      h: () => 'h',
      m: () => 'm',
      s: () => 's',
      ms: () => 'ms',
    },
  },
  units: ['d', 'h', 'm'],
  round: true,
  conjunction: ' and ',
  serialComma: false,
};
