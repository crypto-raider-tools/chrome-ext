import { Stats } from '@crypto-raider-tools/computation';
import { DetailedSyncRaider } from '../interfaces/detailedsyncraider';
import { IndexedInventoryItem } from '../interfaces/indexedInventoryItem';

/**
 * Handle equip action
 * @param raider
 * @param object
 * @returns
 */
export const triggerCRUpdateRaider = async (
  raider: DetailedSyncRaider,
  equippedItems: Array<IndexedInventoryItem> | undefined = undefined,
  raiderStats: Stats | undefined = undefined
): Promise<boolean> => {
  const equippedItemIndexes = (equippedItems ?? []).map((object) => object.originalIndex);
  const equipRequest = await fetch(`https://play.cryptoraiders.xyz/api/update?id=${raider.raw_data?.tokenId}`, {
    method: 'POST',
    headers: {
      Cookie: document.cookie,
      'Content-Type': 'application/json;charset=UTF-8',
      Accept: 'application/json, text/plain, */*',
    },
    body: JSON.stringify({
      name: raider?.raw_data?.name,
      avatar: raider.raw_data?.avatar,
      inventory_count: raider.raw_data?.inventory.length,
      equipped_indices: equippedItemIndexes,
      stats: raiderStats ? raiderStats : raider.raw_data?.stats,
      abilities: raider.raw_data?.abilities ?? [],
    }),
  });
  if (equipRequest.status !== 200) {
    alert('Unable to equip item, sry 😿');
    return Promise.resolve(false);
  }
  return Promise.resolve(true);
};
