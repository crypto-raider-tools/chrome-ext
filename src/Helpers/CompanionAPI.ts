import { apiEntrypoint, log } from '../config';
import { DetailedDungeon } from '../interfaces/detailedDungeon';
import { DetailedMobs } from '../interfaces/detailedMobs';
import { HydraPaginatedResponse } from '../interfaces/hydra';
import { RaiderAbility } from '../interfaces/raiderability';
import { StuffStats } from '../interfaces/stuffstats';
import { SyncStuff } from '../interfaces/syncstuff';
import { QueryFilter, QueryOrder } from '../PlayComponent/CRCStuff/types';

/**
 * Force server to update data
 */
export const triggerUpdateServer = async (raiderId: number = 0) => {
  try {
    const syncRequest = await fetch(`${apiEntrypoint}/update-sync-raiders${raiderId > 0 ? `/${raiderId}` : ''}`, {
      headers: {
        'x-original-token': document.cookie,
      },
    });
    if (syncRequest.status !== 200) {
      console.warn(`${apiEntrypoint}/update-sync-raiders: ${syncRequest.status} ${syncRequest.statusText}`);
      throw new Error('CRC: Unable to sync your raiders, sry 😿');
    }
    log('Raiders are synced server side');
    return Promise.resolve();
  } catch (error) {
    console.warn(error);
    alert('CRC: Unable to sync your raiders, sry 😿');
    return Promise.reject();
  }
};

export const triggerUpdateQuestServer = async (raiderId: number = 0) => {
  try {
    const syncRequest = await fetch(`${apiEntrypoint}/update-quest-raiders${raiderId > 0 ? `/${raiderId}` : ''}`, {
      headers: {
        'x-original-token': document.cookie,
      },
    });
    if (syncRequest.status !== 200) {
      console.warn(`${apiEntrypoint}/update-quest-raiders: ${syncRequest.status} ${syncRequest.statusText}`);
      throw new Error("CRC: Unable to sync your raider's quest, sry 😿");
    }
    log("Raider's quest are synced server side");
    return Promise.resolve();
  } catch (error) {
    console.warn(error);
    alert("CRC: Unable to sync your raider's quest, sry 😿");
    return Promise.reject();
  }
};

export const fetchStuffStats = async (): Promise<StuffStats[]> => {
  const request = await fetch(`${apiEntrypoint}/stuff_stats?itemsPerPage=9999`, {
    headers: {
      'x-original-token': document.cookie,
    },
  });
  if (request.status !== 200) {
    log(`/stuffStats: ${request.status} ${request.statusText}`);
    return Promise.reject('Unable to get stuff stats, sry 😿');
  }
  const data = await request.json();
  log('StuffStats Fetch', data);
  const newStuffStats = data?.['hydra:member'];
  return Promise.resolve(newStuffStats);
};

export const fetchRaiderAbilities = async (): Promise<RaiderAbility[]> => {
  const request = await fetch(`${apiEntrypoint}/raider_abilities`, {
    headers: {
      'x-original-token': document.cookie,
    },
  });
  if (request.status !== 200) {
    log(`/raider_abilities: ${request.status} ${request.statusText}`);
    return Promise.reject('Unable to get Raider Abilities data, sry 😿');
  }
  const data = await request.json();
  log('Raider Abilities Fetch', data);
  const abilities = data?.['hydra:member'];
  return Promise.resolve(abilities);
};

export const fetchDungeonsData = async (): Promise<DetailedDungeon[]> => {
  log('Dungeon Fetch');
  const request = await fetch(`${apiEntrypoint}/dungeons?page=1&itemsPerPage=100`);
  const dungeons: any = await request.json();
  if (request.status !== 200) {
    log(`/dungeons: ${request.status} ${request.statusText}`);
    return Promise.reject('Unable to get mobs data, sry 😿');
  }
  const dungeonsList = dungeons?.['hydra:member'];
  log('Dungeon Fetch DONE');
  return Promise.resolve(dungeonsList);
};

export const fetchMobsData = async (): Promise<DetailedMobs[]> => {
  log('Mobs Fetch');
  const request = await fetch(`${apiEntrypoint}/mobs`, {
    headers: {
      'x-original-token': document.cookie,
    },
  });
  if (request.status !== 200) {
    log(`/mobs: ${request.status} ${request.statusText}`);
    return Promise.reject('Unable to get mobs data, sry 😿');
  }
  const data: any = await request.json();
  log('Mobs Fetch DONE');
  const newMobsData = data?.['hydra:member'];
  return Promise.resolve(newMobsData);
};

export const fetchSyncStuffs = async (
  elementPerPage: number,
  page: number,
  metamask: string,
  currentFilters: QueryFilter[],
  currentOrderBys: QueryOrder[]
): Promise<HydraPaginatedResponse<SyncStuff> | undefined> => {
  let url = new URL(`${apiEntrypoint}/sync_stuffs`);
  url.searchParams.append('itemsPerPage', elementPerPage.toString());
  url.searchParams.append('page', page.toString());
  url.searchParams.append('owner.metamaskAddress', metamask);
  currentFilters.forEach((filter) => {
    url.searchParams.append(filter.queryString, filter.value as string);
  });
  currentOrderBys.forEach((orderBy) => {
    url.searchParams.append(orderBy.queryString, orderBy.value as string);
  });
  const response = await fetch(url.toString(), {
    headers: {
      'x-original-token': document.cookie,
    },
  });
  if (response.status !== 200) {
    log(`/sync_stuffs: ${response.status} ${response.statusText}`);
    return Promise.reject('Unable to get sync stuffs, sry 😿');
  }
  const syncStuff: HydraPaginatedResponse<SyncStuff> | undefined = await response.json();
  return Promise.resolve(syncStuff);
};
