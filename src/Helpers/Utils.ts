import {
  BuildResult,
  BuildType,
  createFighterBuild,
  createStats,
  FighterBuild,
  FighterStats,
  fixStats,
  getFighterBuildNameCap,
  getMetaScoreOutputLarge,
  Item,
  MetaOutput,
  Race,
  Slot,
  Stats,
} from '@crypto-raider-tools/computation';
import { log } from '../config';
import { CacheBuildCap } from '../interfaces/cacheBuildCap';
import { DetailedSyncRaider } from '../interfaces/detailedsyncraider';
import { ItemPureness, BasePureness } from '../interfaces/pureness';
import { QuestName, RaiderQuestStatus } from '../interfaces/quest';
import { StuffStats } from '../interfaces/stuffstats';
import { TammyAvatar } from '../interfaces/tammyAvatar';
import { collectReward, comingBack, grimWeedIcon, inQuest, newtIcon, notQuesting, sporeBarkIcon } from './Constants';

export function htmlToElement(html: string): ChildNode {
  var template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild!;
}

export function wordToCamelCase(word: string): string {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

export function isPremium(): boolean {
  return JSON.parse(localStorage.getItem('CRC_customer') ?? '{"isPremium":false}').isPremium;
}
export function getPremiumExpirationDate(): string {
  return JSON.parse(localStorage.getItem('CRC_customer') ?? '{"premiumExpirationDate":""}').premiumExpirationDate;
}

export function raceToRaceName(race: string | Race): string {
  let raceName = race.replace('_', ' ');
  const arr = raceName.split(' ');
  for (var i = 0; i < arr.length; i++) {
    arr[i] = wordToCamelCase(arr[i]);
  }
  raceName = arr.join(' ');
  return raceName;
}

export const extractDetailedRaiderStats = (raider: DetailedSyncRaider) => {
  const stats = createStats();
  stats.strength = raider.strength ?? stats.strength;
  stats.intelligence = raider.intelligence ?? stats.intelligence;
  stats.agility = raider.agility ?? stats.agility;
  stats.wisdom = raider.wisdom ?? stats.wisdom;
  stats.charm = raider.charm ?? stats.charm;
  stats.luck = raider.luck ?? stats.luck;
  return stats;
};

export const extractDetailedRaiderAvatar = (raider: DetailedSyncRaider) => {
  return {
    body: raider.avatarBody ?? TammyAvatar.body,
    face: raider.avatarFace ?? TammyAvatar.face,
    hair: raider.avatarHair ?? TammyAvatar.hair,
  };
};

export function getItemPureness(currentItem: Item, stuffstats: StuffStats | undefined): ItemPureness {
  let pureness: ItemPureness = { ...BasePureness };
  if (!stuffstats) return pureness;
  const stats = fixStats(currentItem?.stats);
  const totalMainStats = stats.strength + stats.intelligence + stats.agility;
  const totalSubStats = stats.wisdom + stats.charm + stats.luck;
  const totalStats = totalMainStats + totalSubStats;
  pureness.sum = stuffstats?.pointQtyMax === 0 ? -1 : (totalStats / (stuffstats?.pointQtyMax ?? 0)) * 100;
  for (let statName of Object.keys(stats)) {
    pureness[statName as keyof Stats] = (stats[statName as keyof Stats] / totalStats) * 100;
  }
  return pureness;
}

export const buildBySlot: Record<Slot, (fighterStats: FighterStats, item: Item) => void> = {
  main_hand: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.mainHand = copy;
  },
  dress: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.dress = copy;
  },
  helmet: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.helmet = copy;
  },
  knickknack: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.knickknack = copy;
  },
  neck: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.neck = copy;
  },
  background: (__fighterStats, __item) => {},
  finger: (fighterStats, item) => {
    const copy = { ...item };
    copy.stats = fixStats(item?.stats);
    fighterStats.finger = copy;
  },
};

export const fighterBuildValueToString = (buildKeyName: string, build: FighterBuild) => {
  switch (buildKeyName) {
    case 'Health':
      return build.maxHP.toFixed(0) + ' Hp';
    case 'Min Damages':
      return build.minDamage.toFixed(0) + ' Min Damages';
    case 'Max Damages':
      return build.maxDamage.toFixed(0) + ' Max Damages';
    case 'Hit Chance':
      return build.hitChance.toFixed(1) + '% Hit Chance';
    case 'First Hit':
      return build.hitFirst.toFixed(1) + '% First Hit';
    case 'Crit Chance':
      return build.meleeCrit.toFixed(1) + '% Crit Chance';
    case 'Crit Multiplier':
      return build.critDamageMultiplier.toFixed(2) + ' Crit Multiplier';
    case 'Crit Resist':
      return build.critResist.toFixed(2) + ' Crit Resist';
    case 'Evade Chance':
      return build.evadeChance.toFixed(1) + '% Evade Chance';
    case 'Melee Resist':
      return build.meleeResist.toFixed(1) + '% Melee Resist';
  }
  return 'Error';
};

export function getDataSetForFighterBuild(
  build: FighterBuild,
  cacheBuildCap: Record<keyof FighterBuild, number>
): number[] {
  const res: number[] = [];

  res.push(Math.round((build.maxHP / 550) * 100));
  res.push(Math.round((build.minDamage / 80) * 100));
  res.push(Math.round((build.maxDamage / 120) * 100));
  res.push(Math.round((build.hitChance / cacheBuildCap['hitChance']) * 100));
  res.push(Math.round((build.hitFirst / cacheBuildCap['hitFirst']) * 100));
  res.push(Math.round((build.meleeCrit / cacheBuildCap['meleeCrit']) * 100));
  res.push(Math.round((build.critDamageMultiplier / cacheBuildCap['critDamageMultiplier']) * 100));
  res.push(Math.round((build.critResist / cacheBuildCap['critResist']) * 100));
  res.push(Math.round((build.evadeChance / cacheBuildCap['evadeChance']) * 100));
  res.push(Math.round((build.meleeResist / cacheBuildCap['meleeResist']) * 100));
  return res;
}

export function getFighterBuildMetaOutputsLarge(
  build: FighterBuild,
  cacheBuildCap: CacheBuildCap,
  level: number
): MetaOutput[] {
  let metas: MetaOutput[] = [];
  const baseBuild: FighterBuild = createFighterBuild();
  const ensureBuild = {
    ...baseBuild,
    ...build,
  };
  metas.push(getMetaScoreOutputLarge(ensureBuild.maxHP - baseBuild.maxHP, 400));
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.minDamage + ensureBuild.maxDamage - (baseBuild.minDamage + baseBuild.maxDamage),
      180
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.hitChance - baseBuild.hitChance,
      cacheBuildCap[level]['hitChance'] - baseBuild.hitChance
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.hitFirst - baseBuild.hitFirst,
      cacheBuildCap[level]['hitFirst'] - baseBuild.hitFirst
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.critDamageMultiplier - baseBuild.critDamageMultiplier,
      cacheBuildCap[level]['critDamageMultiplier'] - baseBuild.critDamageMultiplier
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.meleeCrit - baseBuild.meleeCrit,
      cacheBuildCap[level]['meleeCrit'] - cacheBuildCap[1]['meleeCrit']
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.critResist - baseBuild.critResist,
      cacheBuildCap[level]['critResist'] - baseBuild.critResist
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.evadeChance - baseBuild.evadeChance,
      cacheBuildCap[level]['evadeChance'] - baseBuild.evadeChance
    )
  );
  metas.push(
    getMetaScoreOutputLarge(
      ensureBuild.meleeResist - baseBuild.meleeResist,
      cacheBuildCap[level]['meleeResist'] - baseBuild.meleeResist
    )
  );
  return metas;
}

export const getItemRecommentationScore = (
  current: BuildResult,
  equipped: BuildResult | undefined,
  currentClass: BuildType
): number => {
  if (!equipped) return 2;
  let dif = current.classes[currentClass] - equipped.classes[currentClass];
  if (dif === 0) return dif;
  let isBetterThanEquipped: boolean = dif > 0;
  if (!isBetterThanEquipped) dif *= -1;
  let result = (dif * 2) / equipped.classes[currentClass];
  if (!isBetterThanEquipped) result *= -1;
  return result;
};

function fallbackCopyTextToClipboard(text: string) {
  var textArea = document.createElement('textarea');
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = '0';
  textArea.style.left = '0';
  textArea.style.position = 'fixed';

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}

export function copyTextToClipboard(text: string) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(
    function () {
      log('Async: Copying to clipboard was successful!');
    },
    function (err) {
      console.error('Async: Could not copy text: ', err);
    }
  );
}

export const getTotalStatsPointCount = (stats: Stats) => {
  let totalStats = 0;
  Object.values(stats).forEach((stats) => {
    totalStats += stats;
  });
  return totalStats;
};

export const getTotalFighterStatsPointCount = (fighterStats: FighterStats) => {
  let totalStats = 0;
  Object.keys(fighterStats.stats).forEach((stats) => {
    const statName = stats as keyof Stats;
    totalStats += fighterStats.stats[statName];
    if (typeof fighterStats.mainHand !== 'undefined') totalStats += fighterStats.mainHand.stats[statName];
    if (typeof fighterStats.dress !== 'undefined') totalStats += fighterStats.dress.stats[statName];
    if (typeof fighterStats.finger !== 'undefined') totalStats += fighterStats.finger.stats[statName];
    if (typeof fighterStats.neck !== 'undefined') totalStats += fighterStats.neck.stats[statName];
    if (typeof fighterStats.helmet !== 'undefined') totalStats += fighterStats.helmet.stats[statName];
  });
  return totalStats;
};

export const createBuildCapCache = (level: number) => {
  return {
    maxHP: getFighterBuildNameCap('maxHP', level) as number,
    minDamage: getFighterBuildNameCap('minDamage', level) as number,
    maxDamage: getFighterBuildNameCap('maxDamage', level) as number,
    hitChance: getFighterBuildNameCap('hitChance', level) as number,
    hitFirst: getFighterBuildNameCap('hitFirst', level) as number,
    meleeCrit: getFighterBuildNameCap('meleeCrit', level) as number,
    critDamageMultiplier: getFighterBuildNameCap('critDamageMultiplier', level) as number,
    critResist: getFighterBuildNameCap('critResist', level) as number,
    evadeChance: getFighterBuildNameCap('evadeChance', level) as number,
    meleeResist: getFighterBuildNameCap('meleeResist', level) as number,
  };
};

export const getMaxAbilityNumberForRaiderLevel = (level: number) => {
  if (level >= 2 && level < 5) return 1;
  else if (level >= 5 && level < 10) return 2;
  else if (level >= 10) return 3;
  else return 0;
};

export const questToIcon = (quest: QuestName): string => {
  switch (quest) {
    case 'Grimweed':
      return grimWeedIcon;
    case 'Newt':
      return newtIcon;
    case 'Sporebark':
      return sporeBarkIcon;
  }
  return '';
};

export const questStatusToName = (quest: number): string => {
  switch (quest) {
    case notQuesting:
      return 'Not Questing';
    case inQuest:
      return 'In Quest';
    case comingBack:
      return 'Coming Back';
    case collectReward:
      return 'Collect Reward';
  }
  return '';
};

export const extractRaiderQuestStatus = (raider: DetailedSyncRaider | undefined): RaiderQuestStatus => {
  let status: RaiderQuestStatus = {
    questStatus: 0,
    questName: 'Grimweed',
  };
  if (raider) {
    status = {
      ...raider,
      returnTime: raider.questReturnTime,
      totalReturnTime: raider.questTotalReturnTime,
      nextReward: raider.questNextReward,
      earnedReward: raider.questEarnedReward ?? 0,
      expRewardAmount: raider.questExpRewardAmount ?? 0,
      herbalismExp: raider.questHerbalismExp ?? 0,
    };
  }
  return status;
};
