import { BuildType, FighterBuild, Rarity } from '@crypto-raider-tools/computation';

export const RarityAsNumber: Record<Rarity, number> = {
  common: 0,
  uncommon: 1,
  rare: 2,
  epic: 3,
  legendary: 4,
  mythic: 5,
  heirloom: 6,
  limited: 7,
};

export const FighterBuildNameToText: Record<keyof FighterBuild, string> = {
  maxHP: 'Health',
  minDamage: 'Damages',
  maxDamage: 'Damages',
  hitChance: 'Hit Chance',
  hitFirst: 'Hit First',
  meleeCrit: 'Crit Chance',
  critDamageMultiplier: 'Crit Multiplier',
  critResist: 'Crit Resistance',
  evadeChance: 'Evade Chance',
  meleeResist: 'Melee Resistance',
};

export const BuiltTypeToString: Record<BuildType, string> = {
  none: 'None',
  warrior: 'Warrior',
  paladin: 'Paladin',
  barbarian: 'Barbarian',
  wizard: 'Wizard',
  rogue: 'Rogue',
  druid: 'Druid',
};
