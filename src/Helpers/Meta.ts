import {
  MetaOutput,
  Item,
  getMetaScoreOutputLarge,
  fixStats,
  computeStatsBuildMetaScore,
} from '@crypto-raider-tools/computation';

export function getItemMeta(item: Item, currentMaxMeta: number): MetaOutput {
  const stats = fixStats(item?.stats);
  if (item.name.includes('Bash')) return 'Meta';
  else if (item.name.includes('Ice Barrier')) return 'OP';
  else if (item.name.includes('Blind')) return 'Trash';
  else if (item.name.includes('Stone Skin')) return 'Average';
  else if (item.name.includes('Escape Artist')) return 'Good';
  return getMetaScoreOutputLarge(computeStatsBuildMetaScore(stats).metaScore, currentMaxMeta);
}

export function getItemMetaAsNumber(item: Item, currentMaxMeta: number): number {
  const stats = fixStats(item?.stats);
  if (item.name.includes('Bash')) return 7;
  else if (item.name.includes('Ice Barrier')) return 8;
  else if (item.name.includes('Blind')) return 1;
  else if (item.name.includes('Stone Skin')) return 5;
  else if (item.name.includes('Escape Artist')) return 3;
  else if (item.name.includes('Enflame')) return 7;
  else if (item.name.includes('Thorns')) return 6;
  const result = getMetaScoreOutputLarge(computeStatsBuildMetaScore(stats).metaScore, currentMaxMeta);
  switch (result) {
    case 'Ruygye':
      return 0;
    case 'Trash':
      return 1;
    case 'Inferior':
      return 2;
    case 'Moderate':
      return 3;
    case 'Average':
      return 4;
    case 'Decent':
      return 5;
    case 'Good':
      return 6;
    case 'Meta':
      return 7;
    case 'OP':
      return 8;
  }
}
