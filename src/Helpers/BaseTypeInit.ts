import { createStats } from '@crypto-raider-tools/computation';

export function createCryptoRaiderData() {
  return {
    dungeonStats: [],
    lastUpdate: new Date(Date.now()).toJSON(),
  };
}

export function createDungeonStats() {
  return {
    dungeonName: '',
    difficulty: '',
    runs: [],
  };
}

export function createDungeonRun() {
  return {
    date: new Date(Date.now()).toJSON(),
    output: 'NONE',
    raiderId: '',
    raiderStats: createStats(),
    equippedItems: [],
  };
}
