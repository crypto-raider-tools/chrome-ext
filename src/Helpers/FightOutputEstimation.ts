import { FighterBuild } from '@crypto-raider-tools/computation';

const getPotentialDamages = (player: FighterBuild, opponent: FighterBuild): number => {
  let playerPotentialDamages = player.minDamage + (player.maxDamage - player.minDamage) / 2;
  let playerCrit =
    playerPotentialDamages * (player.meleeCrit / 100) * (player.critDamageMultiplier - opponent.critResist / 100);
  playerPotentialDamages += playerCrit - playerPotentialDamages * (player.meleeCrit / 100);
  const hitChanceMultiplier = (player.hitChance - opponent.evadeChance) / 100;
  playerPotentialDamages *= hitChanceMultiplier < 1 ? hitChanceMultiplier : 1;
  playerPotentialDamages *= 1 - opponent.meleeResist / 100;
  return playerPotentialDamages;
};

export const computeFightOutputEstimation = (player: FighterBuild, opponent: FighterBuild): Record<string, number> => {
  const playerPotentialDamages = getPotentialDamages(player, opponent);
  const opponentPotentialDamages = getPotentialDamages(opponent, player);
  const numberAttackToKillOpponent = Math.ceil(opponent.maxHP / playerPotentialDamages);
  const numberAttackToKillPlayer = Math.ceil(player.maxHP / opponentPotentialDamages);
  let winRate = 50;
  let dif = numberAttackToKillPlayer - numberAttackToKillOpponent;
  let isWinning = true;
  if (dif < 0) {
    dif *= -1;
    isWinning = false;
  }
  const ratio = (100 * dif) / numberAttackToKillOpponent;
  if (dif > numberAttackToKillOpponent) {
    winRate = isWinning ? 100 : 0;
  } else {
    winRate += ((ratio * 50) / 100) * (isWinning ? 1 : -1);
  }

  return { numberAttackToKillOpponent, numberAttackToKillPlayer, winRate };
};
