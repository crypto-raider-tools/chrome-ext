import { Item, RaiderApiData, Stats } from '@crypto-raider-tools/computation';

export type CryptoRaiderContextData = {
  raidersData: CryptoRaiderData[];
  currentMaxMeta: number;
};

export type CryptoRaiderData = {
  raider?: RaiderApiData;
  dungeonStats: DungeonStat[];
  lastUpdate: string;
};

export type DungeonStat = {
  dungeonName: string;
  difficulty: string;
  runs: DungeonRun[];
};

export type DungeonRun = {
  date: string;
  output: 'WIN' | 'LOSE' | 'NONE';
  raiderId: string;
  raiderStats: Stats;
  equippedItems: Item[];
};

export type Message = {
  type: 'CONNECT' | 'RAIDER_START_DUNGEON';
  raiders: CryptoRaiderData[];
  raiderStartDungeon: RaiderInDungeon;
};

export type RaiderInDungeon = {
  raiderData: CryptoRaiderData;
  dungeonName: string;
};

export type MessageResponse = {
  type: 'DONE' | 'FAILED';
};
