import './App.css';
import CryptoRaiderCompanion from './Components/CryptoRaiderCompanion';

function App() {
  return (
    <div className="App">
      <CryptoRaiderCompanion />
    </div>
  );
}

export default App;
