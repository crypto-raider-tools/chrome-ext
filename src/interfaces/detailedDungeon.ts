import { MobBuildName } from '@crypto-raider-tools/computation';
import { Dungeon } from './dungeon';

export interface DetailedDungeon extends Dungeon {
  cliName?: 'none' | MobBuildName;
}
