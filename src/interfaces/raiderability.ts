export interface RaiderAbility {
  '@id'?: string;
  name?: string;
  description?: string;
  type?: string;
  iconUrl?: string;
  internalName?: string;
  energyCost?: number;
}
