export interface StuffStats {
  '@id'?: string;
  dungeons?: string[];
  readonly name?: string;
  readonly slot?: string;
  readonly count?: number;
  readonly strengthMax?: number;
  readonly strengthMin?: number;
  readonly intelligenceMax?: number;
  readonly intelligenceMin?: number;
  readonly agilityMax?: number;
  readonly agilityMin?: number;
  readonly wisdomMax?: number;
  readonly wisdomMin?: number;
  readonly charmMax?: number;
  readonly charmMin?: number;
  readonly luckMax?: number;
  readonly luckMin?: number;
  readonly internalName?: string;
  readonly gearType?: string;
  readonly rarity?: string;
  readonly levelMax?: number;
  readonly pointQtyMax?: number;
  readonly pointQtyMin?: number;
}
