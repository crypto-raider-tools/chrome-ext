import { Dungeon } from './dungeon';
import { Mobs } from './mobs';

export interface DetailedMobs extends Mobs {
  dungeons?: Array<Partial<Dungeon>>;
}
