import { FighterBuild } from '@crypto-raider-tools/computation';

export type CacheBuildCap = Record<number, Record<keyof FighterBuild, number>>;
