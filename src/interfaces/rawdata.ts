export interface RawData {
  '@id'?: string;
  customer: string;
  data?: any;
  readonly updated_at?: Date;
}
