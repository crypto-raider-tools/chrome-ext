export interface User {
  '@id'?: string;
  username?: string;
  roles?: any;
  password?: string;
  readonly id?: number;
}
