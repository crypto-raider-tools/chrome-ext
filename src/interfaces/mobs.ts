export interface Mobs {
  '@id'?: string;
  critDamageMultiplier?: string;
  critResist?: string;
  effectiveAgi?: number;
  effectiveChr?: number;
  effectiveInt?: number;
  effectiveLuk?: number;
  effectiveStr?: number;
  effectiveWis?: number;
  evadeChance?: string;
  extraHP?: string;
  hitChance?: string;
  hitFirst?: string;
  maxDamage?: string;
  maxHP?: string;
  meleeCrit?: string;
  meleeResist?: string;
  minDamage?: string;
  level?: number;
  name?: string;
  type?: string;
  dungeons?: any;
  idleUrl?: string;
  idleDimensionX?: number;
  idleDimensionY?: number;
  idleAnimationSteps?: number;
  readonly id?: number;
}
