export interface Dungeon {
  '@id'?: string;
  name?: string;
  cliName?: string;
  act?: string;
  keyCost?: number;
  levelMin?: number;
  levelMax?: number;
  internalName?: string;
  winXp?: number;
  loseXp?: number;
  droppableItems?: string[];
  iconPosX?: number;
  iconPosY?: number;
  iconUrl?: string;
  difficulty?: string;
  mobs?: string[];
  recommendedTotalStatPoints?: number;
}
