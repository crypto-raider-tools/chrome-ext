import { Avatar } from '@crypto-raider-tools/computation';
import { Customer } from './customer';

export type CustomFilterName = 'races' | 'levels' | 'generations' | 'avatarBodies';

export type CustomFilters = {
  races: Array<string>;
  levels: Array<number>;
  generations: Array<number>;
  avatarBodies: Array<Avatar>;
  raiders: Record<string, string>;
};

export interface DetailedCustomer extends Customer {
  readonly filters?: CustomFilters;
}
