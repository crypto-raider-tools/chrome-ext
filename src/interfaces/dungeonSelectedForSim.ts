export type DungeonSelectedForSim = {
  name: string;
  backgroundPosition: string;
};
