import { RaiderApiData } from '@crypto-raider-tools/computation';
import { QuestName } from './quest';
import { SyncRaider } from './syncraider';

export interface DetailedSyncRaider extends SyncRaider {
  readonly raw_data?: RaiderApiData;
  readonly id?: number;
  readonly questStatus: number; // 1 questing | 2 coming back or already back
  readonly questName?: QuestName; // time when 1 switched to 2
}
