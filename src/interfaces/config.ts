export interface Config {
  '@id'?: string;
  key?: string;
  value?: string;
  readonly id?: number;
}
