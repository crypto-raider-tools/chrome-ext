export interface DungeonToMob {
  '@id'?: string;
  dungeonName?: string;
  mobName?: string;
  readonly id?: number;
}
