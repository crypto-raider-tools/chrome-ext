import { KnickknackName, MobBuildName, StuffStats } from '@crypto-raider-tools/computation';
import { Simulation } from './simulation';

export type SimulationFighter = {
  id?: number | MobBuildName;
  level?: number;
  stats?: StuffStats;
  gears?: Array<StuffStats>;
  knickknack?: {
    name: KnickknackName;
  };
};

export type SimulationState = '0 - Dropped' | '1 - Pending' | '2 - Running' | '3 - Done';

export type SimulationResult = {
  fighterAWinCount: number;
  fighterBWinCount: number;
  fighterAAverage: {
    damagePerSim: number;
    remainingLife: number;
  };
  fighterBAverage: {
    damagePerSim: number;
    remainingLife: number;
  };
};

export interface DetailedSimulation extends Simulation {
  fighterA?: SimulationFighter;
  fighterB?: SimulationFighter;
  readonly state?: SimulationState;
  readonly result?: SimulationResult;
}
