import { RaiderApiData } from '@crypto-raider-tools/computation';
import { Mobs } from './mobs';

export type CompareObject = {
  type: 'Custom' | 'Raider' | 'Mob';
  raiderData?: RaiderApiData;
  mob?: Mobs;
};
