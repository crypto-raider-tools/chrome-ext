import { Stats } from '@crypto-raider-tools/computation';

export type ItemPureness = {
  sum: number;
} & Stats;

export const BasePureness: ItemPureness = {
  sum: -1,
  strength: -1,
  intelligence: -1,
  agility: -1,
  wisdom: -1,
  charm: -1,
  luck: -1,
};
