import { QuestAction } from './quest';

export interface Web3InjectInit {
  status: 'Web3Ok' | 'MissingWeb3';
}
export interface Web3InjectRequest {
  type: 'PremiumDonation';
  amount: number;
}

export type Hash = string;

export interface TxState {
  txHash: Hash;
  type: 'Create' | 'Sent' | 'Mined' | 'Delete';
}

export interface Web3InjectResponse extends TxState {
  action: QuestAction;
  raiderId: number;
}
