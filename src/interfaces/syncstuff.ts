export interface SyncStuff {
  '@id'?: string;
  readonly isEquipped?: boolean;
  readonly category?: string;
  readonly gearType?: string;
  readonly internalName?: string;
  readonly level?: number;
  readonly name?: string;
  readonly rarity?: string;
  readonly slot?: string;
  readonly strength?: number;
  readonly intelligence?: number;
  readonly agility?: number;
  readonly wisdom?: number;
  readonly charm?: number;
  readonly luck?: number;
  readonly raider?: string;
  readonly owner?: string;
  readonly stuffStat?: any;
  readonly index?: number;
  readonly sellingPrice?: number;
  readonly totalStatPoints?: number;
}
