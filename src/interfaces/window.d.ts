import { Web3InjectRequest } from './web3Inject';

declare global {
  interface Window {
    crCompanion: {
      requestWeb3: (request: Web3InjectRequest) => void;
    };
  }
}
