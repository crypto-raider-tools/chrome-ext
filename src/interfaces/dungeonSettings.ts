export interface DisplayInfo {
  monitorId: number;
  workArea: WindowSize;
  bounds: {
    top: number;
    left: number;
  };
}

export interface DungeonRunData {
  secret: string;
  url: string;
  dungeonName: string;
  raiderId: number;
}

export interface DungeonGridConfig {
  displayInfo: DisplayInfo;
  cellSize: WindowSize;
  config: {
    row: number;
    column: number;
    nbRaidPerRun: number;
  };
  isMuteTab: boolean;
}

export interface WindowSize {
  width: number;
  height: number;
}
export interface Position {
  x: number;
  y: number;
}
