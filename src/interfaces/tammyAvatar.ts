export type AvatarTrigger = {
  body: boolean;
  face: boolean;
  hair: boolean;
};

export const TammyAvatar = {
  body: 'HUMAN_FEMALE_WHITE_FEMALE_BASIC_BODY_1',
  face: 'HUMAN_FEMALE_BASIC_FACE_1',
  hair: 'SHORT_FEMALE_BLUE_HAIR',
};
