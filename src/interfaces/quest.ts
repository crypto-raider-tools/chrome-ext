import { DetailedSyncRaider } from './detailedsyncraider';

export type QuestName = 'Grimweed' | 'Newt' | 'Sporebark';
export type QuestAction = 'startQuest' | 'endQuest' | 'getReward' | 'teleport' | 'equipMount' | 'unEquipMount';
export interface RaiderQuestStatus {
  questStatus: number; //Not Questing 1, In Quest 2, Coming Back 3, Collect Reward 4
  questName?: QuestName;
  questStartedTime?: number;
  questEndedTime?: number;
  questDuration?: number; //timeQuesting
  returnTime?: number; //calcReturn
  totalReturnTime?: number; //timeTillHome
  nextReward?: number;
  earnedReward?: number;
  expRewardAmount?: number;
  herbalismExp?: number;
  mountId?: string;
}

export type QuestFilters = Partial<Record<keyof DetailedSyncRaider, string | number>>;
export type QuestOrderBys = Partial<Record<keyof DetailedSyncRaider, 'asc' | 'desc'>>;
export type QuestFields = Array<keyof DetailedSyncRaider>;
