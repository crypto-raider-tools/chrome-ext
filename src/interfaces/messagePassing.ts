import { DungeonClearMessage, DungeonStartMessage } from './dungeonMessage';
import { DisplayInfo } from './dungeonSettings';
import { Web3InjectInit, Web3InjectRequest, Web3InjectResponse } from './web3Inject';

export type MessagePassingTypes =
  | 'Ping'
  | 'Pong'
  | 'StartDungeon'
  | 'StartDuel'
  | 'GetDisplayInfos'
  | 'GettingDisplayInfos'
  | 'ReturnDisplayInfos'
  | 'SendDungeonWSData'
  | 'ParsedDungeonWSData'
  | 'InitWeb3'
  | 'SendTx'
  | 'TxState'
  | 'Error'
  | 'Ok';
export interface MessagePassingData {
  dungeonClearMessage?: DungeonClearMessage;
  displayInfos?: Array<DisplayInfo>;
  dungeonStartMessage?: DungeonStartMessage;
  duelStartMessage?: string;
  web3InjectInit?: Web3InjectInit;
  web3InjectRequest?: Web3InjectRequest;
  web3InjectResponse?: Web3InjectResponse;
}

export interface MessagePassing {
  type: MessagePassingTypes;
  url?: string;
  data: MessagePassingData;
}
