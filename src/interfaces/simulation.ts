export interface Simulation {
  '@id'?: string;
  fighterA?: any;
  fighterB?: any;
  simCount: number;
  webhookUrl?: string;
  webhookAuthorizationHeader?: string;
  customer: string;
  readonly state?: string;
  readonly result?: any;
  readonly created_at?: Date;
  readonly eventName?: string;
}
