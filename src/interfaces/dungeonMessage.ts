import { DungeonGridConfig, Position } from './dungeonSettings';

export type DungeonOutput = 'Victory' | 'Flee';

export interface DungeonClearMessage {
  secret: string;
  output: DungeonOutput;
}

export interface DungeonStartMessage {
  url: string;
  secret: string;
  position: Position;
  config: DungeonGridConfig;
}
