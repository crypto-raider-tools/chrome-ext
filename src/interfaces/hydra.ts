export type HydraPaginatedResponse<X> = {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Array<X>;
  'hydra:search': {
    '@type': 'hydra:IriTemplate';
    'hydra:mapping': Array<{
      '@type': string;
      property: keyof X;
      required: boolean;
      variable: string;
    }>;
    'hydra:template': string;
    'hydra:variableRepresentation': string;
  };
  'hydra:totalItems': 75;
  'hydra:view': {
    '@id': string;
    '@type': string;
    'hydra:first': string;
    'hydra:last': string;
    'hydra:next': string;
  };
};

type HydraCriteriaTemplateKeys<X> = (string & keyof X) | 'id';
type HydraCriteriaOptions = 'lt' | 'lte' | 'gt' | 'gte' | 'between' | '';
export type HydraCriteriaVariables<X> =
  | keyof X
  | `order[${HydraCriteriaTemplateKeys<X>}]`
  | `${HydraCriteriaTemplateKeys<X>}[${HydraCriteriaOptions}]`;

export type HydraCriteria<X> = {
  variable: HydraCriteriaVariables<X>;
  value: string | number;
};
