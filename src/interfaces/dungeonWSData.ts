import { Item } from '@crypto-raider-tools/computation';

export interface ActiveStats {
  maxHP: number;
  currHP: number;
  maxShieldHP: number;
  shieldHP: number;
  energyPoints: number;
}

export interface Stats {
  strength: number;
  wisdom: number;
  luck: number;
  agility?: number;
  charm?: number;
}

export interface CombatAction {
  type: string;
  usesPerInstance: number;
}

export interface EquippedItem {
  internalName: string;
  name: string;
  rarity: string;
  category: string;
  slot: string;
  constraints?: any;
  stats: Stats;
  flavor: string;
  icon?: any;
  gearType: string;
  level: number;
  combatAction: CombatAction;
  affinity: string;
}

export interface ActionTemplate<X> {
  type: X;
  usesPerInstance: any;
}
export interface Action {
  type: string;
  usesPerInstance: any;
}

export interface Avatar {
  body: string;
  face: string;
  hair: string;
}
export interface InstanceParticipant {
  combatId: string;
  type: string;
  name: string;
  activeStats: ActiveStats;
  equippedItems: EquippedItem[];
  actions: Action[];
  effects: any[];
  isNPC: boolean;
  tokenId: number;
  level: number;
  avatar: Avatar;
}
export interface Opponent {
  combatId: string;
  type: string;
  name: string;
  activeStats: ActiveStats;
  equippedItems: EquippedItem[];
  actions: Action[];
  effects: any[];
  isNPC: boolean;
  tokenId: number;
  level: number;
  avatar: Avatar;
}

export interface Turn {
  participantId: string;
  timeToAction: number;
}

export interface Effect {
  name: string;
  type: string;
  traits: string[];
  shieldHP: number;
  maxShieldHP: number;
  interactionsRemaining: number;
  turnsRemaining?: number;
}

export interface Change {
  type: string;
  actorId: string;
  targetId: string;
  isHit: boolean;
  isCrit: boolean;
  energyPoints: number;
  additionalEffectAdded: boolean;
  actionType: string;
  currHP: number;
  shieldHP: number;
  effect: Effect;
  change: string;
}

export interface ChoicesType {
  id: string;
  name: string;
}

export interface Choice {
  type: ChoicesType;
  statMultiplier: number;
  staticChange: number;
}
export interface Loot {
  items: Item[];
  experience: number;
  aurum: number;
}
export interface EventData {
  loot: Loot;
  opponents: Opponent[];
  choices: Choice[];
  changes: Change[];
  musicURL: string;
}
export interface DataMessage {
  instanceType: string;
  instanceDifficulty: string;
  instanceCompleted: boolean;
  instanceParticipants: InstanceParticipant[];
  eventType: string;
  eventData: EventData;
}
