export interface Customer {
  '@id'?: string;
  metamaskAddress?: string;
  readonly isDonator?: boolean;
  readonly syncRaiders?: any;
  readonly lastDonationDate?: Date;
  readonly filters?: any;
  readonly reservedRaiders?: any;
  readonly premiumExpirationDate?: Date;
  readonly isPremium?: boolean;
}
