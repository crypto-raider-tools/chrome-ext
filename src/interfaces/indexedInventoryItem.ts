import { BuildResult, InventoryItem, Slot, Stats } from '@crypto-raider-tools/computation';

export type IndexedInventoryItem = {
  originalIndex: number;
  filteredIndex: number;
  computedBuild: BuildResult;
  newWinrate?: string;
  lastScreenPosition?: DOMRect;
} & InventoryItem;

export type InventoryFilterSlotType = 'all' | Slot;

export type InventoryOrderablePropertyName =
  | 'Metascore'
  | 'Rarity'
  | 'Pureness'
  | 'Date'
  | 'Recommendation'
  | 'Name'
  | 'Level'
  | 'Total points'
  | keyof Stats;

export type OrderableStack = InventoryOrderablePropertyName[];

export type InventoryFiltersShouldUpdateState = 'None' | 'Filters' | 'Order' | 'CompleteUpdate';
