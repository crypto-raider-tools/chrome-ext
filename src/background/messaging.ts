import { log } from '../config';
import { DisplayInfo, DungeonGridConfig, Position } from '../interfaces/dungeonSettings';
import { MessagePassing } from '../interfaces/messagePassing';

export const registeredRunningDungeon: Record<string, number> = {};
export var isReady: boolean = false;

(async () => {
  if (!isReady) {
    const localRegisteredRunningDungeon = await chrome.storage.local.get('registeredRunningDungeon');
    log('localRegisteredRunningDungeon', localRegisteredRunningDungeon);
    isReady = true;
  }
})();

const getDisplayInfos = (tabId: number): void => {
  chrome.system.display.getInfo((displays) => {
    const displayInfos: Array<DisplayInfo> = [];
    let idx = 0;
    for (const display of displays) {
      displayInfos.push({
        monitorId: idx,
        workArea: {
          width: display.workArea.width,
          height: display.workArea.height,
        },
        bounds: {
          top: display.bounds.top,
          left: display.bounds.left,
        },
      });
      idx++;
    }
    chrome.tabs.sendMessage(
      tabId,
      { type: 'ReturnDisplayInfos', data: { displayInfos: displayInfos } },
      function (__response) {}
    );
  });
};

const openDungeonWindow = async (
  url: string,
  lastWindowPosition: Position,
  config: DungeonGridConfig,
  muted: boolean
) => {
  let window = await chrome.windows.create({
    focused: true,
    url: url,
    height: config.cellSize.height,
    width: config.cellSize.width,
    top: lastWindowPosition.x * config.cellSize.height + config.displayInfo.bounds.top,
    left: lastWindowPosition.y * config.cellSize.width + config.displayInfo.bounds.left,
  });
  if (window.tabs?.[0].id) {
    await chrome.tabs.update(window.tabs[0].id, { muted });
  }
};

export const setupPing = () => {
  chrome.runtime.onMessage.addListener(async (request: MessagePassing, sender, sendResponse) => {
    if (request.type === 'Ping') {
      sendResponse({ ...request, type: 'Pong' });
    } else if (request.type === 'StartDungeon') {
      if (sender.tab && request.data.dungeonStartMessage) {
        sendResponse({ ...request, type: 'Ok' });
        let muteState = request.data.dungeonStartMessage.config.isMuteTab;
        if (request.data.dungeonStartMessage.secret === 'rickRolled') muteState = false;
        else {
          registeredRunningDungeon[request.data.dungeonStartMessage.secret] = sender.tab.id ?? 0;
          await chrome.storage.local.set({ registeredRunningDungeon });
        }
        await openDungeonWindow(
          request.data.dungeonStartMessage.url,
          request.data.dungeonStartMessage.position,
          request.data.dungeonStartMessage.config,
          muteState
        );
      } else sendResponse({ ...request, type: 'Error' });
    } else if (request.type === 'GetDisplayInfos') {
      sendResponse({ type: 'GettingDisplayInfos', data: {} });
      getDisplayInfos(sender.tab?.id ?? 0);
    } else if (request.type === 'StartDuel') {
      if (request.data.duelStartMessage) {
        await await chrome.windows.create({
          focused: true,
          url: request.data.duelStartMessage,
        });
        sendResponse({ ...request, type: 'Ok' });
      } else sendResponse({ ...request, type: 'Error' });
    } else if (request.type === 'SendTx') {
      console.log('send message to ' + sender!.tab!.id);
      chrome.tabs.sendMessage(sender!.tab!.id!, request);
      sendResponse({ tab: sender!.tab!.id });
    } else sendResponse({ ...request, type: 'Error' });
    return true;
  });
};
