import { log } from '../config';
import { setupPing } from './messaging';

setupPing();

chrome.runtime.onMessageExternal.addListener(async (message, sender, sendResponse) => {
  let tabs = await chrome.tabs.query({
    url: 'https://play.cryptoraiders.xyz/*',
  });
  for (let tab of tabs) {
    chrome.tabs.sendMessage(tab.id!, message);
  }
  sendResponse({ tabId: sender.tab?.id });
});

chrome.runtime.onInstalled.addListener(() => {
  log('Installed');

  chrome.notifications.create('INSTALL_RELOAD_WARNING', {
    type: 'basic',
    iconUrl: './img/logo-128.png',
    title: 'Thanks for installing Crypto Raiders Companion!',
    message: "Don't forget to refresh your crypto raider home page :)",
    priority: 2,
  });
});

const empty = {};

/***********************************/
/** PLEASE STAY ALIVE ... DAMN IT **/
/***********************************/

const stayAlive = () => {
  let port: chrome.runtime.Port;
  const hostName = chrome.runtime.id;
  const t = new Date().getTime();
  const nativeMessagingPort = chrome.runtime.connect(hostName);

  async function* keepAlive() {
    while (true) {
      if (chrome.runtime.lastError) {
        log(chrome.runtime.lastError);
        port.disconnect();
        nativeMessagingPort.disconnect();
        break;
      }
      yield new Promise((resolve) => {
        setTimeout(
          () => resolve(`ServiceWorker still alive at ${(new Date().getTime() - t) / 1000 / 60}`),
          1000 * 60 * 4.9
        );
      });
    }
  }

  async function eventDriven() {
    for await (const aliveKey of keepAlive()) {
      log(aliveKey);
    }
  }
  nativeMessagingPort.onDisconnect.addListener(async () => {
    log('Disconnected', nativeMessagingPort);
  });
  log(nativeMessagingPort);
  chrome.runtime.onInstalled.addListener(async () => {
    eventDriven();
  });
  chrome.runtime.onConnectExternal.addListener((incomingPort) => {
    port = incomingPort;
    nativeMessagingPort.onMessage.addListener((message) => {
      port.postMessage(message);
      log(message);
    });
    port.onMessage.addListener(async (message) => {
      if (message.disconnect) {
        nativeMessagingPort.disconnect();
        port.disconnect();
        return;
      }
      nativeMessagingPort.postMessage(message);
      log(message);
    });
  });
};

stayAlive();

export default empty;
