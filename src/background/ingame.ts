import { ActiveStats, DataMessage } from '../interfaces/dungeonWSData';

export const getInGameResponse = (wsData: DataMessage): any => {
  if (typeof wsData === 'undefined' || typeof wsData.eventType === 'undefined') return { type: 'No Interaction' };
  if (wsData.eventType === 'combat') {
    var resultData: Record<keyof ActiveStats, Array<number>> = {
      currHP: [],
      maxHP: [],
      maxShieldHP: [],
      shieldHP: [],
      energyPoints: [],
    };
    for (const buildStats of Object.keys(resultData)) {
      resultData[buildStats as keyof ActiveStats].push(
        wsData!.instanceParticipants[0].activeStats[buildStats as keyof ActiveStats]
      );
      resultData[buildStats as keyof ActiveStats].push(
        wsData!.eventData.opponents[0].activeStats[buildStats as keyof ActiveStats]
      );
    }
    return { type: 'Overlay', data: resultData };
  } else if (wsData?.eventType === 'loot_result') {
    return {
      type: 'Loot',
      data:
        wsData.eventData.loot && wsData.eventData.loot.items && wsData.eventData.loot.items.length > 0
          ? wsData.eventData.loot.items[0]
          : undefined,
    };
  }
  return { type: 'No Interaction' };
};
