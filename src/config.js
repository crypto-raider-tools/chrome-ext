export const apiEntrypoint = 'https://api.crypto-raiders-companion.xyz';
// export const apiEntrypoint = 'https://preprod-api.crypto-raiders-companion.xyz';
// export const apiEntrypoint = 'http://localhost';

export const debug = false;

export const log = (...args) => {
  debug && console.warn(['CRC extension:', ...args]);
};
