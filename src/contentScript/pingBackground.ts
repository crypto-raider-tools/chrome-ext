/***********************************/
/** PLEASE STAY ALIVE ... DAMN IT **/
/***********************************/

import { log } from '../config';

export const setupPingBackground = () => {
  log('pingBackground start');
  function sendMessage() {
    return { message: `MV3 ServiceWorker test at ${new Date()}` };
  }
  let timer: NodeJS.Timeout;
  let id = chrome.runtime.id;
  let port = chrome.runtime.connect(id);
  port.onMessage.addListener((e) => {
    log('pingBackground', e);
    timer = setTimeout(() => {
      port.postMessage(sendMessage());
    }, 1000 * 60 * 6);
  });
  port.onDisconnect.addListener((e) => {
    clearTimeout(timer);
    log('pingBackground', e);
  });
  port.postMessage(sendMessage());
};
