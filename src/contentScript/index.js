import { log } from '../config';
import { playMenuInjector } from '../PlayComponent/menuInjector';
import { setupPingBackground } from './pingBackground';

setupPingBackground();

if (window.location.host === 'play.cryptoraiders.xyz') {
  function checkInject() {
    if (window.location.pathname !== '/signin') {
      playMenuInjector();
    } else {
      setTimeout(() => {
        checkInject();
      }, 1000);
    }
  }
  checkInject();

  /**
   * Injection web3
   */
  log('Injection web3');

  let input = document.createElement('input');
  input.type = 'hidden';
  input.id = 'CRCExtensionId';
  // eslint-disable-next-line no-undef
  input.value = chrome.runtime.id;
  document.body.prepend(input);

  var scriptWeb3 = document.createElement('script');
  // eslint-disable-next-line no-undef
  scriptWeb3.src = chrome.runtime.getURL('lib/web3.js');
  document.head.appendChild(scriptWeb3);
}
