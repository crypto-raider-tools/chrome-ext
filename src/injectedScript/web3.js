(function () {
  const Web3 = require('web3');
  let web3 = undefined;

  const constants = require('../config');
  const donationAddress = '0xa3602881b28571500f0CAb8b8f697A973ae87F9b';
  //Crypto Raider WEB3
  const questAbi = [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'address',
          name: 'previousOwner',
          type: 'address',
        },
        {
          indexed: true,
          internalType: 'address',
          name: 'newOwner',
          type: 'address',
        },
      ],
      name: 'OwnershipTransferred',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'address',
          name: 'account',
          type: 'address',
        },
      ],
      name: 'Paused',
      type: 'event',
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: 'address',
          name: 'account',
          type: 'address',
        },
      ],
      name: 'Unpaused',
      type: 'event',
    },
    {
      inputs: [],
      name: 'baseRewardTime',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'calcEarned',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'calcRaiderRewardTime',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'calcReturnTime',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: '_boosted',
          type: 'address',
        },
      ],
      name: 'changeBoostedContract',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256',
        },
      ],
      name: 'changeExperienceAmount',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256',
        },
      ],
      name: 'changeFiftyPercentBoostAmount',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256',
        },
      ],
      name: 'changeMinExpAmount',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256',
        },
      ],
      name: 'changeReturnHomeTimeDivisor',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_time',
          type: 'uint256',
        },
      ],
      name: 'changeRewardTime',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256',
        },
      ],
      name: 'changeTwentyFivePercentBoostAmount',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'endQuest',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [],
      name: 'experienceAmount',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'fiftyPercentRewardsBoostExperience',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'getRewards',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [],
      name: 'minProfessionExp',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'nextReward',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'owner',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'pause',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [],
      name: 'paused',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'professionId',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'questEndedTime',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'questStartedTime',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'raiderStatus',
      outputs: [
        {
          internalType: 'enum Questable.Status',
          name: '',
          type: 'uint8',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'renounceOwnership',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [],
      name: 'returnHomeTimeDivisor',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'rewardEarned',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'runHome',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'startQuest',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'timeHome',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'timeQuesting',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '_raiderId',
          type: 'uint256',
        },
      ],
      name: 'timeTillHome',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: 'newOwner',
          type: 'address',
        },
      ],
      name: 'transferOwnership',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      name: 'transferredRaider',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'twentyFivePercentBoostExperience',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256',
        },
      ],
      stateMutability: 'view',
      type: 'function',
    },
    {
      inputs: [],
      name: 'unpause',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function',
    },
  ];
  const mountAbi = [
    { inputs: Array(5), stateMutability: 'nonpayable', type: 'constructor' },
    { anonymous: false, inputs: Array(3), name: 'Dismounted', type: 'event' },
    { anonymous: false, inputs: Array(3), name: 'Mounted', type: 'event' },
    { anonymous: false, inputs: Array(2), name: 'OwnershipTransferred', type: 'event' },
    { anonymous: false, inputs: Array(1), name: 'Paused', type: 'event' },
    { anonymous: false, inputs: Array(1), name: 'Unpaused', type: 'event' },
    { inputs: Array(2), name: 'addMount', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
    { inputs: Array(1), name: 'dismount', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
    {
      inputs: Array(1),
      name: 'emergencyRemoveMount',
      outputs: Array(0),
      stateMutability: 'nonpayable',
      type: 'function',
    },
    {
      inputs: Array(1),
      name: 'emergencyReturnMount',
      outputs: Array(0),
      stateMutability: 'nonpayable',
      type: 'function',
    },
    { inputs: Array(1), name: 'mountOwner', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(1), name: 'mountRaiderId', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(1), name: 'mountUsed', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(4), name: 'onERC721Received', outputs: Array(1), stateMutability: 'nonpayable', type: 'function' },
    { inputs: Array(0), name: 'owner', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(0), name: 'pause', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
    { inputs: Array(0), name: 'paused', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(1), name: 'raiderMountId', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(1), name: 'raiderMounted', outputs: Array(1), stateMutability: 'view', type: 'function' },
    { inputs: Array(0), name: 'renounceOwnership', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
    { inputs: Array(1), name: 'transferOwnership', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
    { inputs: Array(0), name: 'unpause', outputs: Array(0), stateMutability: 'nonpayable', type: 'function' },
  ];
  // const mountContractAddress = '0x90eFeEe015603F8916E186803992834C7758d7cD';
  const grimweedQuestContractAddress = '0xe193364370F0E2923b41a8d1850F442B45E5ccA7';
  const newtQuestContractAddress = '0x98a195e3eC940f590D726557c95786C8EBb0A2D2';
  const sporebarkQuestContractAddress = '0xc81f43Eb261c1708bFfA84D75DDd945341723f1F';
  // let mountContract;
  let grimweedQuestContract;
  let newtQuestContract;
  let sporebarkQuestContract;

  var extensionId = document.querySelector('#CRCExtensionId').value;
  const contentScriptPort = chrome.runtime.connect(extensionId);
  var account = '';

  initWeb3Companion();

  async function getAccount() {
    const accounts = await window.ethereum.request({
      method: 'eth_requestAccounts',
    });
    if (typeof accounts !== 'undefined') {
      return accounts[0];
    }
    return '';
  }

  async function createTxObj({ to, value = 0, data = undefined }) {
    //const nonce = await web3.eth.getTransactionCount(account, 'pending');
    const txObj = {
      from: account,
      to: to,
    };
    if (value > 0) txObj.value = web3.utils.toWei(web3.utils.toBN(value), 'ether');
    if (data !== undefined) txObj.data = data;
    const estimatedGas = await web3.eth.estimateGas(txObj);
    const gasPrice = web3.utils.toWei(web3.utils.toBN(80), 'gwei');
    txObj.gas = estimatedGas;
    txObj.gasPrice = gasPrice;
    return txObj;
  }

  async function initWeb3Companion() {
    if (typeof web3 === 'undefined') {
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
      } else throw Error('No Web3 in window');
    }
    // mountContract = new web3.eth.Contract(mountAbi, mountContractAddress);
    grimweedQuestContract = new web3.eth.Contract(questAbi, grimweedQuestContractAddress);
    newtQuestContract = new web3.eth.Contract(questAbi, newtQuestContractAddress);
    sporebarkQuestContract = new web3.eth.Contract(questAbi, sporebarkQuestContractAddress);
    window.ethereum.on('accountsChanged', (accounts) => {
      if (typeof accounts !== 'undefined') {
        account = accounts[0];
      }
    });
    chrome.runtime.sendMessage(
      extensionId,
      {
        type: 'InitWeb3',
        data: {},
      },
      onInitResponse
    );

    contentScriptPort.postMessage({
      type: 'InitWeb3',
      data: {},
    });

    contentScriptPort.onMessage.addListener(async (message, sender, sendResponse) => {
      console.log('onMessage');
    });
  }

  function onInitResponse(response) {
    console.log('init web3 companion done properly');
  }

  async function web3Requests(requestArray) {
    for (const request of requestArray) {
      let waitForTxSent = true;
      await handleTxRequest(request, () => {
        console.log('onTxSent');
        waitForTxSent = false;
      });
      while (waitForTxSent === true) await new Promise((r) => setTimeout(r, 500));
    }
  }

  async function handleTxRequest(data, onTxSent) {
    account = await getAccount();
    let txObj = {};
    let txHashCallBack = () => {};
    let txMintCallBack = () => {};
    switch (data.type) {
      case 'PremiumDonation':
        txObj = await createTxObj({ to: donationAddress, value: data.amount });
        txHashCallBack = (hash) => {
          console.log('Hash', hash);
          fetch(`${constants.apiEntrypoint}/get_premium/${hash}`, {
            method: 'GET',
            headers: {
              'x-original-token': document.cookie,
            },
          }).then(async (r) => {
            if (isTxMinted === true) {
              await new Promise((resolve) => setTimeout(resolve, 1000));
              window.location.reload();
            }
            isFetchDone = true;
          });
        };
        txMintCallBack = async (receipt) => {
          console.log('Receipt', receipt);
          if (isFetchDone === true) {
            await new Promise((resolve) => setTimeout(resolve, 1000));
            window.location.reload();
          }
          isTxMinted = true;
        };
        break;
      case 'QuestRequest':
        let contract;
        switch (data.quest) {
          case 'Grimweed':
            contract = grimweedQuestContract;
            break;
          case 'Newt':
            contract = newtQuestContract;
            break;
          case 'Sporebark':
            contract = sporebarkQuestContract;
            break;
          default:
            console.log('Error wrong quest target', data.quest);
            return;
        }
        let method;
        switch (data.action) {
          case 'startQuest':
            method = contract.methods.startQuest(data.raiderId).encodeABI();
            break;
          case 'endQuest':
            method = contract.methods.endQuest(data.raiderId).encodeABI();
            break;
          case 'getReward':
            method = contract.methods.getRewards(data.raiderId).encodeABI();
            break;
          case 'teleport':
            method = contract.methods.runHome(data.raiderId).encodeABI();
            break;
          case 'equipMount':
            method = contract.methods.addMount(data.raiderId, data.mountId).encodeABI();
            break;
          case 'unEquipMount':
            method = contract.methods.dismount(data.raiderId).encodeABI();
            break;
          default:
            throw Error('Wrong action');
        }
        txObj = await createTxObj({ to: contract._address, data: method });
        txHashCallBack = (hash) => {
          console.log('txHashCallBack');
          chrome.runtime.sendMessage(
            extensionId,
            {
              type: 'TxState',
              data: {
                web3InjectResponse: {
                  type: 'Sent',
                  raiderId: data.raiderId,
                  action: method,
                  txHash: hash,
                },
              },
            },
            onInitResponse
          );
        };
        txMintCallBack = (receipt) => {
          chrome.runtime.sendMessage(
            extensionId,
            {
              type: 'TxState',
              data: {
                web3InjectResponse: {
                  type: 'Mined',
                  raiderId: data.raiderId,
                  action: method,
                  txHash: receipt.transactionHash,
                },
              },
            },
            onInitResponse
          );
        };
        break;
      default:
        throw Error('Wrong request type');
    }
    console.log('Transaction', txObj);
    let isFetchDone = false;
    let isTxMinted = false;
    web3.eth
      .sendTransaction(txObj)
      .once('transactionHash', onTxSent)
      .once('transactionHash', txHashCallBack)
      .on('error', (error) => {
        console.log('Error', error);
        onTxSent();
        chrome.runtime.sendMessage(
          extensionId,
          {
            type: 'TxState',
            data: {
              web3InjectResponse: {
                type: 'Delete',
                raiderId: data.raiderId,
                action: '',
                txHash: '',
              },
            },
          },
          onInitResponse
        );
      })
      .then(txMintCallBack);
  }
  window.crCompanion = {
    requestWeb3: web3Requests,
  };
})();
