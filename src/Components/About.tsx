export function About() {
  const divStyle: React.CSSProperties = {
    color: '#ffffff',
    overflow: 'auto',
    padding: '1em',
  };
  const headStyle: React.CSSProperties = {
    color: '#ffffff',
  };
  const textStyle: React.CSSProperties = {
    color: 'rgb(127,145,176)',
    textTransform: 'none',
    fontWeight: 100,
    fontSize: '0.9em',
    lineHeight: '1.3em',
    textAlign: 'left',
  };
  const listStyle: React.CSSProperties = {
    textAlign: 'left',
  };

  return (
    <div style={divStyle} id="scrollBar">
      <h2 style={headStyle}>Why synchronization is needed?</h2>
      <p style={textStyle}>
        To enhance metascore, pureness and some other stuff, we need to get a lot of gears from players. Retrieving them
        this way is easier, more reliable.
      </p>
      <h2 style={headStyle}>Why Chrome webstore tells us you collect Crypto Raider credentials?</h2>
      <p style={textStyle}>
        On server side, we need to ensure each user's stuff is secured and cannot be cheated by creating or injecting
        wrong or bad data. To do so, we decided to use the Crypto Raider token to ensure you're a real player. Here's
        more information about the CRC API{' '}
        <a href="https://api.crypto-raiders-companion.xyz/" target="_blank" rel="noreferrer">
          https://api.crypto-raiders-companion.xyz/
        </a>
        :
      </p>
      <ul>
        <li style={listStyle}>It does not store your token (as it would be too dangerous).</li>
        <li style={listStyle}>It ensures your token validity by calling Crypto Raider servers with your token.</li>
        <li style={listStyle}>
          It retrieves your wallet address from the account.json URL (the one CR use in your account page) so we can
          detect if you're a donator or not{' '}
        </li>
      </ul>
      <h2 style={headStyle}>Can I use your API on my website?</h2>
      <p style={textStyle}>
        The API is documented here:{' '}
        <a href="https://api.crypto-raiders-companion.xyz/" target="_blank" rel="noreferrer">
          https://api.crypto-raiders-companion.xyz/
        </a>
        . Public endpoints can be used without any problem, but if you need to use any secure endpoint, you will need
        some credentials. For more information, DM @storm1er on discord.
      </p>
      <h2 style={headStyle}>Who are you?</h2>
      <p style={textStyle}>
        Just some random guys lost in a big crypto world. Feel free to come and talk on discord:
        <br />- <code>@OSi | FLAG</code>
        <br />- <code>@Bellay</code>
        <br />- <code>@storm1er</code>
        <br />
      </p>
      <h2 style={headStyle}>How do I become a donator?</h2>
      <p style={textStyle}>
        Clicking the button donation addy will copy our polygon address into your clipboard, you can then make a
        donation of any crypto you want on polygon network. You should become a donator after ~1h maximum. You will be
        able to tell if you're premium by looking at the upper premium state icons. For now there's no limit to
        donation, but please be fair, it's obvious that if everyone send 0.0000001 matic we will have to take action
      </p>
      <h2 style={headStyle}>What are premium features?</h2>
      <ul>
        <li style={listStyle}>Quest informations</li>
        <li style={listStyle}>Raids in grid features</li>
        <li style={listStyle}>Raider builder &amp; comparator</li>
        <li style={listStyle}>Sell multiple items in bulk</li>
      </ul>
      <p style={textStyle}>
        A donation will grant you a premium access for 16 days. After that period, you will have to donate again to keep
        premium features.
      </p>
    </div>
  );
}
