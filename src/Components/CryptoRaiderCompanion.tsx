import { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import { metaMaskAddy } from '../Helpers/Constants';
import { About } from './About';
import { copyTextToClipboard } from '../Helpers/Utils';

const CryptoRaiderCompanion = () => {
  const [addyText, setAddyText] = useState<string>('Donation addy ❤️');

  return (
    <div className="popup">
      <h1 style={{ color: '#fff', paddingTop: '10px' }}>About Crypto Raiders Companion</h1>
      <hr />
      <About />
      <div style={{ paddingBottom: '10px' }}>
        <Button
          className="addy-button"
          onClick={() => {
            copyTextToClipboard(metaMaskAddy);
            setAddyText('Copied!');
          }}
        >
          {addyText}
        </Button>
      </div>
    </div>
  );
};

export default CryptoRaiderCompanion;
