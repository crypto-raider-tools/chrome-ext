import React from 'react';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { HydraCriteria, HydraCriteriaVariables, HydraPaginatedResponse } from '../../interfaces/hydra';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { FilterRadiusButton } from '../Utils/FiltersRadiusButton';
import { isPremium } from '../../Helpers/Utils';

type RaiderOrderProps = {
  raiderList: HydraPaginatedResponse<DetailedSyncRaider> | undefined;
  isLoading: boolean;
  askRaiderPage: (page: number) => Promise<void>;
  updateRaiderCriterias: (criterias: Array<HydraCriteria<DetailedSyncRaider>>) => void;
  raiderCriterias: Array<HydraCriteria<DetailedSyncRaider>>;
  style?: React.CSSProperties;
};

export const RaiderOrder = (props: RaiderOrderProps) => {
  type CriteriaForRaiderOrder = {
    name: string;
    variable: HydraCriteriaVariables<DetailedSyncRaider>;
    needPremium: boolean;
  };

  const criteriaList: Array<CriteriaForRaiderOrder> = [
    { name: 'In Wallet', variable: 'order[currentlyInWallet]', needPremium: false },
    { name: 'Quest Status', variable: 'order[questStatus]', needPremium: true },
    { name: "Time 'till home", variable: 'order[questReturnTime]', needPremium: true },
    { name: 'Missions left', variable: 'order[raidsRemaining]', needPremium: false },
    { name: 'Generation', variable: 'order[generation]', needPremium: false },
    { name: 'Level', variable: 'order[xp]', needPremium: false },
    { name: 'ID', variable: 'order[id]', needPremium: false },
  ];

  const handleClick = (criteria: CriteriaForRaiderOrder): void => {
    let value = 'DESC';
    const oldValue = props.raiderCriterias.find(
      (raiderCriteria) => raiderCriteria.variable === criteria.variable
    )?.value;
    if (oldValue === 'DESC') {
      value = 'ASC';
    } else if (oldValue === 'ASC') {
      value = '';
    }

    const newCriterias = props.raiderCriterias.filter(
      (raiderCriteria) => raiderCriteria.variable !== criteria.variable
    );

    if (!value) {
      props.updateRaiderCriterias(newCriterias);
      return;
    }

    newCriterias.push({
      variable: criteria.variable,
      value: value,
    });
    props.updateRaiderCriterias(newCriterias);
  };

  const noneBarStyle: React.CSSProperties = {
    borderRadius: '2px',
    width: '15px',
    height: '6px',
    backgroundColor: 'white',
  };

  const arrowStyle: React.CSSProperties = {
    transition: '0.5s',
    width: '35px',
    height: '35px',
  };

  const isCurrentlyPremium = isPremium();
  return (
    <section style={props.style}>
      <div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap' }}>
        {criteriaList.map((criteria) => {
          const order = props.raiderCriterias.find((raiderCriteria) => raiderCriteria.variable === criteria.variable)
            ?.value as 'ASC' | 'DESC' | undefined;
          if (criteria.needPremium && !isCurrentlyPremium) {
            return null;
          }
          return (
            <FilterRadiusButton
              key={`order_${criteria.variable}`}
              name={criteria.name}
              onClick={() => handleClick(criteria)}
              buttonStyle={{ opacity: order === undefined ? '0.75' : '1' }}
              selectorDiv={
                order === undefined ? (
                  <div style={noneBarStyle} />
                ) : (
                  <ArrowDropDownIcon
                    style={{ ...arrowStyle, transform: order === 'ASC' ? 'rotate(180deg)' : 'rotate(0deg)' }}
                  />
                )
              }
            />
          );
        })}
      </div>
    </section>
  );
};
