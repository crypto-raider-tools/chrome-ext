import { QueryFilter, StuffFilter, StuffQueryStrings } from './types';
import { GenericFilter } from './GenericFilter';
import { HydraPaginatedResponse } from '../../interfaces/hydra';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { colorPalette } from '../../Helpers/Constants';

type StuffFiltersProps = {
  filters: StuffFilter[];
  currentFilters: QueryFilter[];
  handleCriteriaChange: (incomingFilters: StuffFilter[]) => void;
  raiders: undefined | HydraPaginatedResponse<DetailedSyncRaider>;
  disabledFilters: Array<StuffQueryStrings>;
  resetFilters: () => void;
};
export const StuffFilters = (props: StuffFiltersProps) => {
  const btnStyle: React.CSSProperties = {
    backgroundColor: colorPalette['experience'],
    borderRadius: '2px',
    color: 'white',
    fontWeight: '500',
    width: 'fit-content',
    marginTop: '10px',
    marginLeft: '5px',
    padding: '0 .2em .1em .2em',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    height: '30px',
  };
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <ClickableContainer
        onClick={() => {
          props.resetFilters();
        }}
        style={btnStyle}
      >
        Reset filters
      </ClickableContainer>
      <div
        style={{
          margin: '10px',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center',
        }}
      >
        {props.filters.map((filter) => {
          return (
            <div
              key={filter.queryString}
              style={{
                display: 'flex',
                flexDirection: 'column',
                flexWrap: 'nowrap',
                justifyContent: 'flex-start',
                alignItems: 'stretch',
                alignContent: 'stretch',
                marginLeft: '8px',
                marginTop: '5px',
              }}
            >
              <div style={{ color: colorPalette['lightBlueGrey'], fontWeight: '500' }}>{filter.displayName}:</div>
              <div>
                {(() => {
                  switch (filter.valueType) {
                    case 'text':
                    case 'number':
                    case 'boolean':
                      return (
                        <GenericFilter
                          type={filter.valueType}
                          onChange={(newValue) => {
                            props.handleCriteriaChange([
                              {
                                ...filter,
                                value: newValue,
                              },
                            ]);
                          }}
                          disabled={props.disabledFilters.includes(filter.queryString)}
                          value={props.currentFilters.find((f) => f.queryString === filter.queryString)?.value}
                        />
                      );
                    case 'slots':
                      const slotOptions = [
                        { label: 'main hand', value: 'main_hand' },
                        { label: 'dress', value: 'dress' },
                        { label: 'knickknack', value: 'knickknack' },
                        { label: 'finger', value: 'finger' },
                        { label: 'background', value: 'background' },
                        { label: 'neck', value: 'neck' },
                        { label: 'helmet', value: 'helmet' },
                      ];
                      return (
                        <select
                          multiple
                          style={{
                            width: 'calc(100% - 2px)',
                            color: '#000',
                          }}
                          disabled={props.disabledFilters.includes(filter.queryString)}
                          onChange={(e) => {
                            let newValue = Array.from(e.currentTarget.options)
                              .filter(function (option) {
                                return option.selected;
                              })
                              .map(function (option) {
                                return option.value;
                              });
                            props.handleCriteriaChange([
                              {
                                ...filter,
                                value: newValue,
                              },
                            ]);
                          }}
                          value={
                            props.currentFilters
                              .filter((f) => f.queryString === filter.queryString)
                              .map((f) => slotOptions.find((o) => o.value === f.value)?.value) as string[]
                          }
                        >
                          {slotOptions.map((option) => {
                            return (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            );
                          })}
                        </select>
                      );
                    case 'rarities':
                      const rarityOptions = [
                        { label: 'common', value: 'common' },
                        { label: 'uncommon', value: 'uncommon' },
                        { label: 'rare', value: 'rare' },
                        { label: 'epic', value: 'epic' },
                        { label: 'legendary', value: 'legendary' },
                        { label: 'mythic', value: 'mythic' },
                        { label: 'heirloom', value: 'heirloom' },
                        { label: 'limited', value: 'limited' },
                      ];
                      return (
                        <select
                          multiple
                          style={{
                            width: 'calc(100% - 2px)',
                            color: '#000',
                          }}
                          disabled={props.disabledFilters.includes(filter.queryString)}
                          onChange={(e) => {
                            let newValue = Array.from(e.currentTarget.options)
                              .filter(function (option) {
                                return option.selected;
                              })
                              .map(function (option) {
                                return option.value;
                              });
                            props.handleCriteriaChange([
                              {
                                ...filter,
                                value: newValue,
                              },
                            ]);
                          }}
                          value={
                            props.currentFilters
                              .filter((f) => f.queryString === filter.queryString)
                              .map((f) => rarityOptions.find((o) => o.value === f.value)?.value) as string[]
                          }
                        >
                          {rarityOptions.map((option) => {
                            return (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            );
                          })}
                        </select>
                      );
                    case 'raiders':
                      const selectedRaiders = props.currentFilters
                        .filter((f) => f.queryString === filter.queryString)
                        .map(
                          (f) => props?.raiders?.['hydra:member']?.find((o) => o['@id'] === f.value)?.['@id']
                        ) as string[];
                      return (
                        <select
                          multiple
                          style={{
                            width: 'calc(100% - 2px)',
                            color: '#000',
                          }}
                          onChange={(e) => {
                            let newValue = Array.from(e.currentTarget.options)
                              .filter(function (option) {
                                return option.selected;
                              })
                              .map(function (option) {
                                return option.value;
                              });
                            props.handleCriteriaChange([
                              {
                                ...filter,
                                value: newValue,
                              },
                            ]);
                          }}
                          value={selectedRaiders}
                        >
                          <>
                            {props?.raiders?.['hydra:member']?.map((raider) => (
                              <option key={raider['@id']} value={raider['@id']}>
                                {raider.name + ' - ' + raider.id}
                              </option>
                            )) ?? <option>Loading ...</option>}
                          </>
                        </select>
                      );
                  }
                })()}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
