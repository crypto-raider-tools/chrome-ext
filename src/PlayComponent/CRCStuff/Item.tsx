import { Stats } from '@crypto-raider-tools/computation';
import { colorPalette } from '../../Helpers/Constants';
import { wordToCamelCase } from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { SyncStuff } from '../../interfaces/syncstuff';
import { RaiderPicture } from '../Raider/RaiderPicture';
import { BoxShadow, ClickableContainer } from '../Utils/ClickableContainer';

type ItemProps = {
  isLoading: boolean;
  item?: SyncStuff;
  isExpanded: boolean;
  toggleExpand: () => void;
  owner: DetailedSyncRaider | undefined;
  isSelected: boolean;
  toggleSelect: () => void;
  unitSell: () => void;
  isSelectable: boolean;
};
export const Item = (props: ItemProps) => {
  const slotName = (slot?: string): string => {
    switch (slot) {
      case 'main_hand':
        return 'Weapon';
      case 'dress':
        return 'Armor';
      case 'knickknack':
        return 'Rune';
      case 'finger':
        return 'Finger';
      case 'background':
        return 'Background';
      case 'neck':
        return 'Necklace';
      case 'helmet':
        return 'Helmet';
      default:
        return '';
    }
  };

  const itemShortDescriptionString = () => {
    return `${props.item?.rarity}\n${props.item?.gearType}\n(${slotName(props.item?.slot)})`;
  };

  const itemShortDescriptionElement = () => {
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <p>
          <span style={{ fontWeight: '500', color: colorPalette[props.item?.rarity ?? 'common'] }}>
            {wordToCamelCase(props.item?.rarity ?? '')}
          </span>
        </p>
        <p style={{ fontSize: '12px', marginLeft: '8px' }}>
          <span title="Level" style={{ fontWeight: '500', color: colorPalette['lightBlueGrey'] }}>
            {' LVL '}
          </span>
          <span title="Level" style={{ fontWeight: '500', color: 'white' }}>
            {props.item?.level}
          </span>
          <br />
          <span title="Total Stats Points" style={{ fontWeight: '500', color: colorPalette['lightBlueGrey'] }}>
            {' TSP '}
          </span>
          <span title="Total Stats Points" style={{ fontWeight: '500', color: 'white' }}>
            {props.item?.totalStatPoints}
          </span>
        </p>
      </div>
    );
  };

  type ItemSingleStatProps = {
    name: keyof Stats;
  };
  const ItemSingleStat = (singleStatProps: ItemSingleStatProps) => {
    if (!props.item?.[singleStatProps.name]) return null;
    const divStyle: React.CSSProperties = {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: '30px',
      fontSize: '12px',
      fontWeight: '500',
    };
    const statNameStyle: React.CSSProperties = {
      backgroundColor: colorPalette[singleStatProps.name],
      width: '100%',
    };
    return (
      <div style={divStyle} title={singleStatProps.name}>
        <div style={statNameStyle}>{singleStatProps.name.substring(0, 3).toUpperCase()}</div>
        {props.item?.[singleStatProps.name] ?? (props.isLoading ? '00' : '')}
      </div>
    );
  };

  const boxShadows: string[] = [
    BoxShadow(colorPalette[props.item?.rarity ?? 'commong'], { x: 6, y: 6 }, 15, 1, true),
    BoxShadow(colorPalette['white'], { x: -6, y: -6 }, 15, 1, true),
  ];
  const selectedStyle: React.CSSProperties = {
    boxShadow: boxShadows[0] + ', ' + boxShadows[1],
    transform: 'scale(1.15)',
  };

  return (
    <div
      style={{
        background: 'rgb(45, 55, 72)',
        borderRadius: '3px',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        alignContent: 'stretch',
        width: props.isExpanded ? '360px' : '105px',
        height: '160px',
        margin: '2px',
      }}
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        props.toggleExpand();
      }}
      title={props.item?.isEquipped === true ? 'Item is equipped' : undefined}
    >
      <ClickableContainer
        onClick={() => {
          if ((props.item?.sellingPrice ?? 0) > 0) props.toggleSelect();
        }}
        className={props.isLoading ? 'loading' : ''}
        style={{
          margin: '5px',
          width: '100px',
          height: '100px',
          overflow: 'hidden',
          borderRadius: '8px',
          borderColor: colorPalette[props.item?.rarity ?? 'common'],
          borderWidth: '3px',
          ...(props.isLoading ? {} : { background: 'rgb(46, 46, 46)' }),
          ...(props.isSelected ? selectedStyle : {}),
        }}
        boxShadows={[
          BoxShadow(colorPalette[props.item?.rarity ?? 'common'], { x: 4, y: 4 }),
          BoxShadow(colorPalette[props.item?.rarity ?? 'common'], { x: -4, y: -4 }),
        ]}
        disabled={!props.isSelectable}
      >
        <div style={{ position: 'relative' }}>
          {!props.isLoading && (
            <img
              alt={props.item?.name}
              src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${
                props.item?.internalName ?? ''
              }_ICON.png`}
              style={{
                width: '100px',
                height: '100px',
                imageRendering: 'pixelated',
                ...(props.item?.isEquipped === true || props.isSelectable === false
                  ? { filter: 'brightness(0.5)' }
                  : {}),
              }}
            />
          )}
          {props.item?.isEquipped === true && (
            <div
              style={{
                position: 'absolute',
                width: '100px',
                fontWeight: '500',
                color: colorPalette['lightBlueGrey'],
                bottom: '37px',
                transform: 'rotate(45deg)',
                textAlign: 'center',
                fontSize: '20px',
              }}
            >
              Equipped
            </div>
          )}
        </div>
      </ClickableContainer>
      {props.isExpanded && (
        <div
          style={{
            width: '140px',
            margin: '5px',
            overflow: 'hidden',
            height: '100px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <div className={props.isLoading ? 'loading' : ''} style={{ whiteSpace: 'nowrap' }} title={props.item?.name}>
            {props.isLoading && <>&nbsp;</>}
            {!props.isLoading && props.item?.name}
          </div>
          <div
            className={props.isLoading ? 'loading' : ''}
            style={{ whiteSpace: 'nowrap' }}
            title={itemShortDescriptionString()}
          >
            {props.isLoading && <>&nbsp;</>}
            {!props.isLoading && itemShortDescriptionElement()}
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'nowrap',
              justifyContent: 'flex-start',
              alignItems: 'baseline',
              alignContent: 'stretch',
              textAlign: 'center',
              width: '100%',
            }}
          >
            <ItemSingleStat name="strength" />
            <ItemSingleStat name="intelligence" />
            <ItemSingleStat name="agility" />
            <ItemSingleStat name="wisdom" />
            <ItemSingleStat name="charm" />
            <ItemSingleStat name="luck" />
          </div>
        </div>
      )}
      {props.isExpanded && (
        <div
          style={{
            width: '85px',
            margin: '5px',
            overflow: 'hidden',
            height: '100px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div
            style={{ width: '75px', height: '75px' }}
            className={props.isLoading || typeof props.owner === 'undefined' ? 'loading' : ''}
          >
            {props.owner && <RaiderPicture raider={props.owner.raw_data!} pictureSize={75} />}
          </div>
          <p
            style={{ fontWeight: '500' }}
            className={props.isLoading || typeof props.owner === 'undefined' ? 'loading' : ''}
          >
            {props.owner?.id}
          </p>
        </div>
      )}
      <div
        style={{
          width: props.isExpanded ? '360px' : '105px',
          margin: '0px 5px 5px',
          overflow: 'hidden',
          height: '40px',
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'nowrap',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          alignContent: 'center',
          textAlign: 'center',
        }}
      >
        {(props.item?.sellingPrice ?? 0) > 0 ? (
          <p style={{ fontSize: '1.1em', fontWeight: '500', display: 'flex', alignItems: 'center' }}>
            {(props.item?.sellingPrice ?? 0).toFixed(3) + ' '}
            <img
              style={{ width: '20px', height: '20px', imageRendering: 'pixelated', marginLeft: '4px' }}
              alt="aurum"
              src="https://storage.googleapis.com/crypto-raiders-assets/web/currencies/aurum.png"
            />
          </p>
        ) : (
          <p
            style={{
              fontSize: '1.1em',
              color: colorPalette['lightBlueGrey'],
              fontWeight: '500',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            0
            <img
              style={{ width: '20px', height: '20px', imageRendering: 'pixelated', marginLeft: '4px' }}
              alt="aurum"
              src="https://storage.googleapis.com/crypto-raiders-assets/web/currencies/aurum.png"
            />
          </p>
        )}
      </div>
    </div>
  );
};
