import { QueryOrder, StuffFilter, StuffOrderBy, StuffQueryStrings } from './types';
import { GenericOrderBy } from './GenericOrderBy';

type StuffOrderBysProps = {
  orderBys: StuffOrderBy[];
  currentOrderBys: QueryOrder[];
  handleOrderByCriteriaChange: (incomingOrderBys: StuffOrderBy[]) => void;
  handleFilterCriteriaChange: (incomingFilters: StuffFilter[]) => void;
  disabledFilters: Array<StuffQueryStrings>;
  updateDisabledFilters: (filters: Array<StuffQueryStrings>) => void;
};
export const StuffOrderBys = (props: StuffOrderBysProps) => {
  const noneBarStyle: React.CSSProperties = {
    borderRadius: '2px',
    width: '15px',
    height: '6px',
    backgroundColor: 'white',
  };

  const arrowStyle: React.CSSProperties = {
    transition: '0.5s',
    width: '35px',
    height: '35px',
  };
  return (
    <div
      style={{
        margin: '10px',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
      }}
    >
      {props.orderBys.map((orderBy) => {
        return (
          <GenericOrderBy
            key={orderBy.queryString}
            orderBy={orderBy}
            currentOrderBy={props.currentOrderBys.find((o) => o.queryString === orderBy.queryString)}
            handleFilterCriteriaChange={props.handleFilterCriteriaChange}
            handleOrderByCriteriaChange={props.handleOrderByCriteriaChange}
            disabledFilters={props.disabledFilters}
            updateDisabledFilters={props.updateDisabledFilters}
            noneBarStyle={noneBarStyle}
            arrowStyle={arrowStyle}
          />
        );
      })}
    </div>
  );
};
