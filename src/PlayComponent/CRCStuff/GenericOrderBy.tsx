import { FilterRadiusButton } from '../Utils/FiltersRadiusButton';
import { QueryOrder, StuffFilter, StuffOrderBy, StuffQueryStrings } from './types';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

type GenericOrderByProps = {
  orderBy: StuffOrderBy;
  currentOrderBy: QueryOrder | undefined;
  noneBarStyle: React.CSSProperties;
  arrowStyle: React.CSSProperties;
  handleOrderByCriteriaChange: (incomingOrderBys: StuffOrderBy[]) => void;
  handleFilterCriteriaChange: (incomingFilters: StuffFilter[]) => void;
  disabledFilters: Array<StuffQueryStrings>;
  updateDisabledFilters: (filters: Array<StuffQueryStrings>) => void;
};
export const GenericOrderBy = (props: GenericOrderByProps) => {
  const currentValue = props.currentOrderBy ? props.currentOrderBy.value : undefined;
  let onChange = (newOrderBy: string | undefined) => {
    const filterQueryStringMin = `${props.orderBy.queryString.replace(
      /order\[(.*)\]/,
      '$1'
    )}[gte]` as StuffQueryStrings;
    let filterQueryStringMinValue: string | undefined = undefined;
    let newDisabledFilters = [...props.disabledFilters.filter((f) => f !== filterQueryStringMin)];

    if (
      (newOrderBy === 'DESC' && props.orderBy.needsMinFilter === true) ||
      (newOrderBy === 'ASC' && props.orderBy.needsMaxFilter === true)
    ) {
      filterQueryStringMinValue = '0';
      newDisabledFilters.push(filterQueryStringMin);
    }

    props.handleFilterCriteriaChange([
      {
        queryString: filterQueryStringMin,
        value: filterQueryStringMinValue,
        displayName: '',
        multiple: false,
        valueType: 'number',
      },
    ]);
    props.updateDisabledFilters(newDisabledFilters);

    if (!newOrderBy || (newOrderBy !== 'DESC' && newOrderBy !== 'ASC')) {
      props.handleOrderByCriteriaChange([
        {
          ...props.orderBy,
          value: undefined,
        },
      ]);
      return;
    }
    props.handleOrderByCriteriaChange([
      {
        ...props.orderBy,
        value: newOrderBy,
      },
    ]);
  };
  return (
    <FilterRadiusButton
      key={props.orderBy.queryString}
      name={props.orderBy.displayName}
      onClick={() => {
        let value = currentValue;
        if (typeof value === 'undefined') value = 'DESC';
        else if (value === 'DESC') value = 'ASC';
        else value = undefined;
        onChange(value);
      }}
      buttonStyle={{ opacity: currentValue === undefined ? '0.75' : '1' }}
      selectorDiv={
        currentValue === undefined ? (
          <div style={props.noneBarStyle} />
        ) : (
          <ArrowDropDownIcon
            style={{ ...props.arrowStyle, transform: currentValue === 'ASC' ? 'rotate(180deg)' : 'rotate(0deg)' }}
          />
        )
      }
    />
  );
};
