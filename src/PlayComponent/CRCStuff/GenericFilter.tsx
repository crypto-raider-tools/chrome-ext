import { useState } from 'react';

type GenericFilterProps = {
  type: 'text' | 'number' | 'boolean' | 'range';
  onChange: (newValue: undefined | string | number | Array<string | number>) => void;
  disabled: boolean;
  value?: string | number | Array<string | number>;
};
export const GenericFilter = (props: GenericFilterProps) => {
  const [groupId] = useState<string>(Math.random().toString(36).substring(2, 15));
  if (['text', 'number'].includes(props.type)) {
    return (
      <input
        style={{
          width: 'calc(100% - 2px)',
          color: '#000',
        }}
        disabled={props.disabled}
        type={props.type}
        onChange={(e) => {
          if (e.currentTarget.value === '') {
            props.onChange(undefined);
          } else {
            props.onChange(e.target.value);
          }
        }}
        // @ts-expect-error
        value={props.value ?? ''}
      />
    );
  }
  if ('boolean' === props.type) {
    const labelStyle: React.CSSProperties = {
      minWidth: '60px',
      width: '33%',
      display: 'inline-block',
      textAlign: 'center',
      cursor: 'pointer',
    };
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <label style={labelStyle}>
          <input
            disabled={props.disabled}
            onChange={() => props.onChange(undefined)}
            type="radio"
            name={groupId}
            checked={typeof props.value === 'undefined'}
          />{' '}
          All
        </label>
        <label style={labelStyle}>
          <input
            disabled={props.disabled}
            onChange={() => props.onChange('1')}
            type="radio"
            name={groupId}
            checked={props.value === '1'}
            value="1"
          />{' '}
          Yes
        </label>
        <label style={labelStyle}>
          <input
            disabled={props.disabled}
            onChange={() => props.onChange('0')}
            type="radio"
            name={groupId}
            checked={props.value === '0'}
            value="0"
          />{' '}
          No
        </label>
      </div>
    );
  }
  return <span>This Generic filter has not been implemented yet</span>;
};
