import { HydraCriteriaVariables } from '../../interfaces/hydra';
import { SyncRaider } from '../../interfaces/syncraider';
import { SyncStuff } from '../../interfaces/syncstuff';

export type StuffFilterValueTypes = 'boolean' | 'text' | 'number' | 'slots' | 'rarities' | 'raiders';
export type StuffQueryStrings = HydraCriteriaVariables<SyncStuff> | `raider.${HydraCriteriaVariables<SyncRaider>}`;
export type StuffFilter = {
  displayName: string;
  queryString: StuffQueryStrings;
  valueType: StuffFilterValueTypes;
  multiple: boolean;
  value?: string | number | Array<string | number>;
};
export type StuffOrderBy = {
  displayName: string;
  queryString: StuffQueryStrings;
  value?: 'DESC' | 'ASC';
  needsMinFilter: boolean;
  needsMaxFilter: boolean;
};
export type QueryFilter = {
  displayName: string;
  queryString: StuffQueryStrings;
  valueType: StuffFilterValueTypes;
  multiple: boolean;
  value?: string | number;
};
export type QueryOrder = {
  displayName: string;
  queryString: StuffQueryStrings;
  value?: string | number;
};
