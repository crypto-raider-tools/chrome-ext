import { Avatar } from '@crypto-raider-tools/computation';

type RaiderAvatarData = {
  body: string;
  face: string;
  hair: string;
};

type RaiderAvatarPictureProps = {
  avatar?: Avatar;
  showBackground: boolean;
  style?: React.CSSProperties;
  pictureSize?: number;
  scaleMultiplier?: number;
  onClick?: () => void;
};

export function RaiderAvatarPicture(props: RaiderAvatarPictureProps) {
  const avatarData: RaiderAvatarData = {
    body: props.avatar?.body ?? 'HUMAN_FEMALE_WHITE_FEMALE_BASIC_BODY_1',
    face: props.avatar?.face ?? 'HUMAN_FEMALE_BASIC_FACE_1',
    hair: props.avatar?.hair ?? 'SHORT_FEMALE_BLUE_HAIR',
  };
  let pictureSize: number = props.pictureSize ?? 224;
  let transformScale: number = pictureSize / 224;
  if (props.scaleMultiplier) transformScale *= props.scaleMultiplier;

  const backgroundStyle: React.CSSProperties = {
    backgroundSize: 'cover',
    backgroundImage: 'url("https://crypto-raiders-assets.storage.googleapis.com/bg.png")',
    width: '100%',
    height: '100%',
  };

  return (
    <div
      style={{
        ...props.style,
        height: pictureSize + 'px',
        width: pictureSize + 'px',
      }}
      onClick={props.onClick}
    >
      <div style={props.showBackground ? backgroundStyle : { width: '100%', height: '100%' }}>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative',
            width: '100%',
            height: '100%',
            transform: 'scale(' + transformScale + ')',
          }}
        >
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/body/' + avatarData.body + '.png)',
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/face/' + avatarData.face + '.png)',
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/hair/' + avatarData.hair + '.png)',
            }}
          />
        </div>
      </div>
    </div>
  );
}
