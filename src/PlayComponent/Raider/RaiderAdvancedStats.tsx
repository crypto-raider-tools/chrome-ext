import { BuildResult, computeRaiderBuild, FighterBuild, FighterStats } from '@crypto-raider-tools/computation';
import { FighterBuildNameToText } from '../../Helpers/Maps';
import { getFighterBuildMetaOutputsLarge, wordToCamelCase } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { H2 } from '../Theme';
import { InventoryOverviewComputeBuildSplit } from './RaiderResume/RaiderComputeBuildSplit';
type AdvancedStatsType = {
  style?: React.CSSProperties;
  fighterStats: FighterStats;
  currentRaiderBuild: BuildResult;
  cacheBuildCap: CacheBuildCap;
  currentBuild?: FighterBuild;
  compareBuild?: FighterBuild;
  invertComparison?: boolean;
};

export const RaiderAdvancedStats = (props: AdvancedStatsType) => {
  let fighterBuild = props.currentBuild;
  if (!fighterBuild) {
    try {
      fighterBuild = computeRaiderBuild(props.fighterStats);
    } catch (error) {
      fighterBuild = {
        maxHP: 0,
        minDamage: 0,
        maxDamage: 0,
        hitChance: 0,
        hitFirst: 0,
        meleeCrit: 0,
        critDamageMultiplier: 0,
        critResist: 0,
        evadeChance: 0,
        meleeResist: 0,
      };
    }
  }
  const buildMeta: string[] = getFighterBuildMetaOutputsLarge(
    fighterBuild,
    props.cacheBuildCap,
    props.fighterStats.level
  );
  const defaultStyle: React.CSSProperties = {
    width: '300px',
    paddingTop: '8px',
    ...props.style,
    ...(typeof props.compareBuild !== 'undefined' ? { width: '280px' } : {}),
  };
  const cellStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    width: '100%',
    height: '20px',
    fontSize: '12px',
    fontWeight: '500',
    whiteSpace: 'nowrap',
  };
  return (
    <div style={defaultStyle}>
      <H2>{wordToCamelCase(props.currentRaiderBuild.currentClass)}</H2>
      {Object.keys(fighterBuild).map((rawFighterBuildName, index) => {
        const fighterBuildName = rawFighterBuildName as keyof FighterBuild;
        let buildStat = fighterBuild![fighterBuildName].toFixed(1);
        let buildCap = props.cacheBuildCap[props.fighterStats.level][fighterBuildName];
        if (fighterBuildName === 'minDamage') {
          buildStat = fighterBuild!.minDamage.toFixed(1) + ' / ' + fighterBuild!.maxDamage.toFixed(1);
        } else if (fighterBuildName === 'maxDamage') return null;
        const metaIndex: number = index < 2 ? index : index - 1; //Because we skip the maxDamages
        return (
          <InventoryOverviewComputeBuildSplit
            key={index}
            buildMeta={buildMeta[metaIndex].toLowerCase()}
            buildStat={buildStat}
            buildStatName={FighterBuildNameToText[fighterBuildName]}
            buildCap={buildCap}
            cellStyle={cellStyle}
            compareBuildStat={
              props.compareBuild
                ? props.invertComparison
                  ? fighterBuild![fighterBuildName] - props.compareBuild[fighterBuildName]
                  : props.compareBuild[fighterBuildName] - fighterBuild![fighterBuildName]
                : undefined
            }
          />
        );
      })}
    </div>
  );
};
