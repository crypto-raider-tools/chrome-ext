import {
  BuildResult,
  FighterBuild,
  createFighterBuild,
  FighterStats,
  createFighterStats,
  Stats,
  createStats,
  computeStatsBuildMetaScore,
  fixStats,
  computeRaiderBuild,
  BuildType,
} from '@crypto-raider-tools/computation';
import { useCallback, useEffect, useState } from 'react';
import {
  buildBySlot,
  extractDetailedRaiderStats,
  getItemPureness,
  getItemRecommentationScore,
} from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { DungeonSelectedForSim } from '../../interfaces/dungeonSelectedForSim';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import { Mobs } from '../../interfaces/mobs';
import { ItemPureness } from '../../interfaces/pureness';
import { RaiderAbility } from '../../interfaces/raiderability';
import { StuffStats } from '../../interfaces/stuffstats';
import { Raider } from './Raider';

type RaiderContextProps = {
  raider: DetailedSyncRaider;
  style?: React.CSSProperties;
  onPictureClick: () => void;
  isExpanded: boolean;
  handleUpdateRaiderServerSide: (
    raider: DetailedSyncRaider,
    objects?: Array<IndexedInventoryItem>,
    raiderStats?: Stats
  ) => Promise<void>;
  stuffStats: StuffStats[];
  currentMaxMeta: number;
  dungeonSelectedForSim: DungeonSelectedForSim;
  cacheBuildCap: CacheBuildCap;
  mobsData: Mobs[];
  raiderAbilitiesData: RaiderAbility[];
};

export const RaiderContext = (props: RaiderContextProps) => {
  /**
   * Inventory Datas
   */

  const [localInventory, setLocalInventory] = useState<IndexedInventoryItem[]>([]);
  const [hoverItem, setHoverItem] = useState<IndexedInventoryItem | undefined>();
  const [equippedItems, setEquippedItems] = useState<Record<number, IndexedInventoryItem>>({});
  /**
   * CurrentRaiderDatas
   */
  const [inventoryComputedBuilds, setInventoryComputedBuilds] = useState<BuildResult[]>([]);
  const [inventoryComputedPureness, setInventoryComputedPureness] = useState<ItemPureness[]>([]);
  const [inventoryRecommendationsScore, setInventoryRecommendationsScore] = useState<number[]>([]);
  const [currentClassRecommentation, setCurrentClassRecommentation] = useState<BuildType>('none');
  const [currentRaiderBuild, setCurrentRaiderBuild] = useState<BuildResult>({
    metaScore: 0,
    classes: {
      none: 0,
      warrior: 0,
      paladin: 0,
      barbarian: 0,
      wizard: 0,
      rogue: 0,
      druid: 0,
    },
    currentClass: 'none',
  });
  const [fighterBuild, setFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [fighterStats, setFighterStats] = useState<FighterStats>(createFighterStats());

  useEffect(() => {
    const newComputedPureness: ItemPureness[] = [];
    props.raider.raw_data?.inventory?.forEach((object) => {
      const computedPureness = getItemPureness(
        object.item,
        props.stuffStats.find((i) => i.internalName === object.item.internalName)
      );
      newComputedPureness.push(computedPureness);
    });
    setInventoryComputedPureness(newComputedPureness);
    // eslint-disable-next-line
  }, [props.stuffStats, props.raider]);

  useEffect(() => {
    async function computeInventory() {
      const newComputedBuilds: BuildResult[] = [];
      const newEquippedItems: Record<number, IndexedInventoryItem> = {};

      const newFighterStats: FighterStats = createFighterStats();
      newFighterStats.level = props.raider.level ?? 1;

      const newFullStats: Stats = { ...(extractDetailedRaiderStats(props.raider) ?? createStats()) };
      newFighterStats.stats = { ...newFullStats };
      setLocalInventory(
        props.raider.raw_data?.inventory?.map((object, index) => {
          const computedBuild = computeStatsBuildMetaScore(fixStats(object.item.stats));
          newComputedBuilds.push(computedBuild);

          const indexedItem: IndexedInventoryItem = {
            originalIndex: index,
            filteredIndex: index,
            computedBuild: computedBuild,
            equipped: object.equipped,
            item: { ...object.item, stats: fixStats(object.item.stats) },
          };
          if (indexedItem.equipped) {
            newEquippedItems[index] = indexedItem;
            buildBySlot[indexedItem.item.slot](newFighterStats, {
              ...indexedItem.item,
              name: indexedItem.item.name.replace(' - Spell Rune', ''),
            });
          }
          return indexedItem;
        }) ?? []
      );
      setInventoryComputedBuilds(newComputedBuilds);
      try {
        setFighterBuild(computeRaiderBuild(newFighterStats));
      } catch (error) {
        setFighterBuild({
          maxHP: 0,
          minDamage: 0,
          maxDamage: 0,
          hitChance: 0,
          hitFirst: 0,
          meleeCrit: 0,
          critDamageMultiplier: 0,
          critResist: 0,
          evadeChance: 0,
          meleeResist: 0,
        });
      }

      Object.keys(newFullStats).forEach((statName) => {
        newFullStats[statName as keyof Stats] +=
          (newFighterStats.mainHand?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.dress?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.finger?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.neck?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.helmet?.stats[statName as keyof Stats] ?? 0);
      });
      setFighterStats(newFighterStats);
      const newRaiderBuild: BuildResult = computeStatsBuildMetaScore(newFullStats);
      setCurrentRaiderBuild(newRaiderBuild);
      setEquippedItems(newEquippedItems);

      const newRecommendationsScore: number[] = [];
      props.raider.raw_data?.inventory?.forEach((object, index) => {
        newRecommendationsScore.push(
          getItemRecommentationScore(
            newComputedBuilds[index],
            Object.values(newEquippedItems).find((eq) => eq.item.slot === object.item.slot)?.computedBuild,
            newRaiderBuild.currentClass
          )
        );
      });
      setInventoryRecommendationsScore(newRecommendationsScore);
      setCurrentClassRecommentation(newRaiderBuild.currentClass);
    }
    computeInventory();
  }, [props.raider]);

  const localEquipItem = useCallback(
    (currentItem: IndexedInventoryItem): void => {
      const newEquippedItems = { ...equippedItems };
      const isUnequip = typeof newEquippedItems[currentItem.originalIndex] !== 'undefined';
      if (isUnequip) delete newEquippedItems[currentItem.originalIndex];
      const newFighterStats: FighterStats = createFighterStats();
      const newFullStats: Stats = { ...(extractDetailedRaiderStats(props.raider) ?? createStats()) };
      newFighterStats.level = props.raider.level ?? 1;
      newFighterStats.stats = { ...newFullStats };

      if (!isUnequip) {
        for (const item of Object.values(newEquippedItems)) {
          if (item.item.slot === currentItem.item.slot) {
            delete newEquippedItems[item.originalIndex];
          }
        }
        newEquippedItems[currentItem.originalIndex] = currentItem;
      }
      for (const equippedItem of Object.values(newEquippedItems)) {
        buildBySlot[equippedItem.item.slot](newFighterStats, equippedItem.item);
      }
      setEquippedItems(newEquippedItems);
      Object.keys(newFullStats).forEach((statName) => {
        newFullStats[statName as keyof Stats] +=
          (newFighterStats.mainHand?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.dress?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.finger?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.neck?.stats[statName as keyof Stats] ?? 0) +
          (newFighterStats.helmet?.stats[statName as keyof Stats] ?? 0);
      });
      //InitFighterBuild

      try {
        setFighterBuild(computeRaiderBuild(newFighterStats));
      } catch (error) {
        setFighterBuild({
          maxHP: 0,
          minDamage: 0,
          maxDamage: 0,
          hitChance: 0,
          hitFirst: 0,
          meleeCrit: 0,
          critDamageMultiplier: 0,
          critResist: 0,
          evadeChance: 0,
          meleeResist: 0,
        });
      }

      setFighterStats(newFighterStats);
      const newRaiderBuild: BuildResult = computeStatsBuildMetaScore(newFullStats);
      setCurrentRaiderBuild(newRaiderBuild);

      const newRecommendationsScore: number[] = [];
      props.raider.raw_data?.inventory?.forEach((object, index) => {
        newRecommendationsScore.push(
          getItemRecommentationScore(
            inventoryComputedBuilds[index],
            Object.values(newEquippedItems).find((eq) => eq.item.slot === object.item.slot)?.computedBuild,
            newRaiderBuild.currentClass
          )
        );
      });
      setInventoryRecommendationsScore(newRecommendationsScore);
      setCurrentClassRecommentation(newRaiderBuild.currentClass);
    },
    [props.raider, equippedItems, inventoryComputedBuilds]
  );

  const updateFighterStats = (value: FighterStats) => {
    try {
      setFighterBuild(computeRaiderBuild(value));
    } catch (error) {
      setFighterBuild({
        maxHP: 0,
        minDamage: 0,
        maxDamage: 0,
        hitChance: 0,
        hitFirst: 0,
        meleeCrit: 0,
        critDamageMultiplier: 0,
        critResist: 0,
        evadeChance: 0,
        meleeResist: 0,
      });
    }
    setFighterStats(value);
  };

  return (
    <Raider
      raider={props.raider}
      onPictureClick={props.onPictureClick}
      isExpanded={props.isExpanded}
      handleUpdateRaiderServerSide={props.handleUpdateRaiderServerSide}
      localInventory={localInventory}
      setLocalInventory={setLocalInventory}
      setHoverItem={setHoverItem}
      fighterBuild={fighterBuild}
      fighterStats={fighterStats}
      hoverItem={hoverItem}
      currentRaiderBuild={currentRaiderBuild}
      handleLocalEquipItem={localEquipItem}
      equippedItems={equippedItems}
      inventoryComputedBuilds={inventoryComputedBuilds}
      currentMaxMeta={props.currentMaxMeta}
      stuffStats={props.stuffStats}
      currentClassRecommentation={currentClassRecommentation}
      setCurrentClassRecommentation={setCurrentClassRecommentation}
      cacheBuildCap={props.cacheBuildCap}
      inventoryComputedPureness={inventoryComputedPureness}
      inventoryRecommendationsScore={inventoryRecommendationsScore}
      updateFighterStats={updateFighterStats}
      mobsData={props.mobsData}
      raiderAbilitiesData={props.raiderAbilitiesData}
    />
  );
};
