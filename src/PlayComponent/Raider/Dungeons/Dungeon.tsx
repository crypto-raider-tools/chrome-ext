import { FighterBuild, Rarity } from '@crypto-raider-tools/computation';
import React, { useContext, useState } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { RarityAsNumber } from '../../../Helpers/Maps';
import { DetailedDungeon } from '../../../interfaces/detailedDungeon';
import { DetailedMobs } from '../../../interfaces/detailedMobs';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { StuffStats } from '../../../interfaces/stuffstats';
import DropRateContext from '../../Context/DropRateContext';
import DungeonSettingsContext from '../../Context/DungeonSettingsContext';
import { ClickableContainer } from '../../Utils/ClickableContainer';
import { DungeonIcon } from './DungeonIcon';
import { DungeonItem } from './DungeonItem';
import { DungeonItemHover } from './DungeonItemHover';
import { DungeonWinRateEstimation } from './DungeonWinRateEstimation';

type DungeonProps = {
  dungeon: DetailedDungeon;
  openDungeon: (dungeon: DetailedDungeon) => void;
  stuffStats: StuffStats[];
  raider: DetailedSyncRaider;
  currentFighterBuild: FighterBuild;
  style?: React.CSSProperties;
  mobs?: DetailedMobs[];
};

export const Dungeon = (props: DungeonProps) => {
  const [currentHoverItem, setCurrentHoverItem] = useState<StuffStats>();
  const [currentHoverDOMRect, setCurrentHoverDOMRect] = useState<DOMRect>();

  const { dungeonGridConfig } = useContext(DungeonSettingsContext);
  const { dropRatesData } = useContext(DropRateContext);

  const defaultStyle: React.CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    width: '100%',
    borderRadius: '2px',
    padding: '2px 5px',
    marginBottom: '8px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    ...props.style,
  };

  const dungeonContainerStyle: React.CSSProperties = {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: '70px',
    alignItems: 'center',
  };

  const droppableContainerStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  };

  const nameTextStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
  };

  const buttonStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['lightBlueGrey'],
    margin: '5px 8px',
    display: 'flex',
    alignItems: 'center',
    padding: '8px',
    height: '30px',
  };

  const raidButtonStyle: React.CSSProperties = {
    ...buttonStyle,
    backgroundColor: colorPalette['meta'],
  };

  let droppableStuffStats: StuffStats[] = [];

  if (props.dungeon.droppableItems) {
    props.dungeon.droppableItems.forEach((itemName) => {
      const stuffStats = props.stuffStats.find((ss) => ss['@id'] === itemName);
      if (stuffStats) {
        droppableStuffStats.push(stuffStats);
      }
    });
    droppableStuffStats = droppableStuffStats.sort((a, b) => {
      if (!a.internalName) return 1;
      if (!b.internalName) return -1;
      if (a.internalName < b.internalName) {
        return -1;
      }
      if (a.internalName > b.internalName) {
        return 1;
      }
      return 0;
    });
    droppableStuffStats = droppableStuffStats.sort((a, b) => {
      if (!a.rarity) return 1;
      if (!b.rarity) return -1;
      return RarityAsNumber[b.rarity as Rarity] - RarityAsNumber[a.rarity as Rarity];
    });
  }

  return (
    <div
      style={defaultStyle}
      onMouseLeave={() => {
        setCurrentHoverItem(undefined);
      }}
    >
      <div style={dungeonContainerStyle}>
        <div style={{ ...dungeonContainerStyle, width: '50%', justifyContent: 'flex-start' }}>
          <DungeonIcon x={props.dungeon.iconPosX} y={props.dungeon.iconPosY} url={props.dungeon.iconUrl} />
          <div>
            <p style={nameTextStyle}>{props.dungeon.name}</p>
            <p>
              <span style={{ color: colorPalette['lightBlueGrey'] }}>Lvl: </span>
              <span style={{ fontWeight: '500' }}>
                {props.dungeon.levelMin}
                {props.dungeon.levelMin !== props.dungeon.levelMax && ` - ${props.dungeon.levelMax}`}
              </span>
              <span style={{ marginLeft: '8px', color: colorPalette['lightBlueGrey'] }}> Xp: </span>
              <span style={{ fontWeight: '500' }}>{props.dungeon.winXp}</span>
            </p>
          </div>
        </div>
        <div style={{ ...dungeonContainerStyle, width: '50%', justifyContent: 'flex-end' }}>
          <p>🗝️ x {props.dungeon.keyCost}</p>
          <ClickableContainer
            onClick={() => {
              const awnser = window.confirm(
                'Are you sure? You are about to start ' + dungeonGridConfig.config.nbRaidPerRun + " dungeon's run."
              );
              if (awnser) {
                props.openDungeon(props.dungeon);
              }
            }}
            style={raidButtonStyle}
          >
            <span>Raid!🤺</span>
          </ClickableContainer>
        </div>
      </div>
      {typeof props.mobs !== 'undefined' && props.mobs.length > 0 && (
        <DungeonWinRateEstimation
          playerFighterBuild={props.currentFighterBuild}
          mobs={props.mobs}
          style={{ margin: '0px 0px 10px 0px' }}
        />
      )}
      {droppableStuffStats.length > 0 && (
        <div style={droppableContainerStyle}>
          <p>Loots:</p>
          {droppableStuffStats.map((stuffStats) => {
            return (
              <DungeonItem
                key={stuffStats['@id']}
                stuffStats={stuffStats}
                onHover={(stuffStats: StuffStats, bounds: DOMRect): void => {
                  setCurrentHoverDOMRect(bounds);
                  setCurrentHoverItem(stuffStats);
                }}
              />
            );
          })}
        </div>
      )}
      <DungeonItemHover
        stuffStats={currentHoverItem}
        hoverDOMRect={currentHoverDOMRect}
        dropRateData={dropRatesData[props.dungeon.name ?? '']}
      />
    </div>
  );
};
