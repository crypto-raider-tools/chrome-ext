import { isPremium } from '../../../Helpers/Utils';
import { DetailedDungeon } from '../../../interfaces/detailedDungeon';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { DisplayInfo, DungeonGridConfig, DungeonRunData } from '../../../interfaces/dungeonSettings';

export const defaultGridConfig = (): DungeonGridConfig => {
  const premium = isPremium();
  const config = {
    displayInfo: {
      monitorId: 0,
      workArea: {
        width: window.screen.width,
        height: window.screen.height,
      },
      bounds: {
        top: 0,
        left: 0,
      },
    },
    cellSize: {
      width: window.screen.width,
      height: window.screen.height,
    },
    config: {
      row: premium ? parseInt(localStorage.getItem('CRC_Dungeon_Grid_Config_Row') ?? '1') : 1,
      column: premium ? parseInt(localStorage.getItem('CRC_Dungeon_Grid_Config_Column') ?? '1') : 1,
      nbRaidPerRun: premium ? parseInt(localStorage.getItem('CRC_Dungeon_Grid_Config_Raids_Per_Run') ?? '1') : 1,
    },
    isMuteTab: (localStorage.getItem('CRC_Dungeon_Grid_Config_Is_Mute_Tab') ?? 'true') === 'true',
  };
  return config;
};

export const updateConfigWithDisplayInfo = (display: DisplayInfo, config: DungeonGridConfig) => {
  config.displayInfo = display;
  config.cellSize = {
    width: Math.floor(display.workArea.width / config.config.column),
    height: Math.floor(display.workArea.height / config.config.row),
  };
};

export const buildDungeonUrl = async (
  raider: DetailedSyncRaider,
  dungeon: DetailedDungeon
): Promise<{ url: string; data: DungeonRunData }> => {
  const dataRun: DungeonRunData = {
    secret: '',
    url: '',
    dungeonName: dungeon.name ?? '',
    raiderId: raider['id'] ?? -1,
  };
  const gotRickRolled: boolean = Math.floor(Math.random() * (2000 - 0) + 0) === 666;
  if (gotRickRolled)
    return Promise.resolve({
      url: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
      data: { ...dataRun, secret: 'rickRolled' },
    });
  const url = `https://play.cryptoraiders.xyz/api/instance/raid?id=${raider['id']}&instanceType=${encodeURIComponent(
    dungeon.internalName ?? ''
  )}&instanceTitle=${encodeURIComponent(dungeon.name ?? '')}&clientSeed=${Math.random()
    .toString()
    .replace('.', '')
    .substring(1, 11)}&difficulty=${dungeon.difficulty ?? ''}&guildUserAction=false`;
  const request = await fetch(url, {
    headers: {
      Accept: 'application/json',
      Cookie: document.cookie,
    },
  });
  if (request.status !== 200) {
    return Promise.resolve({
      url: '',
      data: dataRun,
    });
  }
  const response: Record<'secret', string> = await request.json();
  const dungeonUrl = `https://dungeon.cryptoraiders.gg/?secret=${encodeURIComponent(response.secret)}`;
  console.log('If the run crash click on the link below 😊');
  console.log(dungeonUrl);
  dataRun.secret = response.secret ?? '';
  return Promise.resolve({
    url: dungeonUrl,
    data: dataRun,
  });
};

export const buildDuelsUrl = async (raider: DetailedSyncRaider, targetId: string): Promise<string> => {
  const url = `https://play.cryptoraiders.xyz/api/instance/battle?id=${raider['id']}&duelId=${encodeURIComponent(
    targetId ?? ''
  )}&clientSeed=${Math.random().toString().replace('.', '').substring(1, 11)}`;
  const request = await fetch(url, {
    headers: {
      Accept: 'application/json',
      Cookie: document.cookie,
    },
  });
  if (request.status !== 200) {
    return Promise.resolve('');
  }
  const response: Record<'secret', string> = await request.json();
  const duelUrl = `https://dungeon.cryptoraiders.gg/?secret=${encodeURIComponent(response.secret)}`;

  console.log('If the run crash click on the link below 😊');
  console.log(duelUrl);
  return Promise.resolve(duelUrl);
};
