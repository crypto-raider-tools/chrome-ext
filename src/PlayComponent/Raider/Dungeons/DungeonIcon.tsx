type DungeonIconProps = {
  x?: number;
  y?: number;
  url?: string;
  style?: React.CSSProperties;
};

export const DungeonIcon = (props: DungeonIconProps) => {
  return (
    <div
      style={{
        width: '48px',
        height: '48px',
        imageRendering: 'pixelated',
        filter: 'drop-shadow(0 0 3px #0009)',
        background: `url(${props.url ?? 'https://storage.googleapis.com/crypto-raiders-assets/items_o.png'})`,
        backgroundPosition: (props.x ?? '0') + 'px ' + (props.y ?? '0') + 'px',
        ...props.style,
      }}
    />
  );
};
