type TabPanelProps = {
  children?: React.ReactNode;
  index: number;
  value: number;
  style?: React.CSSProperties;
};

export function TabPanel(props: TabPanelProps) {
  return (
    <div
      role="tabpanel"
      hidden={props.value !== props.index}
      id={`tabpanel-${props.index}`}
      aria-labelledby={`tab-${props.index}`}
      style={props.style}
    >
      {props.value === props.index && props.children}
    </div>
  );
}
