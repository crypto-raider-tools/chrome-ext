import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { colorPalette } from '../../../Helpers/Constants';
import { TabPanel } from './TabPanel';
import { DetailedDungeon } from '../../../interfaces/detailedDungeon';
import { DungeonList } from './DungeonList';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { StuffStats } from '../../../interfaces/stuffstats';
import { FighterBuild } from '@crypto-raider-tools/computation';
import { Mobs } from '../../../interfaces/mobs';

type DungeonTabsProps = {
  dungeonsMapByFilters: Record<string, DetailedDungeon[]>;
  raider: DetailedSyncRaider;
  stuffStats: StuffStats[];
  currentFighterBuild: FighterBuild;
  openDungeon: (dungeon: DetailedDungeon) => void;
  mobsData: Mobs[];
};

export const DungeonTabs = (props: DungeonTabsProps) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (__event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%', height: '100%', backgroundColor: colorPalette['darkBlueGrey'] }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider', width: '100%' }}>
        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
          {Object.keys(props.dungeonsMapByFilters).map((filter) => (
            <Tab
              key={filter}
              label={filter}
              sx={{
                color: 'white',
              }}
            />
          ))}
        </Tabs>
      </Box>
      {Object.keys(props.dungeonsMapByFilters).map((filter, index) => {
        return (
          <TabPanel
            key={filter}
            value={value}
            index={index}
            style={{ height: '100%', backgroundColor: colorPalette['darkBlueGrey'] }}
          >
            <DungeonList
              dungeonList={props.dungeonsMapByFilters[filter]}
              openDungeon={props.openDungeon}
              raider={props.raider}
              stuffStats={props.stuffStats}
              currentFighterBuild={props.currentFighterBuild}
              mobsData={props.mobsData}
            />
          </TabPanel>
        );
      })}
    </Box>
  );
};
