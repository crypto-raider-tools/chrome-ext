import { FighterBuild } from '@crypto-raider-tools/computation';
import { useCallback, useContext, useEffect, useState } from 'react';
import { apiEntrypoint, log } from '../../../config';
import { DetailedDungeon } from '../../../interfaces/detailedDungeon';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { DungeonStartMessage } from '../../../interfaces/dungeonMessage';
import { Position } from '../../../interfaces/dungeonSettings';
import { HydraPaginatedResponse } from '../../../interfaces/hydra';
import { MessagePassing } from '../../../interfaces/messagePassing';
import { Mobs } from '../../../interfaces/mobs';
import { StuffStats } from '../../../interfaces/stuffstats';
import DungeonSettingsContext from '../../Context/DungeonSettingsContext';
import { buildDungeonUrl } from './DungeonRaidGridHelpers';
import { DungeonSettings } from './DungeonSettings';
import { DungeonTabs } from './DungeonTabs';

type DungeonsPopUpProps = {
  onClick: () => void;
  raider: DetailedSyncRaider;
  stuffStats: StuffStats[];
  currentFighterBuild: FighterBuild;
  mobsData: Mobs[];
};

export const DungeonsPopUp = (props: DungeonsPopUpProps) => {
  const [keys, setKeys] = useState<number>(0);
  const [dungeonList, setDungeonList] = useState<undefined | HydraPaginatedResponse<DetailedDungeon>>();
  const [dungeonsMapByFilters, setDungeonsMapByFilters] = useState<Record<string, DetailedDungeon[]>>({});
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const { currentRunningRaids, setCurrentRunningRaids, lastWindowPosition, setLastWindowPosition, dungeonGridConfig } =
    useContext(DungeonSettingsContext);

  const asyncKeyStash = useCallback(async () => {
    try {
      const url = 'https://play.cryptoraiders.xyz/api/stash';
      const request = await fetch(url, {
        headers: {
          Accept: 'application/json',
          Cookie: document.cookie,
        },
      });
      if (request.status !== 200) {
        throw new Error(`${url}: ${request.status} ${request.statusText}`);
      }
      const data = await request.json();
      setKeys(data.products.GAME_DUNGEON_KEY_0001);
    } catch (error) {
      console.warn(error);
      alert('Unable to fetch for keys, sry 😿');
    }
  }, [setKeys]);

  useEffect(() => {
    setIsLoading(true);
    asyncKeyStash();
    const asyncCall = async () => {
      try {
        const url = `${apiEntrypoint}/dungeons?page=1&itemsPerPage=100`;
        const request = await fetch(url);
        if (request.status !== 200) {
          throw new Error(`${url}: ${request.status} ${request.statusText}`);
        }
        const dungeons: HydraPaginatedResponse<DetailedDungeon> = await request.json();
        setDungeonList(dungeons);
      } catch (error) {
        console.warn(error);
        alert('Unable to load dungeons, sry 😿');
      }
    };
    asyncCall();
  }, [asyncKeyStash]);

  useEffect(() => {
    const filterDungeons = () => {
      const dungeonsAct: string[] = [];
      const dungeonMap: Record<string, DetailedDungeon[]> = {};
      dungeonList?.['hydra:member'].forEach((dungeon) => {
        if (!dungeon.act) return;
        if (!dungeonsAct.includes(dungeon.act)) {
          dungeonsAct.push(dungeon.act);
          dungeonMap[dungeon.act] = [];
        }
        dungeonMap[dungeon.act].push(dungeon);
      });
      setDungeonsMapByFilters(dungeonMap);
    };
    filterDungeons();
    setIsLoading(typeof dungeonList === 'undefined');
  }, [dungeonList]);

  const initBackgroundRun = (message: DungeonStartMessage) => {
    chrome.runtime.sendMessage<MessagePassing>(
      {
        type: 'StartDungeon',
        data: { dungeonStartMessage: message },
      },
      (response: MessagePassing) => {
        log(response.type);
      }
    );
  };

  const openDungeon = async (dungeon: DetailedDungeon) => {
    try {
      let idx = dungeonGridConfig.config.nbRaidPerRun;
      let newPosition: Position = { ...lastWindowPosition };
      const newCurrentRunningRaids = [...currentRunningRaids];
      while (idx > 0) {
        const result = await buildDungeonUrl(props.raider, dungeon);
        if (typeof result.data.secret === 'undefined' || result.data.secret === '') {
          idx--;
          continue;
        }
        log(result.url);
        if (newPosition.y < dungeonGridConfig.config.column - 1) newPosition.y++;
        else if (newPosition.x < dungeonGridConfig.config.row - 1) {
          newPosition.y = 0;
          newPosition.x++;
        } else {
          newPosition.x = 0;
          newPosition.y = 0;
        }
        if (result.data.secret !== 'rickRolled') newCurrentRunningRaids.push(result.data);
        initBackgroundRun({
          url: result.url,
          secret: result.data.secret,
          position: newPosition,
          config: dungeonGridConfig,
        });
        idx--;
      }
      setCurrentRunningRaids(newCurrentRunningRaids);
      setLastWindowPosition(newPosition);
      setTimeout(asyncKeyStash, 1000);
    } catch (error) {
      console.warn(error);
      alert('Unable to open your dungeon, sry 😿');
    }
  };

  const popupContainerStyle: React.CSSProperties = {
    width: '650px',
    height: '600px',
    borderRadius: '2px',
    margin: '16px',
    display: 'flex',
    justifyContent: 'center',
  };

  return (
    <div
      style={{
        position: 'fixed',
        top: '0',
        left: '0',
        width: '100vw',
        height: '100vh',
        margin: '0',
        padding: '0',
        background: 'rgba(0,0,0,0.5)',
        zIndex: '9999',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onClick={() => {
        props.onClick();
      }}
    >
      {isLoading ? (
        <>Loading...</>
      ) : (
        <div
          style={popupContainerStyle}
          onClick={(event) => {
            event.stopPropagation();
          }}
        >
          <DungeonTabs
            dungeonsMapByFilters={dungeonsMapByFilters}
            openDungeon={openDungeon}
            raider={props.raider}
            stuffStats={props.stuffStats}
            currentFighterBuild={props.currentFighterBuild}
            mobsData={props.mobsData}
          />
          {window.screen.width > 700 && <DungeonSettings raider={props.raider} keyLeft={keys} />}
        </div>
      )}
    </div>
  );
};
