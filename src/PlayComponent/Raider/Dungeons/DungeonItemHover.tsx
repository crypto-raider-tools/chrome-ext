import { getMetaScoreOutputLarge, Rarity } from '@crypto-raider-tools/computation';
import { useEffect, useRef, useState } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { wordToCamelCase } from '../../../Helpers/Utils';
import { StuffStats } from '../../../interfaces/stuffstats';
import { DropRateData } from '../../Context/DropRateContext';

type DungeonItemHoverProps = {
  stuffStats?: StuffStats;
  hoverDOMRect?: DOMRect;
  style?: React.CSSProperties;
  dropRateData: DropRateData;
};

export const DungeonItemHover = (props: DungeonItemHoverProps) => {
  const [currentBounds, setCurrentBounds] = useState<DOMRect>();
  const [needBoundsUpdate, setNeedBoundsUpdate] = useState<boolean>();

  const divRef = useRef(null);
  useEffect(() => {
    if (needBoundsUpdate) {
      const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
      if (bounds) {
        setCurrentBounds(bounds);
        setNeedBoundsUpdate(false);
      }
    }
    // eslint-disable-next-line
  }, [needBoundsUpdate, setNeedBoundsUpdate, setCurrentBounds]);

  useEffect(() => {
    const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
    if (bounds && (bounds?.height !== currentBounds?.height || bounds?.width !== currentBounds?.width)) {
      setCurrentBounds(bounds);
    } else setNeedBoundsUpdate(true);
    // eslint-disable-next-line
  }, [setCurrentBounds, setNeedBoundsUpdate, props.hoverDOMRect]);
  if (!props.hoverDOMRect || !props.stuffStats) return null;
  const defaultStyle: React.CSSProperties = {
    position: 'absolute',
    pointerEvents: 'none',
    padding: '5px',
    backgroundColor: '#1a202cd5',
    borderRadius: '2px',
    zIndex: '10',
  };

  const overviewStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'space-between',
    textAlign: 'right',
  };

  const overviewIconStyle: React.CSSProperties = {
    width: '100px',
    height: '100px',
    transform: 'scale(1)',
    imageRendering: 'pixelated',
  };

  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    whiteSpace: 'nowrap',
    color: colorPalette['lightBlueGrey'],
  };

  let offsetTop = props.hoverDOMRect.height;
  if (window.screen.height - props.hoverDOMRect.bottom < (currentBounds?.height ?? 0) + offsetTop + 150) {
    offsetTop = (currentBounds?.height ?? 0) * -1;
  }
  let offsetLeft = 0.5 * (currentBounds?.width ?? 0) - props.hoverDOMRect.width / 2;
  if (window.screen.width - props.hoverDOMRect.right < offsetLeft) {
    offsetLeft -= offsetLeft - props.hoverDOMRect.width;
  }

  let primaryStatsSorted = [
    { name: 'strength', value: props.stuffStats?.strengthMax ?? 0 },
    { name: 'intelligence', value: props.stuffStats?.intelligenceMax ?? 0 },
    { name: 'agility', value: props.stuffStats?.agilityMax ?? 0 },
  ]
    .filter((p) => p.value > 0)
    .sort(function (e, t) {
      return t.value - e.value;
    });
  let dropRate = ((props.stuffStats.count ?? 0) / props.dropRateData.totalItems) * 100;
  if (props.stuffStats.slot === 'knickknack') {
    if (props.dropRateData.dungeon.difficulty === 'HEROIC') dropRate = 4;
    else dropRate = 2;
  }
  let rarityDropRate =
    (props.dropRateData.rarityCounts[props.stuffStats.rarity as Rarity] / props.dropRateData.totalItems) * 100;
  let itemInrarityDropRate =
    ((props.stuffStats.count ?? 0) / props.dropRateData.rarityCounts[props.stuffStats.rarity as Rarity]) * 100;

  return (
    <div
      ref={divRef}
      style={{
        ...defaultStyle,
        top: props.hoverDOMRect.y + offsetTop,
        left: props.hoverDOMRect.x - offsetLeft,
      }}
    >
      <div style={{ ...textStyle, color: 'white' }}>{props.stuffStats.name}</div>
      <div style={overviewStyle}>
        <img
          style={overviewIconStyle}
          alt={props.stuffStats.name}
          src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.stuffStats.internalName}_ICON.png`}
        />
        <div>
          <div style={textStyle}>
            Rarity:&nbsp;
            <span
              style={{
                color: colorPalette[(props.stuffStats.rarity ?? '').toLowerCase()],
              }}
            >
              {props.stuffStats.rarity}
            </span>
          </div>
          <div style={textStyle}>
            Max stats points:&nbsp;
            <span
              style={{
                color:
                  props.stuffStats.pointQtyMax && props.stuffStats.pointQtyMax > 0 ? 'white' : colorPalette['ruygye'],
              }}
            >
              {props.stuffStats.pointQtyMax && props.stuffStats.pointQtyMax > 0
                ? props.stuffStats.pointQtyMax
                : 'Missing Data'}
            </span>
          </div>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            {primaryStatsSorted.map((p) => (
              <div
                key={p.name + p.value}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  marginRight: '5px',
                  fontSize: '15px',
                  fontWeight: '500',
                  borderRadius: '2px',
                  backgroundColor: colorPalette[p.name],
                  height: '25px',
                  width: '45px',
                }}
              >
                {p.name.substring(0, 3).toUpperCase()}
              </div>
            ))}
          </div>
          <div style={textStyle}>
            Drop Rates:&nbsp;
            <span
              style={{
                color: colorPalette[getMetaScoreOutputLarge(dropRate * 2, 100).toLowerCase()],
              }}
            >
              {dropRate.toFixed(2)}
            </span>
            <span style={{ color: 'white' }}>%</span>
          </div>
          {props.stuffStats.slot !== 'knickknack' && (
            <>
              <div style={textStyle}>
                <span style={{ color: colorPalette[props.stuffStats.rarity ?? 'common'] }}>
                  {wordToCamelCase(props.stuffStats.rarity ?? '') + ' '}
                </span>
                Drop Rates:&nbsp;
                <span
                  style={{
                    color: colorPalette[getMetaScoreOutputLarge(rarityDropRate * 2, 100).toLowerCase()],
                  }}
                >
                  {rarityDropRate.toFixed(1)}
                </span>
                <span style={{ color: 'white' }}>%</span>
              </div>
              <div style={textStyle}>
                Inside
                <span style={{ color: colorPalette[props.stuffStats.rarity ?? 'common'] }}>
                  {' ' + wordToCamelCase(props.stuffStats.rarity ?? '') + ' '}
                </span>
                Drop Rates:&nbsp;
                <span
                  style={{
                    color: colorPalette[getMetaScoreOutputLarge(itemInrarityDropRate * 2, 100).toLowerCase()],
                  }}
                >
                  {itemInrarityDropRate.toFixed(1)}
                </span>
                <span style={{ color: 'white' }}>%</span>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};
