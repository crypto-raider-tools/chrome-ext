import { useContext } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { WindowSize } from '../../../interfaces/dungeonSettings';
import DungeonSettingsContext from '../../Context/DungeonSettingsContext';
import { ClickableContainer } from '../../Utils/ClickableContainer';
import { PlusMinusIncrementor } from '../../Utils/PlusMinusIncrementor';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import VolumeMuteIcon from '@mui/icons-material/VolumeMute';
import { isPremium } from '../../../Helpers/Utils';

type DungeonSettingsProps = {
  raider: DetailedSyncRaider;
  keyLeft: number;
};

export const DungeonSettings = (props: DungeonSettingsProps) => {
  const isCurrentlyPremium = isPremium();

  const {
    dungeonGridConfig,
    updateDungeonGridConfig,
    currentRunningRaids,
    selectedMonitor,
    setSelectedMonitor,
    numberMonitors,
  } = useContext(DungeonSettingsContext);

  const getCellSizeForRowAndColumn = (row: number, column: number): WindowSize => {
    let result: WindowSize = {
      width: Math.floor(dungeonGridConfig.displayInfo.workArea.width / column),
      height: Math.floor(dungeonGridConfig.displayInfo.workArea.height / row),
    };
    return result;
  };

  const getCurrentRaidsPerRun = (): number => {
    if (
      dungeonGridConfig.config.nbRaidPerRun + currentRunningRaids.length >
      dungeonGridConfig.config.row * dungeonGridConfig.config.column
    )
      return dungeonGridConfig.config.row * dungeonGridConfig.config.column;
    return dungeonGridConfig.config.nbRaidPerRun + currentRunningRaids.length;
  };

  const containerStyle: React.CSSProperties = {
    position: 'relative',
    width: '200px',
    height: 'fit-content',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'left',
    backgroundColor: colorPalette['darkBlueGrey'],
    borderTopRightRadius: '2px',
    borderBottomRightRadius: '2px',
  };

  const gridContainerDimension: { width: number; height: number } = {
    width: 200,
    height: (200 * 9) / 16,
  };

  const notPremiumStyle: React.CSSProperties = {
    pointerEvents: 'none',
    opacity: '0.5',
  };

  const gridContainerStyle: React.CSSProperties = {
    width: gridContainerDimension.width + 'px',
    height: gridContainerDimension.height + 'px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: colorPalette['mediumBlueGrey'],
    borderRadius: '2px',
    ...(isCurrentlyPremium ? {} : notPremiumStyle),
  };
  const gridElementStyle: React.CSSProperties = {
    width:
      Math.floor((dungeonGridConfig.cellSize.width / dungeonGridConfig.displayInfo.workArea.width) * 100).toFixed(2) +
      '%',
    height:
      Math.floor((dungeonGridConfig.cellSize.height / dungeonGridConfig.displayInfo.workArea.height) * 100).toFixed(2) +
      '%',
    padding: '5px',
  };
  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
  };
  const settingsContainerStyle: React.CSSProperties = {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    ...(isCurrentlyPremium ? {} : notPremiumStyle),
  };
  const settingStyle: React.CSSProperties = {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  };
  return (
    <div style={containerStyle}>
      <div style={{ position: 'absolute', top: '0px', right: '0px' }}>
        <ClickableContainer
          onClick={() => {
            localStorage.setItem('CRC_Dungeon_Grid_Config_Is_Mute_Tab', '' + !dungeonGridConfig.isMuteTab);
            updateDungeonGridConfig({
              ...dungeonGridConfig,
              isMuteTab: !dungeonGridConfig.isMuteTab,
            });
          }}
          boxShadows={[]}
        >
          {!dungeonGridConfig.isMuteTab ? <VolumeUpIcon /> : <VolumeMuteIcon />}
        </ClickableContainer>
      </div>
      <p style={textStyle}>
        <span style={{ color: colorPalette['lightBlueGrey'] }}>🗝️ Keys left: </span>
        {props.keyLeft}
      </p>
      <p style={textStyle}>
        <span style={{ color: colorPalette['lightBlueGrey'] }}>Missions left: </span>
        {props.raider.raidsRemaining}
      </p>
      <p style={textStyle}>
        <span style={{ color: colorPalette['lightBlueGrey'] }}>Endless left: </span>
        {props.raider.endlessRemaining}
      </p>
      <div style={settingsContainerStyle}>
        <div style={settingStyle}>
          <p style={{ ...textStyle, color: colorPalette['lightBlueGrey'] }}>Raids per run:</p>
          <PlusMinusIncrementor
            minValue={1}
            maxValue={10}
            value={dungeonGridConfig.config.nbRaidPerRun}
            setValue={(newValue) => {
              updateDungeonGridConfig({
                ...dungeonGridConfig,
                config: { ...dungeonGridConfig.config, nbRaidPerRun: newValue },
              });
              localStorage.setItem('CRC_Dungeon_Grid_Config_Raids_Per_Run', '' + newValue);
            }}
          />
        </div>
        <div style={{ ...settingStyle, ...(currentRunningRaids.length > 0 ? notPremiumStyle : {}) }}>
          <p style={{ ...textStyle, color: colorPalette['lightBlueGrey'] }}>Rows:</p>
          <PlusMinusIncrementor
            minValue={1}
            maxValue={2}
            value={dungeonGridConfig.config.row}
            setValue={(newValue) => {
              const cellDimensions: WindowSize = getCellSizeForRowAndColumn(newValue, dungeonGridConfig.config.column);
              updateDungeonGridConfig({
                ...dungeonGridConfig,
                cellSize: cellDimensions,
                config: { ...dungeonGridConfig.config, row: newValue },
              });
              localStorage.setItem('CRC_Dungeon_Grid_Config_Row', '' + newValue);
            }}
          />
        </div>
        <div style={{ ...settingStyle, ...(currentRunningRaids.length > 0 ? notPremiumStyle : {}) }}>
          <p style={{ ...textStyle, color: colorPalette['lightBlueGrey'] }}>Columns:</p>
          <PlusMinusIncrementor
            minValue={1}
            maxValue={6}
            value={dungeonGridConfig.config.column}
            setValue={(newValue) => {
              const cellDimensions: WindowSize = getCellSizeForRowAndColumn(dungeonGridConfig.config.row, newValue);
              updateDungeonGridConfig({
                ...dungeonGridConfig,
                cellSize: cellDimensions,
                config: { ...dungeonGridConfig.config, column: newValue },
              });
              localStorage.setItem('CRC_Dungeon_Grid_Config_Column', '' + newValue);
            }}
          />
        </div>
        {numberMonitors > 1 && (
          <div style={{ ...settingStyle, ...(currentRunningRaids.length > 0 ? notPremiumStyle : {}) }}>
            <p style={{ ...textStyle, color: colorPalette['lightBlueGrey'] }}>Monitor :</p>
            <PlusMinusIncrementor
              minValue={0}
              maxValue={numberMonitors - 1}
              value={selectedMonitor}
              setValue={(newValue) => {
                setSelectedMonitor(newValue);
                localStorage.setItem('CRC_Dungeon_Grid_Config_Selected_Monitor', '' + newValue);
              }}
            />
          </div>
        )}
      </div>
      <div style={gridContainerStyle}>
        {[...Array(getCurrentRaidsPerRun())].map((__x, i) => {
          return (
            <div key={i} style={gridElementStyle}>
              <div
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor:
                    i < currentRunningRaids.length ? colorPalette['moderate'] : colorPalette['lightBlueGrey'],
                  borderRadius: '2px',
                  display: 'flex',
                  alignContent: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {i < currentRunningRaids.length && (
                  <p style={{ fontSize: '12px', fontWeight: '500', transform: 'rotateZ(325deg)' }}>Running</p>
                )}
                {i >= currentRunningRaids.length + (props.raider.raidsRemaining ?? 0) && (
                  <p style={{ fontSize: '20px', fontWeight: '500' }}>🗝️</p>
                )}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
