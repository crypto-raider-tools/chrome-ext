import { FighterBuild } from '@crypto-raider-tools/computation';
import { DetailedDungeon } from '../../../interfaces/detailedDungeon';
import { DetailedMobs } from '../../../interfaces/detailedMobs';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { StuffStats } from '../../../interfaces/stuffstats';
import { Dungeon } from './Dungeon';

type DungeonListProps = {
  dungeonList: DetailedDungeon[];
  openDungeon: (dungeon: DetailedDungeon) => void;
  stuffStats: StuffStats[];
  currentFighterBuild: FighterBuild;
  raider: DetailedSyncRaider;
  mobsData: DetailedMobs[];
};

export const DungeonList = (props: DungeonListProps) => {
  const defaultStyle: React.CSSProperties = {
    width: '100%',
    height: '100%',
    overflow: 'auto',
  };
  return (
    <div style={defaultStyle}>
      {props.dungeonList.map((dungeon) => (
        <Dungeon
          key={dungeon['@id']}
          dungeon={dungeon}
          openDungeon={props.openDungeon}
          raider={props.raider}
          stuffStats={props.stuffStats}
          currentFighterBuild={props.currentFighterBuild}
          mobs={props.mobsData.filter(
            (m) =>
              typeof m.dungeons !== 'undefined' &&
              m.dungeons.length > 0 &&
              typeof m.dungeons.find((d) => d['@id'] === dungeon['@id']) !== 'undefined'
          )}
        />
      ))}
    </div>
  );
};
