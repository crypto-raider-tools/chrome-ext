import { useRef } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { StuffStats } from '../../../interfaces/stuffstats';

type DungeonItemProps = {
  stuffStats: StuffStats;
  onHover: (stuffStats: StuffStats, bounds: DOMRect) => void;
};

export const DungeonItem = (props: DungeonItemProps) => {
  const itemRef = useRef(null);

  const internalName = props.stuffStats?.internalName ?? '';
  let rarity = props.stuffStats?.rarity ?? 'common';

  if (props.stuffStats.slot === 'knickknack') rarity = 'rune';
  if (!internalName) return null;
  return (
    <img
      ref={itemRef}
      key={props.stuffStats['@id']}
      onMouseEnter={() => {
        if (itemRef.current) {
          const bounds = (itemRef.current as HTMLElement).getBoundingClientRect();
          props.onHover(props.stuffStats, bounds);
        }
      }}
      src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${internalName}_ICON.png`}
      alt={internalName}
      style={{
        width: '20px',
        height: '20px',
        objectFit: 'contain',
        imageRendering: 'pixelated',
        margin: '2px',
        filter: 'drop-shadow(-1px -1px 1px ' + colorPalette[rarity] + ')',
      }}
    />
  );
};
