import { createFighterBuild, FighterBuild, getMetaScoreOutputLarge } from '@crypto-raider-tools/computation';
import { colorPalette } from '../../../Helpers/Constants';
import { computeFightOutputEstimation } from '../../../Helpers/FightOutputEstimation';
import { DetailedMobs } from '../../../interfaces/detailedMobs';
import { MobIdlePicture } from '../../RaiderBuilder/MobIdlePicture';

type DungeonWinRateEstimationProps = {
  playerFighterBuild: FighterBuild;
  mobs: DetailedMobs[];
  style?: React.CSSProperties;
};

export const DungeonWinRateEstimation = (props: DungeonWinRateEstimationProps) => {
  const defaultStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'start',
    flexWrap: 'wrap',
    width: '100%',
    margin: '5px',
    textAlign: 'center',
    color: colorPalette['lightBlueGrey'],
    fontSize: '16px',
    ...props.style,
  };
  const winrateEnabled: boolean = Boolean(localStorage.getItem('winrateEnabled'));

  return !winrateEnabled ? null : (
    <div style={defaultStyle}>
      <p>Estimated Winrate:</p>
      {Object.values(props.mobs).map((mob, index) => {
        const mobFighterBuild = createFighterBuild();
        for (const fighterBuildKey of Object.keys(mobFighterBuild)) {
          mobFighterBuild[fighterBuildKey as keyof FighterBuild] = parseFloat(
            mob[fighterBuildKey as keyof DetailedMobs] as string
          );
        }
        const winrate = computeFightOutputEstimation(props.playerFighterBuild, mobFighterBuild).winRate;
        return (
          <div key={'Mob:' + index} style={{ display: 'flex', alignContent: 'center' }}>
            <MobIdlePicture
              mob={mob}
              pictureSize={30}
              scaleMultiplier={1.25}
              style={{ marginRight: '5px', marginLeft: '5px' }}
            />
            <span
              style={{ color: colorPalette[getMetaScoreOutputLarge(winrate, 100).toLowerCase()], fontWeight: '500' }}
            >
              {winrate.toFixed(1)}
            </span>
            <span>%</span>
          </div>
        );
      })}
    </div>
  );
};
