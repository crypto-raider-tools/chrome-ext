import { FighterBuild, FighterStats, Item } from '@crypto-raider-tools/computation';
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
  CoreChartOptions,
  ElementChartOptions,
  PluginChartOptions,
  DatasetChartOptions,
  ScaleChartOptions,
  LineControllerChartOptions,
} from 'chart.js';
import { _DeepPartialObject } from 'chart.js/types/utils';
import { Radar } from 'react-chartjs-2';
import { FighterBuildNameToText } from '../../Helpers/Maps';
import { fighterBuildValueToString, getDataSetForFighterBuild } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { StuffStats } from '../../interfaces/stuffstats';

ChartJS.register(RadialLinearScale, PointElement, LineElement, Filler, Tooltip, Legend);

type RaiderGraphProps = {
  fighterBuild: FighterBuild;
  fighterStats: FighterStats;
  style?: React.CSSProperties;
  scaleMultiplier?: number;
  onClick?: () => void;
  cacheBuildCap: CacheBuildCap;
  stuffStats: StuffStats[];
  currentMaxMeta: number;
  compareBuild?: FighterBuild;
};

export function RaiderGraph(props: RaiderGraphProps) {
  let compareBuild: FighterBuild = { ...props.fighterBuild };
  let currentItem: Item | undefined = undefined;

  const data = {
    labels: [...Object.keys(compareBuild).map((build) => FighterBuildNameToText[build as keyof FighterBuild])],
    datasets: [
      {
        label: 'Current Build',
        data: getDataSetForFighterBuild(props.fighterBuild, props.cacheBuildCap[props.fighterStats.level]),
        backgroundColor: 'rgba(9, 89, 209, 0.8)',
        borderColor: 'rgba(55, 121, 182, 1)',
        borderWidth: 1,
      },
    ],
  };

  if (props.compareBuild) {
    data.datasets = [
      {
        label: 'New Build',
        data: getDataSetForFighterBuild(props.compareBuild, props.cacheBuildCap[props.fighterStats.level]),
        backgroundColor: 'rgba(0, 0, 0, 0)',
        borderColor: '#e7862c',
        borderWidth: 2,
      },
      data.datasets[0],
    ];
  }

  const options: _DeepPartialObject<
    CoreChartOptions<'radar'> &
      ElementChartOptions<'radar'> &
      PluginChartOptions<'radar'> &
      DatasetChartOptions<'radar'> &
      ScaleChartOptions<'radar'> &
      LineControllerChartOptions
  > = {
    responsive: true,
    elements: {
      line: {
        borderColor: '#fff',
        backgroundColor: '#fff',
      },
    },
    scales: {
      r: {
        min: 0,
        max: 100,
        grid: {
          color: 'rgb(0, 0, 0, 0)',
          tickLength: 5,
        },
        angleLines: {
          color: 'rgb(0, 0, 0, 0)',
        },
        pointLabels: {
          color: 'white',
        },
        ticks: {
          display: false,
        },
      },
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: true,
        callbacks: {
          title: (tooltipItems) => {
            const toolTipItem: any = tooltipItems[0];
            const isCurrentBuild = !currentItem || (currentItem && toolTipItem.datasetIndex === 1);
            return toolTipItem.label + ' : ' + (isCurrentBuild ? 'Current Build' : 'New Build');
          },
          label: (tooltipItem) => {
            const build: FighterBuild = tooltipItem.datasetIndex === 0 ? compareBuild : props.fighterBuild;
            return fighterBuildValueToString(tooltipItem.label, build);
          },
        },
      },
    },
  };

  const containerStyle: React.CSSProperties = {
    position: 'relative',
    pointerEvents: 'none',
    ...props.style,
  };

  const radarStyle: React.CSSProperties = {
    position: 'absolute',
    top: '-30px',
    width: '300px',
  };

  return (
    <div style={containerStyle}>
      <div style={radarStyle}>
        <Radar data={data} options={options} />
      </div>
    </div>
  );
}
