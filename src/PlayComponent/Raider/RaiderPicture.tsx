import { InventoryItem, RaiderApiData } from '@crypto-raider-tools/computation';
import { useContext } from 'react';
import { TammyAvatar } from '../../interfaces/tammyAvatar';
import TammyAvatarContext from '../Context/TammyAvatarContext';

type RaiderCharacterProps = {
  raider: RaiderApiData | undefined;
  style?: React.CSSProperties;
  pictureSize?: number;
  onClick?: () => void;
};

export function RaiderPicture(props: RaiderCharacterProps) {
  const { tammyAvatarTrigger } = useContext(TammyAvatarContext);

  let dress: string | null = null;
  let weapon: string | null = null;
  let background: string = 'DEFAULT';
  let pictureSize: number = props.pictureSize ?? 224;
  let transformScale: number = pictureSize / 224;
  let genrationTransformScale: number = 224 / 350;
  let finger: string | null = null;
  let neck: string | null = null;
  let rune: string | null = null;
  let helmet: string | null = null;
  const equippedItems = props.raider?.inventory?.filter((item: any) => item.equipped === true);
  if (equippedItems) {
    equippedItems.forEach((equipped: InventoryItem) => {
      switch (equipped.item.slot) {
        case 'main_hand':
          weapon = equipped.item.internalName;
          break;
        case 'dress':
          dress = equipped.item.internalName;
          break;
        case 'background':
          background = equipped.item.internalName;
          break;
        case 'knickknack':
          rune = equipped.item.internalName;
          break;
        case 'finger':
          finger = equipped.item.internalName;
          break;
        case 'neck':
          neck = equipped.item.internalName;
          break;
        case 'helmet':
          helmet = equipped.item.internalName;
          break;
        default:
          break;
      }
    });
  }

  const itemStyle: React.CSSProperties = {
    width: 50 * transformScale + 'px',
    height: 50 * transformScale + 'px',
    imageRendering: 'pixelated',
    position: 'absolute',
    zIndex: '6',
  };

  return (
    <div
      style={{
        ...props.style,
        height: pictureSize + 'px',
        width: pictureSize + 'px',
        position: 'relative',
      }}
      onClick={props.onClick}
    >
      <div
        style={{
          backgroundSize: 'cover',
          backgroundImage:
            'url("https://storage.googleapis.com/crypto-raiders-assets/cosmetic/backgrounds/' + background + '.png")',
          width: '100%',
          height: '100%',
        }}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative',
            width: '100%',
            height: '100%',
            transform: 'scale(' + transformScale + ')',
          }}
        >
          <div
            style={{
              position: 'absolute',
              width: '350px',
              height: '350px',
              zIndex: '5',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/interface/gen' +
                (props.raider?.generation ?? 1) +
                '_border.png)',
              transform: 'scale(' + genrationTransformScale + ')',
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/body/' +
                (tammyAvatarTrigger.body || !props.raider?.avatar?.body ? TammyAvatar.body : props.raider.avatar.body) +
                '.png)',
            }}
          />
          {dress ? (
            <div
              className="raider-image-animated"
              style={{
                height: '96px',
                backgroundImage:
                  'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/dress/' + dress + '.png)',
              }}
            />
          ) : null}
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/face/' +
                (tammyAvatarTrigger.face || !props.raider?.avatar?.face ? TammyAvatar.face : props.raider.avatar.face) +
                '.png)',
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/hair/' +
                (tammyAvatarTrigger.hair || !props.raider?.avatar?.hair ? TammyAvatar.hair : props.raider.avatar.hair) +
                '.png)',
            }}
          />
          {weapon ? (
            <div
              className="raider-image-animated"
              style={{
                height: '96px',
                backgroundImage: 'url(https://crypto-raiders-assets.storage.googleapis.com/items/' + weapon + '.png)',
              }}
            />
          ) : null}
        </div>
      </div>
      <div style={{ ...itemStyle, bottom: '5px', left: '5px' }}>
        {finger ? (
          <img
            src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${finger}_ICON.png`}
            alt={finger}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
            }}
          />
        ) : null}
      </div>
      <div style={{ ...itemStyle, top: '5px', right: '5px' }}>
        {neck ? (
          <img
            src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${neck}_ICON.png`}
            alt={neck}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
            }}
          />
        ) : null}
      </div>
      <div style={{ ...itemStyle, bottom: '5px', right: '5px' }}>
        {rune ? (
          <img
            src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${rune}_ICON.png`}
            alt={rune}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              transform: 'scale(1.25)',
            }}
          />
        ) : null}
      </div>
      <div style={{ ...itemStyle, top: '5px', width: '100%' }}>
        {helmet ? (
          <img
            src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${helmet}_ICON.png`}
            alt={helmet}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              transform: 'scale(1.25)',
            }}
          />
        ) : null}
      </div>
    </div>
  );
}
