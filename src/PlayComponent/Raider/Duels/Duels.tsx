import { FighterBuild, FighterStats, fixStats, RaiderApiData, Rarity } from '@crypto-raider-tools/computation';
import { useState } from 'react';
import { apiEntrypoint, log } from '../../../config';
import { colorPalette } from '../../../Helpers/Constants';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { MessagePassing } from '../../../interfaces/messagePassing';
import { StuffStats } from '../../../interfaces/stuffstats';
import { CompareRaiderBuild } from '../../RaiderBuilder/CompareRaiderBuild';
import { RaiderSearchTextField } from '../../RaiderSearch/RaiderSearchTextField';
import { H2 } from '../../Theme';
import { ClickableContainer } from '../../Utils/ClickableContainer';
import { buildDuelsUrl } from '../Dungeons/DungeonRaidGridHelpers';

type DuelsProps = {
  raider: DetailedSyncRaider;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  style?: React.CSSProperties;
  currentFighterStats: FighterStats;
  currentFighterBuild: FighterBuild;
};

export const Duels = (props: DuelsProps) => {
  const [searchValue, setSearchValue] = useState<string>('');
  const [targetRaiderData, setTargetRaiderData] = useState<RaiderApiData>();

  const defaultStyle: React.CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    borderRadius: '2px',
    padding: '30px',
    ...props.style,
  };

  const containerStyle: React.CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    width: 'fit-content',
    height: '180px',
    borderRadius: '2px',
    padding: '2px 5px',
    marginBottom: '8px',
    position: 'relative',
  };

  const idSelectorContainer: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  };

  const searchForRaider = async (id: string) => {
    const request = await fetch(`${apiEntrypoint}/other-raider/${id}`, {
      headers: {
        'x-original-token': document.cookie,
      },
    });
    if (request.status !== 200) {
      log(`${'/other-raider'}: ${request.status} ${request.statusText}`);
      return Promise.reject('Unable to get raider stats for this id, sry 😿');
    }
    let raiderData: RaiderApiData = await request.json();
    raiderData.stats = fixStats(raiderData.stats);
    raiderData.inventory?.forEach((object, index) => {
      if (!raiderData?.inventory) return;
      raiderData.inventory[index].item.rarity =
        (props.stuffStats.find((i) => i.internalName === object.item.internalName)?.rarity as Rarity) ?? 'common';
    });
    setTargetRaiderData(raiderData);
    return Promise.resolve();
  };

  const btnStyle: React.CSSProperties = {
    borderRadius: '2px',
    width: '100px',
    textAlign: 'center',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    marginTop: '4px',
  };

  return (
    <div
      style={defaultStyle}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <div style={containerStyle}>
        <div style={idSelectorContainer}>
          <H2>Time to dududu duel!</H2>
          <RaiderSearchTextField
            name={'Raider ID'}
            defaultVariable={''}
            handleInputUpdate={function (value: string | undefined): void {
              setSearchValue(value ?? '');
            }}
          />
          <ClickableContainer
            onClick={async () => {
              await searchForRaider(searchValue);
            }}
            style={btnStyle}
          >
            Inspect
          </ClickableContainer>
          <ClickableContainer
            onClick={async () => {
              const url = await buildDuelsUrl(props.raider, searchValue);
              if (url !== '') {
                chrome.runtime.sendMessage<MessagePassing>(
                  {
                    type: 'StartDuel',
                    data: { duelStartMessage: url },
                  },
                  (response: MessagePassing) => {
                    log(response.type);
                  }
                );
              }
            }}
            style={btnStyle}
          >
            Start Duel!
          </ClickableContainer>
        </div>
        {targetRaiderData && (
          <div
            style={{
              position: 'absolute',
              width: '370px',
              ...(window.screen.width < 1010 ? { bottom: '-200px', right: '-80px' } : { top: '0px', right: '-400px' }),
            }}
          >
            <CompareRaiderBuild
              currentMaxMeta={props.currentMaxMeta}
              stuffStats={props.stuffStats}
              raiderData={targetRaiderData}
              onRemove={(): void => {
                setTargetRaiderData(undefined);
              }}
              isMainBuild={false}
              currentFighterBuild={props.currentFighterBuild}
              currentFighterStats={props.currentFighterStats}
              isCollapsed={true}
            />
          </div>
        )}
      </div>
    </div>
  );
};
