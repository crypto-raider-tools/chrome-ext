import {
  BuildResult,
  BuildType,
  createFighterBuild,
  FighterBuild,
  FighterStats,
  fixStats,
  Stats,
  computeRaiderBuild,
  Abilities,
} from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { getMaxAbilityNumberForRaiderLevel } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import { Mobs } from '../../interfaces/mobs';
import { ItemPureness } from '../../interfaces/pureness';
import { RaiderAbility } from '../../interfaces/raiderability';
import { StuffStats } from '../../interfaces/stuffstats';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { DungeonsPopUp } from './Dungeons/DungeonsPopUp';
import { AbilityOverview } from './RaiderAbilities/AbilityOverview';
import { RaiderAbilitiesSelector } from './RaiderAbilities/RaiderAbilitiesSelector';
import { RaiderAdvancedStats } from './RaiderAdvancedStats';
import { RaiderBaseStats } from './RaiderBaseStats';
import { RaiderGraph } from './RaiderGraph';
import { InventoryItemOverview } from './RaiderInventory/RaiderInventoryOverview/InventoryItemOverview';
import { RaiderInventoryParent } from './RaiderInventory/RaiderInventoryParent';
import { RaiderPicture } from './RaiderPicture';
import { RaiderResume } from './RaiderResume';

type RaiderProps = {
  raider: DetailedSyncRaider;
  style?: React.CSSProperties;
  onPictureClick: () => void;
  isExpanded: boolean;
  handleUpdateRaiderServerSide: (
    raider: DetailedSyncRaider,
    objects?: Array<IndexedInventoryItem>,
    raiderStats?: Stats
  ) => Promise<void>;
  localInventory: Array<IndexedInventoryItem>;
  setLocalInventory: (localInventory: Array<IndexedInventoryItem>) => void;
  setHoverItem: (item: IndexedInventoryItem | undefined) => void;
  fighterBuild: FighterBuild;
  fighterStats: FighterStats;
  hoverItem?: IndexedInventoryItem;
  currentRaiderBuild: BuildResult;
  handleLocalEquipItem: (item: IndexedInventoryItem) => void;
  equippedItems: Record<number, IndexedInventoryItem>;
  inventoryComputedBuilds: BuildResult[];
  inventoryComputedPureness: ItemPureness[];
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  currentClassRecommentation: BuildType;
  setCurrentClassRecommentation: (inventory: BuildType) => void;
  cacheBuildCap: CacheBuildCap;
  inventoryRecommendationsScore: number[];
  updateFighterStats: (value: FighterStats) => void;
  mobsData: Mobs[];
  raiderAbilitiesData: RaiderAbility[];
};

export const Raider = (props: RaiderProps) => {
  const [isInventoryOpen, setIsInventoryOpen] = useState<boolean>(false);
  const [isDungeonsOpen, setIsDungeonsOpen] = useState<boolean>(false);
  const [hoverItemFighterBuild, setHoverItemFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [hoverAbility, setHoverAbility] = useState<{ ability: RaiderAbility; bounds: DOMRect } | undefined>();

  const pictureSize: number = 224;
  const rawData = props.raider.raw_data;
  const parentContainer: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    background: '#2D3748',
    margin: '.25em',
    transition: '0.5s',
    ...(props.style ? props.style : {}),
    ...(props.isExpanded ? { width: '100%' } : {}),
  };
  const mainContainerStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    padding: '.5em',
  };
  const cellStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
    marginRight: '.5em',
  };

  useEffect(() => {
    if (props.hoverItem) {
      let compareBuild: FighterBuild = { ...props.fighterBuild };
      const currentItem = { ...props.hoverItem.item };
      currentItem.stats = fixStats(currentItem?.stats);
      try {
        switch (currentItem.slot) {
          case 'main_hand':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, mainHand: currentItem });
            break;
          case 'dress':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, dress: currentItem });
            break;
          case 'knickknack':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, knickknack: currentItem });
            break;
          case 'finger':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, finger: currentItem });
            break;
          case 'neck':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, neck: currentItem });
            break;
          case 'helmet':
            compareBuild = computeRaiderBuild({ ...props.fighterStats, helmet: currentItem });
            break;
        }
      } catch (error) {
        compareBuild = {
          maxHP: 0,
          minDamage: 0,
          maxDamage: 0,
          hitChance: 0,
          hitFirst: 0,
          meleeCrit: 0,
          critDamageMultiplier: 0,
          critResist: 0,
          evadeChance: 0,
          meleeResist: 0,
        };
      }
      setHoverItemFighterBuild(compareBuild);
    }
    // eslint-disable-next-line
  }, [props.hoverItem]);

  return (
    <div style={parentContainer}>
      <div style={mainContainerStyle}>
        <ClickableContainer onClick={props.onPictureClick} style={cellStyle}>
          <RaiderPicture
            raider={rawData}
            style={{
              justifyContent: 'space-evenly',
              alignItems: 'center',
            }}
            pictureSize={pictureSize}
          />
        </ClickableContainer>
        <RaiderResume
          raider={props.raider}
          style={{ ...cellStyle }}
          setIsInventoryOpen={setIsInventoryOpen}
          isInventoryOpen={isInventoryOpen}
          setIsDungeonsOpen={setIsDungeonsOpen}
          isDungeonsOpen={isDungeonsOpen}
          fighterStats={props.fighterStats}
          currentMaxMeta={props.currentMaxMeta}
          stuffStats={props.stuffStats}
          fighterBuild={props.fighterBuild}
          handleUpdateRaiderName={(name: string): Promise<void> => {
            return props.handleUpdateRaiderServerSide({
              ...props.raider,
              raw_data: { ...props.raider.raw_data!, name: name },
            });
          }}
        />
        {props.isExpanded || isInventoryOpen ? (
          <>
            <RaiderBaseStats
              raider={props.raider}
              equippedItems={props.equippedItems}
              style={{ ...cellStyle, minWidth: 175 }}
              handleUpdateRaiderServerSide={props.handleUpdateRaiderServerSide}
              fighterStats={props.fighterStats}
              updateFighterStats={props.updateFighterStats}
            />
            <RaiderAdvancedStats
              fighterStats={props.fighterStats}
              style={{ ...cellStyle, minWidth: 175 }}
              currentRaiderBuild={props.currentRaiderBuild}
              cacheBuildCap={props.cacheBuildCap}
              compareBuild={props.hoverItem ? hoverItemFighterBuild : undefined}
            />
            <RaiderGraph
              fighterBuild={props.fighterBuild}
              fighterStats={props.fighterStats}
              cacheBuildCap={props.cacheBuildCap}
              stuffStats={props.stuffStats}
              currentMaxMeta={props.currentMaxMeta}
              style={{
                margin: '0px 60px 0px 8px',
                width: '250px',
                height: '220px',
              }}
              compareBuild={props.hoverItem ? hoverItemFighterBuild : undefined}
            />
            {isInventoryOpen && props.hoverItem && (
              <InventoryItemOverview
                hoverItem={props.hoverItem}
                equippedItems={props.equippedItems}
                inventoryComputedPureness={props.inventoryComputedPureness}
                currentMaxMeta={props.currentMaxMeta}
                stuffStats={props.stuffStats.find((s) => s.internalName === props.hoverItem!.item.internalName)!}
              />
            )}
            {isInventoryOpen && hoverAbility && (
              <AbilityOverview hoverAbility={hoverAbility.ability} bounds={hoverAbility.bounds} />
            )}
            {isInventoryOpen && (
              <RaiderInventoryParent
                raider={props.raider}
                style={cellStyle}
                handleUpdateRaiderServerSide={props.handleUpdateRaiderServerSide}
                localInventory={props.localInventory}
                setLocalInventory={props.setLocalInventory}
                setHoverItem={props.setHoverItem}
                handleLocalEquipItem={props.handleLocalEquipItem}
                inventoryComputedBuilds={props.inventoryComputedBuilds}
                equippedItems={props.equippedItems}
                currentMaxMeta={props.currentMaxMeta}
                stuffStats={props.stuffStats}
                currentRaiderClass={props.currentRaiderBuild.currentClass}
                currentClassRecommentation={'none'}
                setCurrentClassRecommentation={props.setCurrentClassRecommentation}
                inventoryComputedPureness={props.inventoryComputedPureness}
                inventoryRecommendationsScore={props.inventoryRecommendationsScore}
              />
            )}
            {isInventoryOpen && (
              <RaiderAbilitiesSelector
                abilities={props.raider.raw_data?.abilities ?? []}
                raiderAbilities={props.raiderAbilitiesData}
                handleUpdateAbilities={(abilities: Abilities) => {
                  return props.handleUpdateRaiderServerSide({
                    ...props.raider,
                    raw_data: {
                      ...props.raider.raw_data!,
                      abilities: props.raider.raw_data!.abilities.map((a) => {
                        return {
                          ...a,
                          equipped: abilities.find((nA) => nA.ability === a.ability)?.equipped ?? false,
                        };
                      }),
                    },
                  });
                }}
                setHoverAbility={(ability: RaiderAbility | undefined, bounds: DOMRect | undefined) => {
                  if (ability && bounds) setHoverAbility({ ability, bounds });
                  else setHoverAbility(undefined);
                }}
                maxEquippedNumber={getMaxAbilityNumberForRaiderLevel(props.raider.level ?? 1)}
              />
            )}
          </>
        ) : null}
        {isDungeonsOpen && (
          <DungeonsPopUp
            onClick={() => {
              setIsDungeonsOpen(!isDungeonsOpen);
            }}
            raider={props.raider}
            stuffStats={props.stuffStats}
            currentFighterBuild={props.fighterBuild}
            mobsData={props.mobsData}
          />
        )}
      </div>
    </div>
  );
};
