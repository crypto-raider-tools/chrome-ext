import { FighterStats, Stats } from '@crypto-raider-tools/computation';
import { useContext, useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import {
  extractDetailedRaiderAvatar,
  extractDetailedRaiderStats,
  getTotalFighterStatsPointCount,
  wordToCamelCase,
} from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import LoadingContext from '../Context/LoadingContext';
import { ClickableContainer, BoxShadow } from '../Utils/ClickableContainer';
import { PlusMinusIncrementor } from '../Utils/PlusMinusIncrementor';
import { RaiderAvatarPicture } from './RaiderAvatarPicture';

type RaiderBaseStatsType = {
  raider: DetailedSyncRaider;
  equippedItems: Record<number, IndexedInventoryItem>;
  handleUpdateRaiderServerSide: (
    raider: DetailedSyncRaider,
    objects: Array<IndexedInventoryItem>,
    raiderStats?: Stats
  ) => Promise<void>;
  fighterStats: FighterStats;
  updateFighterStats: (value: FighterStats) => void;
  style?: React.CSSProperties;
};

export const RaiderBaseStats = (props: RaiderBaseStatsType) => {
  const [canUpdateStats, setCanUpdateStats] = useState<boolean>(false);
  const [statsPointLeft, setStatsPointLeft] = useState<number>(0);
  const [baseRaiderStatsCount, setBaseRaiderStatsCount] = useState<number>(0);

  const { isLoading } = useContext(LoadingContext);

  useEffect(() => {
    const raiderMaxPoint = ((props.raider.level ?? 1) - 1) * 7;
    let baseStatsPointsCount = 0;
    const raiderStats: Stats = extractDetailedRaiderStats(props.raider);
    Object.values(raiderStats).forEach((stats) => {
      baseStatsPointsCount += stats;
    });

    let currentStatsPointsCount = 0;
    Object.values(props.fighterStats.stats).forEach((stats) => {
      currentStatsPointsCount += stats;
    });
    if (currentStatsPointsCount === 0) currentStatsPointsCount = baseStatsPointsCount;
    if (currentStatsPointsCount !== raiderMaxPoint) {
      setStatsPointLeft(raiderMaxPoint - currentStatsPointsCount);
    }
    setCanUpdateStats(currentStatsPointsCount !== baseStatsPointsCount && props.raider.questStatus === 1);
    setBaseRaiderStatsCount(baseStatsPointsCount);
  }, [props.fighterStats, props.raider]);

  const defaultStyle: React.CSSProperties = {
    paddingTop: '8px',
    ...props.style,
  };
  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    textAlign: 'center',
  };
  const buttonStyle: React.CSSProperties = {
    ...textStyle,
    borderRadius: '2px',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    marginLeft: '100px',
    width: '80px',
  };

  const disableStyle: React.CSSProperties = {
    pointerEvents: 'none',
    opacity: '0.2',
  };
  const cellStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  };
  const nameStyle: React.CSSProperties = {
    minWidth: '100px',
    width: 'calc(1/7 * 100%)',
    fontWeight: '500',
  };
  const statStyle: React.CSSProperties = {
    width: '40px',
    textAlign: 'center',
  };
  const iconStyle: React.CSSProperties = {
    width: '40px',
    height: '40px',
    imageRendering: 'pixelated',
  };
  const baseProps = {
    style: cellStyle,
    nameStyle,
    statStyle,
  };
  return (
    <div style={defaultStyle}>
      <div style={cellStyle}>
        <div style={nameStyle}></div>
        <div style={{ width: '80px', display: 'flex', justifyContent: 'center' }}>
          <RaiderAvatarPicture
            avatar={extractDetailedRaiderAvatar(props.raider)}
            showBackground={false}
            pictureSize={40}
            scaleMultiplier={1.5}
            style={{ transform: 'translate(0px, -5px)' }}
          />
        </div>
        <div>
          {props.fighterStats.mainHand ? (
            <img
              style={iconStyle}
              alt={props.fighterStats.mainHand.name}
              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.fighterStats.mainHand.internalName}_ICON.png`}
            />
          ) : (
            <div style={iconStyle} />
          )}
        </div>
        <div>
          {props.fighterStats.dress ? (
            <img
              style={iconStyle}
              alt={props.fighterStats.dress.name}
              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.fighterStats.dress.internalName}_ICON.png`}
            />
          ) : (
            <div style={iconStyle} />
          )}
        </div>
        <div>
          {props.fighterStats.finger ? (
            <img
              style={iconStyle}
              alt={props.fighterStats.finger.name}
              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.fighterStats.finger.internalName}_ICON.png`}
            />
          ) : (
            <div style={iconStyle} />
          )}
        </div>
        <div>
          {props.fighterStats.neck ? (
            <img
              style={iconStyle}
              alt={props.fighterStats.neck.name}
              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.fighterStats.neck.internalName}_ICON.png`}
            />
          ) : (
            <div style={iconStyle} />
          )}
        </div>
        <div>
          {props.fighterStats.helmet ? (
            <img
              style={iconStyle}
              alt={props.fighterStats.helmet.name}
              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${props.fighterStats.helmet.internalName}_ICON.png`}
            />
          ) : (
            <div style={iconStyle} />
          )}
        </div>
        <div style={statStyle}></div>
      </div>
      {Object.keys(props.fighterStats.stats).map((key) => {
        const statName = key as keyof Stats;
        return (
          <StatLine
            key={key}
            raiderBase={props.raider[statName] ?? 0}
            raiderPointLeft={statsPointLeft}
            updatePoints={(value: number) => {
              const newStats = { ...props.fighterStats.stats };
              newStats[statName] = value;
              const raiderMaxPoint = ((props.raider.level ?? 1) - 1) * 7;
              let totalStats = 0;
              Object.values(newStats).forEach((stats) => {
                totalStats += stats;
              });
              setStatsPointLeft(raiderMaxPoint - totalStats);
              props.updateFighterStats({ ...props.fighterStats, stats: newStats });
              console.log(totalStats, baseRaiderStatsCount);
              setCanUpdateStats(totalStats !== baseRaiderStatsCount);
            }}
            finger={props.fighterStats?.finger?.stats[statName] ?? 0}
            neck={props.fighterStats?.neck?.stats[statName] ?? 0}
            helmet={props.fighterStats?.helmet?.stats[statName] ?? 0}
            name={wordToCamelCase(statName)}
            raider={props.fighterStats.stats[statName] ?? 0}
            mainHand={props.fighterStats?.mainHand?.stats[statName] ?? 0}
            dress={props.fighterStats?.dress?.stats[statName] ?? 0}
            {...baseProps}
            nameStyle={nameStyle}
            selectorStyle={{ ...(!isLoading ? {} : disableStyle) }}
          />
        );
      })}
      <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
        <ClickableContainer
          boxShadows={[
            BoxShadow(colorPalette['lightBlueGrey'], { x: 4, y: 4 }),
            BoxShadow(colorPalette['mediumBlue'], { x: -4, y: -4 }),
          ]}
          onClick={async (): Promise<void> => {
            await props.handleUpdateRaiderServerSide(
              props.raider,
              Object.values(props.equippedItems),
              props.fighterStats.stats
            );
          }}
          style={{ ...buttonStyle, ...(!isLoading && canUpdateStats ? {} : disableStyle) }}
        >
          <p style={textStyle}>{'Apply'}</p>
        </ClickableContainer>
        <div style={{ fontWeight: '500' }}>
          <span style={{ color: colorPalette['lightBlueGrey'] }}>{'Total: '}</span>
          <span>{getTotalFighterStatsPointCount(props.fighterStats)}</span>
        </div>
      </div>
    </div>
  );
};

type StatLineType = {
  name: string;
  raiderBase: number;
  raiderPointLeft: number;
  raider: number;
  updatePoints: (value: number) => void;
  mainHand: number;
  dress: number;
  finger: number;
  neck: number;
  helmet: number;
  style?: React.CSSProperties;
  nameStyle?: React.CSSProperties;
  selectorStyle?: React.CSSProperties;
  statStyle?: React.CSSProperties;
};

const StatLine = (props: StatLineType) => {
  const total = props.raider + props.mainHand + props.dress + props.finger + props.neck + props.helmet;

  return (
    <div style={props.style}>
      <div style={props.nameStyle}>{props.name}</div>
      <PlusMinusIncrementor
        minValue={props.raiderBase}
        maxValue={props.raider + props.raiderPointLeft}
        value={props.raider}
        setValue={(value) => {
          props.updatePoints(value);
        }}
        style={props.selectorStyle}
      />
      <div
        style={{
          ...props.statStyle,
          ...(props.mainHand > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {props.mainHand}
      </div>
      <div
        style={{
          ...props.statStyle,
          ...(props.dress > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {props.dress}
      </div>
      <div
        style={{
          ...props.statStyle,
          ...(props.finger > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {props.finger}
      </div>
      <div
        style={{
          ...props.statStyle,
          ...(props.neck > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {props.neck}
      </div>
      <div
        style={{
          ...props.statStyle,
          ...(props.helmet > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {props.helmet}
      </div>
      <div
        style={{
          ...props.statStyle,
          ...(total > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
        }}
      >
        {total}
      </div>
    </div>
  );
};
