import { useContext, useEffect, useState } from 'react';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { IndexedInventoryItem } from '../../../interfaces/indexedInventoryItem';
import { RaiderInventoryItem } from './RaiderInventoryItem';
import { Grid } from '@mui/material';
import { StuffStats } from '../../../interfaces/stuffstats';
import { BuildResult, BuildType, Stats } from '@crypto-raider-tools/computation';
import { RaiderInventorySelect } from './RaiderInventoryFilters/RaiderInventorySelect';
import { BoxShadow, ClickableContainer } from '../../Utils/ClickableContainer';
import { colorPalette } from '../../../Helpers/Constants';
import { ItemPureness } from '../../../interfaces/pureness';
import LoadingContext from '../../Context/LoadingContext';

type RaiderInventoryParentProps = {
  raider: DetailedSyncRaider;
  style?: React.CSSProperties;
  localInventory: Array<IndexedInventoryItem>;
  setLocalInventory: (localInventory: Array<IndexedInventoryItem>) => void;
  inventoryComputedBuilds: BuildResult[];
  inventoryComputedPureness: ItemPureness[];
  equippedItems: Record<number, IndexedInventoryItem>;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  currentRaiderClass: BuildType;
  currentClassRecommentation: BuildType;
  setCurrentClassRecommentation: (inventory: BuildType) => void;
  handleUpdateRaiderServerSide: (
    raider: DetailedSyncRaider,
    objects: Array<IndexedInventoryItem>,
    raiderStats?: Stats
  ) => Promise<void>;
  setHoverItem: (item: IndexedInventoryItem | undefined) => void;
  handleLocalEquipItem: (item: IndexedInventoryItem) => void;
  inventoryRecommendationsScore: number[];
};

export const RaiderInventoryParent = (props: RaiderInventoryParentProps) => {
  const [canApplyChanges, setCanApplyChanges] = useState<boolean>(false);
  const [baseEquippedItemsId, setBaseEquippedItemsId] = useState<Array<number>>([]);

  const { isLoading } = useContext(LoadingContext);

  useEffect(() => {
    const isLocalEquippedItemsDifferents = (): boolean => {
      let result: boolean = baseEquippedItemsId.length !== Object.keys(props.equippedItems).length;
      baseEquippedItemsId.forEach((object) => {
        let isMissing = true;
        for (const item of Object.values(props.equippedItems)) {
          if (item.originalIndex === object) {
            isMissing = false;
            break;
          }
        }
        if (isMissing) result = true;
      });
      return result;
    };
    setCanApplyChanges(isLocalEquippedItemsDifferents());
  }, [baseEquippedItemsId, props.equippedItems]);

  useEffect(() => {
    const newBaseEquipped: Array<number> = [];
    props.raider.raw_data?.inventory?.forEach((object, index) => {
      if (!object.equipped) return;
      newBaseEquipped.push(index);
    });
    setBaseEquippedItemsId(newBaseEquipped);
  }, [props.raider]);

  const handleUpdate = async () => {
    await props.handleUpdateRaiderServerSide(props.raider, Object.values(props.equippedItems));
  };

  const inventoryContainerStyle: React.CSSProperties = {
    maxHeight: '400px',
    overflowX: 'hidden',
    overflowY: 'auto',
  };

  const updateStyle: React.CSSProperties = {
    fontSize: '20px',
    fontWeight: '500',
    textAlign: 'center',
    borderRadius: '4px',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    margin: '8px',
    width: '100px',
    height: '50px',
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: '0.5s',
  };

  const canNotUpdateStyle: React.CSSProperties = {
    width: '80px',
    height: '40px',
    fontSize: '16px',
    opacity: '0.5',
    pointerEvents: 'none',
  };

  return (
    <div
      style={{ ...props.style }}
      onMouseLeave={() => {
        props.setHoverItem(undefined);
      }}
    >
      <RaiderInventorySelect
        raider={props.raider}
        localInventory={props.localInventory}
        setLocalInventory={props.setLocalInventory}
        inventoryComputedBuilds={props.inventoryComputedBuilds}
        equippedItems={props.equippedItems}
        currentMaxMeta={props.currentMaxMeta}
        stuffStats={props.stuffStats}
        currentRaiderClass={props.currentRaiderClass}
        currentClassRecommentation={props.currentClassRecommentation}
        setCurrentClassRecommentation={props.setCurrentClassRecommentation}
        inventoryComputedPureness={props.inventoryComputedPureness}
        inventoryRecommendationScore={props.inventoryRecommendationsScore}
      />
      <div
        style={{ ...inventoryContainerStyle }}
        onMouseLeave={() => {
          props.setHoverItem(undefined);
        }}
      >
        <Grid
          container
          spacing={2}
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          sx={{ padding: '5px' }}
        >
          {props.localInventory?.map((item) => {
            return (
              <Grid key={`${props.raider.raw_data?.tokenId}-${item.originalIndex}-${item.item.internalName}`} item>
                <RaiderInventoryItem
                  raider={props.raider}
                  object={item}
                  isLoading={isLoading}
                  isBaseEquipped={baseEquippedItemsId.includes(item.originalIndex)}
                  isEquipped={typeof props.equippedItems[item.originalIndex] !== 'undefined'}
                  onClick={() => {
                    props.raider.currentlyInWallet && !isLoading && props.handleLocalEquipItem(item);
                  }}
                  setHoverItem={props.setHoverItem}
                  recommentationScore={props.inventoryRecommendationsScore[item.originalIndex]}
                />
              </Grid>
            );
          })}
        </Grid>
      </div>
      {props.raider.currentlyInWallet && (
        <ClickableContainer
          boxShadows={[
            BoxShadow(colorPalette['lightBlueGrey'], { x: 4, y: 4 }),
            BoxShadow(colorPalette['mediumBlue'], { x: -4, y: -4 }),
          ]}
          onClick={handleUpdate}
          style={{ ...updateStyle, ...(!isLoading && canApplyChanges ? {} : canNotUpdateStyle) }}
        >
          <p>Apply Changes</p>
        </ClickableContainer>
      )}
    </div>
  );
};
