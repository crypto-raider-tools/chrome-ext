import { useRef } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { DetailedSyncRaider } from '../../../interfaces/detailedsyncraider';
import { IndexedInventoryItem } from '../../../interfaces/indexedInventoryItem';
import { BoxShadow, ClickableContainer } from '../../Utils/ClickableContainer';
import { ItemRecommendationArrow } from './RaiderInventoryOverview/ItemRecommendationArrow';

type RaiderInventoryItemProps = {
  onClick: () => void;
  isLoading: boolean;
  isBaseEquipped: boolean;
  isEquipped: boolean;
  raider: DetailedSyncRaider;
  object: IndexedInventoryItem;
  recommentationScore: number;
  style?: React.CSSProperties;
  setHoverItem: (item: IndexedInventoryItem) => void;
};

export const RaiderInventoryItem = (props: RaiderInventoryItemProps) => {
  const itemRef = useRef(null);

  let rarity: string = props.object.item.rarity ?? 'common';
  if (props.object.item.slot === 'knickknack') rarity = 'rune';
  const baseStyle: React.CSSProperties = {
    width: '64px',
    height: '64px',
    border: '2px solid',
    borderRadius: '8px',
    backgroundColor: 'rgb(46, 46, 46)',
    borderColor: colorPalette[rarity],
    imageRendering: 'pixelated',
  };
  const equippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    width: '100%',
    filter: 'brightness(150%)',
    boxShadow: colorPalette[rarity] + ' 0px 0px 8px 4px',
  };
  const loadingStyle: React.CSSProperties = {
    filter: 'brightness(75%)',
  };
  const boxShadows: string[] = [
    BoxShadow(colorPalette[rarity], { x: 6, y: 6 }, 15, 1, true),
    BoxShadow(colorPalette['white'], { x: -6, y: -6 }, 15, 1, true),
  ];
  const iconEquippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    boxShadow: boxShadows[0] + ', ' + boxShadows[1],
    transform: 'scale(1.15)',
  };
  const iconBaseEquippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    boxShadow:
      BoxShadow(colorPalette[rarity], { x: 3, y: 3 }, 10, 1, true) +
      ', ' +
      BoxShadow(colorPalette[rarity], { x: -3, y: -3 }, 10, 1, true),
    transform: 'scale(1.15)',
  };

  return (
    <ClickableContainer
      boxShadows={boxShadows}
      onClick={() => props.onClick()}
      onHover={(isHover) => {
        if (isHover && itemRef.current) {
          const bounds = (itemRef.current as HTMLElement).getBoundingClientRect();
          props.setHoverItem({ ...props.object, lastScreenPosition: bounds });
        }
      }}
      style={{
        ...props.style,
        ...baseStyle,
        ...(props.isLoading ? loadingStyle : {}),
      }}
    >
      <div
        ref={itemRef}
        style={{ position: 'relative', height: '100%', width: '100%', ...(props.isEquipped ? equippedStyle : {}) }}
      >
        <img
          src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${props.object.item.internalName}_ICON.png`}
          alt={props.object.item.name}
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'contain',
            transition: '0.2s',
            ...(props.isBaseEquipped ? iconBaseEquippedStyle : {}),
            ...(props.isEquipped ? iconEquippedStyle : {}),
          }}
        />
        <ItemRecommendationArrow recommendationScore={props.recommentationScore} />
      </div>
    </ClickableContainer>
  );
};
