import {
  BuildResult,
  BuildType,
  computeStatsBuildMetaScore,
  fixStats,
  InventoryItem,
} from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { DetailedSyncRaider } from '../../../../interfaces/detailedsyncraider';
import {
  IndexedInventoryItem,
  InventoryFilterSlotType,
  InventoryOrderablePropertyName,
  OrderableStack,
} from '../../../../interfaces/indexedInventoryItem';
import { ItemPureness } from '../../../../interfaces/pureness';
import { StuffStats } from '../../../../interfaces/stuffstats';
import { RaiderInventoryFiltersSlot } from './RaiderInventoryFiltersSlot';
import { RaiderInventoryOrders } from './RaiderInventoryOrders';
import { applyOrderableStackIndexed, orderIndexedInventory } from './RaiderInventoryOrdersHelpers';

type RaiderInventorySelectProps = {
  raider: DetailedSyncRaider;
  localInventory: IndexedInventoryItem[];
  setLocalInventory: (inventory: IndexedInventoryItem[]) => void;
  inventoryComputedBuilds: BuildResult[];
  inventoryComputedPureness: ItemPureness[];
  inventoryRecommendationScore: number[];
  equippedItems: Record<number, IndexedInventoryItem>;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  currentRaiderClass: BuildType;
  currentClassRecommentation: BuildType;
  setCurrentClassRecommentation: (inventory: BuildType) => void;
  style?: React.CSSProperties;
};

export const RaiderInventorySelect = (props: RaiderInventorySelectProps) => {
  const [filterName, setFilterName] = useState<string>('');
  const [filterSlot, setFilterSlot] = useState<InventoryFilterSlotType>('all');
  const [orderByStack, setOrderByStack] = useState<OrderableStack>(['Date']);

  useEffect(() => {
    let filteredIndex = 0;
    let newInventory: IndexedInventoryItem[] = [];
    props.raider.raw_data?.inventory?.forEach((object: InventoryItem, index: number) => {
      if (filterName && !object.item.name.toLocaleLowerCase().includes(filterName.toLocaleLowerCase())) {
        return;
      }
      if (filterSlot !== 'all' && object.item.slot !== filterSlot) {
        return;
      }
      const indexedItem: IndexedInventoryItem = {
        originalIndex: index,
        filteredIndex: filteredIndex,
        computedBuild:
          props.inventoryComputedBuilds[index] ?? computeStatsBuildMetaScore(fixStats({ ...object.item.stats })),
        equipped: object.equipped,
        item: { ...object.item, stats: fixStats(object.item.stats) },
      };
      filteredIndex++;
      newInventory.push(indexedItem);
    });
    props.setLocalInventory(
      applyOrderableStackIndexed(
        orderByStack,
        orderByStack.length - 1,
        newInventory,
        props.currentMaxMeta,
        props.inventoryComputedPureness,
        props.inventoryRecommendationScore,
        props.currentClassRecommentation
      )
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.raider,
    props.currentMaxMeta,
    props.stuffStats,
    props.inventoryComputedPureness,
    props.inventoryComputedBuilds,
    props.inventoryRecommendationScore,
    props.setLocalInventory,
    filterName,
    filterSlot,
    orderByStack,
  ]);

  const setNewFilterSlot = (filterSlot: InventoryFilterSlotType) => {
    setFilterSlot(filterSlot);
  };

  const setNewFilterName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFilterName(e.target.value);
  };

  const orderInventory = (statName: InventoryOrderablePropertyName) => {
    orderIndexedInventory(
      orderByStack,
      statName,
      props.localInventory,
      props.currentMaxMeta,
      props.inventoryComputedPureness,
      props.inventoryRecommendationScore,
      props.setLocalInventory,
      setOrderByStack,
      props.currentClassRecommentation === 'none' ? props.currentRaiderClass : props.currentClassRecommentation
    );
  };

  const formFieldStyle: React.CSSProperties = {
    width: '160px',
    minWidth: '0px',
    outline: 'transparent solid 2px',
    outlineOffset: '2px',
    position: 'relative',
    appearance: 'none',
    transitionProperty: 'var(--chakra-transition-property-common)',
    transitionDuration: 'var(--chakra-transition-duration-normal)',
    fontSize: 'var(--chakra-fontSizes-md)',
    paddingInlineStart: 'var(--chakra-space-4)',
    paddingInlineEnd: 'var(--chakra-space-10)',
    height: 'var(--chakra-sizes-10)',
    borderRadius: 'var(--chakra-radii-md)',
    borderWidth: '1px',
    borderStyle: 'solid',
    borderImage: 'initial',
    borderColor: 'inherit',
    background: 'var(--chakra-colors-gray-800)',
    marginRight: '8px',
  };

  return (
    <div>
      <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'column', justifyContent: 'flex-start' }}>
        <div
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}
        >
          <input type="text" style={formFieldStyle} placeholder="Item Name" onChange={setNewFilterName} />
          <RaiderInventoryFiltersSlot currentFilterSlot={filterSlot} filterSlot={'all'} onClick={setNewFilterSlot} />
          <RaiderInventoryFiltersSlot
            currentFilterSlot={filterSlot}
            filterSlot={'main_hand'}
            onClick={setNewFilterSlot}
          />
          <RaiderInventoryFiltersSlot currentFilterSlot={filterSlot} filterSlot={'dress'} onClick={setNewFilterSlot} />
          <RaiderInventoryFiltersSlot
            currentFilterSlot={filterSlot}
            filterSlot={'knickknack'}
            onClick={setNewFilterSlot}
          />
          <RaiderInventoryFiltersSlot currentFilterSlot={filterSlot} filterSlot={'neck'} onClick={setNewFilterSlot} />
          <RaiderInventoryFiltersSlot currentFilterSlot={filterSlot} filterSlot={'helmet'} onClick={setNewFilterSlot} />
          <RaiderInventoryFiltersSlot
            currentFilterSlot={filterSlot}
            filterSlot={'background'}
            onClick={setNewFilterSlot}
          />
          <RaiderInventoryFiltersSlot currentFilterSlot={filterSlot} filterSlot={'finger'} onClick={setNewFilterSlot} />
        </div>
        <RaiderInventoryOrders
          raider={props.raider}
          localInventory={props.localInventory}
          stuffStats={props.stuffStats}
          currentMaxMeta={props.currentMaxMeta}
          currentRaiderClass={props.currentRaiderClass}
          currentClassRecommentation={props.currentClassRecommentation}
          orderInventory={orderInventory}
          orderByStack={orderByStack}
          setOrderByStack={setOrderByStack}
        />
      </div>
    </div>
  );
};
