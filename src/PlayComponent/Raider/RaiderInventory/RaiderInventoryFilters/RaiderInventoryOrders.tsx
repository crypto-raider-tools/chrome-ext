import { BuildType } from '@crypto-raider-tools/computation';
import { DetailedSyncRaider } from '../../../../interfaces/detailedsyncraider';
import {
  IndexedInventoryItem,
  InventoryOrderablePropertyName,
  OrderableStack,
} from '../../../../interfaces/indexedInventoryItem';
import { StuffStats } from '../../../../interfaces/stuffstats';
import { OrderByBouton } from './RaiderInventoryOrdersHelpers';

type RaiderInventoryOrdersProps = {
  raider: DetailedSyncRaider;
  style?: React.CSSProperties;
  localInventory: Array<IndexedInventoryItem>;
  stuffStats: StuffStats[];
  currentMaxMeta: number;
  currentRaiderClass: BuildType;
  currentClassRecommentation: BuildType;
  orderByStack: OrderableStack;
  setOrderByStack: (stack: OrderableStack) => void;
  orderInventory: (order: InventoryOrderablePropertyName) => void;
};

export const RaiderInventoryOrders = (props: RaiderInventoryOrdersProps) => {
  const orderStyle: React.CSSProperties = {
    padding: '10px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  };
  const inventoryHeaderItemCount: React.CSSProperties = {
    position: 'absolute',
    fontSize: '14px',
    right: '5px',
    top: '41px',
  };
  return (
    <div title="order by" style={orderStyle}>
      <span style={inventoryHeaderItemCount}>{props.localInventory.length}</span>
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('strength')}
        nameOrderBy="strength"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('intelligence')}
        nameOrderBy="intelligence"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('agility')}
        nameOrderBy="agility"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('wisdom')}
        nameOrderBy="wisdom"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('charm')}
        nameOrderBy="charm"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('luck')}
        nameOrderBy="luck"
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Metascore')}
        nameOrderBy="Metascore"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Rarity')}
        nameOrderBy="Rarity"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Pureness')}
        nameOrderBy="Pureness"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Date')}
        nameOrderBy="Date"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Recommendation')}
        nameOrderBy="Recommendation"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Name')}
        nameOrderBy="Name"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Level')}
        nameOrderBy="Level"
        long={true}
      />
      <OrderByBouton
        orderableStack={props.orderByStack}
        onClick={() => props.orderInventory('Total points')}
        nameOrderBy="Total points"
        long={true}
      />
    </div>
  );
};
