import { BuildType, fixStats } from '@crypto-raider-tools/computation';
import { RarityAsNumber } from '../../../../Helpers/Maps';
import { getItemMetaAsNumber } from '../../../../Helpers/Meta';
import {
  OrderableStack,
  InventoryOrderablePropertyName,
  IndexedInventoryItem,
} from '../../../../interfaces/indexedInventoryItem';
import { ItemPureness } from '../../../../interfaces/pureness';
import { FilterRadiusButton } from '../../../Utils/FiltersRadiusButton';

type OrderByBtnProps = {
  orderableStack: OrderableStack;
  nameOrderBy: InventoryOrderablePropertyName;
  onClick: () => void;
  long?: boolean;
};

export const OrderByBouton = (props: OrderByBtnProps) => {
  const idx = props.orderableStack.findIndex((o) => o === props.nameOrderBy);
  const isEnabled: boolean = idx !== -1 ? true : false;
  const orderNumberStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    textAlign: 'center',
  };
  return (
    <FilterRadiusButton
      name={!props.long ? props.nameOrderBy.toLocaleUpperCase().substring(0, 3) : props.nameOrderBy}
      onClick={props.onClick}
      selectorDiv={<p style={orderNumberStyle}>{isEnabled ? idx : null}</p>}
    />
  );
};

export const applyOrderableStackIndexed = (
  orderableStack: OrderableStack,
  index: number,
  inventory: IndexedInventoryItem[],
  currentMaxMeta: number,
  inventoryComputedPureness: ItemPureness[],
  inventoryRecommendationScore: number[],
  currentClassRecommentation: BuildType
): IndexedInventoryItem[] => {
  if (index < 0) return inventory;
  const orderable: InventoryOrderablePropertyName = orderableStack[index];
  switch (orderable) {
    case 'Metascore':
      inventory = inventory.sort((a, b) => {
        return getItemMetaAsNumber(b.item, currentMaxMeta) - getItemMetaAsNumber(a.item, currentMaxMeta);
      });
      break;
    case 'Rarity':
      inventory = inventory.sort((a, b) => {
        return RarityAsNumber[b.item.rarity ?? 'common'] - RarityAsNumber[a.item.rarity ?? 'common'];
      });
      break;
    case 'Pureness':
      inventory = inventory.sort((a, b) => {
        return inventoryComputedPureness[b.originalIndex]?.sum - inventoryComputedPureness[a.originalIndex]?.sum;
      });
      break;
    case 'Date':
      inventory = inventory.sort((a, b) => {
        return b.originalIndex - a.originalIndex;
      });
      break;
    case 'Recommendation':
      inventory = inventory.sort((a, b) => {
        return inventoryRecommendationScore[b.originalIndex] - inventoryRecommendationScore[a.originalIndex];
      });
      break;
    case 'Name':
      inventory = inventory.sort((a, b) => {
        if (a.item.internalName < b.item.internalName) {
          return -1;
        }
        if (a.item.internalName > b.item.internalName) {
          return 1;
        }
        return 0;
      });
      break;
    case 'Level':
      inventory = inventory.sort((a, b) => {
        return (b.item.level ?? 0) - (a.item.level ?? 0);
      });
      break;
    case 'Total points':
      inventory = inventory.sort((a, b) => {
        const totalPointA =
          (a.item.stats.agility ?? 0) +
          (a.item.stats.charm ?? 0) +
          (a.item.stats.intelligence ?? 0) +
          (a.item.stats.luck ?? 0) +
          (a.item.stats.strength ?? 0) +
          (a.item.stats.wisdom ?? 0);
        const totalPointB =
          (b.item.stats.agility ?? 0) +
          (b.item.stats.charm ?? 0) +
          (b.item.stats.intelligence ?? 0) +
          (b.item.stats.luck ?? 0) +
          (b.item.stats.strength ?? 0) +
          (b.item.stats.wisdom ?? 0);
        return totalPointB - totalPointA;
      });
      break;
    default:
      inventory = inventory.sort((a, b) => {
        return (fixStats(b.item.stats)[orderable!] ?? 0) - (fixStats(a.item?.stats)[orderable!] ?? 0);
      });
      break;
  }
  return applyOrderableStackIndexed(
    orderableStack,
    index - 1,
    inventory,
    currentMaxMeta,
    inventoryComputedPureness,
    inventoryRecommendationScore,
    currentClassRecommentation
  );
};

export function orderIndexedInventory(
  orderableStack: OrderableStack,
  orderable: InventoryOrderablePropertyName,
  inventory: IndexedInventoryItem[],
  currentMaxMeta: number,
  inventoryComputedPureness: ItemPureness[],
  inventoryRecommendationScore: number[],
  setInventory: (inventory: IndexedInventoryItem[]) => void,
  setOrderBy: (orderableStack: OrderableStack) => void,
  currentClassRecommentation: BuildType
) {
  const newOrderableStack = [...orderableStack];
  const idxOrderable = newOrderableStack.findIndex((o) => o === orderable);
  if (idxOrderable !== -1) {
    newOrderableStack.splice(idxOrderable, 1);
  } else newOrderableStack.push(orderable);
  if (newOrderableStack.length === 0) {
    setInventory(
      [...inventory].sort((a, b) => {
        return a.originalIndex - b.originalIndex;
      })
    );
  } else
    setInventory(
      applyOrderableStackIndexed(
        newOrderableStack,
        newOrderableStack.length - 1,
        [...inventory],
        currentMaxMeta,
        inventoryComputedPureness,
        inventoryRecommendationScore,
        currentClassRecommentation
      )
    );
  setOrderBy(newOrderableStack);
}
