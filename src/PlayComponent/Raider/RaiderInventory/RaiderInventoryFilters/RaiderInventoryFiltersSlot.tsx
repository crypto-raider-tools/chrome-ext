import { colorPalette } from '../../../../Helpers/Constants';
import { InventoryFilterSlotType } from '../../../../interfaces/indexedInventoryItem';
import { FilterRadiusButton } from '../../../Utils/FiltersRadiusButton';

type RaiderInventoryFiltersSlotProps = {
  currentFilterSlot: InventoryFilterSlotType;
  filterSlot: InventoryFilterSlotType;
  onClick: (filterSlot: InventoryFilterSlotType) => void;
};

export const RaiderInventoryFiltersSlot = (props: RaiderInventoryFiltersSlotProps) => {
  const slotToText: Record<InventoryFilterSlotType, string> = {
    all: 'All',
    main_hand: 'Weapons',
    dress: 'Armors',
    knickknack: 'Runes',
    background: 'Backgrounds',
    finger: 'Rings',
    neck: 'Necklaces',
    helmet: 'Helmets',
  };
  const isEnabled: boolean = props.filterSlot === props.currentFilterSlot;

  return (
    <FilterRadiusButton
      name={slotToText[props.filterSlot]}
      onClick={() => props.onClick(props.filterSlot)}
      buttonStyle={{ backgroundColor: isEnabled ? colorPalette['darkBlueGrey'] : colorPalette['lightBlueGrey'] }}
      selectorDiv={<></>}
    />
  );
};
