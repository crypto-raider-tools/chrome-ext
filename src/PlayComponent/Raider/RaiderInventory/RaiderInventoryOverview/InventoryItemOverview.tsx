import {
  createStats,
  fixStats,
  getMetaScoreOutputLarge,
  Item,
  MetaOutput,
  Stats,
} from '@crypto-raider-tools/computation';
import { useEffect, useRef, useState } from 'react';
import { colorPalette } from '../../../../Helpers/Constants';
import { getItemMeta } from '../../../../Helpers/Meta';
import { getTotalStatsPointCount } from '../../../../Helpers/Utils';
import { IndexedInventoryItem } from '../../../../interfaces/indexedInventoryItem';
import { ItemPureness } from '../../../../interfaces/pureness';
import { StuffStats } from '../../../../interfaces/stuffstats';
import { InventoryItemStats } from './InventoryItemStats';

type InventoryItemOverviewProps = {
  hoverItem: IndexedInventoryItem;
  equippedItems: Record<number, IndexedInventoryItem>;
  inventoryComputedPureness: ItemPureness[];
  currentMaxMeta: number;
  onClick?: () => void;
  style?: React.CSSProperties;
  isFixed?: boolean;
  stuffStats: StuffStats;
};

export function InventoryItemOverview(props: InventoryItemOverviewProps) {
  const [currentBounds, setCurrentBounds] = useState<DOMRect>();
  const [needBoundsUpdate, setNeedBoundsUpdate] = useState<boolean>();

  const currentItem: Item = { ...props.hoverItem.item };
  currentItem.stats = fixStats(currentItem?.stats);
  let compareItemStats: Stats = createStats();
  for (const item of Object.values(props.equippedItems)) {
    if (item.item.slot === currentItem.slot) compareItemStats = fixStats(item.item.stats);
  }

  const itemMetaScore: MetaOutput = getItemMeta(currentItem, props.currentMaxMeta);
  const pureness: ItemPureness = props.inventoryComputedPureness[props.hoverItem.originalIndex];
  let primaryStatsSorted = [
    { name: 'strength', value: props.stuffStats?.strengthMax ?? 0 },
    { name: 'intelligence', value: props.stuffStats?.intelligenceMax ?? 0 },
    { name: 'agility', value: props.stuffStats?.agilityMax ?? 0 },
  ]
    .filter((p) => p.value > 0)
    .sort(function (e, t) {
      return t.value - e.value;
    });

  const defaultStyle: React.CSSProperties = {
    position: typeof props.isFixed !== 'undefined' && props.isFixed === true ? 'fixed' : 'absolute',
    pointerEvents: 'none',
    padding: '5px',
    backgroundColor: '#1a202cd5',
    borderRadius: '2px',
    zIndex: '10',
    transition: '0.2s',
    ...props.style,
  };

  const overviewStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'space-between',
    textAlign: 'right',
    position: 'relative',
  };

  const overviewIconStyle: React.CSSProperties = {
    width: '100px',
    transform: 'scale(0.7)',
    imageRendering: 'pixelated',
  };

  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    whiteSpace: 'nowrap',
    color: colorPalette['lightBlueGrey'],
  };

  const hoverStatsParentStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'space-between',
    textAlign: 'right',
    width: '100%',
  };

  const divRef = useRef(null);

  useEffect(() => {
    if (needBoundsUpdate) {
      const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
      if (bounds) {
        setCurrentBounds(bounds);
        setNeedBoundsUpdate(false);
      }
    }
    // eslint-disable-next-line
  }, [needBoundsUpdate, setNeedBoundsUpdate, setCurrentBounds]);

  useEffect(() => {
    const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
    if (bounds && (bounds?.height !== currentBounds?.height || bounds?.width !== currentBounds?.width)) {
      setCurrentBounds(bounds);
    } else setNeedBoundsUpdate(true);
    // eslint-disable-next-line
  }, [setCurrentBounds, setNeedBoundsUpdate, props.hoverItem, props.hoverItem.lastScreenPosition]);

  if (!props.hoverItem || !props.hoverItem.lastScreenPosition) return null;
  const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
  let offsetTop = props.hoverItem.lastScreenPosition.height;
  if (
    window.screen.height - props.hoverItem.lastScreenPosition.bottom <
    (currentBounds?.height ?? 0) + offsetTop + 40
  ) {
    offsetTop = (currentBounds?.height ?? 0) * -1;
  }
  let offsetLeft = 0.5 * (currentBounds?.width ?? 0) - props.hoverItem.lastScreenPosition.width / 2;
  if (window.screen.width - props.hoverItem.lastScreenPosition.right < offsetLeft) {
    offsetLeft -= offsetLeft - props.hoverItem.lastScreenPosition.width;
  }

  return (
    <div
      ref={divRef}
      style={{
        top:
          props.hoverItem.lastScreenPosition.y +
          offsetTop +
          (typeof props.isFixed !== 'undefined' && props.isFixed === true ? 0 : scrollTop),
        left: props.hoverItem.lastScreenPosition.x - offsetLeft,
        ...defaultStyle,
      }}
    >
      <div style={{ ...textStyle, color: 'white' }}>
        {currentItem.name + (currentItem.level ? ' Level ' + currentItem.level : '')}
      </div>
      <div style={overviewStyle}>
        <img
          style={overviewIconStyle}
          alt={currentItem.name}
          src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${currentItem.internalName}_ICON.png`}
        />
        <div>
          <div style={textStyle}>
            MetaScore:&nbsp;
            <span
              style={{
                color: colorPalette[itemMetaScore.toLowerCase()],
              }}
            >
              {itemMetaScore}
            </span>
          </div>
          <div style={textStyle}>
            Pureness:&nbsp;
            <span
              style={{
                color: colorPalette[getMetaScoreOutputLarge(pureness.sum, 100).toLowerCase()],
              }}
            >
              {pureness.sum >= 0 ? pureness.sum.toFixed(0) + '%' : 'Missing data'}
            </span>
          </div>
          <div style={textStyle}>
            Rarity:&nbsp;
            <span
              style={{
                color: colorPalette[currentItem.rarity?.toLowerCase() ?? 'white'],
              }}
            >
              {currentItem.rarity}
            </span>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            {primaryStatsSorted.map((p) => (
              <div
                key={p.name + p.value}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  marginRight: '5px',
                  fontSize: '15px',
                  fontWeight: '500',
                  borderRadius: '2px',
                  backgroundColor: colorPalette[p.name],
                  height: '25px',
                  width: '45px',
                }}
              >
                {p.name.substring(0, 3).toUpperCase()}
              </div>
            ))}
            <p style={textStyle}>
              <span>{'Total: '}</span>
              <span style={{ color: colorPalette['white'] }}>{getTotalStatsPointCount(currentItem.stats)}</span>
            </p>
          </div>
        </div>
      </div>
      <div style={{ ...hoverStatsParentStyle, marginTop: '0px' }}>
        <InventoryItemStats
          stats={fixStats(currentItem.stats)}
          pureness={pureness}
          comparedStats={fixStats(compareItemStats)}
          style={{ width: '100%' }}
        />
      </div>
    </div>
  );
}
