import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { colorPalette } from '../../../../Helpers/Constants';

type ItemRecommendationArrowProps = {
  recommendationScore: number;
  style?: React.CSSProperties;
};

const recommendationScoreToMeta: Record<string, string> = {
  '0': 'decent',
  '1': 'good',
  '2': 'meta',
  '3': 'op',
  '-1': 'moderate',
  '-2': 'inferior',
  '-3': 'trash',
};

export const ItemRecommendationArrow = (props: ItemRecommendationArrowProps) => {
  const defaultStyle: React.CSSProperties = {
    position: 'absolute',
    top: '0px',
    width: '100%',
    height: '100%',
    ...props.style,
  };

  const arrowContainerStyle: React.CSSProperties = {
    position: 'absolute',
    top: '0px',
    right: '0px',
    width: '40px',
    height: '40px',
  };
  let increment = props.recommendationScore >= 0 ? 0.5 : -0.5;
  if (props.recommendationScore === 0) increment = 0;
  const recommendationScore = Math.floor(props.recommendationScore + increment);
  let nbArrows = recommendationScore >= 0 ? recommendationScore : recommendationScore * -1;
  if (nbArrows > 4) nbArrows = 4;
  let arrowColor = colorPalette['ruygye'];

  if (Object.keys(recommendationScoreToMeta).includes(recommendationScore.toString()))
    arrowColor = colorPalette[recommendationScoreToMeta[recommendationScore.toString()]];
  else arrowColor = colorPalette[recommendationScore >= 0 ? 'op' : 'trash'];

  const arrowStyle: React.CSSProperties = {
    position: 'absolute',
    width: '100%',
    height: '100%',
    transition: '0.5s',
    transform: recommendationScore >= 0 ? 'rotate(180deg)' : 'rotate(0deg)',
    color: arrowColor,
    zIndex: '5',
  };

  const recommendationArrow = (arrowNumber: number) => {
    const top: number = 8;
    const right: number = -1;
    const width: number = 40 + 2 * arrowNumber;
    const height: number = 40 + 2 * arrowNumber;
    return (
      <div
        style={{
          ...arrowContainerStyle,
          top: top + 'px',
          right: right + 'px',
          width: width + 'px',
          height: height + 'px',
        }}
      >
        <KeyboardArrowDownIcon style={{ ...arrowStyle }}></KeyboardArrowDownIcon>
        {arrowNumber < nbArrows - 1 && recommendationArrow(arrowNumber + 1)}
      </div>
    );
  };
  return (
    <div style={defaultStyle}>
      {nbArrows > 0 && (
        <div
          style={{
            ...arrowContainerStyle,
            top: '-20px',
            right: '-15px',
            width: '40px',
            height: '40px',
          }}
        >
          <KeyboardArrowDownIcon style={{ ...arrowStyle }}></KeyboardArrowDownIcon>
          {nbArrows > 1 && recommendationArrow(1)}
        </div>
      )}
    </div>
  );
};
