import { Stats, getMetaScoreOutputLarge } from '@crypto-raider-tools/computation';
import { colorPalette } from '../../../../Helpers/Constants';
import { ItemPureness } from '../../../../interfaces/pureness';

type InventoryItemStatsProps = {
  stats: Stats;
  comparedStats: Stats;
  pureness: ItemPureness;
  style?: React.CSSProperties;
};

export const InventoryItemStats = (props: InventoryItemStatsProps) => {
  const defaultStyle: React.CSSProperties = {
    ...props.style,
  };

  const statsContainerStyle: React.CSSProperties = {
    height: '20px',
    fontSize: '14px',
    fontWeight: '500',
    display: 'flex',
    justifyContent: 'space-between',
  };

  const statsStyle: React.CSSProperties = {
    display: 'flex',
    fontSize: '14px',
    alignItems: 'center',
    justifyContent: 'space-between',
  };
  const mainStatsStyle: React.CSSProperties = { width: '65%', textAlign: 'start' };
  const purenessStatsStyle: React.CSSProperties = { width: '35%', textAlign: 'end' };
  return (
    <div style={defaultStyle}>
      <div style={statsContainerStyle}>
        <div style={mainStatsStyle}>
          <p>Stats</p>
        </div>
        {props.pureness && (
          <div style={purenessStatsStyle}>
            <p>Distribution</p>
          </div>
        )}
      </div>
      {Object.keys(props.stats).map((name: string) => {
        const statName = name as keyof Stats;
        if (props.stats[statName] <= 0 && props.comparedStats[statName] <= 0) return null;
        const val = props.stats[statName];
        const pureness = props.pureness[statName];
        return (
          <div key={name} style={statsStyle}>
            <div style={mainStatsStyle}>
              <span style={{ ...(val > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }) }}>
                {val}
              </span>
              <span>{' ' + name}</span>
              {props.stats[statName] !== props.comparedStats[statName] && (
                <span>
                  {' ('}
                  <span
                    style={{
                      color:
                        props.stats[statName] < props.comparedStats[statName]
                          ? colorPalette['inferior']
                          : colorPalette['op'],
                      fontSize: '500',
                    }}
                  >
                    {(props.comparedStats[statName] < props.stats[statName] ? '+' : '') +
                      (props.stats[statName] - props.comparedStats[statName])}
                  </span>
                  {')'}
                </span>
              )}
            </div>
            {pureness >= 0 ? (
              <div style={purenessStatsStyle}>
                <span
                  style={{
                    color: colorPalette[getMetaScoreOutputLarge(pureness, 100).toLowerCase()],
                  }}
                >
                  {pureness.toFixed(0)}%
                </span>
              </div>
            ) : null}
          </div>
        );
      })}
    </div>
  );
};
