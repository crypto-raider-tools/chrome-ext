export const RaiderGeneration = (props: RaiderGenerationProps) => {
  const scale: number = props.scale ?? 0.5;
  return (
    <div
      style={{
        position: 'relative',
        width: '50px',
        height: '50px',
        filter: 'drop-shadow(0 0 3px #0009) brightness(100%)',
        ...props.style,
      }}
    >
      <div
        style={{
          position: 'absolute',
          top: '-25px',
          right: '-25px',
          width: '90px',
          height: '90px',
          backgroundImage:
            'url(https://crypto-raiders-assets.storage.googleapis.com/interface/gen' + props.gen + '_border.png)',
          backgroundPosition: '-7px -10px',
          transform: 'scale(' + scale + ')',
        }}
      />
    </div>
  );
};

type RaiderGenerationProps = {
  gen: number;
  scale?: number;
  style?: React.CSSProperties;
};
