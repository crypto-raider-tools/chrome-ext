import { colorPalette } from '../../../Helpers/Constants';

type InventoryOverviewComputeBuildSplitProps = {
  buildMeta: string;
  buildStatName: string;
  buildStat: string;
  buildCap: number | boolean;
  cellStyle?: React.CSSProperties;
  compareBuildStat?: number;
};

export function InventoryOverviewComputeBuildSplit(props: InventoryOverviewComputeBuildSplitProps) {
  const buildCellStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '60%',
  };
  const capCellStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'start',
    alignItems: 'center',
    width: '20%',
  };
  const computeBarStyle: React.CSSProperties = {
    width: '80%',
    height: '2px',
    margin: '5px',
    borderRadius: '2px',
    backgroundColor: colorPalette['darkBlueGrey'],
  };
  return (
    <div style={props.cellStyle}>
      <div style={buildCellStyle}>
        <p style={{ color: colorPalette['none'] }}>{props.buildStatName}</p>
        <div style={computeBarStyle} />
        <p style={{ color: colorPalette[props.buildMeta] }}>{props.buildStat}</p>
      </div>
      {typeof props.compareBuildStat !== 'undefined' && (
        <>
          <div style={capCellStyle}>
            <div style={computeBarStyle} />
            <p style={{ color: colorPalette['none'] }}>Dif</p>
          </div>
          <div style={capCellStyle}>
            <div style={computeBarStyle} />
            <p style={{ color: props.compareBuildStat > 0 ? colorPalette['good'] : colorPalette['inferior'] }}>
              {(props.compareBuildStat > 0 ? '+' : '') + props.compareBuildStat.toFixed(1)}
            </p>
          </div>
        </>
      )}
      {typeof props.buildCap !== 'boolean' && (
        <>
          <div style={capCellStyle}>
            <div style={computeBarStyle} />
            <p style={{ color: colorPalette['none'] }}>Cap</p>
          </div>
          <div style={capCellStyle}>
            <div style={computeBarStyle} />
            <p style={{ color: 'white' }}>{props.buildCap.toFixed(1)}</p>
          </div>
        </>
      )}
    </div>
  );
}
