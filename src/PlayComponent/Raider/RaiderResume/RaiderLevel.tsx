import { colorPalette } from '../../../Helpers/Constants';

export const RaiderLevel = (props: RaiderLevelProps) => {
  const expSteps = [0, 1000, 2000, 4000, 8000, 12000, 20000, 40000, 80000, 160000, 350000, 460000];
  let idx = 0;
  for (var expStep of expSteps) {
    if (props.experience < expStep) {
      break;
    }
    idx++;
  }
  const expPurcentage = ((props.experience - expSteps[idx - 1]) / (expSteps[idx] - expSteps[idx - 1])) * 100;

  const levelTextStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    textAlign: 'center',
  };

  const experienceBarContainerStyle: React.CSSProperties = {
    backgroundColor: colorPalette['lightBlueGrey'],
    borderRadius: '2px',
    height: '15px',
    width: '100%',
    position: 'relative',
  };

  const experienceBarFillStyle: React.CSSProperties = {
    backgroundColor: colorPalette['experience'],
    borderRadius: '2px',
    height: '15px',
  };

  const experienceTextStyle: React.CSSProperties = {
    position: 'absolute',
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    fontWeight: '700',
    letterSpacing: '0.5px',
    lineHeight: 'inherit',
    textAlign: 'center',
    fontSize: '11px',
    textOverflow: 'clip',
    zIndex: '2',
  };
  return (
    <div style={{ width: '100px', ...props.style }}>
      <p style={levelTextStyle}>{'LVL ' + props.level}</p>
      <div style={experienceBarContainerStyle}>
        <p style={experienceTextStyle}>{props.experience + ' / ' + expSteps[idx] + ' XP'}</p>
        <div style={{ ...experienceBarFillStyle, width: expPurcentage + '%' }} />
      </div>
    </div>
  );
};

type RaiderLevelProps = {
  level: number;
  experience: number;
  style?: React.CSSProperties;
};
