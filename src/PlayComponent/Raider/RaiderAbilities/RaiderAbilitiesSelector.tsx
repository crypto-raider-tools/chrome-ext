import { Abilities } from '@crypto-raider-tools/computation';
import { Grid } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { RaiderAbility } from '../../../interfaces/raiderability';
import LoadingContext from '../../Context/LoadingContext';
import { ClickableContainer, BoxShadow } from '../../Utils/ClickableContainer';
import { RaiderAbilityElement } from './RaiderAbilityElement';
type RaiderAbilitiesSelectorType = {
  style?: React.CSSProperties;
  abilities: Abilities;
  raiderAbilities: Array<RaiderAbility>;
  handleUpdateAbilities: (abilities: Abilities) => void;
  setHoverAbility: (ability: RaiderAbility | undefined, bounds: DOMRect | undefined) => void;
  maxEquippedNumber: number;
};

export const RaiderAbilitiesSelector = (props: RaiderAbilitiesSelectorType) => {
  const [localAbilities, setLocalAbilities] = useState<Abilities>([]);
  const [canApplyChanges, setCanApplyChanges] = useState<boolean>(false);

  const { isLoading } = useContext(LoadingContext);

  useEffect(() => {
    setLocalAbilities([
      ...props.abilities.map((a) => {
        return { ...a };
      }),
    ]);
  }, [props.abilities]);

  const handleLocalEquipAbility = (raiderAbility: RaiderAbility) => {
    const newLocalAbilities = [...localAbilities];
    const abilityToUpdate = newLocalAbilities.find((a) => a.ability === raiderAbility.internalName)!;
    if (abilityToUpdate.equipped === true) {
      abilityToUpdate.equipped = false;
      setLocalAbilities(newLocalAbilities);
    } else {
      const nbEquippedAbilities = localAbilities.filter((a) => a.equipped === true).length;
      if (nbEquippedAbilities === props.maxEquippedNumber) {
        newLocalAbilities.find((a) => a.equipped === true)!.equipped = false;
      }
      abilityToUpdate.equipped = true;
      setLocalAbilities(newLocalAbilities);
    }
    setCanApplyChanges(props.abilities !== newLocalAbilities);
  };

  const handleUpdate = async () => {
    await props.handleUpdateAbilities(localAbilities);
  };

  const defaultStyle: React.CSSProperties = {
    paddingTop: '8px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    ...props.style,
  };
  const updateStyle: React.CSSProperties = {
    fontSize: '20px',
    fontWeight: '500',
    textAlign: 'center',
    borderRadius: '4px',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    margin: '8px',
    width: '100px',
    height: '50px',
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: '0.5s',
  };

  const canNotUpdateStyle: React.CSSProperties = {
    width: '80px',
    height: '40px',
    fontSize: '16px',
    opacity: '0.5',
    pointerEvents: 'none',
  };
  return (
    <div
      style={defaultStyle}
      onMouseLeave={() => {
        props.setHoverAbility(undefined, undefined);
      }}
    >
      <Grid
        container
        spacing={2}
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
        sx={{ padding: '5px' }}
      >
        {localAbilities
          .sort((a, b) => {
            const raA = props.raiderAbilities.find((ra) => ra.internalName === a.ability);
            const raB = props.raiderAbilities.find((ra) => ra.internalName === b.ability);
            if ((raA?.internalName ?? '') < (raB?.internalName ?? '')) {
              return -1;
            }
            if ((raA?.internalName ?? '') > (raB?.internalName ?? '')) {
              return 1;
            }
            return 0;
          })
          .sort((a, b) => {
            const raA = props.raiderAbilities.find((ra) => ra.internalName === a.ability);
            const raB = props.raiderAbilities.find((ra) => ra.internalName === b.ability);
            if ((raA?.type ?? '') < (raB?.type ?? '')) {
              return -1;
            }
            if ((raA?.type ?? '') > (raB?.type ?? '')) {
              return 1;
            }
            return 0;
          })
          .map((ability) => {
            const raiderAbility = props.raiderAbilities.find((ra) => ra.internalName === ability.ability);
            const isBaseEquipped = props.abilities.find((a) => a.ability === ability.ability)?.equipped;
            const isEquipped = localAbilities.find((a) => a.ability === ability.ability)?.equipped;
            return !raiderAbility ? null : (
              <Grid key={`${ability.ability}`} item>
                <RaiderAbilityElement
                  isLoading={isLoading}
                  isBaseEquipped={isBaseEquipped ?? false}
                  isEquipped={isEquipped ?? false}
                  onClick={() => {
                    !isLoading && handleLocalEquipAbility(raiderAbility);
                  }}
                  setHoverAbility={props.setHoverAbility}
                  raiderAbility={raiderAbility}
                />
              </Grid>
            );
          })}
      </Grid>
      <ClickableContainer
        boxShadows={[
          BoxShadow(colorPalette['lightBlueGrey'], { x: 4, y: 4 }),
          BoxShadow(colorPalette['mediumBlue'], { x: -4, y: -4 }),
        ]}
        onClick={handleUpdate}
        style={{ ...updateStyle, ...(!isLoading && canApplyChanges ? {} : canNotUpdateStyle) }}
      >
        <p>Apply Changes</p>
      </ClickableContainer>
    </div>
  );
};
