import { useRef } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { RaiderAbility } from '../../../interfaces/raiderability';
import { BoxShadow, ClickableContainer } from '../../Utils/ClickableContainer';

type RaiderAbilityElementProps = {
  onClick: () => void;
  isLoading: boolean;
  isBaseEquipped: boolean;
  isEquipped: boolean;
  raiderAbility: RaiderAbility;
  style?: React.CSSProperties;
  setHoverAbility: (ability: RaiderAbility | undefined, bounds: DOMRect) => void;
};

export const RaiderAbilityElement = (props: RaiderAbilityElementProps) => {
  const itemRef = useRef(null);

  const abilityColor = colorPalette[(props.raiderAbility.type ?? 'ruygye').toLowerCase()];

  const baseStyle: React.CSSProperties = {
    width: '64px',
    height: '64px',
    border: '2px solid',
    borderRadius: '8px',
    backgroundColor: 'rgb(46, 46, 46)',
    borderColor: abilityColor,
    imageRendering: 'pixelated',
  };
  const equippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    width: '100%',
    filter: 'brightness(150%)',
    boxShadow: abilityColor + ' 0px 0px 8px 4px',
  };
  const loadingStyle: React.CSSProperties = {
    filter: 'brightness(75%)',
  };
  const boxShadows: string[] = [
    BoxShadow(abilityColor, { x: 6, y: 6 }, 15, 1),
    BoxShadow(colorPalette['white'], { x: -6, y: -6 }, 15, 1),
  ];
  const iconEquippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    boxShadow: boxShadows[0] + ', ' + boxShadows[1],
    transform: 'scale(1.15)',
  };
  const iconBaseEquippedStyle: React.CSSProperties = {
    borderRadius: '8px',
    boxShadow: BoxShadow(abilityColor, { x: 3, y: 3 }, 10, 1) + ', ' + BoxShadow(abilityColor, { x: -3, y: -3 }, 10, 1),
    transform: 'scale(1.15)',
    filter: 'brightness(90%)',
  };

  return (
    <ClickableContainer
      boxShadows={boxShadows}
      onClick={() => props.onClick()}
      onHover={(isHover) => {
        if (isHover && itemRef.current) {
          const bounds = (itemRef.current as HTMLElement).getBoundingClientRect();
          props.setHoverAbility({ ...props.raiderAbility }, bounds);
        }
      }}
      style={{
        ...props.style,
        ...baseStyle,
        ...(props.isLoading ? loadingStyle : {}),
        ...(props.isBaseEquipped ? iconBaseEquippedStyle : {}),
        ...(props.isEquipped ? { ...equippedStyle, ...iconEquippedStyle } : {}),
      }}
    >
      <div
        ref={itemRef}
        style={{
          position: 'relative',
          height: '100%',
          width: '100%',
        }}
      >
        <img
          src={props.raiderAbility.iconUrl}
          alt={props.raiderAbility.internalName}
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'contain',
            transition: '0.2s',
          }}
        />
      </div>
    </ClickableContainer>
  );
};
