import { useEffect, useRef, useState } from 'react';
import { colorPalette } from '../../../Helpers/Constants';
import { RaiderAbility } from '../../../interfaces/raiderability';

type AbilityOverviewProps = {
  hoverAbility: RaiderAbility;
  bounds: DOMRect | undefined;
  style?: React.CSSProperties;
  isFixed?: boolean;
};

export function AbilityOverview(props: AbilityOverviewProps) {
  const [currentBounds, setCurrentBounds] = useState<DOMRect>();
  const [needBoundsUpdate, setNeedBoundsUpdate] = useState<boolean>();

  const defaultStyle: React.CSSProperties = {
    position: typeof props.isFixed !== 'undefined' && props.isFixed === true ? 'fixed' : 'absolute',
    pointerEvents: 'none',
    padding: '5px',
    backgroundColor: '#1a202cd5',
    borderRadius: '2px',
    zIndex: '10',
    transition: '0.2s',
    ...props.style,
  };

  const overviewStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'space-between',
    textAlign: 'right',
    position: 'relative',
    borderColor: colorPalette[(props.hoverAbility.type ?? 'ruygye').toLowerCase()],
  };

  const overviewIconStyle: React.CSSProperties = {
    width: '200px',
    height: '200px',
    transform: 'scale(0.7)',
    imageRendering: 'pixelated',
  };

  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    whiteSpace: 'nowrap',
    color: colorPalette['lightBlueGrey'],
  };

  const divRef = useRef(null);

  useEffect(() => {
    if (needBoundsUpdate) {
      const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
      if (bounds) {
        setCurrentBounds(bounds);
        setNeedBoundsUpdate(false);
      }
    }
    // eslint-disable-next-line
  }, [needBoundsUpdate, setNeedBoundsUpdate, setCurrentBounds]);

  useEffect(() => {
    const bounds = divRef.current ? (divRef.current as HTMLElement).getBoundingClientRect() : null;
    if (bounds && (bounds?.height !== currentBounds?.height || bounds?.width !== currentBounds?.width)) {
      setCurrentBounds(bounds);
    } else setNeedBoundsUpdate(true);
    // eslint-disable-next-line
  }, [setCurrentBounds, setNeedBoundsUpdate, props.hoverAbility, props.bounds]);

  if (!props.hoverAbility || !props.bounds) return null;
  const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
  let offsetTop = props.bounds.height;
  if (window.screen.height - props.bounds.bottom < (currentBounds?.height ?? 0) + offsetTop + 40) {
    offsetTop = (currentBounds?.height ?? 0) * -1;
  }
  let offsetLeft = 0.5 * (currentBounds?.width ?? 0) - props.bounds.width / 2;
  if (window.screen.width - props.bounds.right < offsetLeft) {
    offsetLeft -= offsetLeft - props.bounds.width;
  }

  return (
    <div
      ref={divRef}
      style={{
        top:
          props.bounds.y + offsetTop + (typeof props.isFixed !== 'undefined' && props.isFixed === true ? 0 : scrollTop),
        left: props.bounds.x - offsetLeft,
        ...defaultStyle,
      }}
    >
      <div style={{ ...textStyle, color: 'white' }}>{props.hoverAbility.name}</div>
      <div style={overviewStyle}>
        <img style={overviewIconStyle} alt={props.hoverAbility.name} src={props.hoverAbility.iconUrl} />
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
          <div style={textStyle}>
            Type:&nbsp;
            <span
              style={{
                color: colorPalette[(props.hoverAbility.type ?? 'ruygye').toLowerCase()],
              }}
            >
              {props.hoverAbility.type}
            </span>
          </div>
          <div style={{ ...textStyle, whiteSpace: 'normal', width: '300px' }}>
            Description:&nbsp;
            <span
              style={{
                color: colorPalette['white'],
              }}
            >
              {props.hoverAbility.description}
            </span>
          </div>
          <div style={textStyle}>
            Cost:&nbsp;
            <span
              style={{
                color: colorPalette['utility'],
              }}
            >
              {props.hoverAbility.energyCost}
            </span>
            <span
              style={{
                color: colorPalette['white'],
              }}
            >
              {' Energy'}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
