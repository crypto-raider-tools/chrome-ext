import { FighterBuild, FighterStats } from '@crypto-raider-tools/computation';
import { colorPalette } from '../../Helpers/Constants';
import { raceToRaceName } from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { StuffStats } from '../../interfaces/stuffstats';
import { H2 } from '../Theme';
import { BoxShadow, ClickableContainer } from '../Utils/ClickableContainer';
import { RaiderLevel } from './RaiderResume/RaiderLevel';

type RaiderResumeProps = {
  raider: DetailedSyncRaider;
  fighterStats: FighterStats;
  isInventoryOpen: boolean;
  setIsInventoryOpen: (value: boolean) => void;
  isDungeonsOpen: boolean;
  setIsDungeonsOpen: (value: boolean) => void;
  style?: React.CSSProperties;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  fighterBuild: FighterBuild;
  handleUpdateRaiderName: (name: string) => Promise<void>;
};

export const RaiderResume = (props: RaiderResumeProps) => {
  const rawData = props.raider.raw_data;
  const resumeTextStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    textAlign: 'center',
  };

  const resumeStyle: React.CSSProperties = {
    height: '220px',
    width: '130px',
  };

  const btnStyle: React.CSSProperties = {
    ...resumeTextStyle,
    borderRadius: '2px',
    width: '60px',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    marginTop: '4px',
  };

  return (
    <div style={{ ...resumeStyle, ...props.style }}>
      <H2
        style={{
          height: '22px',
          ...(rawData?.name?.length && rawData?.name?.length > 12 ? { fontSize: '15px' } : {}),
          ...(props.raider.currentlyInWallet ? { cursor: 'pointer' } : {}),
        }}
        {...(props.raider.currentlyInWallet
          ? {
              title: 'Change raider name',
              onClick: () => {
                let result = window.prompt('New raider name', props.raider.name ?? 'Best Tammy Ever');
                let newName = result ?? null;
                if (newName) {
                  props.handleUpdateRaiderName(newName);
                }
              },
            }
          : {})}
      >
        {props.raider.name ?? 'Best Tammy Ever'}
      </H2>
      <RaiderLevel style={{ width: '100%' }} level={props.raider.level ?? 0} experience={props.raider.xp ?? 0} />
      <p style={resumeTextStyle}>
        ID: <span style={{ fontWeight: '300' }}>{rawData?.tokenId ?? 'N/D'}</span>
      </p>
      <p style={resumeTextStyle}>{raceToRaceName(props.raider.race ?? '')}</p>
      {props.raider.currentlyInWallet && (
        <>
          <p style={resumeTextStyle}>Missions left: {props.raider.raidsRemaining}</p>
          <p style={resumeTextStyle}>Endless left: {props.raider.endlessRemaining}</p>
        </>
      )}
      {!props.raider.currentlyInWallet && props.raider.questStatus !== 1 && (
        <>
          <p style={resumeTextStyle} title="You must be premium to see quest info 😿">
            Raider in quest
          </p>
        </>
      )}
      <ClickableContainer
        boxShadows={[
          BoxShadow(colorPalette['lightBlueGrey'], { x: 4, y: 4 }),
          BoxShadow(colorPalette['mediumBlue'], { x: -4, y: -4 }),
        ]}
        onClick={() => {
          props.setIsInventoryOpen(!props.isInventoryOpen);
        }}
        style={{ ...btnStyle, width: '100%' }}
      >
        <p style={resumeTextStyle}>
          {props.raider.currentlyInWallet ? (
            props.isInventoryOpen ? (
              <>
                Close
                <br />
                Inventory
              </>
            ) : (
              <>
                Open
                <br />
                Inventory
              </>
            )
          ) : props.isInventoryOpen ? (
            'Close Information'
          ) : (
            'Open Information'
          )}
        </p>
      </ClickableContainer>
    </div>
  );
};
