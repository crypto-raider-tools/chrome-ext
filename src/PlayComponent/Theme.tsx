type H2Type = {
  style?: React.CSSProperties;
  children: JSX.Element | string;
  onClick?: () => void;
  title?: string;
};
export const H2 = (props: H2Type) => {
  const style: React.CSSProperties = {
    fontSize: '1.2em',
    fontWeight: 'bold',
    marginBottom: '.5em',
    ...props.style,
  };
  return (
    <h2 {...props} style={style}>
      {props.children}
    </h2>
  );
};
