import { colorPalette } from '../../Helpers/Constants';
import { getPremiumExpirationDate, isPremium } from '../../Helpers/Utils';
import { DonationPopUp } from '../Premium/DonationPopUp';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { PopUpButton } from '../Utils/PopUpButton';
const humanizeDuration = require('humanize-duration');

export const CompanionState = () => {
  const donationStyle: React.CSSProperties = {
    display: 'flex',
    margin: '8px',
    backgroundColor: colorPalette['experience'],
    justifyContent: 'start',
    alignItems: 'center',
    borderRadius: '2px',
    padding: '5px 8px 5px 8px',
    fontWeight: '500',
  };

  const premiumStyle: React.CSSProperties = {
    display: 'flex',
    position: 'relative',
    margin: '8px',
    justifyContent: 'start',
    alignItems: 'center',
    opacity: isPremium() ? '1' : '0.5',
    height: '30px',
  };

  const image128pxStyle: React.CSSProperties = {
    transform: 'scale(1.25)',
    height: '30px',
  };

  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        height: 'min-content',
      }}
    >
      <PopUpButton
        buttonContent={<p style={{ fontWeight: '500', fontSize: '16px' }}>⭐Subscribe to Premium⭐</p>}
        buttonStyle={{
          ...donationStyle,
          backgroundColor: colorPalette['mediumBlueGrey'],
        }}
      >
        <DonationPopUp />
      </PopUpButton>
      <div style={premiumStyle}>
        <img style={image128pxStyle} src={chrome.runtime.getURL('img/premiumLogo.png')} alt={'Premium'} />
        <p
          style={{
            fontWeight: '500',
            fontSize: '16px',
            textDecoration: !isPremium() ? 'line-through' : '',
            textAlign: 'center',
          }}
        >
          ⭐Premium: {isPremium() ? 'Yes' : 'No'}⭐
          {isPremium() && (
            <span>
              <br />
              {`${humanizeDuration(new Date(getPremiumExpirationDate()).getTime() - new Date().getTime(), {
                language: 'shortEn',
                languages: {
                  shortEn: {
                    d: () => 'days',
                    h: () => 'h',
                  },
                },
                units: ['d', 'h'],
                round: true,
                conjunction: ' ',
                serialComma: false,
              })} left`}
            </span>
          )}
        </p>
        <img style={image128pxStyle} src={chrome.runtime.getURL('img/premiumLogo.png')} alt={'Premium'} />
      </div>
      <ClickableContainer
        onClick={() => {
          window.open('https://discord.gg/JUDPBEBKdB');
        }}
        boxShadows={[]}
        style={{
          margin: '8px',
          display: 'flex',
          alignItems: 'center',
          height: '30px',
        }}
        onHoverStyle={{ filter: 'drop-shadow(1px 1px 3px white) drop-shadow(-1px -1px 3px white)' }}
      >
        <img
          style={{ ...image128pxStyle, imageRendering: 'pixelated' }}
          src={chrome.runtime.getURL('img/discordIcon.png')}
          alt={'Discord'}
        />
        <p style={{ fontWeight: '500', fontSize: '16px', marginLeft: '8px' }}>Join our discord !</p>
      </ClickableContainer>
    </div>
  );
};
