import { useCallback, useEffect, useReducer, useState } from 'react';
import { apiEntrypoint } from '../config';
import { HydraPaginatedResponse } from '../interfaces/hydra';
import { Paginator } from './RaiderList/RaiderPaginator';
import { DetailedSyncRaider } from '../interfaces/detailedsyncraider';
import { ClickableContainer } from './Utils/ClickableContainer';
import { isPremium } from '../Helpers/Utils';
import { QuestRaider } from './CRCQuest/QuestRaider';
import { triggerUpdateQuestServer } from '../Helpers/CompanionAPI';
import { QuestActionButton } from './CRCQuest/QuestActionButton';
import { QuestFilters, QuestName, QuestOrderBys } from '../interfaces/quest';
import { MessagePassing } from '../interfaces/messagePassing';
import { TxState } from '../interfaces/web3Inject';
import { QuestTargets } from './CRCQuest/QuestTargets';
import { DonationPopUp } from './Premium/DonationPopUp';
import { PopUpButton } from './Utils/PopUpButton';
import { btnStyle } from './Styles/BaseStyles';
import { QuestFieldsSelect } from './CRCQuest/QuestFieldsSelect';
import { computeProfessionLevel } from './CRCQuest/ProfessionLevel';
import { CustomFilters } from '../interfaces/detailedcustomer';

type RaiderId = string;

type CRCQuestProps = {
  metamask: string;
};

const txInitialStates: Record<number, TxState> = {};
const questLevel: Record<QuestName, number> = { Grimweed: 1, Newt: 3, Sporebark: 5 };

function txStateReducer(state: Record<number, TxState>, action: { raiderId: number; txState: TxState }) {
  const newTxStates = { ...state };
  if (action.txState.type === 'Delete') delete newTxStates[action.raiderId];
  else newTxStates[action.raiderId] = { ...action.txState };
  return newTxStates;
}

export const CRCQuest = (props: CRCQuestProps) => {
  const [raiderIsLoading, setRaiderIsLoading] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [needsUpdate, setNeedsUpdate] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [raidersList, setRaidersList] = useState<undefined | HydraPaginatedResponse<DetailedSyncRaider>>();
  const [expandedRaiders, setExpandedRaiders] = useState<Record<number, boolean>>({});
  const [selectedRaiders, setSelectedRaiders] = useState<Array<RaiderId>>([]);
  const [selectedQuestTarget, setSelectedQuestTarget] = useState<QuestName>('Sporebark');
  const [txStates, dispatchTxStates] = useReducer(txStateReducer, txInitialStates);
  const [expandAll, setExpandAll] = useState<boolean>(localStorage.getItem('CRC_quest_expand_all') === 'true');

  /** PERSISTANT STATES */
  const [elementPerPage, setElementPerPage] = useState<number>(
    parseInt(localStorage.getItem('CRC_quest_raider_per_page') ?? '10')
  );
  const [fields, setFields] = useState<Array<keyof DetailedSyncRaider>>(
    JSON.parse(localStorage.getItem('CRC_quest_fields') ?? '[]')
  );
  const [customerFilters] = useState<Partial<CustomFilters>>(
    JSON.parse(localStorage.getItem('CRC_customer') ?? '{"filters":[]}').filters
  );
  const [orderBys, setOrderBys] = useState<QuestOrderBys>(
    JSON.parse(localStorage.getItem('CRC_quest_order_bys') ?? '{}')
  );
  const [filters, setFilters] = useState<QuestFilters>(JSON.parse(localStorage.getItem('CRC_quest_filters') ?? '{}'));
  useEffect(() => {
    localStorage.setItem('CRC_quest_raider_per_page', elementPerPage.toString());
    setPage(1);
  }, [elementPerPage]);
  useEffect(() => {
    localStorage.setItem('CRC_quest_order_bys', JSON.stringify(orderBys));
  }, [orderBys]);
  useEffect(() => {
    localStorage.setItem('CRC_quest_fields', JSON.stringify(fields));
  }, [fields]);
  useEffect(() => {
    console.log(filters);
    localStorage.setItem('CRC_quest_filters', JSON.stringify(filters));
  }, [filters]);

  useEffect(() => {
    const newExtendedItems: Record<number, boolean> = {};
    let idx = 0;
    while (idx < elementPerPage) {
      newExtendedItems[idx] = expandAll;
      idx++;
    }
    setExpandedRaiders(newExtendedItems);
  }, [raidersList, expandAll, elementPerPage]);

  useEffect(() => {
    localStorage.setItem('CRC_quest_expand_all', expandAll ? 'true' : 'false');
  }, [expandAll]);

  const triggerRaiderFetch = useCallback(
    async (shouldCancel: boolean) => {
      setRaiderIsLoading(true);
      const url = new URL(`${apiEntrypoint}/sync_raiders`);
      url.searchParams.append('page', page.toString());
      url.searchParams.append('itemsPerPage', elementPerPage.toString());

      for (const field in orderBys) {
        if (!Object.prototype.hasOwnProperty.call(orderBys, field)) {
          continue;
        }
        const value = orderBys[field as keyof DetailedSyncRaider]!;
        url.searchParams.append('order[' + field + ']', value);
      }

      const minMaxFields: Array<keyof DetailedSyncRaider> = [
        'questEarnedReward',
        'questExpRewardAmount',
        'questHerbalismExp',
      ];
      const existFields: Array<keyof DetailedSyncRaider> = ['mountId'];
      for (const __field in filters) {
        const field: keyof DetailedSyncRaider = __field as keyof DetailedSyncRaider;
        if (!Object.prototype.hasOwnProperty.call(filters, field)) {
          continue;
        }
        const value = filters[field]!;
        if (value.toString().indexOf(',') !== -1 && !minMaxFields.includes(field)) {
          const values = value.toString().split(',');
          values.forEach((v) => {
            url.searchParams.append(`${field}[]`, v.trim());
          });
          continue;
        }
        if (value.toString().indexOf(',') !== -1 && minMaxFields.includes(field)) {
          const values = value.toString().split(',');
          url.searchParams.append(`${field}[gte]`, values[0].trim());
          url.searchParams.append(`${field}[lte]`, values[1].trim());
          continue;
        }
        if (existFields.includes(field)) {
          url.searchParams.append(`exists[${field}]`, value.toString());
          continue;
        }
        url.searchParams.append(field, value.toString());
      }

      const data = await fetch(url.toString(), {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      if (!shouldCancel) {
        setRaidersList(await data.json());
        setIsLoading(false);
        setRaiderIsLoading(false);
        setNeedsUpdate(false);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [orderBys, filters, elementPerPage, page, props.metamask]
  );

  const updateRaiderOnMintTx = useCallback(
    async (raiderId: number) => {
      await triggerUpdateQuestServer(raiderId);
      const data = await fetch(`${apiEntrypoint}/sync_raiders/${raiderId}`, {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      const raiderData: DetailedSyncRaider = await data.json();
      if (raidersList) {
        const newRaiders = { ...raidersList };
        newRaiders['hydra:member'].forEach((r, index) => {
          if (r.id === raiderData.id) {
            newRaiders['hydra:member'][index] = raiderData;
          }
        });
        setRaidersList(newRaiders);
      }
      dispatchTxStates({ raiderId: raiderId, txState: { type: 'Delete', txHash: '' } });
    },
    [raidersList]
  );

  useEffect(() => {
    let messageListener = (request: MessagePassing, __sender: any, sendResponse: (result?: any) => void) => {
      if (request.type === 'Ping') sendResponse({ ...request, type: 'Pong' });
      else if (request.type === 'TxState') {
        const newState = {
          raiderId: request.data.web3InjectResponse?.raiderId!,
          txState: request.data.web3InjectResponse!,
        };
        dispatchTxStates(newState);
        if (newState.txState.type === 'Mined') {
          updateRaiderOnMintTx(newState.raiderId);
        }
      }
      sendResponse({ type: 'Ok' });
    };
    chrome.runtime.onMessage.addListener(messageListener);
    return () => {
      chrome.runtime.onMessage.removeListener(messageListener);
    };
    // eslint-disable-next-line
  }, [updateRaiderOnMintTx]);

  useEffect(() => {
    let shouldCancel = false;
    triggerRaiderFetch(shouldCancel);

    return () => {
      shouldCancel = true;
    };
  }, [needsUpdate, triggerRaiderFetch]);

  if (isLoading) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '100px',
        }}
      >
        🔁 Loading
      </div>
    );
  }

  const questBtnStyle: React.CSSProperties = {
    ...btnStyle,
    width: 'fit-content',
    height: 'fit-content',
  };

  if (!isPremium())
    return (
      <div>
        ⚠️You need to be premium to use this feature⚠️
        <PopUpButton
          buttonContent={<p style={{ fontWeight: '500', fontSize: '16px' }}>⭐Subscribe to Premium⭐</p>}
          buttonStyle={btnStyle}
        >
          <DonationPopUp />
        </PopUpButton>
      </div>
    );
  // Global parent
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        alignContent: 'stretch',
      }}
    >
      <div style={{ display: 'flex' }}>
        <ClickableContainer
          style={{ ...btnStyle, ...{ opacity: isLoading ? 0.5 : 1 } }}
          boxShadows={isLoading ? [] : undefined}
          onClick={async () => {
            if (isLoading) return;
            setRaiderIsLoading(true);
            await triggerUpdateQuestServer();
            await triggerRaiderFetch(false);
            setRaiderIsLoading(false);
          }}
        >
          Update 🔁
        </ClickableContainer>
        <ClickableContainer
          style={{ ...btnStyle, ...{ opacity: isLoading ? 0.5 : 1 } }}
          boxShadows={isLoading ? [] : undefined}
          onClick={async () => {
            let currentSelectedRaiders =
              raidersList?.['hydra:member'].filter((r) => r['@id'] && selectedRaiders.includes(r['@id'])) ?? [];
            for (const raider of currentSelectedRaiders) {
              if (raider.id) {
                dispatchTxStates({
                  raiderId: raider.id,
                  txState: { type: 'Mined', txHash: '' },
                });
                updateRaiderOnMintTx(raider.id);
              }
            }
          }}
        >
          Update Selected Raiders 🔁
        </ClickableContainer>
      </div>
      <QuestFieldsSelect
        fields={fields}
        orderBys={orderBys}
        filters={filters}
        setFields={setFields}
        setOrderBys={setOrderBys}
        setFilters={setFilters}
        customerFilters={customerFilters}
      />
      <Paginator
        page={page}
        askPage={setPage}
        elementPerPage={elementPerPage}
        updateElementPerPage={setElementPerPage}
        elementPerPageRange={[10, 20, 50, 100]}
        list={raidersList}
      />
      <div style={{ width: '100%', display: 'flex', justifyContent: 'space-around' }}>
        <ClickableContainer
          onClick={() => {
            setExpandAll(!expandAll);
          }}
          style={btnStyle}
        >
          {expandAll ? 'Minimize Items' : 'Expand Items'}
        </ClickableContainer>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <div style={{ display: 'flex', alignItems: 'flex-start' }}>
            <QuestTargets
              setTarget={(quest) => {
                setSelectedQuestTarget(quest);
                let newSelectedRadiers = [...selectedRaiders];
                raidersList?.['hydra:member'].forEach((raider) => {
                  if (
                    raider['@id'] &&
                    newSelectedRadiers.includes(raider['@id']) &&
                    raider.questStatus === 1 &&
                    computeProfessionLevel(raider.questHerbalismExp ?? 0) < questLevel[quest]
                  ) {
                    newSelectedRadiers = newSelectedRadiers.filter((id) => id !== raider?.['@id']);
                  }
                });
                setSelectedRaiders(newSelectedRadiers);
              }}
              selectedQuestTarget={selectedQuestTarget}
            />
            <ClickableContainer onClick={() => {}} style={questBtnStyle}>
              <QuestActionButton
                raiders={
                  raidersList?.['hydra:member'].filter((r) => r['@id'] && selectedRaiders.includes(r['@id'])) ?? []
                }
                questTarget={selectedQuestTarget}
                onClick={() => {
                  for (const raider of raidersList?.['hydra:member'].filter(
                    (r) => r['@id'] && selectedRaiders.includes(r['@id'])
                  ) ?? []) {
                    dispatchTxStates({ raiderId: raider.id ?? 0, txState: { type: 'Create', txHash: '' } });
                  }
                  setSelectedRaiders([]);
                }}
              />
            </ClickableContainer>
            <ClickableContainer
              onClick={() => {
                setSelectedRaiders([]);
              }}
              style={btnStyle}
            >
              Unselect All
            </ClickableContainer>
            <ClickableContainer
              onClick={() => {
                const newSelectedItems: string[] = [];
                raidersList?.['hydra:member'].forEach((raider) => {
                  if (
                    raider['@id'] &&
                    (raider.questStatus !== 1 ||
                      (raider.questStatus === 1 &&
                        computeProfessionLevel(raider.questHerbalismExp ?? 0) >= questLevel[selectedQuestTarget]))
                  ) {
                    newSelectedItems.push(raider['@id']);
                  }
                });
                setSelectedRaiders(newSelectedItems);
              }}
              style={{ ...btnStyle }}
            >
              Select All raiders in page
            </ClickableContainer>
          </div>
        </div>
      </div>
      <div
        style={{
          marginTop: '10px',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center',
        }}
      >
        {!raiderIsLoading &&
          raidersList?.['hydra:member'].map((raider, index) => {
            return (
              <QuestRaider
                key={raider['@id']}
                raider={raider}
                isLoading={txStates[raider.id!] !== undefined && txStates[raider.id!].type === 'Mined'}
                isExpanded={expandedRaiders[index]}
                toggleExpand={() => {
                  setExpandedRaiders({ ...expandedRaiders, [index]: !expandedRaiders[index] });
                }}
                txState={txStates[raider.id!]}
                isSelected={selectedRaiders.includes(raider?.['@id'] ?? '')}
                isSelectable={
                  raider.questStatus !== 1 ||
                  (raider.questStatus === 1 &&
                    computeProfessionLevel(raider.questHerbalismExp ?? 0) >= questLevel[selectedQuestTarget])
                }
                toggleSelect={() => {
                  if (!raider?.['@id']) {
                    return;
                  }
                  let newSelectedRaiders = [...selectedRaiders];

                  if (newSelectedRaiders.includes(raider?.['@id'] ?? '')) {
                    newSelectedRaiders = newSelectedRaiders.filter((id) => id !== raider?.['@id']);
                  } else {
                    newSelectedRaiders.push(raider?.['@id']);
                  }
                  setSelectedRaiders(newSelectedRaiders);
                }}
              />
            );
          })}
        {raiderIsLoading && (
          <>
            {[...Array(elementPerPage)].map((_i, index) => {
              return (
                <QuestRaider
                  key={index}
                  isLoading={true}
                  isExpanded={expandedRaiders[index]}
                  toggleExpand={() => {}}
                  toggleSelect={() => {}}
                  isSelected={false}
                  isSelectable={false}
                />
              );
            })}
          </>
        )}
      </div>
    </div>
  );
};
