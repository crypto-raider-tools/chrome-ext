import { DetailedDungeon } from '../interfaces/detailedDungeon';
import { useState, useEffect } from 'react';
import { Rarity } from '@crypto-raider-tools/computation';
import { DropRateData, DungeonName } from './Context/DropRateContext';
import { fetchDungeonsData, fetchStuffStats } from '../Helpers/CompanionAPI';
import { StuffStats } from '../interfaces/stuffstats';
import { ItemListStatistics } from './CRCStatistics/ItemListStatistics';
import { isPremium } from '../Helpers/Utils';
import { DonationPopUp } from './Premium/DonationPopUp';
import { btnStyle } from './Styles/BaseStyles';
import { PopUpButton } from './Utils/PopUpButton';

type CRCStatisticsProps = {
  metamask: string;
};

export const CRCStatistics = (__props: CRCStatisticsProps) => {
  const [stuffStats, setStuffStats] = useState<StuffStats[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [dropRatesData, setDropRatesData] = useState<Record<DungeonName, DropRateData>>({});

  useEffect(() => {
    setIsLoading(true);
    const getStuffStats = async (): Promise<void> => {
      let savedStuffStats = localStorage.getItem('CRC_stuffStats');
      if (savedStuffStats) {
        const save = JSON.parse(savedStuffStats);
        const diffTime = Math.abs(Date.now() - save.date);
        if (diffTime / (1000 * 60 * 60) <= 1) {
          setStuffStats(save.stuffStats);
          await initDropRatesData(save.stuffStats);
          return Promise.resolve();
        }
      }
      const newStuffStats = await fetchStuffStats();
      const save = {
        date: Date.now(),
        stuffStats: newStuffStats,
      };
      localStorage.setItem('CRC_stuffStats', JSON.stringify(save));
      setStuffStats(newStuffStats);
      await initDropRatesData(newStuffStats);
      return Promise.resolve();
    };
    const initDropRatesData = async (stuffStats: StuffStats[]): Promise<void> => {
      const dungeons: DetailedDungeon[] = await fetchDungeonsData();
      const dropRatesData: Record<DungeonName, DropRateData> = {};
      for (const dungeon of dungeons) {
        if (!dungeon.name) {
          continue;
        }
        dropRatesData[dungeon.name] = {
          dungeon,
          rarityCounts: {
            uncommon: 0,
            common: 0,
            rare: 0,
            epic: 0,
            legendary: 0,
            mythic: 0,
            heirloom: 0,
            limited: 0,
          },
          totalItems: 0,
        };
        if (!dungeon.droppableItems) {
          continue;
        }
        dungeon.droppableItems.forEach((itemName: string | undefined) => {
          const dStuffStats = stuffStats.find((ss) => ss['@id'] === itemName);
          if (dStuffStats && dStuffStats.slot !== 'knickknack') {
            dropRatesData[dungeon.name!].rarityCounts[dStuffStats.rarity as Rarity] += dStuffStats.count ?? 0;
            dropRatesData[dungeon.name!].totalItems += dStuffStats.count ?? 0;
          }
        });
      }
      dropRatesData['Global'] = {
        dungeon: {},
        rarityCounts: {
          uncommon: 0,
          common: 0,
          rare: 0,
          epic: 0,
          legendary: 0,
          mythic: 0,
          heirloom: 0,
          limited: 0,
        },
        totalItems: 0,
      };
      stuffStats.forEach((item: StuffStats) => {
        if (item) {
          dropRatesData['Global'].rarityCounts[item.rarity as Rarity] += item.count ?? 0;
          dropRatesData['Global'].totalItems += item.count ?? 0;
        }
      });
      dropRatesData['NoDungeon'] = {
        dungeon: {},
        rarityCounts: {
          uncommon: 0,
          common: 0,
          rare: 0,
          epic: 0,
          legendary: 0,
          mythic: 0,
          heirloom: 0,
          limited: 0,
        },
        totalItems: 0,
      };
      stuffStats.forEach((item: StuffStats) => {
        if (!item.dungeons || item.dungeons.length === 0) {
          dropRatesData['NoDungeon'].rarityCounts[item.rarity as Rarity] += item.count ?? 0;
          dropRatesData['NoDungeon'].totalItems += item.count ?? 0;
        }
      });
      setDropRatesData(dropRatesData);
      return Promise.resolve();
    };
    getStuffStats();
    setIsLoading(false);
  }, []);

  const widgetStyle: React.CSSProperties = {
    display: 'flex',
    margin: '8px',
    justifyContent: 'center',
    alignItems: 'center',
  };
  if (!isPremium())
    return (
      <div>
        ⚠️You need to be premium to use this feature⚠️
        <PopUpButton
          buttonContent={<p style={{ fontWeight: '500', fontSize: '16px' }}>⭐Subscribe to Premium⭐</p>}
          buttonStyle={btnStyle}
        >
          <DonationPopUp />
        </PopUpButton>
      </div>
    );
  if (isLoading) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '100px',
        }}
      >
        🔁 Loading
      </div>
    );
  }
  return (
    <div style={widgetStyle}>
      {Object.keys(dropRatesData).length > 0 && (
        <ItemListStatistics stuffStats={stuffStats} dropRateData={dropRatesData} />
      )}
    </div>
  );
};
