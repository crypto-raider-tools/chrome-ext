import { useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { CustomFilters } from '../../interfaces/detailedcustomer';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { QuestFields, QuestFilters, QuestOrderBys } from '../../interfaces/quest';
import { RaiderSearchByGeneration } from '../RaiderSearch/RaiderFilters/RaiderSearchByGeneration';
import { RaiderSearchByLevel } from '../RaiderSearch/RaiderFilters/RaiderSearchByLevel';
import { CriteriaForRaiderSearch } from '../RaiderSearch/RaiderSearch';
import { btnStyle } from '../Styles/BaseStyles';
import { PlusMinusIncrementor } from '../Utils/PlusMinusIncrementor';

interface QuestFieldsSelectProps {
  customerFilters: Partial<CustomFilters>;
  fields: QuestFields;
  orderBys: QuestOrderBys;
  filters: QuestFilters;
  setFields: (value: QuestFields) => void;
  setOrderBys: (value: QuestOrderBys) => void;
  setFilters: (value: QuestFilters) => void;
}

const questFieldsDisplayName: Partial<Record<keyof DetailedSyncRaider, string>> = {
  mountId: 'Mount',
  id: 'Raider Id',
  level: 'Level',
  generation: 'Generation',
  questName: 'Quest Name',
  questStatus: 'Quest Status',
  //questStartedTime: 'Quest Started Time',
  //questEndedTime: 'Quest Ended Time',
  //questDuration: 'Quest Duration',
  questReturnTime: 'Quest Return Time',
  //questTotalReturnTime: 'Quest Total Return Time',
  questNextReward: 'Quest Next Reward',
  questEarnedReward: 'Quest Earned Rewards',
  questExpRewardAmount: 'Quest Earned Exp',
  questHerbalismExp: 'Herbalism Exp',
};

export const QuestFieldsSelect = (props: QuestFieldsSelectProps) => {
  const [customerFilters] = useState<Partial<CustomFilters> | undefined>(
    JSON.parse(localStorage.getItem('CRC_customer') ?? '{"filters":[]}').filters
  );
  const [selectsHover, setSelectsHover] = useState<Record<string, boolean>>({});
  const selectStyle: React.CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    color: 'white',
    width: '300px',
    height: '30px',
    marginLeft: '8px',
    borderRadius: '2px',
  };

  useEffect(() => {
    /**
     * For boolean filters, we need to set the default value to false
     */
    if (props.fields.includes('mountId') && !props.filters.mountId) {
      props.setFilters({ ...props.filters, mountId: '0' });
    }
  }, [props]);

  const orderBySelect = (field: keyof DetailedSyncRaider) => {
    return (
      <select
        onChange={(v) => {
          if (v.target.value === 'none') {
            const newOrderBys = { ...props.orderBys };
            delete newOrderBys[field];
            props.setOrderBys(newOrderBys);
          } else {
            props.setOrderBys({ ...props.orderBys, [field]: v.target.value as 'asc' | 'desc' });
          }
        }}
        style={{ ...selectStyle, width: '64px' }}
      >
        <option value="none">None</option>
        <option value="asc">Asc</option>
        <option value="desc">Desc</option>
      </select>
    );
  };

  const multipleSelect = (field: keyof DetailedSyncRaider, values: string[], display: string[]) => {
    let hoverHeight = 25 * values.length;
    if (hoverHeight > 200) hoverHeight = 200;
    return (
      <div style={{ width: '125px', height: '25px', position: 'relative', margin: '0px 8px' }}>
        <select
          multiple
          onChange={(e) => {
            let newValue = Array.from(e.currentTarget.options)
              .filter(function (option) {
                return option.selected;
              })
              .map(function (option) {
                return option.value;
              });
            props.setFilters({ ...props.filters, [field]: newValue.join(',') });
          }}
          style={{
            ...selectStyle,
            position: 'absolute',
            height: selectsHover[field] ? hoverHeight + 'px' : '25px',
            width: '125px',
            transition: '0.5s',
            zIndex: '11',
            top: '0',
            left: '0',
          }}
          onMouseEnter={() => {
            const newSelectsHover = { ...selectsHover };
            newSelectsHover[field] = true;
            setSelectsHover(newSelectsHover);
          }}
          onMouseLeave={() => {
            const newSelectsHover = { ...selectsHover };
            newSelectsHover[field] = false;
            setSelectsHover(newSelectsHover);
          }}
        >
          {values.map((v, index) => {
            return (
              <option key={`optionMultileSelect-${display[index]}`} value={v}>
                {display[index]}
              </option>
            );
          })}
        </select>
      </div>
    );
  };

  return (
    <>
      <select
        name="field"
        value="Select filter and order by fields"
        onChange={(v) =>
          props.fields.indexOf(v.target.value as keyof DetailedSyncRaider) === -1
            ? props.setFields([...props.fields, v.target.value as keyof DetailedSyncRaider])
            : null
        }
        style={selectStyle}
      >
        <option>Select filter and order by fields</option>
        {Object.keys(questFieldsDisplayName).map((field) => {
          if (props.fields.findIndex((f) => f === field) === -1)
            return (
              <option key={'Select-' + field} value={field}>
                {questFieldsDisplayName[field as keyof DetailedSyncRaider]}
              </option>
            );
          return null;
        })}
      </select>
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {props.fields.map((field) => {
          return (
            <div
              key={'Fields-' + field}
              style={{
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                ...btnStyle,
                borderColor: colorPalette['mediumBlueGrey'],
                borderWidth: '2px',
                backgroundColor: '',
              }}
            >
              <button
                onClick={() => {
                  props.setFields(props.fields.filter((f) => f !== field));

                  const newOrderBys = { ...props.orderBys };
                  delete newOrderBys[field];
                  props.setOrderBys(newOrderBys);

                  const newFilters = { ...props.filters };
                  delete newFilters[field];
                  props.setFilters(newFilters);
                }}
                style={{ marginRight: '8px' }}
              >
                ❌
              </button>
              <div>{questFieldsDisplayName[field as keyof DetailedSyncRaider]}</div>
              {(() => {
                switch (field) {
                  case 'id':
                    return (
                      <>
                        {props.customerFilters &&
                          props.customerFilters.raiders &&
                          multipleSelect(
                            field,
                            Object.keys(props.customerFilters.raiders).map((r) => '' + r),
                            Object.keys(props.customerFilters.raiders).map(
                              (r) => `${r} (${customerFilters!.raiders![Number(r)]})`
                            )
                          )}
                        {orderBySelect(field)}
                      </>
                    );
                  case 'generation':
                    return (
                      <>
                        <RaiderSearchByGeneration
                          criteria={{
                            multiple: false,
                            name: '',
                            placeholder: undefined,
                            variable: 'raw_data',
                          }}
                          onInputChange={(__criteria: CriteriaForRaiderSearch, value: string | undefined) => {
                            if (value === '') {
                              const newFilters = { ...props.filters };
                              delete newFilters[field];
                              props.setFilters(newFilters);
                            } else {
                              props.setFilters({ ...props.filters, [field]: value });
                            }
                          }}
                          maxGeneration={
                            customerFilters?.generations
                              ? customerFilters?.generations.reduce((previousValue, currentValue) => {
                                  return Math.max(previousValue, currentValue);
                                }, 6)
                              : 6
                          }
                        />
                        {orderBySelect(field)}
                      </>
                    );
                  case 'level':
                    const maxLevel = customerFilters?.levels
                      ? customerFilters?.levels.reduce((previousValue, currentValue) => {
                          return Math.max(previousValue, currentValue);
                        }, 10)
                      : 10;
                    return (
                      <>
                        <RaiderSearchByLevel
                          criteria={{
                            multiple: false,
                            name: '',
                            placeholder: undefined,
                            variable: 'raw_data',
                          }}
                          onInputChange={(__criteria: CriteriaForRaiderSearch, value: string | undefined) => {
                            if (value === '') {
                              const newFilters = { ...props.filters };
                              delete newFilters[field];
                              props.setFilters(newFilters);
                            } else {
                              props.setFilters({ ...props.filters, [field]: value });
                            }
                          }}
                          maxLevel={maxLevel}
                          style={{ margin: '0px 16px', width: `${maxLevel * 16}px` }}
                        />
                        {orderBySelect(field)}
                      </>
                    );
                  case 'questName':
                    return (
                      <>
                        {multipleSelect(field, [`Grimweed`, `Newt`, `SporeBark`], [`Grimweed`, `Newt`, `SporeBark`])}
                        {orderBySelect(field)}
                      </>
                    );
                  case 'questStatus':
                    return (
                      <>
                        {multipleSelect(
                          field,
                          ['1', '2', '3', '4'],
                          [`Not Questing`, `In Quest`, `Coming Back`, `Collect Reward`]
                        )}
                        {orderBySelect(field)}
                      </>
                    );
                  case 'questStartedTime':
                  case 'questEndedTime':
                  case 'questDuration':
                  case 'questNextReward':
                  case 'questReturnTime':
                  case 'questTotalReturnTime':
                    return <>{orderBySelect(field)}</>;
                  case 'questEarnedReward':
                  case 'questExpRewardAmount':
                  case 'questHerbalismExp':
                    const values = ('' + props.filters[field]).split(',');
                    const min = values.length > 0 && values[0] !== 'undefined' ? values[0] : 0;
                    const max = values.length > 1 && values[1] !== 'undefined' ? values[1] : 10;
                    console.log('Vlues', values);
                    return (
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div
                          style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginLeft: '8px' }}
                        >
                          Min
                          <PlusMinusIncrementor
                            minValue={0}
                            maxValue={100000}
                            value={Number(min)}
                            setValue={(value) => {
                              props.setFilters({ ...props.filters, [field]: value + ',' + max });
                            }}
                          />
                        </div>
                        <div
                          style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginLeft: '8px' }}
                        >
                          Max
                          <PlusMinusIncrementor
                            minValue={0}
                            maxValue={100000}
                            value={Number(max)}
                            increment={100}
                            setValue={(value) => {
                              props.setFilters({ ...props.filters, [field]: min + ',' + value });
                            }}
                          />
                        </div>
                        {orderBySelect(field)}
                      </div>
                    );
                  case 'mountId':
                    return (
                      <input
                        style={{ marginLeft: '8px', backgroundColor: colorPalette['mediumBlueGrey'] }}
                        onChange={(event) => {
                          props.setFilters({ ...props.filters, [field]: event.currentTarget.checked ? '1' : '0' });
                        }}
                        type="checkbox"
                        name={field}
                      />
                    );
                }
                return <></>;
              })()}
            </div>
          );
        })}
      </div>
    </>
  );
};
