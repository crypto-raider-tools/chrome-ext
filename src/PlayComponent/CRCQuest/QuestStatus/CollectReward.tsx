import { herbalismIcon } from '../../../Helpers/Constants';
import { questToIcon } from '../../../Helpers/Utils';
import { RaiderQuestStatus } from '../../../interfaces/quest';

interface CollectRewardProps {
  raiderQuestStatus: RaiderQuestStatus;
  style?: React.CSSProperties;
}

export const CollectReward = (props: CollectRewardProps) => {
  return (
    <div>
      <p>{`You can collect :`}</p>
      <p style={{ display: 'flex', fontWeight: '500', color: 'white' }}>
        {props.raiderQuestStatus.earnedReward}
        <img
          style={{ width: '25px', height: '25px', imageRendering: 'pixelated', marginLeft: '4px' }}
          alt={props.raiderQuestStatus.questName}
          src={questToIcon(props.raiderQuestStatus.questName!)}
        />
      </p>
      <p>{`And`}</p>
      <p style={{ display: 'flex', fontWeight: '500', color: 'white' }}>
        {props.raiderQuestStatus.expRewardAmount + ' xp'}
        <img
          style={{ width: '25px', height: '25px', imageRendering: 'pixelated', marginLeft: '4px' }}
          alt={'herbalismIcon'}
          src={herbalismIcon}
        />
      </p>
    </div>
  );
};
