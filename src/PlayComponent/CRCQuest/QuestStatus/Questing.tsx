import { colorPalette, humanizeBaseConfig } from '../../../Helpers/Constants';
import { RaiderQuestStatus } from '../../../interfaces/quest';
const humanizeDuration = require('humanize-duration');

interface QuestingProps {
  raiderQuestStatus: RaiderQuestStatus;
  style?: React.CSSProperties;
}

export const Questing = (props: QuestingProps) => {
  const textStyle: React.CSSProperties = {
    fontWeight: '500',
    color: colorPalette['lightBlueGrey'],
  };
  return (
    <div>
      <p style={textStyle}>
        Questing since <br />
        <span style={{ color: 'white' }}>
          {humanizeDuration((props.raiderQuestStatus.questDuration ?? 0) * 1000, humanizeBaseConfig)}
        </span>
        <br />
        <span>Next Reward in :</span>
        <br />
        <span style={{ color: 'white' }}>
          {humanizeDuration((props.raiderQuestStatus.nextReward ?? 0) * 1000, humanizeBaseConfig)}
        </span>
        <br />
        <span style={textStyle}>Expect to be home in :</span>
        <br />
        <span style={{ color: 'white' }}>
          {humanizeDuration(((props.raiderQuestStatus.questDuration ?? 0) * 1000) / 4, humanizeBaseConfig)}
        </span>
      </p>
    </div>
  );
};
