import { humanizeBaseConfig } from '../../../Helpers/Constants';
import { RaiderQuestStatus } from '../../../interfaces/quest';
import { ProgressBar } from '../../Utils/ProgressBar';
const humanizeDuration = require('humanize-duration');

interface ComingBackProps {
  raiderQuestStatus: RaiderQuestStatus;
  style?: React.CSSProperties;
}

export const ComingBack = (props: ComingBackProps) => {
  return (
    <div style={{ height: 'fit-content' }}>
      <p>Will be home in </p>

      <ProgressBar
        value={(1 - props.raiderQuestStatus.returnTime! / props.raiderQuestStatus.totalReturnTime!) * 100}
        content={humanizeDuration(props.raiderQuestStatus.returnTime! * 1000, humanizeBaseConfig)}
      />
    </div>
  );
};
