import { useEffect, useState } from 'react';
import { collectReward, comingBack, inQuest, notQuesting } from '../../Helpers/Constants';
import { wordToCamelCase } from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { QuestAction, QuestName } from '../../interfaces/quest';
interface QuestActionButtonProps {
  raiders: Array<DetailedSyncRaider>;
  questTarget: QuestName;
  onClick?: () => void;
}

export const QuestActionButton = (props: QuestActionButtonProps) => {
  const [canInteract, setCanInteract] = useState<boolean>(false);
  const [questActions, setQuestActions] = useState<Record<QuestAction, number>>({
    startQuest: 0,
    endQuest: 0,
    getReward: 0,
    teleport: 0,
    equipMount: 0,
    unEquipMount: 0,
  });
  const [totalActionNumber, setTotalActionNumber] = useState<number>(0);

  useEffect(() => {
    setCanInteract(props.raiders.length > 0);
    const newQuestActions = {
      startQuest: 0,
      endQuest: 0,
      getReward: 0,
      teleport: 0,
      equipMount: 0,
      unEquipMount: 0,
    };
    let newTotalActionNumber = 0;
    props.raiders.forEach((r) => {
      switch (r.questStatus) {
        case notQuesting:
          newQuestActions.startQuest += 1;
          newTotalActionNumber += 1;
          break;
        case inQuest:
          newQuestActions.endQuest += 1;
          newTotalActionNumber += 1;
          break;
        case comingBack:
          newQuestActions.teleport += 1;
          newTotalActionNumber += 1;
          break;
        case collectReward:
          newQuestActions.getReward += 1;
          newTotalActionNumber += 1;
          break;
      }
    });
    setQuestActions(newQuestActions);
    setTotalActionNumber(newTotalActionNumber);
  }, [props.raiders]);

  const btnStyle: React.CSSProperties = {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'left',
  };
  return (
    <div style={{ ...btnStyle, pointerEvents: canInteract ? 'all' : 'none' }}>
      <div
        onClick={() => {
          if (props.onClick) props.onClick();
        }}
        dangerouslySetInnerHTML={{
          __html: `<div style="position:absolute; width: 100%; height: 100%;" onclick="window.crCompanion.requestWeb3([
          ${props.raiders
            .map((raider) => {
              let method: QuestAction = 'startQuest';
              switch (raider.questStatus) {
                case notQuesting:
                  method = 'startQuest';
                  break;
                case inQuest:
                  method = 'endQuest';
                  break;
                case comingBack:
                  method = 'teleport';
                  break;
                case collectReward:
                  method = 'getReward';
                  break;
              }
              let quest = props.questTarget;
              switch (method) {
                case 'startQuest':
                  break;
                case 'endQuest':
                case 'teleport':
                case 'getReward':
                  quest = raider.questName ?? quest;
                  break;
              }
              return `{ type: 'QuestRequest', quest: '${quest}', action: '${method}', raiderId: ${raider.id}}`;
            })
            .join(',')}]);
        "></div>`,
        }}
      />
      {Object.keys(questActions).map((q) => {
        if (questActions[q as QuestAction] === 0) return '';
        return (
          <p key={`questActionButton-${q}`}>
            {wordToCamelCase(q)} {questActions[q as QuestAction]}
          </p>
        );
      })}
      {totalActionNumber === 0 && <p>No selected raiders</p>}
    </div>
  );
};
