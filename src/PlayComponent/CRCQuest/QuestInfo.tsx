import { collectReward, comingBack, inQuest, notQuesting } from '../../Helpers/Constants';
import { questToIcon } from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { RaiderQuestStatus } from '../../interfaces/quest';
import { CollectReward } from './QuestStatus/CollectReward';
import { ComingBack } from './QuestStatus/ComingBack';
import { Questing } from './QuestStatus/Questing';
interface QuestInfoProps {
  raider: DetailedSyncRaider;
  raiderQuestStatus: RaiderQuestStatus;
  style?: React.CSSProperties;
}

export const QuestInfo = (props: QuestInfoProps) => {
  return (
    <div>
      {props.raiderQuestStatus.questStatus !== notQuesting && (
        <p style={{ fontSize: '1.1em', display: 'flex', fontWeight: '500', alignItems: 'center' }}>
          {props.raiderQuestStatus.questName && (
            <img
              style={{ width: '25px', height: '25px', imageRendering: 'pixelated', marginRight: '4px' }}
              alt={props.raiderQuestStatus.questName}
              src={questToIcon(props.raiderQuestStatus.questName)}
            />
          )}
          {props.raiderQuestStatus.questName}
        </p>
      )}
      {props.raiderQuestStatus.questStatus === inQuest && <Questing raiderQuestStatus={props.raiderQuestStatus} />}
      {props.raiderQuestStatus.questStatus === comingBack && <ComingBack raiderQuestStatus={props.raiderQuestStatus} />}
      {props.raiderQuestStatus.questStatus === collectReward && (
        <CollectReward raiderQuestStatus={props.raiderQuestStatus} />
      )}
    </div>
  );
};
