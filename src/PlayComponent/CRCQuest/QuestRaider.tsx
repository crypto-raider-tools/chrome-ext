import { useEffect, useState } from 'react';
import { colorPalette, herbalismIcon, notQuesting } from '../../Helpers/Constants';
import { extractRaiderQuestStatus, questStatusToName, questToIcon } from '../../Helpers/Utils';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { RaiderQuestStatus } from '../../interfaces/quest';
import { TxState } from '../../interfaces/web3Inject';
import { RaiderPicture } from '../Raider/RaiderPicture';
import { BoxShadow, ClickableContainer } from '../Utils/ClickableContainer';
import { ProfessionLevel } from './ProfessionLevel';
import { QuestInfo } from './QuestInfo';

type QuestRaiderProps = {
  isLoading: boolean;
  isExpanded: boolean;
  raider?: DetailedSyncRaider;
  toggleExpand: () => void;
  isSelected: boolean;
  toggleSelect: () => void;
  isSelectable: boolean;
  txState?: TxState;
};
export const QuestRaider = (props: QuestRaiderProps) => {
  const [mountImage, setMountImage] = useState<string>('');
  const [raiderQuestStatus, setRaiderQuestStatus] = useState<RaiderQuestStatus>(extractRaiderQuestStatus(props.raider));

  useEffect(() => {
    setRaiderQuestStatus(extractRaiderQuestStatus(props.raider));
  }, [props.raider]);

  useEffect(() => {
    const getFetchMountInfo = async () => {
      if (raiderQuestStatus.mountId) {
        const result = await fetch(`https://api.cryptoraiders.xyz/mount/${raiderQuestStatus.mountId}`);
        const data = await result.json();
        setMountImage(data.image);
      }
    };
    getFetchMountInfo();
  }, [raiderQuestStatus.mountId]);

  const boxShadows: string[] = [
    BoxShadow(colorPalette[props.raider?.race ?? 'human'], { x: 6, y: 6 }, 15, 1, true),
    BoxShadow(colorPalette['white'], { x: -6, y: -6 }, 15, 1, true),
  ];
  const selectedStyle: React.CSSProperties = {
    boxShadow: boxShadows[0] + ', ' + boxShadows[1],
    transform: 'scale(1.15)',
  };

  return (
    <div
      style={{
        background: 'rgb(45, 55, 72)',
        borderRadius: '3px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: props.isExpanded ? '310px' : '115px',
        height: 'fit-content',
        margin: '2px',
        position: 'relative',
      }}
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
        props.toggleExpand();
      }}
    >
      {props.txState && props.txState.type !== 'Mined' && (
        <div
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colorPalette['blackOpaque'],
            fontSize: '15px',
            fontWeight: '500',
            zIndex: '10',
          }}
        >
          <div
            style={{ width: '20px', height: '20px', alignSelf: 'center', marginBottom: '30px', transform: 'scale(2)' }}
          >
            <div className="lds-dual-ring" style={{ width: '20px', height: '20px' }} />
          </div>
          {props.txState.type === 'Create' && (
            <p>
              Building the
              <br />
              Transaction
            </p>
          )}
          {props.txState.type === 'Sent' && (
            <p>
              Processing
              <br />
              Transaction
            </p>
          )}
        </div>
      )}
      {!props.isSelectable && !props.isLoading && (
        <div
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colorPalette['blackOpaque'],
            fontSize: '15px',
            fontWeight: '500',
            zIndex: '10',
            textAlign: 'center',
          }}
        >
          <p>
            Herbalism lvl
            <br />
            is too low
          </p>
        </div>
      )}
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          width: '115px',
        }}
      >
        <ClickableContainer
          onClick={() => {
            props.toggleSelect();
          }}
          className={props.isLoading ? 'loading' : ''}
          style={{
            margin: '5px',
            width: '106px',
            height: '106px',
            overflow: 'hidden',
            borderRadius: '1px',
            borderColor: colorPalette[props.raider?.race ?? 'human'],
            borderWidth: '3px',
            ...(props.isLoading ? {} : { background: 'rgb(46, 46, 46)' }),
            ...(props.isSelected ? selectedStyle : {}),
          }}
          boxShadows={[
            BoxShadow(colorPalette[props.raider?.race ?? 'human'], { x: 4, y: 4 }),
            BoxShadow(colorPalette[props.raider?.race ?? 'human'], { x: -4, y: -4 }),
          ]}
          disabled={!props.isSelectable}
        >
          <div style={{ position: 'relative' }}>
            {!props.isLoading && props.raider && <RaiderPicture raider={props.raider.raw_data!} pictureSize={100} />}
            {!props.isLoading && raiderQuestStatus.mountId && (
              <div
                style={{
                  position: 'absolute',
                  right: '0px',
                  top: '0px',
                  width: '50px',
                  height: '50px',
                  imageRendering: 'pixelated',
                  zIndex: '10',
                }}
                className={mountImage === '' ? 'loading' : ''}
              >
                {mountImage !== '' && <img src={mountImage} alt="mount" />}
              </div>
            )}
            {!props.isLoading && (
              <p
                style={{
                  position: 'absolute',
                  bottom: '0px',
                  width: '100%',
                  zIndex: '10',
                  textAlign: 'center',
                  fontSize: '13px',
                  fontWeight: '500',
                  color: 'white',
                }}
              >
                {props.raider?.id}
              </p>
            )}
          </div>
        </ClickableContainer>
        <div
          style={{
            width: '115px',
            margin: '5x 0px',
            overflow: 'hidden',
            height: 'fit-content',
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'nowrap',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            alignContent: 'center',
            textAlign: 'center',
            userSelect: 'none',
          }}
        >
          <p style={{ fontSize: '1.1em', fontWeight: '500', display: 'flex', alignItems: 'center' }}>
            {questStatusToName(raiderQuestStatus.questStatus)}
          </p>
          {raiderQuestStatus.questStatus > notQuesting && (
            <div
              style={{
                width: '100%',
                textAlign: 'center',
                fontSize: '1em',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                fontWeight: '500',
                color: colorPalette['lightBlueGrey'],
              }}
              className={props.isLoading ? 'loading' : ''}
            >
              <span>{`Earned`}&nbsp;</span>
              <p style={{ display: 'flex', fontWeight: '500', color: 'white' }}>
                {raiderQuestStatus.earnedReward}
                {raiderQuestStatus.questName && (
                  <img
                    style={{ width: '25px', height: '25px', imageRendering: 'pixelated', marginLeft: '4px' }}
                    alt={raiderQuestStatus.questName}
                    src={questToIcon(raiderQuestStatus.questName)}
                  />
                )}
              </p>
              <p style={{ display: 'flex', fontWeight: '500', color: 'white' }}>
                {raiderQuestStatus.expRewardAmount + ' xp'}
                <img
                  style={{ width: '25px', height: '25px', imageRendering: 'pixelated', marginLeft: '4px' }}
                  alt={raiderQuestStatus.questName}
                  src={herbalismIcon}
                />
              </p>
            </div>
          )}
        </div>
      </div>
      {props.isExpanded && (
        <div
          style={{
            margin: '5px',
            height: 'fit-content',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <div
            className={props.isLoading ? 'loading' : ''}
            style={{ whiteSpace: 'nowrap', height: '80px', width: '180px' }}
          >
            {props.isLoading && <>&nbsp;</>}
            {!props.isLoading && (
              <ProfessionLevel experience={raiderQuestStatus.herbalismExp ?? 0} professionName={'Herbalism'} />
            )}
          </div>
          <div className={props.isLoading ? 'loading' : ''} style={{ whiteSpace: 'nowrap' }} title={'Quest Info'}>
            {props.isLoading && <>&nbsp;</>}
            {!props.isLoading && <QuestInfo raiderQuestStatus={raiderQuestStatus} raider={props.raider!} />}
          </div>
        </div>
      )}
    </div>
  );
};
