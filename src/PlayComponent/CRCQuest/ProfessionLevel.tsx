import { alchemyIcon, herbalismIcon, levelSocketIcon } from '../../Helpers/Constants';
import { ProgressBar } from '../Utils/ProgressBar';

type ProfessionLevelProps = {
  experience: number;
  professionName: string;
  style?: React.CSSProperties;
};

const nextLevelExp = (level: number) => {
  if (level === 1) {
    return 90;
  } else {
    return 90 * 1.1 ** (level - 1);
  }
};

export const computeProfessionLevel = (exp: number): number => {
  if (exp < 90) return 1;
  let level = 1;
  let total = 0;
  while (total < exp) {
    total = total + nextLevelExp(level);
    if (total <= exp) level++;
  }
  return level;
};

const getLevelFloorXp = (level: number) => {
  if (level === 1) return 0;
  let exp = 0;
  for (let i = 1; i < level; i++) exp += nextLevelExp(i);
  return exp;
};

export const ProfessionLevel = (props: ProfessionLevelProps) => {
  const level = computeProfessionLevel(props.experience);
  const floorXp = getLevelFloorXp(level);
  const nextLevelXp = nextLevelExp(level);
  const expPurcentage = ((props.experience - floorXp) / nextLevelXp) * 100;

  const levelTextStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    textAlign: 'center',
    margin: '1px 8px',
  };

  let professionIcon;
  switch (props.professionName) {
    case 'Herbalism':
      professionIcon = herbalismIcon;
      break;
    case 'Alchemy':
      professionIcon = alchemyIcon;
      break;
  }
  return (
    <div style={{ display: 'flex', alignItems: 'center', ...props.style }}>
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'start', alignItems: 'center' }}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <img
            style={{ width: '40px', height: '40px', imageRendering: 'pixelated', marginLeft: '4px' }}
            alt={props.professionName}
            src={professionIcon}
          />
          <p style={levelTextStyle}>{props.professionName}</p>
        </div>
        <ProgressBar
          value={expPurcentage}
          content={props.experience + ' / ' + Math.floor(floorXp + nextLevelXp) + ' XP'}
        />
      </div>
      <div
        style={{
          width: '35px',
          height: '35px',
          imageRendering: 'pixelated',
          marginLeft: '10px',
          backgroundImage: `url(${levelSocketIcon})`,
          backgroundPosition: 'center center',
          backgroundSize: '35px',
          alignContent: 'center',
          alignItems: 'center',
        }}
      >
        <p style={{ width: '100%', textAlign: 'center', fontSize: '18px', fontWeight: '500', marginTop: '3px' }}>
          {level}
        </p>
      </div>
    </div>
  );
};
