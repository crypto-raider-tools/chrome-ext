import { colorPalette, grimWeedIcon, newtIcon, sporeBarkIcon } from '../../Helpers/Constants';
import { QuestName } from '../../interfaces/quest';
import { ClickableContainer } from '../Utils/ClickableContainer';

interface QuestTargetsProps {
  setTarget: (value: QuestName) => void;
  selectedQuestTarget: QuestName;
}

export const QuestTargets = (props: QuestTargetsProps) => {
  const questTargetBtnStyle: React.CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    borderRadius: '150px',
    color: 'white',
    fontWeight: '500',
    width: '25px',
    height: '25px',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    opacity: '0.75',
  };

  const questTargetBtnSelectedStyle: React.CSSProperties = {
    transform: 'scale(1.5)',
    pointerEvents: 'none',
    backgroundColor: colorPalette['lightBlueGrey'],
  };
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: colorPalette['mediumBlueGrey'],
        borderRadius: '8px',
        borderWidth: '2px',
        borderColor: colorPalette['mediumBlueGrey'],
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center',
          backgroundColor: colorPalette['darkBlueGrey'],
          borderTopLeftRadius: '8px',
          borderTopRightRadius: '8px',
          height: '35px',
        }}
      >
        <ClickableContainer
          onClick={() => {
            props.setTarget('Grimweed');
          }}
          style={{
            ...questTargetBtnStyle,
            ...(props.selectedQuestTarget === 'Grimweed' ? questTargetBtnSelectedStyle : {}),
          }}
        >
          <img
            style={{ width: '25px', height: '25px', imageRendering: 'pixelated' }}
            alt={'Select Grimweed'}
            src={grimWeedIcon}
          />
        </ClickableContainer>
        <ClickableContainer
          onClick={() => {
            props.setTarget('Newt');
          }}
          style={{
            ...questTargetBtnStyle,
            ...(props.selectedQuestTarget === 'Newt' ? questTargetBtnSelectedStyle : {}),
          }}
        >
          <img
            style={{ width: '25px', height: '25px', imageRendering: 'pixelated' }}
            alt={'Select Newt'}
            src={newtIcon}
          />
        </ClickableContainer>
        <ClickableContainer
          onClick={() => {
            props.setTarget('Sporebark');
          }}
          style={{
            ...questTargetBtnStyle,
            ...(props.selectedQuestTarget === 'Sporebark' ? questTargetBtnSelectedStyle : {}),
          }}
        >
          <img
            style={{ width: '25px', height: '25px', imageRendering: 'pixelated' }}
            alt={'Select Sporebark'}
            src={sporeBarkIcon}
          />
        </ClickableContainer>
      </div>
      <div style={{ width: '100%', color: 'white', fontWeight: '500' }}>Select Quest Target</div>
    </div>
  );
};
