import { useContext, useEffect } from 'react';
import { colorPalette, tammyAvatarTriggerLSName } from '../../Helpers/Constants';
import { isPremium } from '../../Helpers/Utils';
import { AvatarTrigger, TammyAvatar } from '../../interfaces/tammyAvatar';
import TammyAvatarContext from '../Context/TammyAvatarContext';

export type TammySkinSelectorProps = {
  style?: React.CSSProperties;
};

export const TammySkinSelector = (props: TammySkinSelectorProps) => {
  const { tammyAvatarTrigger, setTammyAvatarTrigger } = useContext(TammyAvatarContext);

  useEffect(() => {
    const localValue = localStorage.getItem(tammyAvatarTriggerLSName);
    if (localValue) setTammyAvatarTrigger(JSON.parse(localValue));
  }, [setTammyAvatarTrigger]);

  let pictureSize: number = 80;
  let transformScale: number = pictureSize / 224;

  const buttonStyle: React.CSSProperties = {
    border: '4px solid ' + colorPalette['experience'],
    backgroundColor: colorPalette['lightBlueGrey'],
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    borderRadius: '50%',
    MozBorderRadius: '50%',
    WebkitBorderRadius: '50%',
    height: '22px',
    width: '22px',
    marginTop: '5px',
  };

  const mainStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['darkBlueGrey'],
    display: 'flex',
    height: pictureSize + 'px',
    width: pictureSize + 'px',
    ...(isPremium() ? { cursor: 'pointer' } : { opacity: '0.5', pointerEvents: 'none' }),
    ...props.style,
  };

  const handleTammyAvatarTrigger = async (avatarPart: string) => {
    const trigger: AvatarTrigger = { ...tammyAvatarTrigger };
    trigger[avatarPart as keyof AvatarTrigger] = !trigger[avatarPart as keyof AvatarTrigger];

    await chrome.storage.local.set({ TammyAvatarTrigger: trigger });
    localStorage.setItem('TammyAvatarTrigger', JSON.stringify(trigger));
    setTammyAvatarTrigger(trigger);
  };

  return (
    <div style={mainStyle}>
      <div style={{ width: '100%', height: '100%' }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'center',
            position: 'relative',
            width: '100%',
            height: '100%',
            transform: 'scale(' + transformScale + ')',
          }}
        >
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/body/' + TammyAvatar.body + '.png)',
              ...(tammyAvatarTrigger.body ? {} : { opacity: '0.5' }),
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/face/' + TammyAvatar.face + '.png)',
              ...(tammyAvatarTrigger.face ? {} : { opacity: '0.5' }),
            }}
          />
          <div
            className="raider-image-animated"
            style={{
              height: '96px',
              backgroundImage:
                'url(https://crypto-raiders-assets.storage.googleapis.com/char_new/hair/' + TammyAvatar.hair + '.png)',
              ...(tammyAvatarTrigger.hair ? {} : { opacity: '0.5' }),
            }}
          />
        </div>
      </div>
      <div style={{ width: '100%', height: '100%' }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <div
            onClick={() => {
              handleTammyAvatarTrigger('hair');
            }}
            style={{
              ...buttonStyle,
              ...(tammyAvatarTrigger.hair ? { backgroundColor: colorPalette['mediumBlueGrey'] } : {}),
            }}
          />
          <div
            onClick={() => {
              handleTammyAvatarTrigger('face');
            }}
            style={{
              ...buttonStyle,
              ...(tammyAvatarTrigger.face ? { backgroundColor: colorPalette['mediumBlueGrey'] } : {}),
            }}
          />{' '}
          <div
            onClick={() => {
              handleTammyAvatarTrigger('body');
            }}
            style={{
              ...buttonStyle,
              ...(tammyAvatarTrigger.body ? { backgroundColor: colorPalette['mediumBlueGrey'] } : {}),
            }}
          />
        </div>
      </div>
    </div>
  );
};
