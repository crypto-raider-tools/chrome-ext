import { useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { H2 } from '../Theme';
import { PlusMinusIncrementor } from '../Utils/PlusMinusIncrementor';

interface DonationPopUpProps {
  style?: React.CSSProperties;
}

export const DonationPopUp = (props: DonationPopUpProps) => {
  const [weeks, setWeeks] = useState<number>(1);
  const [price] = useState<number>(2);
  const [isProcessingTx, setIsProcessingTx] = useState<boolean>(false);
  const popupContainerStyle: React.CSSProperties = {
    width: '270px',
    height: '200px',
    borderRadius: '2px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '16px',
    backgroundColor: colorPalette['darkBlueGrey'],
    ...props.style,
  };
  const btnStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['mediumBlue'],
    marginTop: '4px',
    cursor: 'pointer',
  };

  const image128pxStyle: React.CSSProperties = {
    height: '30px',
    marginLeft: '5px',
  };

  return (
    <div
      style={popupContainerStyle}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      {!isProcessingTx ? (
        <div>
          <H2>Get Premium</H2>
          <p>1 week premium is worth {price} Matic</p>
          <p>Select how many weeks you want</p>
          <div style={{ display: 'flex', justifyContent: 'space-between', margin: '10px' }}>
            <PlusMinusIncrementor minValue={1} maxValue={999} value={weeks} setValue={setWeeks} />
            <div style={{ display: 'flex', justifyContent: 'end' }}>
              <p>{`${price * weeks} Matic`}</p>
              <img
                style={{ ...image128pxStyle, imageRendering: 'pixelated' }}
                src={chrome.runtime.getURL('img/matic.png')}
                alt={'Matic Icon'}
              />
            </div>
          </div>
          <div
            onClick={() => {
              setIsProcessingTx(true);
            }}
            style={btnStyle}
            dangerouslySetInnerHTML={{
              __html: `<div style="padding: 1px 5px" onclick="window.crCompanion.requestWeb3([{ type: 'PremiumDonation', amount: ${
                weeks * price
              }}]);"><p>Get ${weeks} weeks Premium</p></div>`,
            }}
          />
        </div>
      ) : (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <H2>Processing Transaction Please Wait..</H2>
          <div style={{ width: '50px', height: '50px', alignSelf: 'center', margin: '15px' }}>
            <div className="lds-dual-ring" style={{ width: '20px', height: '20px', transform: 'scale(2.5)' }} />
          </div>
          <p style={{}}>The page will reload as soon as the Tx is completed</p>
        </div>
      )}
    </div>
  );
};
