import { Stats } from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { DungeonSelectedForSim } from '../../interfaces/dungeonSelectedForSim';
import { HydraPaginatedResponse } from '../../interfaces/hydra';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import { Mobs } from '../../interfaces/mobs';
import { RaiderAbility } from '../../interfaces/raiderability';
import { StuffStats } from '../../interfaces/stuffstats';
import { RaiderContext } from '../Raider/RaiderContext';

type RaiderListProps = {
  raiderPage: number;
  raiderList: undefined | HydraPaginatedResponse<DetailedSyncRaider>;
  isLoading: boolean;
  askRaiderPage: (page: number) => Promise<void>;
  expandAllRaider: boolean;
  style?: React.CSSProperties;
  handleUpdateRaiderServerSide: (
    raider: DetailedSyncRaider,
    objects?: Array<IndexedInventoryItem>,
    raiderStats?: Stats
  ) => Promise<void>;
  stuffStats: StuffStats[];
  currentMaxMeta: number;
  dungeonSelectedForSim: DungeonSelectedForSim;
  cacheBuildCap: CacheBuildCap;
  mobsData: Mobs[];
  raiderAbilitiesData: RaiderAbility[];
};

export const RaiderList = (props: RaiderListProps) => {
  const [expandedRaider, setExpandedRaider] = useState<Record<string, boolean>>({});

  useEffect(() => {
    const newExtendedRaiders: Record<string, boolean> = {};
    for (const raider of props.raiderList?.['hydra:member'] ?? []) {
      newExtendedRaiders[raider['@id'] ?? ''] = props.expandAllRaider;
    }
    setExpandedRaider(newExtendedRaiders);
  }, [props.raiderList, props.expandAllRaider]);

  if (props.isLoading) {
    return null;
  }

  const raiderListStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    width: '100%',
    ...props.style,
  };
  return (
    <section style={raiderListStyle}>
      {props.raiderList?.['hydra:member'].map((raider) => {
        const raiderId = raider['@id'];
        const isExpanded = expandedRaider[raiderId!];
        return (
          <RaiderContext
            key={raiderId!}
            isExpanded={expandedRaider[raiderId!]}
            onPictureClick={() => {
              setExpandedRaider({ ...expandedRaider, [raiderId!]: !isExpanded });
            }}
            raider={raider}
            handleUpdateRaiderServerSide={props.handleUpdateRaiderServerSide}
            stuffStats={props.stuffStats}
            currentMaxMeta={props.currentMaxMeta}
            dungeonSelectedForSim={props.dungeonSelectedForSim}
            cacheBuildCap={props.cacheBuildCap}
            mobsData={props.mobsData}
            raiderAbilitiesData={props.raiderAbilitiesData}
          />
        );
      })}
    </section>
  );
};
