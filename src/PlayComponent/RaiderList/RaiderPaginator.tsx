import { Pagination } from '@mui/material';
import React from 'react';
import { apiEntrypoint, log } from '../../config';
import { HydraPaginatedResponse } from '../../interfaces/hydra';
import { RaiderPerPage } from '../RaiderSearch/RaiderPerPage';

type PaginatorProps = {
  page: number;
  elementPerPage: number;
  list: undefined | HydraPaginatedResponse<any>;
  askPage: (page: number) => void;
  updateElementPerPage: (page: number) => void;
  elementPerPageRange?: Array<number>;
};

export const Paginator = (props: PaginatorProps) => {
  const range = props.elementPerPageRange?.map((val) => val.toString()) ?? ['5', '10', '15', '20', '25', '50'];
  const handleChange = (__event: React.ChangeEvent<unknown>, value: number) => {
    props.askPage(value);
  };
  const lastPage = parseInt(
    new URL(apiEntrypoint + '/' + props.list?.['hydra:view']['hydra:last'] ?? '').searchParams.get('page') ?? '1'
  );
  const paginatorContainerStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  };

  const textStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    color: 'white',
    marginLeft: '8px',
  };

  return (
    <section style={paginatorContainerStyle}>
      {lastPage === 1 ? null : (
        <Pagination
          count={lastPage}
          page={props.page}
          onChange={handleChange}
          siblingCount={0}
          showFirstButton
          showLastButton
          sx={{
            color: 'white',
            '& .MuiPaginationItem-root': {
              color: 'white',
            },
            '& .MuiPaginationItem-root.Mui-selected': {
              '& .MuiTouchRipple-root': {
                backgroundColor: 'white',
                opacity: '0.2',
              },
            },
          }}
        />
      )}
      <RaiderPerPage
        name={'Page Size'}
        value={props.elementPerPage.toString()}
        values={range}
        formatValueForDisplay={(value: string): JSX.Element => {
          return <p style={textStyle}>{value.toString()}</p>;
        }}
        handleInputUpdate={(value: string | undefined) => {
          log('Change raider per page:', value);
          props.updateElementPerPage(parseInt(value ?? ''));
        }}
        style={{ width: '85px' }}
      />
    </section>
  );
};
