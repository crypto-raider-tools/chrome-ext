import { CompanionContext } from './CompanionContext';

type CompanionProps = {
  metamask: string;
};

export const Companion = (props: CompanionProps) => {
  return <CompanionContext metamask={props.metamask} />;
};
