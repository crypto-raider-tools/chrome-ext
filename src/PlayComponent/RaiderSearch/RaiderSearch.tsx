import React, { useEffect, useState } from 'react';
import { CustomFilters, DetailedCustomer } from '../../interfaces/detailedcustomer';
import { DetailedSyncRaider } from '../../interfaces/detailedsyncraider';
import { HydraCriteria, HydraCriteriaVariables, HydraPaginatedResponse } from '../../interfaces/hydra';
import { WrapContainer } from '../Utils/WrapContainer';
import { RaiderSearchByGeneration } from './RaiderFilters/RaiderSearchByGeneration';
import { RaiderSearchByLevel } from './RaiderFilters/RaiderSearchByLevel';
import { RaiderSeachByName } from './RaiderFilters/RaiderSearchByName';
import { RaiderSeachByAvatar } from './RaiderFilters/RaiderSearchByAvatar';
import { RaiderSearchBoolean } from './RaiderFilters/RaiderSearchBoolean';

type RaiderSearchProps = {
  raiderList: HydraPaginatedResponse<DetailedSyncRaider> | undefined;
  isLoading: boolean;
  updateRaiderCriterias: (criterias: Array<HydraCriteria<DetailedSyncRaider>>) => void;
  raiderCriterias: Array<HydraCriteria<DetailedSyncRaider>>;
  style?: React.CSSProperties;
};

export type CriteriaForRaiderSearch = {
  multiple: boolean;
  name: string;
  placeholder?: string;
  variable: HydraCriteriaVariables<DetailedSyncRaider>;
};

export const RaiderSearch = (props: RaiderSearchProps) => {
  const [customerFilters, setCustomerFilters] = useState<Partial<CustomFilters> | undefined>(
    JSON.parse(localStorage.getItem('CRC_customer') ?? '{"filters":[]}').filters
  );

  const criteriaList: Array<CriteriaForRaiderSearch> = [
    { multiple: true, name: 'Generation', variable: 'generation[]', placeholder: '1,2,3' },
    { multiple: true, name: 'Level', variable: 'level[]', placeholder: '1,2,3' },
    { multiple: false, name: 'Name', variable: 'name' },
    { multiple: true, name: 'Race', variable: 'avatarBody[]' },
    { multiple: false, name: 'In wallet', variable: 'currentlyInWallet' },
  ];

  useEffect(() => {
    const customer: DetailedCustomer = JSON.parse(localStorage.getItem('CRC_customer') ?? '');
    if (customer.filters) {
      setCustomerFilters(customer.filters);
    }
  }, []);

  const handleInputUpdate = (criteria: CriteriaForRaiderSearch, value: string | undefined): void => {
    const newCriterias = props.raiderCriterias.filter(
      (raiderCriteria) => raiderCriteria.variable !== criteria.variable
    );
    if (!value) {
      props.updateRaiderCriterias(newCriterias);
      return;
    }
    if (criteria.multiple) {
      const values = value.split(',');
      values.forEach((value) => {
        newCriterias.push({
          variable: criteria.variable,
          value: value,
        });
      });
    } else {
      newCriterias.push({
        variable: criteria.variable,
        value: value,
      });
    }
    props.updateRaiderCriterias(newCriterias);
  };
  const searchStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'flex-start',
  };
  return (
    <section style={props.style}>
      <div style={searchStyle}>
        {criteriaList.map((criteria) => {
          switch (criteria.name) {
            case 'Generation':
              return (
                <WrapContainer key={criteria.name} containerName={criteria.name}>
                  <RaiderSearchByGeneration
                    key={`search_${criteria.variable}`}
                    criteria={criteria}
                    onInputChange={handleInputUpdate}
                    maxGeneration={
                      customerFilters?.generations
                        ? customerFilters?.generations.reduce((previousValue, currentValue) => {
                            return Math.max(previousValue, currentValue);
                          }, 6)
                        : 6
                    }
                    style={{ padding: '8px' }}
                  />
                </WrapContainer>
              );
            case 'Level':
              return (
                <WrapContainer key={criteria.name} containerName={criteria.name}>
                  <RaiderSearchByLevel
                    key={`search_${criteria.variable}`}
                    criteria={criteria}
                    onInputChange={handleInputUpdate}
                    maxLevel={
                      customerFilters?.levels
                        ? customerFilters?.levels.reduce((previousValue, currentValue) => {
                            return Math.max(previousValue, currentValue);
                          }, 10)
                        : 10
                    }
                    style={{ width: '100%' }}
                  />
                </WrapContainer>
              );
            case 'Race':
              return (
                <WrapContainer key={criteria.name} containerName={criteria.name}>
                  <RaiderSeachByAvatar
                    key={`search_${criteria.variable}`}
                    criteria={criteria}
                    onInputChange={handleInputUpdate}
                    avatars={customerFilters?.avatarBodies ?? []}
                  />
                </WrapContainer>
              );
            case 'Name':
              return (
                <WrapContainer key={criteria.name} containerName={criteria.name}>
                  <RaiderSeachByName
                    key={`search_${criteria.variable}`}
                    criteria={criteria}
                    onInputChange={handleInputUpdate}
                  />
                </WrapContainer>
              );
            case 'In wallet':
              return (
                <WrapContainer key={criteria.name} containerName={criteria.name}>
                  <RaiderSearchBoolean
                    key={`search_${criteria.variable}`}
                    criteria={criteria}
                    onInputChange={handleInputUpdate}
                    defaultValue="1"
                  />
                </WrapContainer>
              );
            default:
              return null;
          }
        })}
      </div>
    </section>
  );
};
