import React from 'react';
import { TextField } from '@mui/material';

export const RaiderSearchTextField = (props: RaiderSearchTextFieldType) => {
  const [value, setValue] = React.useState<string>('');

  const searchSliderStyle: React.CSSProperties = {
    width: '150px',
    margin: '16px',
    ...props.style,
  };
  return (
    <div style={searchSliderStyle}>
      <TextField
        label={props.name}
        sx={{
          '& .MuiInputLabel-root': {
            color: '#3779b6',
            '&.Mui-focused': {
              color: 'white',
            },
          },
          '& .MuiOutlinedInput-input': {
            color: 'white',
          },
          '& .MuiInput-underline:after': {
            borderBottomColor: '#1976d2',
          },
          '& .MuiOutlinedInput-root': {
            '& fieldset': {
              borderColor: '#1976d2',
            },
            '&:hover fieldset': {
              borderColor: '#3779b6',
            },
            '&.Mui-focused fieldset': {
              borderColor: 'white',
            },
          },
        }}
        defaultValue={props.defaultVariable}
        onFocus={(e) => {
          setValue(e.currentTarget.value);
        }}
        onBlur={(e) => {
          if (value !== e.currentTarget.value) {
            setValue(e.currentTarget.value);
            props.handleInputUpdate(e.currentTarget.value);
          }
        }}
        onKeyUp={(e) => {
          if (e.key === 'Enter') {
            if (value !== (e.target as HTMLInputElement).value) {
              setValue((e.target as HTMLInputElement).value);
              props.handleInputUpdate((e.target as HTMLInputElement).value);
            }
          }
        }}
      />
    </div>
  );
};

type RaiderSearchTextFieldType = {
  name: string;
  defaultVariable: string;
  handleInputUpdate: (value: string | undefined) => void;
  style?: React.CSSProperties;
};
