import { Avatar } from '@crypto-raider-tools/computation';
import { Grid } from '@mui/material';
import { log } from '../../config';

export function gridElementsToString(elements: RaiderSearchGridElement[]) {
  let res: string = '';
  let idx = 0;
  for (const element of elements) {
    if (!element.isSelected) {
      idx++;
      continue;
    }
    if (res !== '' && idx < elements.length) res += ',';
    res += element.value;
    idx++;
  }
  log('|' + res + '|');
  return res;
}

export const RaiderSearchGrid = (props: RaiderSearchGridType) => {
  return (
    <div style={{ ...props.style }}>
      <Grid container spacing={0} direction="row" justifyContent="space-evenly" alignItems="center">
        {props.elements.map((el) => (
          <Grid key={el.value} item>
            <div onClick={() => props.onClickHandler(el)}>{props.gridElementDisplay(el)}</div>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export type RaiderSearchGridElement = {
  value: string;
  object?: Avatar;
  isSelected: boolean;
};

type RaiderSearchGridType = {
  elements: RaiderSearchGridElement[];
  onClickHandler: (element: RaiderSearchGridElement) => void;
  gridElementDisplay: (value: RaiderSearchGridElement) => JSX.Element;
  style?: React.CSSProperties;
};
