import { Avatar } from '@crypto-raider-tools/computation';
import React, { useEffect } from 'react';
import { RaiderAvatarPicture } from '../../Raider/RaiderAvatarPicture';
import { CriteriaForRaiderSearch } from '../RaiderSearch';
import { gridElementsToString, RaiderSearchGrid, RaiderSearchGridElement } from '../ReactSearchGrid';

type RaiderSearchType = {
  criteria: CriteriaForRaiderSearch;
  avatars: Avatar[];
  onInputChange: (criteria: CriteriaForRaiderSearch, value: string | undefined) => void;
  style?: React.CSSProperties;
};

export const RaiderSeachByAvatar = (props: RaiderSearchType) => {
  const [avatars, setAvatars] = React.useState<Record<string, RaiderSearchGridElement>>({});

  useEffect(() => {
    const initAvatar: Record<string, RaiderSearchGridElement> = {};
    for (const avatar of props.avatars) {
      initAvatar[avatar.body] = {
        value: avatar.body,
        object: avatar,
        isSelected: false,
      };
    }
    setAvatars(initAvatar);
  }, [props.avatars]);

  const avatarItem = (element: RaiderSearchGridElement) => (
    <RaiderAvatarPicture
      style={{
        paddingTop: '1em',
        cursor: 'pointer',
        filter: element.isSelected
          ? 'drop-shadow(0 0 3px #0009) brightness(100%)'
          : 'drop-shadow(0 0 3px #0009) brightness(50%)',
      }}
      avatar={element.object}
      showBackground={false}
      pictureSize={60}
      scaleMultiplier={1.5}
    />
  );
  return (
    <div style={{ transition: '0.5s', marginBottom: '16px', ...props.style }}>
      <RaiderSearchGrid
        elements={Object.values(avatars)}
        gridElementDisplay={avatarItem}
        onClickHandler={(element) => {
          const elements: Record<string, RaiderSearchGridElement> = avatars;
          elements[element.value].isSelected = !element.isSelected;
          props.onInputChange(props.criteria, gridElementsToString(Object.values(elements)));
          setAvatars(elements);
        }}
      />
    </div>
  );
};
