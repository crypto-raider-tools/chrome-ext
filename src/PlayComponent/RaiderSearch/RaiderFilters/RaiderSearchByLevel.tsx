import React from 'react';
import { CriteriaForRaiderSearch } from '../RaiderSearch';
import { RaiderSearchSlider } from '../RaiderSearchSlider';

export const RaiderSearchByLevel = (props: RaiderSearchType) => {
  return (
    <div style={{ transition: '0.5s' }}>
      <RaiderSearchSlider
        range={[1, props.maxLevel]}
        name={'Level'}
        prefix={'Level'}
        handleInputUpdate={(value: string | undefined): void => {
          props.onInputChange(props.criteria, value);
        }}
        style={props.style}
      />
    </div>
  );
};

type RaiderSearchType = {
  criteria: CriteriaForRaiderSearch;
  onInputChange: (criteria: CriteriaForRaiderSearch, value: string | undefined) => void;
  maxLevel: number;
  style?: React.CSSProperties;
};
