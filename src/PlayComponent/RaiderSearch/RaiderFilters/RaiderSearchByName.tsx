import React from 'react';
import { CriteriaForRaiderSearch } from '../RaiderSearch';
import { RaiderSearchTextField } from '../RaiderSearchTextField';

type RaiderSearchProps = {
  criteria: CriteriaForRaiderSearch;
  onInputChange: (criteria: CriteriaForRaiderSearch, value: string | undefined) => void;
  style?: React.CSSProperties;
};

export const RaiderSeachByName = (props: RaiderSearchProps) => {
  return (
    <RaiderSearchTextField
      name={'Name'}
      defaultVariable={''}
      handleInputUpdate={(value: string | undefined): void => {
        props.onInputChange(props.criteria, value);
      }}
      style={{ alignSelf: 'center', width: '100%', margin: '8px' }}
    />
  );
};
