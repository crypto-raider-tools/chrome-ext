import React, { useEffect } from 'react';
import { CriteriaForRaiderSearch } from '../RaiderSearch';
import { gridElementsToString, RaiderSearchGrid, RaiderSearchGridElement } from '../ReactSearchGrid';

export const RaiderSearchByGeneration = (props: RaiderSearchType) => {
  const [generations, setGenerations] = React.useState<Record<string, RaiderSearchGridElement>>({});

  useEffect(() => {
    const initGen: Record<string, RaiderSearchGridElement> = {};
    let idx = 1;
    while (idx <= props.maxGeneration) {
      initGen[idx.toString()] = {
        value: idx.toString(),
        isSelected: false,
      };
      idx++;
    }
    setGenerations(initGen);
  }, [props.maxGeneration]);

  const generationItem = (element: RaiderSearchGridElement) => {
    return (
      <div
        style={{
          position: 'relative',
          width: '50px',
          height: '50px',
          filter: element.isSelected
            ? 'drop-shadow(0 0 3px #0009) brightness(100%)'
            : 'drop-shadow(0 0 3px #0009) brightness(50%)',
          ...props.style,
        }}
      >
        <div
          style={{
            position: 'absolute',
            top: '-25px',
            right: '-25px',
            width: '90px',
            height: '90px',
            backgroundImage:
              'url(https://crypto-raiders-assets.storage.googleapis.com/interface/gen' + element.value + '_border.png)',
            backgroundPosition: '-7px -10px',
            transform: 'scale(0.5)',
          }}
        />
      </div>
    );
  };
  return (
    <div style={{ transition: '0.5s', ...props.style }}>
      <RaiderSearchGrid
        elements={Object.values(generations)}
        gridElementDisplay={generationItem}
        onClickHandler={(element) => {
          const elements: Record<string, RaiderSearchGridElement> = generations;
          elements[element.value].isSelected = !element.isSelected;
          props.onInputChange(props.criteria, gridElementsToString(Object.values(elements)));
          setGenerations(elements);
        }}
      />
    </div>
  );
};

type RaiderSearchType = {
  criteria: CriteriaForRaiderSearch;
  onInputChange: (criteria: CriteriaForRaiderSearch, value: string | undefined) => void;
  maxGeneration: number;
  style?: React.CSSProperties;
};
