import React, { useEffect, useState } from 'react';
import { CriteriaForRaiderSearch } from '../RaiderSearch';

type RaiderSeachBooleanProps = {
  criteria: CriteriaForRaiderSearch;
  onInputChange: (criteria: CriteriaForRaiderSearch, value: string | undefined) => void;
  style?: React.CSSProperties;
  labelStyle?: React.CSSProperties;
  inputStyle?: React.CSSProperties;
  defaultValue?: undefined | '1' | '0';
};

export const RaiderSearchBoolean = (props: RaiderSeachBooleanProps) => {
  const [value, setValue] = useState<undefined | '1' | '0'>(props.defaultValue);

  const handleChange = (value: undefined | '1' | '0') => {
    setValue(value);
    props.onInputChange(props.criteria, value);
  };

  useEffect(() => {
    if (props.defaultValue) {
      handleChange(props.defaultValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const labelStyle: React.CSSProperties = {
    cursor: 'pointer',
    padding: '.25em',
    ...props.inputStyle,
  };

  const inputStyle: React.CSSProperties = {
    cursor: 'pointer',
    display: 'inline-block',
    marginRight: '.25em',
    ...props.inputStyle,
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        alignContent: 'center',
        ...props.style,
      }}
    >
      <label style={labelStyle}>
        <input
          style={inputStyle}
          checked={typeof value === 'undefined' ? true : false}
          onChange={() => {
            handleChange(undefined);
          }}
          type="radio"
          name={props.criteria.name}
        />
        All
      </label>
      <label style={labelStyle}>
        <input
          style={inputStyle}
          checked={value === '1' ? true : false}
          onChange={() => {
            handleChange('1');
          }}
          type="radio"
          name={props.criteria.name}
        />
        Yes
      </label>
      <label style={labelStyle}>
        <input
          style={inputStyle}
          checked={value === '0' ? true : false}
          onChange={() => {
            handleChange('0');
          }}
          type="radio"
          name={props.criteria.name}
        />
        No
      </label>
    </div>
  );
};
