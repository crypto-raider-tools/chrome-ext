import React from 'react';
import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { colorPalette } from '../../Helpers/Constants';

type RaiderPerPageProps = {
  name: string;
  value: string;
  values: string[];
  formatValueForDisplay: (value: string) => JSX.Element;
  handleInputUpdate: (value: string | undefined) => void;
  style?: React.CSSProperties;
};

export const RaiderPerPage = (props: RaiderPerPageProps) => {
  const handleChange = (event: SelectChangeEvent) => {
    props.handleInputUpdate(event.target.value);
  };

  const searchSelectStyle: React.CSSProperties = {
    width: '150px',
    margin: '16px',
    ...props.style,
  };
  return (
    <div style={searchSelectStyle}>
      <FormControl
        fullWidth
        sx={{
          '& .MuiInputLabel-root': {
            color: '#3779b6',
            '&.Mui-focused': {
              color: 'white',
            },
          },
          '& .MuiOutlinedInput-input': {
            color: 'white',
          },
          '& .MuiInput-underline:after': {
            borderBottomColor: '#1976d2',
          },
          '& .MuiOutlinedInput-root': {
            '& fieldset': {
              borderColor: '#1976d2',
            },
            '&:hover fieldset': {
              borderColor: '#3779b6',
            },
            '&.Mui-focused fieldset': {
              borderColor: 'white',
            },
          },
        }}
      >
        <InputLabel id={'select-label-' + props.name}>{props.name}</InputLabel>
        <Select
          labelId={'select-label-' + props.name}
          id={'select-' + props.name}
          value={props.value}
          label={props.name}
          onChange={handleChange}
          sx={{
            '& .MuiMenuItem-root': {
              '& .Mui-selected': {
                backgroundColor: colorPalette['green'],
              },
            },
          }}
        >
          {props.values.map((v) => (
            <MenuItem
              key={v}
              value={v}
              sx={{
                backgroundColor: colorPalette['darkBlueGrey'],
                ':hover': {
                  backgroundColor: colorPalette['mediumBlueGrey'],
                },
                '.Mui-selected': {
                  backgroundColor: colorPalette['green'],
                },
              }}
            >
              {props.formatValueForDisplay(v)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};
