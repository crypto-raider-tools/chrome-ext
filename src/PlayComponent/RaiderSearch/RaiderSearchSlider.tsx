import React from 'react';
import Slider from '@mui/material/Slider';

export const RaiderSearchSlider = (props: RaiderSearchSliderType) => {
  const [range, setRange] = React.useState<number[]>(props.range);
  const [max] = React.useState<number>(props.range[1]);

  type Marks = { value: number; label: string };

  function initMarks(): Marks[] {
    let marks: Marks[] = [];
    let i = range[0];
    while (i <= range[1]) {
      marks.push({
        value: i,
        label: i.toString(),
      });
      i++;
    }
    return marks;
  }

  const handleChange = (__event: Event, newValue: number | number[]) => {
    setRange(newValue as number[]);
  };

  const displayCustomMarks = () => {
    let array: number[] = [];
    let i = range[0];
    while (i <= range[1]) {
      array.push(i);
      i++;
    }
    return <div style={{ display: 'flex' }}>{array.map((value) => props.customMarks!(value.toString()))}</div>;
  };

  function rangeText(value: number) {
    return `${props.prefix} ${value}`;
  }

  function rangeToString(range: number[]) {
    let res: string = '' + range[0];
    let i = range[0] + 1;
    while (i <= range[1]) {
      if (i <= range[1]) res += ', ';
      res += i.toString();
      i++;
    }
    return res;
  }
  const searchSliderStyle: React.CSSProperties = {
    margin: '16px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    ...props.style,
  };
  return (
    <div style={searchSliderStyle}>
      <Slider
        getAriaLabel={() => 'Generation range'}
        value={range}
        onChange={handleChange}
        onChangeCommitted={() => {
          props.handleInputUpdate(rangeToString(range));
        }}
        onKeyUp={(e) => {
          if (e.key === 'Enter') {
            props.handleInputUpdate(rangeToString(range));
          }
        }}
        valueLabelDisplay="off"
        getAriaValueText={rangeText}
        min={1}
        max={max}
        marks={props.customMarks === undefined ? initMarks() : false}
        sx={{
          '& .MuiSlider-thumb': {
            borderRadius: '1px',
          },
          '& .MuiSlider-markLabel': {
            color: 'white',
          },
        }}
      />
      {props.customMarks !== undefined && displayCustomMarks()}
    </div>
  );
};

type RaiderSearchSliderType = {
  range: number[];
  name: string;
  prefix: string;
  handleInputUpdate: (value: string | undefined) => void;
  style?: React.CSSProperties;
  customMarks?: (value: string) => JSX.Element;
};
