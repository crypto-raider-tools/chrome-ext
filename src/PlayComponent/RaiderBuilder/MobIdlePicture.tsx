import { Mobs } from '../../interfaces/mobs';
import styled, { keyframes } from 'styled-components';

type MobIdlePictureProps = {
  mob: Mobs;
  style?: React.CSSProperties;
  pictureSize?: number;
  onClick?: () => void;
  showBackground?: boolean;
  showDifficulty?: boolean;
  scaleMultiplier?: number;
};

type MobIdleAnimationProps = {
  mob: Mobs;
};

const mobIdle = (mob: Mobs) => keyframes`
  0% { background-position: 0 0; }
  100% { background-position: -${(mob.idleDimensionX ?? 0) * (mob.idleAnimationSteps ?? 0)}px 0; }
`;

const MobIdleAnimation = styled.div<MobIdleAnimationProps>`
  width: ${(props: MobIdleAnimationProps) => props.mob.idleDimensionX}px;
  image-rendering: pixelated;
  animation: ${(props: MobIdleAnimationProps) => mobIdle(props.mob)}
    ${(props: MobIdleAnimationProps) => ((props.mob.idleAnimationSteps ?? 0) < 10 ? 1 : 1.5)}s
    steps(${(props: MobIdleAnimationProps) => props.mob.idleAnimationSteps}) infinite;
  margin-bottom: 5px;
  position: absolute;
  height: ${(props: MobIdleAnimationProps) => props.mob.idleDimensionY}px;
  background-image: url('${(props: MobIdleAnimationProps) => props.mob.idleUrl}');
`;

export function MobIdlePicture(props: MobIdlePictureProps) {
  let pictureSize: number = props.pictureSize ?? 64;
  let transformScale: number = pictureSize / (props.mob.idleDimensionX ?? 0);

  if (props.scaleMultiplier) transformScale *= props.scaleMultiplier;

  let difficulty = props.mob.dungeons && props.mob.dungeons.length > 0 ? props.mob.dungeons[0].difficulty : 'NORMAL';
  switch (difficulty) {
    case 'NORMAL':
      difficulty = 'rare';
      break;
    case 'HEROIC':
      difficulty = 'mythic';
      break;
    default:
      difficulty = 'fodder';
      break;
  }
  return (
    <div
      style={{
        ...props.style,
        height: pictureSize + 'px',
        width: pictureSize + 'px',
      }}
      onClick={props.onClick}
    >
      <div
        style={{
          backgroundSize: 'cover',
          backgroundImage: props.showBackground
            ? 'url("https://storage.googleapis.com/crypto-raiders-assets/cosmetic/backgrounds/DEFAULT.png")'
            : '',
          width: '100%',
          height: '100%',
          display: 'flex',
          pointerEvents: 'none',
        }}
      >
        <div
          style={{
            backgroundSize: 'cover',
            backgroundImage: props.showDifficulty
              ? `url("https://crypto-raiders-assets.storage.googleapis.com/mobs/borders/rarityborder_${difficulty}.png")`
              : '',
            width: '100%',
            height: '100%',
            display: 'flex',
            pointerEvents: 'none',
          }}
        >
          <div
            style={{
              alignSelf: 'end',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'relative',
              width: '100%',
              height: '85%',
              transform: 'scale(' + transformScale + ')',
            }}
          >
            <MobIdleAnimation mob={props.mob} />
          </div>
        </div>
      </div>
    </div>
  );
}
