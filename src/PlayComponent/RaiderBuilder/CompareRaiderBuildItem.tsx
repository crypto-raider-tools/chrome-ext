import { useRef } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import { BoxShadow } from '../Utils/ClickableContainer';

type CompareRaiderBuildItemProps = {
  item: IndexedInventoryItem;
  onHover: (item: IndexedInventoryItem, bounds: DOMRect) => void;
};

export const CompareRaiderBuildItem = (props: CompareRaiderBuildItemProps) => {
  const itemRef = useRef(null);
  const item = props.item.item;

  const internalName = item.internalName ?? '';
  let rarity: string = item.rarity ?? 'common';
  if (item.slot === 'knickknack') rarity = 'rune';
  if (!internalName) return null;

  return (
    <img
      ref={itemRef}
      onMouseEnter={() => {
        if (itemRef.current) {
          const bounds = (itemRef.current as HTMLElement).getBoundingClientRect();
          props.onHover(props.item, bounds);
        }
      }}
      src={`https://storage.googleapis.com/crypto-raiders-assets/icons/${internalName}_ICON.png`}
      alt={internalName}
      style={{
        width: '40px',
        height: '40px',
        objectFit: 'contain',
        imageRendering: 'pixelated',
        margin: '2px',
        boxShadow:
          BoxShadow(colorPalette[rarity], { x: 3, y: 3 }, 10, 1, true) +
          ', ' +
          BoxShadow(colorPalette[rarity], { x: -3, y: -3 }, 10, 1, true),
      }}
    />
  );
};
