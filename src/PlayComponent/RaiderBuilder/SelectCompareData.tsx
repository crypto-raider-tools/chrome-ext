import { RaiderApiData, fixStats, Rarity } from '@crypto-raider-tools/computation';
import { apiEntrypoint, log } from '../../config';
import { colorPalette } from '../../Helpers/Constants';
import { Mobs } from '../../interfaces/mobs';
import { CompareObject } from '../../interfaces/raiderBuilder';
import { StuffStats } from '../../interfaces/stuffstats';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { CompareSelectorMob } from './CompareSelectorMob';
import { CompareSelectorRaider } from './CompareSelectorRaider';

type SelectCompareDataProps = {
  onSelect: (data: CompareObject) => void;
  style?: React.CSSProperties;
  stuffStats: StuffStats[];
  mobsData: Mobs[];
};

export const SelectCompareData = (props: SelectCompareDataProps) => {
  const defaultStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    ...props.style,
  };

  const searchForRaiderToCompare = async (id: string) => {
    const request = await fetch(`${apiEntrypoint}/other-raider/${id}`, {
      headers: {
        'x-original-token': document.cookie,
      },
    });
    if (request.status !== 200) {
      log(`${'/other-raider'}: ${request.status} ${request.statusText}`);
      return Promise.reject('Unable to get raider stats for this id, sry 😿');
    }
    let raiderData: RaiderApiData = await request.json();
    raiderData.stats = fixStats(raiderData.stats);
    raiderData.inventory?.forEach((object, index) => {
      if (!raiderData?.inventory) return;
      raiderData.inventory[index].item.rarity =
        (props.stuffStats.find((i) => i.internalName === object.item.internalName)?.rarity as Rarity) ?? 'common';
    });
    props.onSelect({ type: 'Raider', raiderData: raiderData });
    return Promise.resolve();
  };

  return (
    <div style={defaultStyle}>
      <CompareSelectorRaider onClick={searchForRaiderToCompare} />
      <div>
        <ClickableContainer
          onClick={() => {
            props.onSelect({ type: 'Custom' });
          }}
          style={{
            margin: '5px',
            padding: '5px',
            borderRadius: '2px',
            background: colorPalette['mediumBlueGrey'],
          }}
        >
          <p>Add Custom Build</p>
        </ClickableContainer>
      </div>
      <CompareSelectorMob
        onClick={(Mob: Mobs) => {
          props.onSelect({ type: 'Mob', mob: Mob });
        }}
        mobsData={props.mobsData}
      />
    </div>
  );
};
