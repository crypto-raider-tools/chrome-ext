import {
  computeStatsBuildMetaScore,
  createFighterBuild,
  createFighterStats,
  FighterBuild,
  FighterStats,
} from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { createBuildCapCache } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { DetailedMobs } from '../../interfaces/detailedMobs';
import { StuffStats } from '../../interfaces/stuffstats';
import { RaiderAdvancedStats } from '../Raider/RaiderAdvancedStats';
import { RaiderGraph } from '../Raider/RaiderGraph';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { FighterBuildEstimatedWinRate } from './FighterBuildEstimatedWinRate';
import { MobIdlePicture } from './MobIdlePicture';

type CompareMobProps = {
  style?: React.CSSProperties;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  mob: DetailedMobs;
  onRemove: () => void;
  isMainBuild: boolean;
  currentFighterBuild: FighterBuild;
  currentFighterStats: FighterStats;
  setCurrentFighterBuild?: (value: FighterBuild) => void;
  setCurrentFighterStat?: (value: FighterStats) => void;
};

export const CompareMob = (props: CompareMobProps) => {
  const [cacheBuildCap] = useState<CacheBuildCap>({
    1: createBuildCapCache(10),
  });
  const [currentFighterBuild, setCurrentFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [currentFighterStat, setCurrentFighterStat] = useState<FighterStats>(createFighterStats());

  useEffect(() => {
    const newFighterStats = createFighterStats();
    const newFighterBuild = createFighterBuild();
    for (const fighterBuildKey of Object.keys(newFighterBuild)) {
      newFighterBuild[fighterBuildKey as keyof FighterBuild] = parseFloat(
        props.mob[fighterBuildKey as keyof DetailedMobs] as string
      );
    }
    setCurrentFighterStat(newFighterStats);
    setCurrentFighterBuild(newFighterBuild);
    if (props.isMainBuild && props.setCurrentFighterStat && props.setCurrentFighterBuild) {
      props.setCurrentFighterStat(newFighterStats);
      props.setCurrentFighterBuild(newFighterBuild);
    }
    // eslint-disable-next-line
  }, [props.mob]);

  const defaultStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['mediumBlueGrey'],
    paddingLeft: '8px',
    position: 'relative',
    height: '650px',
    ...props.style,
  };
  const buildContainers: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
  };
  return (
    <div style={defaultStyle}>
      <div style={buildContainers}>
        <ClickableContainer
          onClick={() => {
            props.onRemove();
          }}
          style={{
            position: 'absolute',
            right: '0px',
            top: '0px',
            margin: '5px',
            padding: '5px',
            background: colorPalette['darkBlueGrey'],
          }}
        >
          <p>❌</p>
        </ClickableContainer>
        <MobIdlePicture mob={props.mob} pictureSize={150} style={{ margin: '5px' }} />
        <div>
          <p style={{ fontSize: '16px', fontWeight: '500' }}>{props.mob.name}</p>
          {props.mob.dungeons &&
            props.mob.dungeons.map((dungeon) => (
              <p
                key={dungeon['@id']}
                style={{ fontSize: '14px', fontWeight: '500', color: colorPalette['lightBlueGrey'] }}
              >
                {dungeon.name}
              </p>
            ))}
        </div>
      </div>
      <RaiderAdvancedStats
        fighterStats={currentFighterStat}
        currentRaiderBuild={computeStatsBuildMetaScore(currentFighterStat.stats)}
        cacheBuildCap={cacheBuildCap}
        compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
        currentBuild={currentFighterBuild}
        invertComparison={true}
      />
      <RaiderGraph
        fighterBuild={currentFighterBuild}
        fighterStats={currentFighterStat}
        cacheBuildCap={cacheBuildCap}
        stuffStats={props.stuffStats}
        currentMaxMeta={props.currentMaxMeta}
        style={{
          margin: '0px 60px 0px 8px',
          width: '250px',
          height: '220px',
        }}
        compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
      />
      {!props.isMainBuild && (
        <FighterBuildEstimatedWinRate
          playerFighterBuild={props.currentFighterBuild}
          opponentFighterBuild={currentFighterBuild}
        />
      )}
    </div>
  );
};
