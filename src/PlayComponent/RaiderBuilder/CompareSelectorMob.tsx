import { useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { Mobs } from '../../interfaces/mobs';
import { RaiderSearchGrid, RaiderSearchGridElement } from '../RaiderSearch/ReactSearchGrid';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { MobIdlePicture } from './MobIdlePicture';

type CompareSelectorMobProps = {
  mobsData: Mobs[];
  onClick: (mobs: Mobs) => void;
  style?: React.CSSProperties;
};

export const CompareSelectorMob = (props: CompareSelectorMobProps) => {
  const [selectedMobId, setSelectedMobId] = useState<number>(-1);
  const [mobsElement, setMobsElement] = useState<RaiderSearchGridElement[]>([]);

  useEffect(() => {
    let elements: RaiderSearchGridElement[] = [];
    props.mobsData.forEach((__mob, index) => {
      elements.push({ isSelected: false, value: '' + index });
    });
    setMobsElement(elements);
    // eslint-disable-next-line
  }, []);

  const defaultStyle: React.CSSProperties = {
    borderRadius: '2px',
    margin: '8px',
    padding: '8px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    backgroundColor: colorPalette['mediumBlueGrey'],
    ...props.style,
  };
  const gridContainer: React.CSSProperties = {
    maxHeight: '360px',
    overflowX: 'hidden',
    overflowY: 'auto',
  };
  const mobItem = (element: RaiderSearchGridElement) => {
    const mob = props.mobsData[parseInt(element.value)];
    return (
      <div style={{ position: 'relative' }}>
        <MobIdlePicture
          style={{
            paddingBottom: '1em',
            cursor: 'pointer',
            filter: element.isSelected
              ? 'drop-shadow(0 0 3px #0009) brightness(100%)'
              : 'drop-shadow(0 0 3px #0009) brightness(50%)',
          }}
          showBackground={true}
          showDifficulty={true}
          pictureSize={80}
          scaleMultiplier={1}
          mob={mob}
        />
      </div>
    );
  };
  return (
    <div style={defaultStyle}>
      <RaiderSearchGrid
        elements={mobsElement}
        gridElementDisplay={mobItem}
        onClickHandler={(element) => {
          let elements: RaiderSearchGridElement[] = [...mobsElement];
          if (selectedMobId !== -1) elements[selectedMobId].isSelected = false;
          elements[parseInt(element.value)].isSelected = !element.isSelected;
          setSelectedMobId(element.isSelected ? parseInt(element.value) : -1);
          setMobsElement(elements);
        }}
        style={gridContainer}
      />
      <ClickableContainer
        onClick={() => {
          if (selectedMobId !== -1) props.onClick(props.mobsData[selectedMobId]);
        }}
        style={{
          margin: '5px',
          padding: '5px',
          background: colorPalette['darkBlueGrey'],
          ...(selectedMobId === -1 ? { opacity: 0.3, pointerEvents: 'none' } : {}),
        }}
      >
        <p>Add Selected Mobs</p>
      </ClickableContainer>
    </div>
  );
};
