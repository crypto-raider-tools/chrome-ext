import {
  computeRaiderBuild,
  computeStatsBuildMetaScore,
  createFighterBuild,
  createFighterStats,
  createStats,
  FighterBuild,
  FighterStats,
  Stats,
} from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { createBuildCapCache, getTotalStatsPointCount, wordToCamelCase } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { StuffStats } from '../../interfaces/stuffstats';
import { RaiderAdvancedStats } from '../Raider/RaiderAdvancedStats';
import { RaiderGraph } from '../Raider/RaiderGraph';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { PlusMinusIncrementor } from '../Utils/PlusMinusIncrementor';
import { FighterBuildEstimatedWinRate } from './FighterBuildEstimatedWinRate';

type CustomBuildProps = {
  style?: React.CSSProperties;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  isMainBuild: boolean;
  currentFighterBuild: FighterBuild;
  currentFighterStats: FighterStats;
  setCurrentFighterBuild?: (value: FighterBuild) => void;
  setCurrentFighterStat?: (value: FighterStats) => void;
  onRemove: () => void;
};

export const CustomBuild = (props: CustomBuildProps) => {
  const [cacheBuildCap, setCacheBuildCap] = useState<CacheBuildCap>({
    1: createBuildCapCache(1),
  });
  const [level, setLevel] = useState<number>(1);
  const [customStats, setCustomStats] = useState<Stats>(createStats());
  const [currentFighterBuild, setCurrentFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [currentFighterStat, setCurrentFighterStat] = useState<FighterStats>(createFighterStats());
  const defaultStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['mediumBlueGrey'],
    paddingLeft: '8px',
    position: 'relative',
    height: '670px',
    ...props.style,
  };
  const buildContainers: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
  };
  const statsContainers: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  };
  const nameStyle: React.CSSProperties = {
    minWidth: '100px',
    width: 'calc(1/7 * 100%)',
    fontWeight: '500',
    color: 'white',
  };

  useEffect(() => {
    const newFighterStats: FighterStats = {
      stats: customStats,
      level: level,
      mainHand: undefined,
      dress: undefined,
      knickknack: undefined,
      finger: undefined,
      neck: undefined,
      helmet: undefined,
    };
    if (typeof cacheBuildCap[level] === 'undefined') {
      const newBuildCap = { ...cacheBuildCap };
      newBuildCap[level] = createBuildCapCache(level);
      setCacheBuildCap(newBuildCap);
    }

    setCurrentFighterStat(newFighterStats);
    let newBuild: FighterBuild = {
      maxHP: 0,
      minDamage: 0,
      maxDamage: 0,
      hitChance: 0,
      hitFirst: 0,
      meleeCrit: 0,
      critDamageMultiplier: 0,
      critResist: 0,
      evadeChance: 0,
      meleeResist: 0,
    };
    try {
      newBuild = computeRaiderBuild(newFighterStats);
    } catch (error) {}
    setCurrentFighterBuild(newBuild);
    if (props.isMainBuild && props.setCurrentFighterStat && props.setCurrentFighterBuild) {
      props.setCurrentFighterStat(newFighterStats);
      props.setCurrentFighterBuild(newBuild);
    }
    // eslint-disable-next-line
  }, [customStats, level]);

  return (
    <div style={defaultStyle}>
      <div style={buildContainers}>
        <div style={statsContainers}>
          <ClickableContainer
            onClick={() => {
              props.onRemove();
            }}
            style={{
              position: 'absolute',
              right: '0px',
              top: '0px',
              margin: '5px',
              padding: '5px',
              background: colorPalette['darkBlueGrey'],
            }}
          >
            <p>❌</p>
          </ClickableContainer>
          <ClickableContainer
            onClick={() => {
              setCustomStats(createStats());
            }}
            style={{
              position: 'absolute',
              right: '0px',
              top: '30px',
              margin: '5px',
              padding: '5px',
              background: colorPalette['darkBlueGrey'],
            }}
          >
            <p>🔄</p>
          </ClickableContainer>
          <div style={{ display: 'flex', width: '100%' }}>
            <p style={{ ...nameStyle, color: colorPalette['lightBlueGrey'] }}>Level :</p>
            <PlusMinusIncrementor
              minValue={1}
              maxValue={9999}
              value={level}
              setValue={(value) => {
                setLevel(value);
              }}
            />
          </div>
          {Object.keys(createStats()).map((stat) => {
            const statName = stat as keyof Stats;
            const currentStatValue = customStats[statName];
            return (
              <div style={{ display: 'flex', justifyContent: 'start', width: '100%' }} key={stat}>
                <div style={nameStyle}>{wordToCamelCase(stat)}</div>
                <PlusMinusIncrementor
                  minValue={0}
                  maxValue={9999}
                  value={currentStatValue}
                  setValue={(value) => {
                    customStats[statName] = value;
                    setCustomStats({ ...customStats });
                  }}
                />
                {!props.isMainBuild && (
                  <>
                    <p style={{ marginLeft: '5px', color: colorPalette['none'] }}>Dif</p>
                    <p
                      style={{
                        marginLeft: '5px',
                        color:
                          currentStatValue - props.currentFighterStats.stats[statName] > 0
                            ? colorPalette['good']
                            : colorPalette['inferior'],
                      }}
                    >
                      {(currentStatValue - props.currentFighterStats.stats[statName] > 0 ? '+' : '') +
                        (currentStatValue - props.currentFighterStats.stats[statName]).toFixed(0)}
                    </p>
                  </>
                )}
              </div>
            );
          })}
          <div style={{ alignSelf: 'flex-start', fontWeight: '500' }}>
            <span style={{ color: colorPalette['lightBlueGrey'] }}>{'Total: '}</span>
            <span>{getTotalStatsPointCount(customStats)}</span>
          </div>
        </div>
      </div>
      <RaiderAdvancedStats
        fighterStats={currentFighterStat}
        currentRaiderBuild={computeStatsBuildMetaScore(customStats)}
        cacheBuildCap={cacheBuildCap}
        compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
        invertComparison={true}
      />
      <RaiderGraph
        fighterBuild={currentFighterBuild}
        fighterStats={currentFighterStat}
        cacheBuildCap={cacheBuildCap}
        stuffStats={props.stuffStats}
        currentMaxMeta={props.currentMaxMeta}
        style={{
          margin: '0px 60px 0px 8px',
          width: '250px',
          height: '220px',
        }}
        compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
      />
      {!props.isMainBuild && (
        <FighterBuildEstimatedWinRate
          playerFighterBuild={props.currentFighterBuild}
          opponentFighterBuild={currentFighterBuild}
        />
      )}
    </div>
  );
};
