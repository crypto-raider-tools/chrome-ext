import { createFighterBuild, createFighterStats, FighterBuild, FighterStats } from '@crypto-raider-tools/computation';
import { useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { Mobs } from '../../interfaces/mobs';
import { CompareObject } from '../../interfaces/raiderBuilder';
import { StuffStats } from '../../interfaces/stuffstats';
import { H2 } from '../Theme';
import { CompareMob } from './CompareMob';
import { CompareRaiderBuild } from './CompareRaiderBuild';
import { CustomBuild } from './CustomBuild';
import { SelectCompareData } from './SelectCompareData';

type RaiderBuilderProps = {
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  mobsData: Mobs[];
  style?: React.CSSProperties;
};

export const RaiderBuilder = (props: RaiderBuilderProps) => {
  const [currentFighterBuild, setCurrentFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [currentFighterStat, setCurrentFighterStat] = useState<FighterStats>(createFighterStats());
  const buildContainers: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    height: '100%',
    width: '100%',
    padding: '8px',
  };
  const raiderBuildParent: React.CSSProperties = {
    width: 'fit-content',
    height: 'fit-content',
    borderRadius: '2px',
    margin: '16px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: colorPalette['darkBlueGrey'],
    ...props.style,
  };
  const mainBuildContainer: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['lightBlueGrey'],
  };
  const buildContainer: React.CSSProperties = {
    marginRight: '8px',
    padding: '5px',
    position: 'relative',
    minHeight: '640px',
    minWidth: '325px',
    maxWidth: '380px',
  };
  const [compareData, setCompareData] = useState<Array<CompareObject>>([]);
  const [mainCompareData, setMainCompareData] = useState<CompareObject>();

  const showCompareData = (compareData: CompareObject, onRemove: () => void, isMain: boolean) => {
    if (typeof compareData === 'undefined') return null;
    switch (compareData.type) {
      case 'Custom':
        return (
          <CustomBuild
            currentMaxMeta={props.currentMaxMeta}
            stuffStats={props.stuffStats}
            currentFighterBuild={currentFighterBuild}
            currentFighterStats={currentFighterStat}
            onRemove={onRemove}
            isMainBuild={isMain}
            setCurrentFighterBuild={setCurrentFighterBuild}
            setCurrentFighterStat={setCurrentFighterStat}
          />
        );
      case 'Raider':
        return (
          <CompareRaiderBuild
            currentFighterBuild={currentFighterBuild}
            currentFighterStats={currentFighterStat}
            currentMaxMeta={props.currentMaxMeta}
            stuffStats={props.stuffStats}
            raiderData={compareData.raiderData!}
            onRemove={onRemove}
            isMainBuild={isMain}
            setCurrentFighterBuild={setCurrentFighterBuild}
            setCurrentFighterStat={setCurrentFighterStat}
          />
        );
      case 'Mob':
        return (
          <CompareMob
            currentFighterBuild={currentFighterBuild}
            currentFighterStats={currentFighterStat}
            currentMaxMeta={props.currentMaxMeta}
            stuffStats={props.stuffStats}
            mob={compareData.mob!}
            onRemove={onRemove}
            isMainBuild={isMain}
            setCurrentFighterBuild={setCurrentFighterBuild}
            setCurrentFighterStat={setCurrentFighterStat}
          />
        );
    }
  };
  return (
    <div
      style={raiderBuildParent}
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <H2>Raider Builder</H2>
      <div style={buildContainers}>
        <div style={{ ...buildContainer, ...mainBuildContainer }}>
          <H2>Main Build</H2>
          {typeof mainCompareData === 'undefined' ? (
            <SelectCompareData
              onSelect={(data: CompareObject): void => {
                setMainCompareData(data);
              }}
              stuffStats={props.stuffStats}
              mobsData={props.mobsData}
            />
          ) : (
            showCompareData(
              mainCompareData,
              () => {
                setMainCompareData(undefined);
              },
              true
            )
          )}
        </div>
        {compareData.map((object, index) => {
          return (
            <div key={object.type + index} style={buildContainer}>
              {showCompareData(
                object,
                () => {
                  const newCompareData = [...compareData];
                  newCompareData.splice(newCompareData.indexOf(object), 1);
                  setCompareData(newCompareData);
                },
                false
              )}
            </div>
          );
        })}
        {compareData.length < Math.round(window.screen.width / 700) && (
          <div style={buildContainer}>
            <H2>Add Build To Compare</H2>
            <SelectCompareData
              onSelect={(data: CompareObject): void => {
                const newCompareData = [...compareData];
                newCompareData.push(data);
                setCompareData(newCompareData);
              }}
              stuffStats={props.stuffStats}
              mobsData={props.mobsData}
            />
          </div>
        )}
      </div>
    </div>
  );
};
