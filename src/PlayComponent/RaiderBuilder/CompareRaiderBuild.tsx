import {
  computeRaiderBuild,
  computeStatsBuildMetaScore,
  createFighterBuild,
  createFighterStats,
  createStats,
  FighterBuild,
  FighterStats,
  fixStats,
  RaiderApiData,
  Stats,
} from '@crypto-raider-tools/computation';
import { useEffect, useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { buildBySlot, createBuildCapCache, getItemPureness, getTotalFighterStatsPointCount } from '../../Helpers/Utils';
import { CacheBuildCap } from '../../interfaces/cacheBuildCap';
import { IndexedInventoryItem } from '../../interfaces/indexedInventoryItem';
import { ItemPureness } from '../../interfaces/pureness';
import { StuffStats } from '../../interfaces/stuffstats';
import { RaiderAdvancedStats } from '../Raider/RaiderAdvancedStats';
import { RaiderGraph } from '../Raider/RaiderGraph';
import { InventoryItemOverview } from '../Raider/RaiderInventory/RaiderInventoryOverview/InventoryItemOverview';
import { RaiderPicture } from '../Raider/RaiderPicture';
import { ClickableContainer } from '../Utils/ClickableContainer';
import { CompareRaiderBuildItem } from './CompareRaiderBuildItem';
import { FighterBuildEstimatedWinRate } from './FighterBuildEstimatedWinRate';

type CompareRaiderBuildProps = {
  style?: React.CSSProperties;
  currentMaxMeta: number;
  stuffStats: StuffStats[];
  raiderData: RaiderApiData;
  onRemove: () => void;
  isMainBuild: boolean;
  currentFighterBuild: FighterBuild;
  currentFighterStats: FighterStats;
  setCurrentFighterBuild?: (value: FighterBuild) => void;
  setCurrentFighterStat?: (value: FighterStats) => void;
  isCollapsed?: boolean;
};

export const CompareRaiderBuild = (props: CompareRaiderBuildProps) => {
  const [cacheBuildCap, setCacheBuildCap] = useState<CacheBuildCap>({
    1: createBuildCapCache(1),
  });
  const [fullStats, setFullStats] = useState<Stats>(createStats());
  const [currentFighterBuild, setCurrentFighterBuild] = useState<FighterBuild>(createFighterBuild());
  const [currentFighterStat, setCurrentFighterStat] = useState<FighterStats>(createFighterStats());
  const [currentHoverItem, setCurrentHoverItem] = useState<IndexedInventoryItem>();
  const [equippedItems, setEquippedItems] = useState<Record<number, IndexedInventoryItem>>({});
  const [inventoryComputedPureness, setInventoryComputedPureness] = useState<ItemPureness[]>([]);

  useEffect(() => {
    const newEquippedItems: Record<number, IndexedInventoryItem> = {};
    const newComputedPureness: ItemPureness[] = [];
    props.raiderData.inventory?.forEach((object, index) => {
      const indexedItem: IndexedInventoryItem = {
        originalIndex: index,
        filteredIndex: index,
        computedBuild: computeStatsBuildMetaScore(fixStats({ ...object.item.stats })),
        equipped: object.equipped,
        item: { ...object.item, stats: fixStats(object.item.stats) },
      };
      newEquippedItems[index] = indexedItem;
      const computedPureness = getItemPureness(
        object.item,
        props.stuffStats.find((i) => i.internalName === object.item.internalName)
      );
      newComputedPureness.push(computedPureness);
    });
    setEquippedItems(newEquippedItems);
    setInventoryComputedPureness(newComputedPureness);

    const stats = { ...createStats(), ...props.raiderData.stats };
    const level = props.raiderData.level;
    const newFighterStats: FighterStats = {
      stats: { ...stats },
      level: level,
      mainHand: undefined,
      dress: undefined,
      knickknack: undefined,
      finger: undefined,
      neck: undefined,
      helmet: undefined,
    };
    props.raiderData.inventory?.forEach((object) => {
      buildBySlot[object.item.slot](newFighterStats, {
        ...object.item,
        name: object.item.name.replace(' - Spell Rune', ''),
      });
      const fStats = fixStats(object.item.stats);
      Object.keys(stats).forEach((s) => {
        const statName = s as keyof Stats;
        stats[statName] += fStats[statName];
      });
    });
    setFullStats(stats);
    if (typeof cacheBuildCap[level] === 'undefined') {
      const newBuildCap = { ...cacheBuildCap };
      newBuildCap[level] = createBuildCapCache(level);
      setCacheBuildCap(newBuildCap);
    }
    setCurrentFighterStat(newFighterStats);

    let newBuild: FighterBuild = {
      maxHP: 0,
      minDamage: 0,
      maxDamage: 0,
      hitChance: 0,
      hitFirst: 0,
      meleeCrit: 0,
      critDamageMultiplier: 0,
      critResist: 0,
      evadeChance: 0,
      meleeResist: 0,
    };
    try {
      newBuild = computeRaiderBuild(newFighterStats);
    } catch (error) {}

    setCurrentFighterBuild(newBuild);
    if (props.isMainBuild && props.setCurrentFighterStat && props.setCurrentFighterBuild) {
      props.setCurrentFighterStat(newFighterStats);
      props.setCurrentFighterBuild(newBuild);
    }
    // eslint-disable-next-line
  }, [props.raiderData]);

  const defaultStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['mediumBlueGrey'],
    paddingLeft: '8px',
    position: 'relative',
    height: props.isCollapsed ? '200px' : '670px',
    ...props.style,
  };
  const buildContainers: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
  };
  const raiderContainer: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  };
  const nameStyle: React.CSSProperties = {
    minWidth: '100px',
    width: 'calc(1/7 * 100%)',
    fontWeight: '500',
    color: 'white',
  };
  const statsContainer: React.CSSProperties = { display: 'flex', flexDirection: 'column' };
  const statsStyle: React.CSSProperties = {
    display: 'flex',
    fontSize: '14px',
    alignItems: 'center',
    justifyContent: 'space-between',
  };
  const mainStatsStyle: React.CSSProperties = {
    display: 'flex',
    alignItems: 'center',
    width: '80px',
    textAlign: 'start',
  };

  const computeBarStyle: React.CSSProperties = {
    width: '80%',
    height: '2px',
    margin: '5px',
    borderRadius: '2px',
    backgroundColor: colorPalette['darkBlueGrey'],
  };
  const itemsContainerGridStyle: React.CSSProperties = {
    display: 'grid',
    gridTemplateColumns: 'repeat(2, 1fr)',
  };
  const itemsContainerFlexStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
  };

  return (
    <div style={defaultStyle}>
      <div style={buildContainers}>
        <ClickableContainer
          onClick={() => {
            props.onRemove();
          }}
          style={{
            position: 'absolute',
            right: '0px',
            top: '0px',
            margin: '5px',
            padding: '5px',
            background: colorPalette['darkBlueGrey'],
          }}
        >
          <p>❌</p>
        </ClickableContainer>
        {typeof currentHoverItem !== 'undefined' && (
          <InventoryItemOverview
            hoverItem={currentHoverItem}
            equippedItems={equippedItems}
            inventoryComputedPureness={inventoryComputedPureness}
            currentMaxMeta={props.currentMaxMeta}
            isFixed={true}
            stuffStats={props.stuffStats.find((s) => s.internalName === currentHoverItem.item.internalName)!}
          />
        )}
        <div
          style={{ display: 'flex', width: 'fit-content', alignItems: 'center' }}
          onMouseLeave={() => {
            setCurrentHoverItem(undefined);
          }}
        >
          <div style={raiderContainer}>
            <RaiderPicture raider={props.raiderData} pictureSize={160} />
            <div
              style={
                props?.raiderData?.inventory?.length && props?.raiderData?.inventory?.length > 4
                  ? itemsContainerGridStyle
                  : itemsContainerFlexStyle
              }
            >
              {Object.values(equippedItems).map((item) => {
                return (
                  <CompareRaiderBuildItem
                    key={item.item.internalName}
                    item={item}
                    onHover={(item: IndexedInventoryItem, bounds: DOMRect) => {
                      setCurrentHoverItem({ ...item, lastScreenPosition: bounds });
                    }}
                  />
                );
              })}
            </div>
          </div>
          <div style={statsContainer}>
            <p style={{ ...nameStyle, color: colorPalette['lightBlueGrey'], margin: '8px 0' }}>
              Level : <span style={nameStyle}>{currentFighterStat.level}</span>
            </p>
            {Object.keys(createStats()).map((stat: string) => {
              const statName = stat as keyof Stats;
              const currentStatValue = currentFighterStat.stats[statName];
              const fullStatValue = fullStats[statName];
              const compareStatValue = props.currentFighterStats.stats[statName];
              return (
                <div key={stat} style={statsStyle}>
                  <div style={mainStatsStyle}>
                    <span style={{ color: colorPalette['lightBlueGrey'] }}>{stat.slice(0, 3).toUpperCase() + ' '}</span>
                    <span
                      style={{
                        ...(currentStatValue > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
                        marginLeft: '5px',
                      }}
                    >
                      {currentStatValue}
                    </span>
                    <div style={computeBarStyle} />
                    <span
                      style={{
                        ...(fullStatValue > 0 ? { fontWeight: '500' } : { color: colorPalette['lightBlueGrey'] }),
                      }}
                    >
                      {fullStatValue}
                    </span>
                  </div>
                  {currentStatValue !== compareStatValue && (
                    <span>
                      {' ('}
                      <span
                        style={{
                          color: currentStatValue < compareStatValue ? colorPalette['inferior'] : colorPalette['op'],
                          fontSize: '500',
                        }}
                      >
                        {(compareStatValue < currentStatValue ? '+' : '') + (currentStatValue - compareStatValue)}
                      </span>
                      {')'}
                    </span>
                  )}
                </div>
              );
            })}
            <div style={{ alignSelf: 'flex-start', fontWeight: '500' }}>
              <span style={{ color: colorPalette['lightBlueGrey'] }}>{'Total: '}</span>
              <span>{getTotalFighterStatsPointCount(currentFighterStat)}</span>
            </div>
          </div>
        </div>
      </div>
      {typeof props.isCollapsed === undefined ||
        (!props.isCollapsed && (
          <>
            <RaiderAdvancedStats
              fighterStats={currentFighterStat}
              currentRaiderBuild={computeStatsBuildMetaScore(currentFighterStat.stats)}
              cacheBuildCap={cacheBuildCap}
              compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
              invertComparison={true}
            />
            <RaiderGraph
              fighterBuild={currentFighterBuild}
              fighterStats={currentFighterStat}
              cacheBuildCap={cacheBuildCap}
              stuffStats={props.stuffStats}
              currentMaxMeta={props.currentMaxMeta}
              style={{
                margin: '0px 60px 0px 8px',
                width: '250px',
                height: '220px',
              }}
              compareBuild={props.isMainBuild ? undefined : props.currentFighterBuild}
            />
          </>
        ))}
      {!props.isMainBuild && (
        <FighterBuildEstimatedWinRate
          playerFighterBuild={props.currentFighterBuild}
          opponentFighterBuild={currentFighterBuild}
        />
      )}
    </div>
  );
};
