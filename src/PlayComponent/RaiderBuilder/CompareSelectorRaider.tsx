import { useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { RaiderSearchTextField } from '../RaiderSearch/RaiderSearchTextField';
import { ClickableContainer } from '../Utils/ClickableContainer';

type CompareSelectorRaiderProps = {
  onClick: (id: string) => void;
  style?: React.CSSProperties;
};

export const CompareSelectorRaider = (props: CompareSelectorRaiderProps) => {
  const [searchValue, setSearchValue] = useState<string>('');
  const defaultStyle: React.CSSProperties = {
    borderRadius: '2px',
    margin: '8px',
    padding: '8px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colorPalette['mediumBlueGrey'],
    ...props.style,
  };
  return (
    <div style={defaultStyle}>
      <RaiderSearchTextField
        name={'Raider ID'}
        defaultVariable={searchValue}
        handleInputUpdate={(value: string | undefined): void => {
          setSearchValue(value ?? '');
        }}
        style={{ width: '100%', margin: '8px' }}
      />
      <ClickableContainer
        onClick={() => {
          props.onClick(searchValue);
        }}
        style={{
          margin: '5px',
          padding: '5px',
          background: colorPalette['darkBlueGrey'],
        }}
      >
        <p>Search</p>
      </ClickableContainer>
    </div>
  );
};
