import { createFighterBuild, FighterBuild, getMetaScoreOutputLarge } from '@crypto-raider-tools/computation';
import { colorPalette } from '../../Helpers/Constants';
import { computeFightOutputEstimation } from '../../Helpers/FightOutputEstimation';
import { DetailedMobs } from '../../interfaces/detailedMobs';

type FighterBuildEstimatedWinRateProps = {
  playerFighterBuild: FighterBuild;
  opponentFighterBuild: FighterBuild;
  style?: React.CSSProperties;
  mobs?: DetailedMobs[];
};

export const FighterBuildEstimatedWinRate = (props: FighterBuildEstimatedWinRateProps) => {
  let winrate = computeFightOutputEstimation(props.playerFighterBuild, props.opponentFighterBuild).winRate;
  const winrateEnabled: boolean = Boolean(localStorage.getItem('winrateEnabled'));

  const defaultStyle: React.CSSProperties = {
    margin: '5px',
    textAlign: 'center',
    color: colorPalette['lightBlueGrey'],
    fontSize: '16px',
    ...props.style,
  };

  if (typeof props.mobs !== 'undefined') {
    let count: number = 0;
    const mobFighterBuild = createFighterBuild();
    for (const mob of props.mobs) {
      for (const fighterBuildKey of Object.keys(mobFighterBuild)) {
        mobFighterBuild[fighterBuildKey as keyof FighterBuild] = parseFloat(
          mob[fighterBuildKey as keyof DetailedMobs] as string
        );
      }
      count += computeFightOutputEstimation(props.playerFighterBuild, mobFighterBuild).winRate;
    }
    winrate = count / props.mobs.length;
  }

  return !winrateEnabled ? null : (
    <div style={defaultStyle}>
      <span>{'Estimated Winrate: '}</span>
      <span style={{ color: colorPalette[getMetaScoreOutputLarge(winrate, 100).toLowerCase()], fontWeight: '500' }}>
        {winrate.toFixed(1)}
      </span>
      <span>%</span>
    </div>
  );
};
