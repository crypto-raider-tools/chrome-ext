import { FighterBuild, Rarity, Stats } from '@crypto-raider-tools/computation';
import { useCallback, useEffect, useState } from 'react';
import { apiEntrypoint, log } from '../config';
import { CacheBuildCap } from '../interfaces/cacheBuildCap';
import { DetailedSyncRaider } from '../interfaces/detailedsyncraider';
import { DungeonSelectedForSim } from '../interfaces/dungeonSelectedForSim';
import { HydraCriteria, HydraPaginatedResponse } from '../interfaces/hydra';
import { IndexedInventoryItem } from '../interfaces/indexedInventoryItem';
import { AvatarTrigger } from '../interfaces/tammyAvatar';
import { StuffStats } from '../interfaces/stuffstats';
import { CompanionState } from './CompanionState/CompanionState';
import { TammyAvataProvider } from './Context/TammyAvatarContext';
import { TammySkinSelector } from './Premium/TammySkinSelector';
import { RaiderList } from './RaiderList/RaiderList';
import { Paginator } from './RaiderList/RaiderPaginator';
import { RaiderOrder } from './RaiderOrder/RaiderOrder';
import { RaiderSearch } from './RaiderSearch/RaiderSearch';
import { DungeonContextParent } from './DungeonContextParent';
import { ClickableContainer } from './Utils/ClickableContainer';
import { colorPalette } from '../Helpers/Constants';
import { LoadingProvider } from './Context/LoadingContext';
import { RaiderBuilder } from './RaiderBuilder/RaiderBuilder';
import { PopUpButton } from './Utils/PopUpButton';
import { createBuildCapCache, isPremium } from '../Helpers/Utils';
import { DetailedMobs } from '../interfaces/detailedMobs';
import { RaiderAbility } from '../interfaces/raiderability';
import { DropRateContextData, DropRateData, DropRateProvider, DungeonName } from './Context/DropRateContext';
import {
  fetchDungeonsData,
  fetchMobsData,
  fetchRaiderAbilities,
  fetchStuffStats,
  triggerUpdateServer,
} from '../Helpers/CompanionAPI';
import { triggerCRUpdateRaider } from '../Helpers/CRApi';
import { DetailedDungeon } from '../interfaces/detailedDungeon';

type CompanionContextProps = {
  metamask: string;
};

export const CompanionContext = (props: CompanionContextProps) => {
  /**
   * Meta Datas
   */
  const [isFilterOpen, setIsFilterOpen] = useState<boolean>(window.innerWidth > 750);
  const [stuffStats, setStuffStats] = useState<StuffStats[]>([]);
  const [mobsData, setMobsData] = useState<DetailedMobs[]>([]);
  const [raiderAbilitiesData, setRaiderAbilitiesData] = useState<RaiderAbility[]>([]);
  const [dropRatesData, setDropRatesData] = useState<DropRateContextData>({ dropRatesData: {} });

  const [currentMaxMeta] = useState<number>(80);
  const [dungeonSelectedForSim] = useState<DungeonSelectedForSim>({
    name: '',
    backgroundPosition: '',
  });
  const [tammyAvatarTrigger, setTammyAvatarTrigger] = useState<AvatarTrigger>({
    body: false,
    face: false,
    hair: false,
  });
  const [cacheBuildCap, setCacheBuildCap] = useState<CacheBuildCap>({});
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [notificationBarHtml, setNotificationBarHtml] = useState<string | undefined>();

  /**
   * Expand all raider
   */
  const [expandAllRaider, setExpandAllRaider] = useState<boolean>(
    !!localStorage.getItem('CRC_expandAllRaider') ?? false
  );
  const updateExpandAllRaider = (shouldExpand: boolean) => {
    if (shouldExpand) {
      localStorage.setItem('CRC_expandAllRaider', '1');
    } else {
      localStorage.removeItem('CRC_expandAllRaider');
    }
    setExpandAllRaider(shouldExpand);
  };

  /**
   * Raider criteria and page
   */
  const [raiderPage, setRaiderPage] = useState<number>(1);
  const [raiderCriterias, setRaiderCriterias] = useState<Array<HydraCriteria<DetailedSyncRaider>>>([]);
  const [raiderPerPage, setRaiderPerPage] = useState<number>(
    parseInt(localStorage.getItem('CRC_raiderPerPage') ?? '5')
  );
  useEffect(() => {
    localStorage.setItem('CRC_raiderPerPage', '' + raiderPerPage);
  }, [raiderPerPage]);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${apiEntrypoint}/configs/notificationBarHtml`);
      const object = await response.json();
      const notificationBarHtml = object.value;
      setNotificationBarHtml(notificationBarHtml ? notificationBarHtml : undefined);
    })();
  }, []);

  /**
   * Reload data from API
   */
  const triggerServerSync = useCallback(
    async (page?: number) => {
      if (!isLoading) setIsLoading(true);
      log('triggerServerSync', raiderCriterias);
      let url = new URL(`${apiEntrypoint}/sync_raiders`);
      url.searchParams.append('page', (page ?? raiderPage).toString());
      url.searchParams.append('itemsPerPage', raiderPerPage.toString());
      url.searchParams.append('customer.metamaskAddress', props.metamask);
      for (let index = 0; index < raiderCriterias.length; index++) {
        const criteria = raiderCriterias[index];
        url.searchParams.append(criteria.variable, criteria.value.toString());
      }
      const raiderRequest = await fetch(url.toString(), {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      if (raiderRequest.status !== 200) {
        log(`${url.toString()}: ${raiderRequest.status} ${raiderRequest.statusText}`);
        alert('Unable to get raider list, sry 😿');
        return;
      }
      const newRaiderList = await raiderRequest.json();
      setRaiderList(newRaiderList);
      setIsLoading(false);
      return Promise.resolve();
    },
    [raiderCriterias, raiderPage, raiderPerPage, isLoading, props.metamask]
  );

  /**
   * Raider list
   */
  const [raiderListIsLoading, setRaiderListIsLoading] = useState<boolean>(true);
  const [raiderList, setRaiderList] = useState<undefined | HydraPaginatedResponse<DetailedSyncRaider>>();
  const askRaiderPage = useCallback(
    async (page: number) => {
      setRaiderListIsLoading(true);
      setRaiderPage(page);
      log(`Getting raiders page ${page} for ${props.metamask}`);
      await triggerServerSync(page);
      setRaiderListIsLoading(false);
      return Promise.resolve();
    },
    [props.metamask, triggerServerSync]
  );

  useEffect(() => {
    const levels = raiderList?.['hydra:member'].reduce((previous: number[], current): Array<number> => {
      if (!previous) {
        previous = [];
      }
      if (!previous.includes(current.level ?? 1)) {
        previous.push(current.level ?? 1);
      }
      return previous;
    }, []) ?? [1];
    const newCapBuild: Record<number, Record<keyof FighterBuild, number>> = {};

    for (const level of [...levels, 1]) {
      newCapBuild[level] = createBuildCapCache(level);
    }
    log('Companion Context setCacheBuildCap: ', newCapBuild);
    setCacheBuildCap(newCapBuild);
  }, [raiderList]);

  useEffect(() => {
    askRaiderPage(1);
    // eslint-disable-next-line
  }, [raiderPerPage, raiderCriterias]);

  useEffect(() => {
    const getStuffStats = async (): Promise<void> => {
      let savedStuffStats = localStorage.getItem('CRC_stuffStats');
      if (savedStuffStats) {
        const save = JSON.parse(savedStuffStats);
        const diffTime = Math.abs(Date.now() - save.date);
        if (diffTime / (1000 * 60 * 60) <= 1) {
          setStuffStats(save.stuffStats);
          return Promise.resolve();
        }
      }
      const newStuffStats = await fetchStuffStats();
      const save = {
        date: Date.now(),
        stuffStats: newStuffStats,
      };
      localStorage.setItem('CRC_stuffStats', JSON.stringify(save));
      setStuffStats(newStuffStats);
      return Promise.resolve();
    };
    const getMobsData = async (): Promise<void> => {
      const newMobsData = await fetchMobsData();
      setMobsData(newMobsData);
      return Promise.resolve();
    };
    const getRaiderAbilitiesData = async (): Promise<void> => {
      const raiderAbilities = await fetchRaiderAbilities();
      setRaiderAbilitiesData(raiderAbilities);
      return Promise.resolve();
    };
    getStuffStats();
    getMobsData();
    getRaiderAbilitiesData();
  }, []);

  useEffect(() => {
    const initDropRatesData = async (): Promise<void> => {
      const dungeons: DetailedDungeon[] = await fetchDungeonsData();
      const dropRatesData: Record<DungeonName, DropRateData> = {};

      for (const dungeon of dungeons) {
        if (!dungeon.name) {
          continue;
        }
        dropRatesData[dungeon.name] = {
          dungeon,
          rarityCounts: {
            uncommon: 0,
            common: 0,
            rare: 0,
            epic: 0,
            legendary: 0,
            mythic: 0,
            heirloom: 0,
            limited: 0,
          },
          totalItems: 0,
        };
        if (!dungeon.droppableItems) {
          continue;
        }
        dungeon.droppableItems.forEach((itemName: string | undefined) => {
          const dStuffStats = stuffStats.find((ss) => ss['@id'] === itemName);
          if (dStuffStats && dStuffStats.slot !== 'knickknack') {
            dropRatesData[dungeon.name!].rarityCounts[dStuffStats.rarity as Rarity] += dStuffStats.count ?? 0;
            dropRatesData[dungeon.name!].totalItems += dStuffStats.count ?? 0;
          }
        });
      }
      setDropRatesData({ dropRatesData });

      return Promise.resolve();
    };
    initDropRatesData();
  }, [stuffStats]);

  /**
   * Handle equip action
   * @param raider
   * @param object
   * @returns
   */
  const handleUpdateRaiderServerSide = async (
    raider: DetailedSyncRaider,
    equippedItems: Array<IndexedInventoryItem> | undefined = undefined,
    raiderStats: Stats | undefined = undefined
  ) => {
    setIsLoading(true);
    if ((await triggerCRUpdateRaider(raider, equippedItems, raiderStats)) === false) return;
    await triggerUpdateServer(raider.id);
    await triggerServerSync();
  };

  /**
   * Feature flags
   */
  const [winrateEnabled, setWinrateEnabled] = useState<boolean>(false);

  const init = useCallback(async () => {
    setWinrateEnabled((await (await fetch(`${apiEntrypoint}/configs/winrateEnabled`)).json()).value === '1');
  }, []);

  useEffect(() => {
    init();
  }, [init]);

  useEffect(() => {
    localStorage.removeItem('winrateEnabled');
    log(`winrateEnabled: ${winrateEnabled}`);
    if (winrateEnabled) {
      localStorage.setItem('winrateEnabled', '1');
    }
  }, [winrateEnabled]);

  /**
   * Rendering
   */

  const raidersContainerStyle: React.CSSProperties = {
    display: 'flex',
  };

  const filterContainerStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: '300px',
    border: '1px solid black',
  };

  const resultContainerStyle: React.CSSProperties = {
    width: '100%',
    overflow: 'hidden',
  };

  const btnStyle: React.CSSProperties = {
    backgroundColor: colorPalette['experience'],
    borderRadius: '2px',
    color: 'white',
    fontWeight: '500',
    width: 'fit-content',
    marginLeft: '5px',
    marginBottom: '10px',
    padding: '0 .2em .1em .2em',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  };

  return (
    <>
      <DungeonContextParent triggerServerSync={triggerServerSync}>
        <DropRateProvider value={dropRatesData}>
          <LoadingProvider
            value={{
              isLoading,
            }}
          >
            <TammyAvataProvider
              value={{ tammyAvatarTrigger: tammyAvatarTrigger, setTammyAvatarTrigger: setTammyAvatarTrigger }}
            >
              <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
                <ClickableContainer
                  style={btnStyle}
                  onClick={() => {
                    setIsFilterOpen(!isFilterOpen);
                  }}
                >
                  Search 🔍
                </ClickableContainer>
                <ClickableContainer
                  style={{ ...btnStyle, ...{ opacity: isLoading ? 0.5 : 1 } }}
                  boxShadows={isLoading ? [] : undefined}
                  onClick={async () => {
                    if (isLoading) return;
                    setIsLoading(true);
                    await triggerUpdateServer();
                    await triggerServerSync();
                    setIsLoading(false);
                  }}
                >
                  Update 🔁
                </ClickableContainer>
                <CompanionState />
                {window.screen.width > 800 && isPremium() && (
                  <PopUpButton
                    buttonContent={
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                          flexDirection: 'column',
                        }}
                      >
                        <p style={{ fontSize: '25px' }}>⚒️</p>
                        <p>Raider Builder!</p>
                      </div>
                    }
                    buttonStyle={{
                      backgroundColor: colorPalette['mediumBlueGrey'],
                    }}
                  >
                    <RaiderBuilder currentMaxMeta={currentMaxMeta} stuffStats={stuffStats} mobsData={mobsData} />
                  </PopUpButton>
                )}
                <TammySkinSelector />
              </div>
              <div style={raidersContainerStyle}>
                {isFilterOpen && (
                  <div style={filterContainerStyle}>
                    <RaiderSearch
                      raiderList={raiderList}
                      isLoading={raiderListIsLoading}
                      updateRaiderCriterias={setRaiderCriterias}
                      raiderCriterias={raiderCriterias}
                      style={{ maxHeight: '100vh', position: 'sticky', top: '0', overflow: 'auto' }}
                    />
                  </div>
                )}
                <div style={resultContainerStyle}>
                  {notificationBarHtml && <div dangerouslySetInnerHTML={{ __html: notificationBarHtml }}></div>}
                  <RaiderOrder
                    raiderList={raiderList}
                    isLoading={raiderListIsLoading}
                    askRaiderPage={askRaiderPage}
                    updateRaiderCriterias={setRaiderCriterias}
                    raiderCriterias={raiderCriterias}
                  />
                  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <p style={{ fontSize: '16px', fontWeight: '500', marginRight: '16px' }}>Results: </p>
                    {raiderListIsLoading ? (
                      <div className="lds-dual-ring" style={{ width: '20px', height: '20px' }} />
                    ) : (
                      <p style={{ fontSize: '16px', fontWeight: '500' }}>{`${
                        raiderList?.['hydra:totalItems'] ?? 0
                      } raider(s)`}</p>
                    )}
                  </div>
                  <Paginator
                    page={raiderPage}
                    list={raiderList}
                    elementPerPage={raiderPerPage}
                    askPage={askRaiderPage}
                    updateElementPerPage={setRaiderPerPage}
                  />
                  <ClickableContainer
                    onClick={() => {
                      updateExpandAllRaider(!expandAllRaider);
                    }}
                    style={btnStyle}
                  >
                    {expandAllRaider ? 'Collapse all raider' : 'Expand all raider'}
                  </ClickableContainer>
                  <RaiderList
                    raiderPage={raiderPage}
                    raiderList={raiderList}
                    isLoading={raiderListIsLoading}
                    askRaiderPage={askRaiderPage}
                    expandAllRaider={expandAllRaider}
                    handleUpdateRaiderServerSide={handleUpdateRaiderServerSide}
                    stuffStats={stuffStats}
                    currentMaxMeta={currentMaxMeta}
                    dungeonSelectedForSim={dungeonSelectedForSim}
                    cacheBuildCap={cacheBuildCap}
                    mobsData={mobsData}
                    raiderAbilitiesData={raiderAbilitiesData}
                  />
                  <Paginator
                    page={raiderPage}
                    list={raiderList}
                    elementPerPage={raiderPerPage}
                    askPage={askRaiderPage}
                    updateElementPerPage={setRaiderPerPage}
                  />
                </div>
              </div>
            </TammyAvataProvider>
          </LoadingProvider>
        </DropRateProvider>
      </DungeonContextParent>
    </>
  );
};
