import { useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';
import { BoxShadow, ClickableContainer } from './ClickableContainer';

type PopUpButtonProps = {
  buttonContent: JSX.Element;
  children: JSX.Element;
  style?: React.CSSProperties;
  buttonStyle?: React.CSSProperties;
  buttonBoxShadows?: string[];
};

export const PopUpButton = (props: PopUpButtonProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const buttonStyle: React.CSSProperties = {
    borderRadius: '2px',
    backgroundColor: colorPalette['darkBlueGrey'],
    padding: '1px 5px',
    marginTop: '8px',
  };

  const popupParent: React.CSSProperties = {
    position: 'fixed',
    top: '0',
    left: '0',
    width: '100vw',
    height: '100vh',
    margin: '0',
    padding: '0',
    background: 'rgba(0,0,0,0.5)',
    zIndex: '9999',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  };

  return (
    <div>
      {isOpen ? (
        <div
          style={popupParent}
          onClick={() => {
            setIsOpen(false);
          }}
        >
          {props.children}
        </div>
      ) : (
        <ClickableContainer
          boxShadows={
            props.buttonBoxShadows
              ? props.buttonBoxShadows
              : [
                  BoxShadow(colorPalette['lightBlueGrey'], { x: 4, y: 4 }),
                  BoxShadow(colorPalette['mediumBlue'], { x: -4, y: -4 }),
                ]
          }
          onClick={() => {
            setIsOpen(true);
          }}
          style={{ ...buttonStyle, ...props.buttonStyle }}
        >
          {props.buttonContent}
        </ClickableContainer>
      )}
    </div>
  );
};
