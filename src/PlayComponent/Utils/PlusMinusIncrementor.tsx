import { colorPalette } from '../../Helpers/Constants';
import { BoxShadow, ClickableContainer } from './ClickableContainer';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { useEffect, useState } from 'react';

type PlusMinusIncrementorProps = {
  minValue: number;
  maxValue: number;
  value: number;
  setValue: (newValue: number) => void;
  increment?: number;
  style?: React.CSSProperties;
};

export const PlusMinusIncrementor = (props: PlusMinusIncrementorProps) => {
  const [inputValue, setInputValue] = useState<string>('' + props.value);

  const baseStyle: React.CSSProperties = {
    width: '80px',
    height: '20px',
    display: 'flex',
    ...props.style,
  };
  const valueContainerStyle: React.CSSProperties = {
    cursor: 'text',
    backgroundColor: colorPalette['mediumBlueGrey'],
    display: 'flex',
    width: '40%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  };
  const buttonStyle: React.CSSProperties = {
    cursor: 'pointer',
    display: 'flex',
    width: '30%',
    height: '100%',
    alignItems: 'center',
  };
  const disableButtonStyle: React.CSSProperties = {
    pointerEvents: 'none',
    transition: '0.5s',
    opacity: '0.2',
  };
  const minusButtonStyle: React.CSSProperties = {
    ...buttonStyle,
    backgroundColor: colorPalette['lightBlueGrey'],
    borderTopLeftRadius: '2px',
    borderBottomLeftRadius: '2px',
    ...(props.value - (props.increment ?? 1) < props.minValue ? disableButtonStyle : {}),
  };
  const plusButtonStyle: React.CSSProperties = {
    ...buttonStyle,
    backgroundColor: colorPalette['lightBlueGrey'],
    borderTopRightRadius: '2px',
    borderBottomRightRadius: '2px',
    ...(props.value + (props.increment ?? 1) > props.maxValue ? disableButtonStyle : {}),
  };

  useEffect(() => {
    setInputValue('' + props.value);
  }, [props.value]);

  const handlePlus = () => {
    const inc = props.increment ?? 1;
    if (props.value + inc <= props.maxValue) {
      props.setValue(props.value + inc);
    }
  };

  const handleMinus = () => {
    const inc = props.increment ?? 1;
    if (props.value - inc >= props.minValue) {
      props.setValue(props.value - inc);
    }
  };

  const applyInput = () => {
    const value = parseInt(inputValue);
    if (isNaN(value) || value === props.value) setInputValue('' + props.value);
    else if (value < props.minValue) {
      setInputValue('' + props.minValue);
      props.setValue(props.minValue);
    } else if (value > props.maxValue) {
      setInputValue('' + props.maxValue);
      props.setValue(props.maxValue);
    } else props.setValue(value);
  };

  return (
    <div style={baseStyle}>
      <ClickableContainer
        onClick={handleMinus}
        style={minusButtonStyle}
        boxShadows={[
          BoxShadow('rgb(169, 169, 169)', { x: 2, y: 2 }, 0, 0),
          BoxShadow('rgb(169, 169, 169)', { x: -2, y: -2 }, 0, 0),
        ]}
        multipleClickOnHoldSpeed={100}
      >
        <RemoveIcon />
      </ClickableContainer>
      <input
        style={valueContainerStyle}
        type="text"
        value={inputValue}
        onChange={(event) => {
          setInputValue(event.target.value);
        }}
        onFocus={(e) => {
          e.target.select();
          applyInput();
        }}
        onBlur={() => {
          applyInput();
        }}
        onKeyUp={(e) => {
          if (e.key === 'Enter') {
            applyInput();
          }
        }}
      />
      <ClickableContainer
        onClick={handlePlus}
        style={plusButtonStyle}
        boxShadows={[
          BoxShadow('rgb(169, 169, 169)', { x: 2, y: 2 }, 0, 2),
          BoxShadow('rgb(169, 169, 169)', { x: -2, y: -2 }, 0, 2),
        ]}
        multipleClickOnHoldSpeed={100}
      >
        <AddIcon />
      </ClickableContainer>
    </div>
  );
};
