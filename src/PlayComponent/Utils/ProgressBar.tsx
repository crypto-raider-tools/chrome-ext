import { colorPalette } from '../../Helpers/Constants';

interface ProgressBarProps {
  value: number;
  content: string;
  textStyle?: React.CSSProperties;
  barFillStyle?: React.CSSProperties;
  style?: React.CSSProperties;
}

export const ProgressBar = (props: ProgressBarProps) => {
  const barContainerStyle: React.CSSProperties = {
    backgroundColor: colorPalette['lightBlueGrey'],
    borderRadius: '2px',
    height: '15px',
    width: '100%',
    position: 'relative',
    ...props.style,
  };

  const barFillStyle: React.CSSProperties = {
    backgroundColor: colorPalette['experience'],
    borderRadius: '2px',
    height: '15px',
    ...props.barFillStyle,
  };

  const textStyle: React.CSSProperties = {
    position: 'absolute',
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    fontWeight: '700',
    letterSpacing: '0.5px',
    lineHeight: 'inherit',
    textAlign: 'center',
    fontSize: '11px',
    textOverflow: 'clip',
    zIndex: '2',
    ...props.textStyle,
  };
  return (
    <div style={barContainerStyle}>
      <p style={textStyle}>{props.content}</p>
      <div style={{ ...barFillStyle, width: props.value + '%' }} />
    </div>
  );
};
