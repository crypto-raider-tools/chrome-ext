import { ReactChild, ReactFragment, ReactPortal, useCallback, useEffect, useState } from 'react';

export const BoxShadow = (
  color: string,
  offset: { x: number; y: number },
  blurRadius?: number,
  spreadRadius?: number,
  inset?: boolean
): string => {
  let bR: number = blurRadius ? blurRadius : 15;
  let sR: number = spreadRadius ? spreadRadius : 1;
  let iSet: string = inset ? 'inset ' : '';
  return iSet + offset.x + 'px ' + offset.y + 'px ' + bR + 'px ' + sR + 'px ' + color;
};

type ClickableContainerProps = {
  boxShadows?: string[];
  onClick: () => void;
  onHover?: (isHover: boolean) => void;
  children: boolean | ReactChild | ReactFragment | ReactPortal | null | undefined;
  multipleClickOnHoldSpeed?: number;
  style?: React.CSSProperties;
  onHoverStyle?: React.CSSProperties;
  className?: string;
  disabled?: boolean;
};

export const ClickableContainer = (props: ClickableContainerProps) => {
  const [isHolding, setIsHolding] = useState(false);
  const [isHovered, setIsHovered] = useState<boolean>(false);
  let boxShadow = '';
  if (props.boxShadows) {
    let idx = 0;
    for (const bS of props.boxShadows) {
      boxShadow += bS;
      idx++;
      if (idx < props.boxShadows.length) boxShadow += ', ';
    }
  } else boxShadow = BoxShadow('rgb(169, 169, 169)', { x: 4, y: 4 }) + ', ' + BoxShadow('white', { x: -4, y: -4 });
  const hoveredStyle: React.CSSProperties = {
    boxShadow: boxShadow,
    ...props.onHoverStyle,
  };
  const baseStyle: React.CSSProperties = {
    transition: '0.2s',
    cursor: 'pointer',
  };

  const style: React.CSSProperties = {
    ...baseStyle,
    ...(isHovered ? hoveredStyle : {}),
    ...props.style,
  };

  useEffect(() => {
    let timerId: any;
    if (isHolding) {
      timerId = setTimeout(props.onClick, props.multipleClickOnHoldSpeed);
    } else {
      clearTimeout(timerId);
    }

    return () => {
      clearTimeout(timerId);
    };
  }, [props.onClick, props.multipleClickOnHoldSpeed, isHolding]);

  const startHolding = useCallback(() => {
    setIsHolding(true);
  }, []);
  const stopHolding = useCallback(() => {
    setIsHolding(false);
  }, []);

  return (
    <div
      className={props.className}
      onClick={(e) => {
        if (props.disabled) return;
        e.preventDefault();
        e.stopPropagation();
        props.onClick();
      }}
      onMouseDown={() => {
        if (props.disabled) return;
        if (props.multipleClickOnHoldSpeed) {
          startHolding();
        }
      }}
      onMouseUp={() => {
        if (props.disabled) return;
        if (props.multipleClickOnHoldSpeed) {
          stopHolding();
        }
      }}
      onMouseEnter={(e) => {
        if (props.disabled) return;
        e.preventDefault();
        e.stopPropagation();
        setIsHovered(true);
        if (props.onHover) {
          props.onHover(true);
        }
      }}
      onMouseLeave={(e) => {
        if (props.disabled) return;
        e.preventDefault();
        e.stopPropagation();
        setIsHovered(false);
        if (props.onHover) {
          props.onHover(false);
        }
        if (props.multipleClickOnHoldSpeed) {
          stopHolding();
        }
      }}
      style={style}
    >
      {props.children}
    </div>
  );
};
