import React, { useState } from 'react';
import { colorPalette } from '../../Helpers/Constants';

type FilterRadiusButtonProps = {
  name: string;
  onClick: () => void;
  selectorDiv: JSX.Element;
  style?: React.CSSProperties;
  buttonStyle?: React.CSSProperties;
};

export const FilterRadiusButton = (props: FilterRadiusButtonProps) => {
  const [isHovered, setIsHovered] = useState<boolean>(false);
  const hoveredStyle: React.CSSProperties = {
    filter: 'drop-shadow(0 0 6px #0009) brightness(140%)',
  };
  const parentButtonStyle: React.CSSProperties = {
    display: 'flex',
    transition: '0.5s',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: '8px',
    marginTop: '8px',
    backgroundColor: '#4da1f07a',
    borderRadius: '2px',
    ...(isHovered ? hoveredStyle : {}),
    ...props.style,
    cursor: 'pointer',
  };
  const buttonStyle: React.CSSProperties = {
    border: '4px solid ' + colorPalette['experience'],
    backgroundColor: colorPalette['lightBlueGrey'],
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '8px',
    position: 'relative',
    borderRadius: '50%',
    MozBorderRadius: '50%',
    WebkitBorderRadius: '50%',
    height: '30px',
    width: '30px',
  };

  const textStyle: React.CSSProperties = {
    fontWeight: '500',
    fontSize: '16px',
    color: 'white',
    margin: '0px 8px 0px 8px',
  };

  return (
    <div
      style={parentButtonStyle}
      onClick={() => props.onClick()}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <p style={textStyle}>{props.name}</p>
      <div style={{ ...buttonStyle, ...props.buttonStyle }}>{props.selectorDiv}</div>
    </div>
  );
};
