import React from 'react';
import { ReactChild, ReactFragment, ReactPortal } from 'react';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { colorPalette } from '../../Helpers/Constants';

export const WrapContainer = (props: WrapContainerType) => {
  const [isOpen, setIsOpen] = React.useState<boolean>(true);

  const wrapHeaderStyle: React.CSSProperties = {
    height: '40px',
    border: '1px solide black',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colorPalette['mediumBlueGrey'],
  };
  const wrapHeaderTextStyle: React.CSSProperties = {
    fontSize: '16px',
    fontWeight: '500',
    color: 'white',
  };
  const wrapContainerStyle: React.CSSProperties = {
    overflow: 'hidden',
    maxHeight: isOpen ? '1000px' : '0px',
  };

  const wrapArrowStyle: React.CSSProperties = {
    transition: '0.5s',
    transform: isOpen ? 'rotate(0deg)' : 'rotate(180deg)',
  };

  return (
    <div
      style={{
        width: '100%',
        ...props.style,
      }}
    >
      <div style={wrapHeaderStyle} onClick={() => setIsOpen(!isOpen)}>
        <ArrowDropDownIcon style={wrapArrowStyle} />
        <p style={wrapHeaderTextStyle}>{props.containerName}</p>
      </div>
      <div style={wrapContainerStyle}>{props.children}</div>
    </div>
  );
};

type WrapContainerType = {
  containerName: string;
  children: boolean | ReactChild | ReactFragment | ReactPortal | null | undefined;
  style?: React.CSSProperties;
};
