import { useCallback, useEffect, useState } from 'react';
import { apiEntrypoint, log } from '../config';
import { HydraPaginatedResponse } from '../interfaces/hydra';
import { SyncStuff } from '../interfaces/syncstuff';
import { Paginator } from './RaiderList/RaiderPaginator';
import { StuffFilters } from './CRCStuff/StuffFilters';
import { Item } from './CRCStuff/Item';
import { StuffFilter, StuffQueryStrings, StuffOrderBy, QueryFilter, QueryOrder } from './CRCStuff/types';
import { DetailedSyncRaider } from '../interfaces/detailedsyncraider';
import { StuffOrderBys } from './CRCStuff/StuffOrderBys';
import { ClickableContainer } from './Utils/ClickableContainer';
import { colorPalette } from '../Helpers/Constants';
import { isPremium } from '../Helpers/Utils';
import { fetchSyncStuffs, triggerUpdateServer } from '../Helpers/CompanionAPI';

const filters: Array<StuffFilter> = [
  {
    displayName: 'Equipped',
    queryString: 'isEquipped',
    valueType: 'boolean',
    multiple: false,
  },
  {
    displayName: 'In wallet',
    queryString: 'raider.currentlyInWallet',
    valueType: 'boolean',
    multiple: false,
  },
  {
    displayName: 'Slot',
    queryString: 'slot[]',
    valueType: 'slots',
    multiple: true,
  },
  {
    displayName: 'Rarity',
    queryString: 'rarity[]',
    valueType: 'rarities',
    multiple: true,
  },
  {
    displayName: 'Raider',
    queryString: 'raider.id[]',
    valueType: 'raiders',
    multiple: true,
  },
  {
    displayName: 'Item level (min)',
    queryString: 'level[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Item level (max)',
    queryString: 'level[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Strength (min)',
    queryString: 'strength[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Strength (max)',
    queryString: 'strength[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Intelligence (min)',
    queryString: 'intelligence[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Intelligence (max)',
    queryString: 'intelligence[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Agility (min)',
    queryString: 'agility[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Agility (max)',
    queryString: 'agility[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Wisdom (min)',
    queryString: 'wisdom[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Wisdom (max)',
    queryString: 'wisdom[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Charm (min)',
    queryString: 'charm[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Charm (max)',
    queryString: 'charm[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Luck (min)',
    queryString: 'luck[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Luck (max)',
    queryString: 'luck[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Price (min)',
    queryString: 'sellingPrice[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Price (max)',
    queryString: 'sellingPrice[lte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Total Stat Points (min)',
    queryString: 'totalStatPoints[gte]',
    valueType: 'number',
    multiple: false,
  },
  {
    displayName: 'Total Stat Points (max)',
    queryString: 'totalStatPoints[lte]',
    valueType: 'number',
    multiple: false,
  },
];

const orderBys: Array<StuffOrderBy> = [
  {
    displayName: 'Strength',
    queryString: 'order[strength]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Intelligence',
    queryString: 'order[intelligence]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Agility',
    queryString: 'order[agility]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Wisdom',
    queryString: 'order[wisdom]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Charm',
    queryString: 'order[charm]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Luck',
    queryString: 'order[luck]',
    needsMinFilter: true,
    needsMaxFilter: false,
  },
  {
    displayName: 'Slot',
    queryString: 'order[slot]',
    needsMinFilter: false,
    needsMaxFilter: false,
  },
  {
    displayName: 'Name',
    queryString: 'order[name]',
    needsMinFilter: false,
    needsMaxFilter: false,
  },
  {
    displayName: 'Rarity',
    queryString: 'order[rarity]',
    needsMinFilter: false,
    needsMaxFilter: false,
  },
  {
    displayName: 'Total stat points',
    queryString: 'order[totalStatPoints]',
    needsMinFilter: false,
    needsMaxFilter: false,
  },
  {
    displayName: 'Selling price',
    queryString: 'order[sellingPrice]',
    needsMinFilter: true,
    needsMaxFilter: true,
  },
  {
    displayName: 'Level',
    queryString: 'order[level]',
    needsMinFilter: false,
    needsMaxFilter: false,
  },
];

type SyncStuffId = string;

type CRCStuffProps = {
  metamask: string;
};

export const CRCStuff = (props: CRCStuffProps) => {
  const [syncStuffs, setSyncStuffs] = useState<undefined | HydraPaginatedResponse<SyncStuff>>();
  const [isSyncStuffLoading, setIsSyncStuffLoading] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [needsUpdate, setNeedsUpdate] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);
  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [elementPerPage, setElementPerPage] = useState<number>(100);
  const [raiders, setRaiders] = useState<undefined | HydraPaginatedResponse<DetailedSyncRaider>>();
  const [expandedItems, setExpandedItems] = useState<Record<number, boolean>>({});
  const [selectedItems, setSelectedItems] = useState<Array<SyncStuffId>>([]);
  const [processItemSell, setProcessItemSell] = useState<string | undefined>();
  const [expandAllItems, setExpandAllItems] = useState<boolean>(
    localStorage.getItem('CRC_stuff_expand_all') === 'true'
  );
  const [currentFilters, setCurrentFilters] = useState<Array<QueryFilter>>(
    JSON.parse(localStorage.getItem('CRC_stuff_current_filters') ?? '[]')
  );
  const [disabledFilters, setDisabledFilters] = useState<Array<StuffQueryStrings>>(
    JSON.parse(localStorage.getItem('CRC_stuff_disabled_filters') ?? '[]')
  );
  const [currentOrderBys, setCurrentOrderBys] = useState<Array<QueryOrder>>(
    JSON.parse(localStorage.getItem('CRC_stuff_current_order_bys') ?? '[]')
  );

  useEffect(() => {
    localStorage.setItem('CRC_stuff_current_filters', JSON.stringify(currentFilters));
  }, [currentFilters]);

  useEffect(() => {
    localStorage.setItem('CRC_stuff_current_order_bys', JSON.stringify(currentOrderBys));
  }, [currentOrderBys]);

  useEffect(() => {
    localStorage.setItem('CRC_stuff_disabled_filters', JSON.stringify(disabledFilters));
  }, [disabledFilters]);

  useEffect(() => {
    const newExtendedItems: Record<number, boolean> = {};
    let idx = 0;
    while (idx < elementPerPage) {
      newExtendedItems[idx] = expandAllItems;
      idx++;
    }
    setExpandedItems(newExtendedItems);
  }, [syncStuffs, expandAllItems, elementPerPage]);

  useEffect(() => {
    localStorage.setItem('CRC_stuff_expand_all', expandAllItems ? 'true' : 'false');
  }, [expandAllItems]);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${apiEntrypoint}/sync_raiders?itemsPerPage=100000`, {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      const json = await response.json();
      setRaiders(json);
    })();
  }, []);

  const triggerSyncStuffFetch = useCallback(
    async (shouldCancel: boolean) => {
      setIsSyncStuffLoading(true);
      const data = await fetchSyncStuffs(elementPerPage, page, props.metamask, currentFilters, currentOrderBys);
      if (!shouldCancel) {
        setSyncStuffs(data);
        setIsLoading(false);
        setIsSyncStuffLoading(false);
        setNeedsUpdate(false);
      }
    },
    [currentFilters, currentOrderBys, elementPerPage, page, props.metamask]
  );

  useEffect(() => {
    let shouldCancel = false;
    triggerSyncStuffFetch(shouldCancel);

    return () => {
      shouldCancel = true;
    };
  }, [needsUpdate, triggerSyncStuffFetch]);

  if (isLoading) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '100px',
        }}
      >
        🔁 Loading
      </div>
    );
  }

  const handleFilterCriteriaChange = (incomingFilters: StuffFilter[]) => {
    // Create new filters without the one that was changed
    const queryStrings = incomingFilters.map((filter) => filter.queryString);
    let newFilters = [...currentFilters.filter((f) => !queryStrings.includes(f.queryString))];

    incomingFilters.forEach((incomingFilter) => {
      // If no value, do nothing
      if (
        typeof incomingFilter.value === 'undefined' ||
        (typeof incomingFilter.value === 'object' && incomingFilter.value.length === 0)
      ) {
        return;
      }
      // If simple, add to existing filters
      if (
        incomingFilter.multiple === false &&
        typeof incomingFilter.value === 'string' &&
        incomingFilter.value.length > 0
      ) {
        newFilters.push(incomingFilter as QueryFilter);
        return;
      }
      // If multiple, add to existing filters in loop
      if (
        incomingFilter.multiple === true &&
        typeof incomingFilter.value === 'object' &&
        incomingFilter.value.length > 0
      ) {
        incomingFilter.value.forEach((f) => {
          newFilters.push({
            ...incomingFilter,
            value: f,
          });
        });
        return;
      }
    });
    setCurrentFilters(newFilters);
  };

  const handleOrderByCriteriaChange = (incomingOrderBys: StuffOrderBy[]) => {
    // Create new filters without the one that was changed
    const queryStrings = incomingOrderBys.map((orderBy) => orderBy.queryString);
    let newOrderBys = [...currentOrderBys.filter((o) => !queryStrings.includes(o.queryString))];

    incomingOrderBys.forEach((incomingOrderBy) => {
      // If no value, do nothing
      if (typeof incomingOrderBy.value === 'undefined') {
        return;
      }
      // If simple, add to existing filters
      if (typeof incomingOrderBy.value === 'string') {
        newOrderBys.push(incomingOrderBy);
        return;
      }
    });

    setCurrentOrderBys(newOrderBys);
  };

  const btnStyle: React.CSSProperties = {
    backgroundColor: colorPalette['experience'],
    borderRadius: '2px',
    color: 'white',
    fontWeight: '500',
    width: 'fit-content',
    marginLeft: '5px',
    marginBottom: '10px',
    padding: '0 .2em .1em .2em',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  };

  const handleUnitSell = (stuff: SyncStuff) => {
    if (!window.confirm(`Sell ${stuff?.['rarity']} item "${stuff?.['name']}"?`)) {
      return;
    }
    if (selectedItems.length > 0 && !window.confirm('⚠️ This will unselect all selected items. Are you sure?')) {
      return;
    }
    setSelectedItems([]);
    (async () => {
      setIsSyncStuffLoading(true);
      let request = await fetch(`${apiEntrypoint}${stuff?.['@id']}/sell`, {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      let response = await request.json();
      if (response.error) {
        alert(response.error);
        setIsSyncStuffLoading(false);
        return;
      }
      if (page !== 1) {
        setPage(1);
      } else {
        setNeedsUpdate(true);
      }
    })();
  };

  const handleMultipleSell = async () => {
    setProcessItemSell(`Fetching ${selectedItems.length} items...`);
    let stuffsToSell: Array<SyncStuff> = [];
    await Promise.all(
      selectedItems.map((itemId) => {
        return new Promise<void>(async (resolve) => {
          const itemRequest = await fetch(`${apiEntrypoint}${itemId}`, {
            headers: {
              'x-original-token': document.cookie,
            },
          });
          const itemResponse = await itemRequest.json();
          stuffsToSell.push(itemResponse);
          resolve();
        });
      })
    );
    stuffsToSell = stuffsToSell.sort((a, b) => {
      return (b?.['index'] ?? 0) - (a?.['index'] ?? 0);
    });
    let errors = 0;
    for (let index = 0; index < stuffsToSell.length; index++) {
      const stuff = stuffsToSell[index];
      setProcessItemSell(
        `${index + 1}/${selectedItems.length}: Selling item #${stuff?.index} of raider #${stuff?.raider?.replace(
          '/sync_raiders/',
          ''
        )}`
      );
      log(`Selling item ${stuff?.index} of ${stuff?.raider}`);
      const request = await fetch(`${apiEntrypoint}${stuff?.['@id']}/sell`, {
        headers: {
          'x-original-token': document.cookie,
        },
      });
      const response = await request.json();
      if (response.error) {
        errors++;
      }
    }
    if (page !== 1) {
      setPage(1);
    } else {
      setNeedsUpdate(true);
    }
    if (errors > 0) {
      alert(`Error, could not sell ${errors} items.`);
    }
    setProcessItemSell(undefined);
    setSelectedItems([]);
    setTotalPrice(0);
  };

  if (processItemSell) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '150px',
        }}
      >
        🔁 {processItemSell}
      </div>
    );
  }

  // Global parent
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        alignContent: 'stretch',
      }}
    >
      <ClickableContainer
        style={{ ...btnStyle, ...{ opacity: isLoading ? 0.5 : 1 } }}
        boxShadows={isLoading ? [] : undefined}
        onClick={async () => {
          if (isLoading) return;
          setIsSyncStuffLoading(true);
          await triggerUpdateServer();
          await triggerSyncStuffFetch(false);
          setIsSyncStuffLoading(false);
        }}
      >
        Update 🔁
      </ClickableContainer>
      <StuffFilters
        filters={filters}
        currentFilters={currentFilters}
        handleCriteriaChange={handleFilterCriteriaChange}
        raiders={raiders}
        disabledFilters={disabledFilters}
        resetFilters={() => {
          setCurrentFilters([]);
        }}
      />
      <StuffOrderBys
        orderBys={orderBys}
        currentOrderBys={currentOrderBys}
        handleOrderByCriteriaChange={handleOrderByCriteriaChange}
        handleFilterCriteriaChange={handleFilterCriteriaChange}
        disabledFilters={disabledFilters}
        updateDisabledFilters={setDisabledFilters}
      />
      <Paginator
        page={page}
        askPage={setPage}
        elementPerPage={elementPerPage}
        updateElementPerPage={setElementPerPage}
        elementPerPageRange={[100, 250, 500, 1000]}
        list={syncStuffs}
      />
      <div style={{ width: '100%', display: 'flex', justifyContent: 'space-around' }}>
        <ClickableContainer
          onClick={() => {
            setExpandAllItems(!expandAllItems);
          }}
          style={btnStyle}
        >
          {expandAllItems ? 'Minimize Items' : 'Expand Items'}
        </ClickableContainer>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <div style={{ display: 'flex', alignItems: 'flex-start' }}>
            <p
              style={{
                fontSize: '1.1em',
                color: colorPalette['lightBlueGrey'],
                fontWeight: '500',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              Total:&nbsp;<span style={{ color: 'white' }}>{totalPrice.toFixed(3)}</span>
              <img
                style={{ width: '20px', height: '20px', imageRendering: 'pixelated', marginLeft: '4px' }}
                alt="aurum"
                src="https://storage.googleapis.com/crypto-raiders-assets/web/currencies/aurum.png"
              />
            </p>
            <ClickableContainer
              onClick={() => {
                if (window.confirm(`⚠️ You are about to sell ${selectedItems.length} items. Are you sure?`)) {
                  handleMultipleSell();
                }
              }}
              style={btnStyle}
            >
              {`Sell ${selectedItems.length} item${selectedItems.length > 1 ? 's' : ''}`}
            </ClickableContainer>
            <ClickableContainer
              onClick={() => {
                setSelectedItems([]);
                setTotalPrice(0);
              }}
              style={btnStyle}
            >
              Unselect All
            </ClickableContainer>
            <ClickableContainer
              onClick={() => {
                const newSelectedItems: string[] = selectedItems;
                let newTotalPrice = totalPrice;
                syncStuffs?.['hydra:member'].forEach((stuff) => {
                  if (
                    stuff['@id'] &&
                    stuff.isEquipped === false &&
                    (stuff.sellingPrice ?? 0) > 0 &&
                    newSelectedItems.includes(stuff['@id']) === false
                  ) {
                    newSelectedItems.push(stuff['@id']);
                    newTotalPrice += stuff.sellingPrice ?? 0;
                  }
                });
                setSelectedItems(newSelectedItems);
                setTotalPrice(newTotalPrice);
              }}
              style={{ ...btnStyle, ...(isPremium() ? {} : { opacity: '0.5', pointerEvents: 'none' }) }}
            >
              Select All items on page
            </ClickableContainer>
          </div>
          {!isPremium() && (
            <p
              style={{
                fontSize: '1.1em',
                color: colorPalette['lightBlueGrey'],
                fontWeight: '500',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              ⚠️Multiple item selection is a&nbsp;
              <span style={{ color: colorPalette['inferior'] }}> {'Premium'} </span>&nbsp;feature⚠️
            </p>
          )}
        </div>
      </div>
      <div
        style={{
          marginTop: '10px',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center',
        }}
      >
        {!isSyncStuffLoading &&
          syncStuffs?.['hydra:member'].map((stuff, index) => {
            const isInWallet =
              raiders?.['hydra:member'].find((r) => r['@id'] === stuff.raider)?.currentlyInWallet === true;
            return (
              <Item
                key={stuff['@id']}
                isLoading={false}
                item={stuff}
                isExpanded={expandedItems[index]}
                toggleExpand={() => {
                  setExpandedItems({ ...expandedItems, [index]: !expandedItems[index] });
                }}
                owner={raiders?.['hydra:member'].find((r) => r['@id'] === stuff.raider)}
                isSelected={selectedItems.includes(stuff?.['@id'] ?? '')}
                unitSell={() => {
                  handleUnitSell(stuff);
                }}
                isSelectable={
                  stuff?.['@id'] !== undefined &&
                  stuff?.['isEquipped'] === false &&
                  (stuff?.['sellingPrice'] ?? 0) > 0 &&
                  isInWallet
                }
                toggleSelect={() => {
                  if (!stuff?.['@id'] || stuff?.['isEquipped'] === true) {
                    return;
                  }
                  let newSelectedItems = [...selectedItems];
                  let newTotalPrice = totalPrice;

                  if (isPremium()) {
                    if (newSelectedItems.includes(stuff?.['@id'] ?? '')) {
                      newSelectedItems = newSelectedItems.filter((id) => id !== stuff?.['@id']);
                      newTotalPrice = totalPrice - (stuff.sellingPrice ?? 0);
                    } else {
                      newSelectedItems.push(stuff?.['@id']);
                      newTotalPrice = totalPrice + (stuff.sellingPrice ?? 0);
                    }
                  } else {
                    if (newSelectedItems.includes(stuff?.['@id'] ?? '')) {
                      newSelectedItems = [];
                      newTotalPrice = 0;
                    } else {
                      newSelectedItems = [stuff?.['@id']];
                      newTotalPrice = stuff.sellingPrice ?? 0;
                    }
                  }
                  setTotalPrice(newTotalPrice);
                  setSelectedItems(newSelectedItems);
                }}
              />
            );
          })}
        {isSyncStuffLoading && (
          <>
            {[...Array(elementPerPage)].map((_i, index) => {
              return (
                <Item
                  key={index}
                  isLoading={true}
                  isExpanded={expandedItems[index]}
                  toggleExpand={() => {}}
                  owner={undefined}
                  unitSell={() => {}}
                  toggleSelect={() => {}}
                  isSelected={false}
                  isSelectable={false}
                />
              );
            })}
          </>
        )}
      </div>
      <Paginator
        page={page}
        askPage={setPage}
        elementPerPage={elementPerPage}
        updateElementPerPage={setElementPerPage}
        elementPerPageRange={[100, 250, 500, 1000]}
        list={syncStuffs}
      />
    </div>
  );
};
