import React from 'react';
import { colorPalette } from '../../Helpers/Constants';

export const btnStyle: React.CSSProperties = {
  backgroundColor: colorPalette['mediumBlueGrey'],
  borderRadius: '2px',
  color: 'white',
  fontWeight: '500',
  width: 'fit-content',
  marginLeft: '5px',
  marginBottom: '10px',
  padding: '5px 8px 5px 8px',
  display: 'flex',
  alignContent: 'center',
  alignItems: 'center',
  justifyContent: 'center',
  textAlign: 'center',
  margin: '8px',
};
