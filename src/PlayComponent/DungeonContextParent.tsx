import { useState, useEffect } from 'react';
import { apiEntrypoint, log } from '../config';
import { DungeonClearMessage } from '../interfaces/dungeonMessage';
import { DungeonRunData, DungeonGridConfig, DisplayInfo, Position } from '../interfaces/dungeonSettings';
import { MessagePassing } from '../interfaces/messagePassing';
import { DungeonSettingsProvider } from './Context/DungeonSettingsContext';
import { defaultGridConfig, updateConfigWithDisplayInfo } from './Raider/Dungeons/DungeonRaidGridHelpers';

type DungeonContextParentProps = {
  children?: JSX.Element | string;
  triggerServerSync: () => Promise<void>;
};

export const DungeonContextParent = (props: DungeonContextParentProps) => {
  const [currentRunningRaids, setCurrentRunningRaids] = useState<Array<DungeonRunData>>([]);
  const [dungeonGridConfig, setDungeonGridConfig] = useState<DungeonGridConfig>(defaultGridConfig());
  const [selectedMonitor, setSelectedMonitor] = useState<number>(
    parseInt(localStorage.getItem('CRC_Dungeon_Grid_Config_Selected_Monitor') ?? '0')
  );
  const [lastWindowPosition, setLastWindowPosition] = useState<Position>({ x: 0, y: -1 });
  const [displayInfos, setDisplayInfos] = useState<Array<DisplayInfo>>([]);

  const getDisplayInfo = (displays: Array<DisplayInfo>, monitorId: number): DisplayInfo => {
    if (displays.length < monitorId) {
      monitorId = 0;
      setSelectedMonitor(0);
    }
    return displays[monitorId];
  };

  useEffect(() => {
    const message: MessagePassing = {
      type: 'GetDisplayInfos',
      data: {},
    };
    chrome.runtime.sendMessage<MessagePassing>(message, (__response: MessagePassing) => {});
  }, []);

  useEffect(() => {
    let messageListener = (request: MessagePassing, __sender: any, sendResponse: (result?: any) => void) => {
      if (request.type === 'Ping') sendResponse({ ...request, type: 'Pong' });
      else if (request.type === 'ReturnDisplayInfos') {
        setDisplayInfos(request.data.displayInfos ?? []);
        if (request.data.displayInfos) {
          const displayInfo = getDisplayInfo(request.data.displayInfos, selectedMonitor);
          const newConfig = { ...dungeonGridConfig };
          updateConfigWithDisplayInfo(displayInfo, newConfig);
          setDungeonGridConfig(newConfig);
        }
        sendResponse({ type: 'Ok' });
      } else if (request.type === 'SendDungeonWSData') {
        let wsData = request.data as any;

        log('wsData', wsData);
        if (wsData?.eventType !== 'loot_result') {
          log('Not dungeon end');
          return;
        }
        log('Dungeon end, loot:', wsData.eventData?.loot);

        let secret: string = new URL(request.url!).searchParams.get('secret') ?? '';
        let dungeonData: DungeonClearMessage = {
          secret: secret,
          output: 'Victory',
        };
        if (dungeonData.secret) {
          setTimeout(async () => {
            const raiderId = wsData?.instanceParticipants?.[0]?.tokenId ?? 0;
            await fetch(`${apiEntrypoint}/update-sync-raiders${raiderId ? `/${raiderId}` : ''}`, {
              headers: {
                'x-original-token': document.cookie,
              },
            });
            props.triggerServerSync();
          }, 1000);
        }
        const raid = currentRunningRaids.find((d) => d.secret === secret);
        if (!raid) return;
        const newCurrentRunningRaids: Array<DungeonRunData> = [...currentRunningRaids];
        newCurrentRunningRaids.splice(currentRunningRaids.indexOf(raid), 1);
        setCurrentRunningRaids(newCurrentRunningRaids);
        if (newCurrentRunningRaids.length === 0) setLastWindowPosition({ x: 0, y: -1 });
      }
      sendResponse({ type: 'Ok' });
    };
    chrome.runtime.onMessage.addListener(messageListener);
    return () => {
      chrome.runtime.onMessage.removeListener(messageListener);
    };
    // eslint-disable-next-line
  }, [currentRunningRaids]);
  return (
    <DungeonSettingsProvider
      value={{
        currentRunningRaids: currentRunningRaids,
        setCurrentRunningRaids: setCurrentRunningRaids,
        dungeonGridConfig: dungeonGridConfig,
        updateDungeonGridConfig: (config: DungeonGridConfig) => {
          updateConfigWithDisplayInfo(getDisplayInfo(displayInfos, selectedMonitor), config);
          setDungeonGridConfig(config);
        },
        lastWindowPosition: lastWindowPosition,
        setLastWindowPosition: setLastWindowPosition,
        selectedMonitor: selectedMonitor,
        setSelectedMonitor: (newValue: number) => {
          setSelectedMonitor(newValue);
          const newConfig = { ...dungeonGridConfig };
          updateConfigWithDisplayInfo(getDisplayInfo(displayInfos, newValue), newConfig);
          setDungeonGridConfig(newConfig);
        },
        numberMonitors: displayInfos.length > 0 ? displayInfos.length : 1,
      }}
    >
      {props.children}
    </DungeonSettingsProvider>
  );
};
