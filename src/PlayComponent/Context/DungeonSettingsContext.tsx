import React from 'react';
import { DungeonRunData, DungeonGridConfig, Position } from '../../interfaces/dungeonSettings';
import { defaultGridConfig } from '../Raider/Dungeons/DungeonRaidGridHelpers';

export type DungeonSettingsContextData = {
  currentRunningRaids: Array<DungeonRunData>;
  setCurrentRunningRaids: (value: Array<DungeonRunData>) => void;
  dungeonGridConfig: DungeonGridConfig;
  updateDungeonGridConfig: (config: DungeonGridConfig) => void;
  lastWindowPosition: Position;
  setLastWindowPosition: (value: Position) => void;
  selectedMonitor: number;
  setSelectedMonitor: (value: number) => void;
  numberMonitors: number;
};

const DungeonSettingsContext = React.createContext<DungeonSettingsContextData>({
  currentRunningRaids: [],
  setCurrentRunningRaids: (__value) => {},
  dungeonGridConfig: defaultGridConfig(),
  updateDungeonGridConfig: (__value) => {},
  lastWindowPosition: { x: 0, y: 0 },
  setLastWindowPosition: (__value) => {},
  selectedMonitor: 0,
  setSelectedMonitor: (__value) => {},
  numberMonitors: 1,
});
export const DungeonSettingsProvider = DungeonSettingsContext.Provider;
export default DungeonSettingsContext;
