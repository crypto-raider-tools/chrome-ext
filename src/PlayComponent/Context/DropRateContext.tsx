import { Rarity } from '@crypto-raider-tools/computation';
import React from 'react';
import { Dungeon } from '../../interfaces/dungeon';

export type DropRateData = {
  dungeon: Dungeon;
  rarityCounts: Record<Rarity, number>;
  totalItems: number;
};

export type DungeonName = string;

export type DropRateContextData = {
  dropRatesData: Record<DungeonName, DropRateData>;
};

const DropRateContext = React.createContext<DropRateContextData>({
  dropRatesData: {},
});
export const DropRateProvider = DropRateContext.Provider;
export default DropRateContext;
