import React from 'react';

export type LoadingContextData = {
  isLoading: boolean;
};

const LoadingContext = React.createContext<LoadingContextData>({
  isLoading: false,
});
export const LoadingProvider = LoadingContext.Provider;
export default LoadingContext;
