import React from 'react';
import { AvatarTrigger } from '../../interfaces/tammyAvatar';

export type TammyAvatarContextData = {
  tammyAvatarTrigger: AvatarTrigger;
  setTammyAvatarTrigger: (value: AvatarTrigger) => void;
};

const TammyAvatarContext = React.createContext<TammyAvatarContextData>({
  tammyAvatarTrigger: {
    body: false,
    face: false,
    hair: false,
  },
  setTammyAvatarTrigger: (__value) => {},
});
export const TammyAvataProvider = TammyAvatarContext.Provider;
export default TammyAvatarContext;
