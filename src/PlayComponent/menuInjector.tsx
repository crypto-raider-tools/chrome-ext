import { apiEntrypoint, log } from '../config';
import { htmlToElement } from '../Helpers/Utils';
import ReactDOM from 'react-dom';
import { Companion } from './Companion';
import { CRCStuff } from './CRCStuff';
import { CRCQuest } from './CRCQuests';
import { CRCStatistics } from './CRCStatistics';

let sessionMetamask: string | undefined;

let href = '';
setInterval(async () => {
  if (window.location.href !== href) {
    href = window.location.href;
    if (window.location.pathname === '/signin') {
      log('Cleaned sessionMetamask');
      sessionMetamask = undefined;
    }
  }
}, 1000);

/**
 * @returns string or throw if address can't be found.
 */
async function getMetamaskAddress(): Promise<string> {
  if (sessionMetamask) {
    return Promise.resolve(sessionMetamask);
  }
  try {
    let accountJsonUrl: string | undefined = (await (await fetch(`${apiEntrypoint}/configs/AccountJsonUrl`)).json())
      ?.value;
    if (!accountJsonUrl) {
      log('Unable to get Crypto Raider account URL');
      throw new Error('Unable to get Crypto Raider account URL');
    }

    let accountData = await (
      await fetch(accountJsonUrl, {
        headers: {
          Cookie: document.cookie,
        },
      })
    ).json();
    let metamaskFromAccount: string | undefined = accountData?.pageProps?.user?.address;
    if (!metamaskFromAccount) {
      log('Unable to get wallet address using Crypto Raider account URL');
      throw new Error('Unable to get wallet address using Crypto Raider account URL');
    }
    sessionMetamask = metamaskFromAccount.toLocaleLowerCase();
    return Promise.resolve(sessionMetamask);
  } catch (initialError) {
    let metamaskInSave = localStorage.getItem('CRC_customer');
    try {
      metamaskInSave = JSON.parse(metamaskInSave!).metamaskAddress;
    } catch (error) {
      throw new Error(initialError as string);
    }
    if (!metamaskInSave) {
      throw new Error(initialError as string);
    }
    sessionMetamask = metamaskInSave;
    return Promise.resolve(metamaskInSave);
  }
}

export const playMenuInjector = async () => {
  /**
   * Getting selectors from API
   */
  log('Getting playMenuJsSelector');
  const playMenuJsSelector: string = (await (await fetch(`${apiEntrypoint}/configs/playMenuJsSelector`)).json()).value;
  const playMobileMenuJsSelector: string = (
    await (await fetch(`${apiEntrypoint}/configs/playMobileMenuJsSelector`)).json()
  ).value;
  const playMainSectionJsSelector: string = (
    await (await fetch(`${apiEntrypoint}/configs/playMainSectionJsSelector`)).json()
  ).value;

  /**
   * Getting menu container
   */
  let playMenuNode = document.querySelector(playMenuJsSelector);
  let playMobileMenuJsNode = document.querySelector(playMobileMenuJsSelector);
  log('playMenuJsSelector = ', playMenuJsSelector, 'playMenuNode = ', playMenuNode);
  log('playMobileMenuJsSelector = ', playMobileMenuJsSelector, 'playMobileMenuJsNode = ', playMobileMenuJsNode);
  /**
   * Handle wallet address:
   * - Get from api
   * - if fail, get from localStorage
   */
  log('Load wallet address');
  let metamaskAddress = '';
  try {
    metamaskAddress = await getMetamaskAddress();
  } catch (error) {
    alert(`${error} sry 😿`);
    throw new Error((error as any).toString());
  }
  if (!metamaskAddress) {
    alert('You must link a wallet to your crypto raider account');
    throw new Error('no wallet linked');
  }
  log('wallet address', metamaskAddress);

  /**
   * Load all customer data from API
   * And save to localStorage (used above)
   */
  const customerRequest = await fetch(`${apiEntrypoint}/customers/${metamaskAddress.toLowerCase()}`, {
    headers: {
      'x-original-token': document.cookie,
    },
  });
  if (customerRequest.status !== 200) {
    console.warn(`CRC: customerRequest: ${customerRequest.status} ${customerRequest.statusText}`);
    alert('CRC: Error while loading your user data, sry 😿');
    return Promise.reject();
  }
  localStorage.setItem('CRC_customer', JSON.stringify(await customerRequest.json()));

  /**
   * Create the companion btn
   * - onClick will inject the companion page content
   * - listen to the menu container
   * - ensure the btn is reinjected if removed
   * - ensure original btn is overriden once the companion has been open
   */
  let companionIsOpen: boolean = false;
  const companionMobileMenuBtn = htmlToElement(/*html*/ `
        <a class="chakra-stack css-1l4eki1">
            <div aria-hidden="true" class="css-qqfgvy">
                👹
            </div>
            <div class="css-2ygcmq">
                CRC: Raiders
            </div>
        </a>
    `);
  const companionMenuBtn = htmlToElement(/*html*/ `
        <a class="chakra-stack css-1l4eki1">
            <div aria-hidden="true" class="css-qqfgvy">
                👹
            </div>
            <div class="css-2ygcmq">
                CRC: Raiders
            </div>
        </a>
    `);
  let injectCompanion = () => {
    const playMainSectionJsNode = document.querySelector(playMainSectionJsSelector);
    ReactDOM.render(<Companion metamask={metamaskAddress} />, playMainSectionJsNode);
    document.querySelector(playMobileMenuJsSelector)?.setAttribute('hidden', 'true');
    companionIsOpen = true;
  };
  companionMenuBtn.addEventListener('click', injectCompanion);
  companionMobileMenuBtn.addEventListener('click', injectCompanion);

  const crcStuffMobileMenuBtn = htmlToElement(/*html*/ `
        <a class="chakra-stack css-1l4eki1">
            <div aria-hidden="true" class="css-qqfgvy">
                🗄️
            </div>
            <div class="css-2ygcmq">
                CRC: Stuffs
            </div>
        </a>
    `);
  const crcStuffMenuBtn = htmlToElement(/*html*/ `
        <a class="chakra-stack css-1l4eki1">
            <div aria-hidden="true" class="css-qqfgvy">
                🗄️
            </div>
            <div class="css-2ygcmq">
                CRC: Stuffs
            </div>
        </a>
    `);
  let injectStuff = () => {
    const playMainSectionJsNode = document.querySelector(playMainSectionJsSelector);
    ReactDOM.render(<CRCStuff metamask={metamaskAddress} />, playMainSectionJsNode);
    document.querySelector(playMobileMenuJsSelector)?.setAttribute('hidden', 'true');
    companionIsOpen = true;
  };
  crcStuffMobileMenuBtn.addEventListener('click', injectStuff);
  crcStuffMenuBtn.addEventListener('click', injectStuff);

  const crcQuestMobileMenuBtn = htmlToElement(/*html*/ `
  <a class="chakra-stack css-1l4eki1">
      <div aria-hidden="true" class="css-qqfgvy">
          📜
      </div>
      <div class="css-2ygcmq">
          CRC: Quests
      </div>
  </a>
`);
  const crcQuestMenuBtn = htmlToElement(/*html*/ `
  <a class="chakra-stack css-1l4eki1">
      <div aria-hidden="true" class="css-qqfgvy">
          📜
      </div>
      <div class="css-2ygcmq">
          CRC: Quests
      </div>
  </a>
`);
  let injectQuest = () => {
    const playMainSectionJsNode = document.querySelector(playMainSectionJsSelector);
    ReactDOM.render(<CRCQuest metamask={metamaskAddress} />, playMainSectionJsNode);
    document.querySelector(playMobileMenuJsSelector)?.setAttribute('hidden', 'true');
    companionIsOpen = true;
  };
  crcQuestMobileMenuBtn.addEventListener('click', injectQuest);
  crcQuestMenuBtn.addEventListener('click', injectQuest);

  const crcStatisticsMobileMenuBtn = htmlToElement(/*html*/ `
  <a class="chakra-stack css-1l4eki1">
      <div aria-hidden="true" class="css-qqfgvy">
      📈
      </div>
      <div class="css-2ygcmq">
          CRC: Statistics
      </div>
  </a>
`);
  const crcStatisticsMenuBtn = htmlToElement(/*html*/ `
  <a class="chakra-stack css-1l4eki1">
      <div aria-hidden="true" class="css-qqfgvy">
      📈
      </div>
      <div class="css-2ygcmq">
          CRC: Statistics
      </div>
  </a>
`);
  let injectStatistics = () => {
    const playMainSectionJsNode = document.querySelector(playMainSectionJsSelector);
    ReactDOM.render(<CRCStatistics metamask={metamaskAddress} />, playMainSectionJsNode);
    document.querySelector(playMobileMenuJsSelector)?.setAttribute('hidden', 'true');
    companionIsOpen = true;
  };
  crcStatisticsMobileMenuBtn.addEventListener('click', injectStatistics);
  crcStatisticsMenuBtn.addEventListener('click', injectStatistics);

  const handleOriginalBtnClick = () => {
    let handleClick: EventListenerOrEventListenerObject = (event) => {
      if (!companionIsOpen) return;
      let element = event.target as HTMLElement | null;
      while (element && element.tagName.toLowerCase() !== 'a') {
        element = element.parentElement;
      }
      if (!element || !element.getAttribute('href')) {
        window.location.reload();
        return;
      }
      window.location.href = element.getAttribute('href')!;
    };
    document.querySelectorAll(`${playMenuJsSelector} a`).forEach((node) => {
      node.addEventListener('click', handleClick);
    });
    document.querySelectorAll(`${playMobileMenuJsSelector} a`).forEach((node) => {
      node.addEventListener('click', handleClick);
    });
  };

  const playMenuObserver = new MutationObserver(() => {
    playMenuObserver.disconnect();
    if (window.location.pathname === '/signin') {
      log('killing inventory btn observer');
      return;
    }
    playMenuNode = document.querySelector(playMenuJsSelector);
    playMobileMenuJsNode = document.querySelector(playMobileMenuJsSelector);
    if (!companionMenuBtn.isConnected || !crcStuffMenuBtn.isConnected) {
      log('reinjecting companion btns');
      handleOriginalBtnClick();
      playMenuNode?.prepend(crcStatisticsMenuBtn);
      playMobileMenuJsNode?.prepend(crcStatisticsMobileMenuBtn);
      playMenuNode?.prepend(crcQuestMenuBtn);
      playMobileMenuJsNode?.prepend(crcQuestMobileMenuBtn);
      playMenuNode?.prepend(crcStuffMenuBtn);
      playMobileMenuJsNode?.prepend(crcStuffMobileMenuBtn);
      playMenuNode?.prepend(companionMenuBtn);
      playMobileMenuJsNode?.prepend(companionMobileMenuBtn);
    }
    playMenuObserver.observe(document, { childList: true, attributes: false, characterData: false, subtree: true });
  });
  document.querySelectorAll(`${playMenuJsSelector} > a`).forEach((node) => {
    node.addEventListener('click', (event) => {
      if (!companionIsOpen) return;
      let element = event.target as HTMLElement | null;
      while (element && element.tagName.toLowerCase() !== 'a') {
        element = element.parentElement;
      }
      if (!element) return;
      window.location.href = element.getAttribute('href')!;
    });
  });

  handleOriginalBtnClick();
  playMenuNode?.prepend(crcStatisticsMenuBtn);
  playMobileMenuJsNode?.prepend(crcStatisticsMobileMenuBtn);
  playMenuNode?.prepend(crcQuestMenuBtn);
  playMobileMenuJsNode?.prepend(crcQuestMobileMenuBtn);
  playMenuNode?.prepend(crcStuffMenuBtn);
  playMobileMenuJsNode?.prepend(crcStuffMobileMenuBtn);
  playMenuNode?.prepend(companionMenuBtn);
  playMobileMenuJsNode?.prepend(companionMobileMenuBtn);
  playMenuObserver.observe(document, { childList: true, attributes: false, characterData: false, subtree: true });

  /**
   * Dirty fix for this damned
   * mobile sidemenu btn which
   * doesn't work as expected
   */
  setInterval(() => {
    let btn = document.querySelector('.css-1fnm05q');
    let sidebar = document.querySelector('.css-1e4vkuk');
    let isSidebarOpen = !sidebar?.getAttribute('hidden');
    btn?.addEventListener('click', (event) => {
      event.stopImmediatePropagation();
      if (isSidebarOpen) {
        sidebar?.removeAttribute('hidden');
      } else {
        sidebar?.setAttribute('hidden', '');
      }
      isSidebarOpen = !isSidebarOpen;
    });
  }, 1000);

  return Promise.resolve();
};
