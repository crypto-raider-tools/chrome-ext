import { colorPalette } from '../../Helpers/Constants';
import { StuffStats } from '../../interfaces/stuffstats';
import { DropRateData, DungeonName } from '../Context/DropRateContext';
import { useTable, useSortBy, useFilters, Column } from 'react-table';
import { CSSProperties, useMemo, useState } from 'react';
import { getMetaScoreOutput, Rarity } from '@crypto-raider-tools/computation';
import { RarityAsNumber } from '../../Helpers/Maps';

interface ItemListStatisticsProps {
  stuffStats: StuffStats[];
  dropRateData: Record<DungeonName, DropRateData>;
}

// Define a default UI for filtering
function DefaultColumnFilter({ column: { filterValue, setFilter } }: any) {
  return (
    <input
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
      }}
      value={filterValue || ''}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ...`}
    />
  );
}

function SelectRarityFilter({ column: { filterValue, setFilter } }: any) {
  // Render a multi-select box
  return (
    <select
      onClick={(e) => {
        e.preventDefault();
        e.stopPropagation();
      }}
      value={filterValue}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
    >
      <option value="">All</option>
      {Object.keys(RarityAsNumber).map((option, i) => (
        <option key={i} value={RarityAsNumber[option as Rarity]}>
          {option}
        </option>
      ))}
    </select>
  );
}

type RarityMinMaxRecords = {
  minGlobal: number;
  maxGlobal: number;
  minRarity: Record<Rarity, number>;
  maxRarity: Record<Rarity, number>;
};

export const ItemListStatistics = (props: ItemListStatisticsProps) => {
  const [baseDropRates] = useState<Partial<Record<Rarity, number>>>({
    common: (props.dropRateData['Global'].rarityCounts['common'] / props.dropRateData['Global'].totalItems) * 100,
    uncommon: (props.dropRateData['Global'].rarityCounts['uncommon'] / props.dropRateData['Global'].totalItems) * 100,
    rare: (props.dropRateData['Global'].rarityCounts['rare'] / props.dropRateData['Global'].totalItems) * 100,
    epic: (props.dropRateData['Global'].rarityCounts['epic'] / props.dropRateData['Global'].totalItems) * 100,
    legendary: (props.dropRateData['Global'].rarityCounts['legendary'] / props.dropRateData['Global'].totalItems) * 100,
  });
  const [rarityMinMaxRecords, setRarityMinMaxRecords] = useState<RarityMinMaxRecords>({
    minGlobal: 100,
    maxGlobal: 0,
    minRarity: {
      common: 100,
      uncommon: 100,
      rare: 100,
      epic: 100,
      legendary: 100,
      mythic: 100,
      heirloom: 100,
      limited: 100,
    },
    maxRarity: {
      common: 0,
      uncommon: 0,
      rare: 0,
      epic: 0,
      legendary: 0,
      mythic: 0,
      heirloom: 0,
      limited: 0,
    },
  });

  const columns = useMemo<Column[]>(
    () => [
      {
        Header: 'Items Statistics',
        columns: [
          {
            Header: 'Item',
            accessor: 'item',
            filter: 'startWith',
          },
          {
            Header: 'Rarity',
            accessor: 'rarity',
            Filter: SelectRarityFilter,
            filter: 'equals',
          },
          {
            Header: 'Stats Point Range',
            accessor: 'statsPointRange',
          },
          {
            Header: 'Primary Stats',
            accessor: 'primaryStats',
          },
          {
            Header: 'Global Item Repartition',
            accessor: 'globalRepartition',
          },
          {
            Header: 'Drop Rate within Rarity',
            accessor: 'rarityDropRate',
          },
          {
            Header: 'Total Count',
            accessor: 'count',
          },
          {
            Header: 'Id',
            accessor: 'id',
          },
        ],
      },
    ],
    []
  );

  const filteredColumns = ['item', 'rarity'];

  const compileData = (stuffStats: StuffStats[], dropRateData: Record<DungeonName, DropRateData>) => {
    const globalDropRateData = dropRateData['Global'];
    const newRarityMinMaxRecords: RarityMinMaxRecords = {
      minGlobal: 10,
      maxGlobal: 0,
      minRarity: {
        common: 100,
        uncommon: 100,
        rare: 100,
        epic: 100,
        legendary: 100,
        mythic: 100,
        heirloom: 100,
        limited: 100,
      },
      maxRarity: {
        common: 0,
        uncommon: 0,
        rare: 0,
        epic: 0,
        legendary: 0,
        mythic: 0,
        heirloom: 0,
        limited: 0,
      },
    };
    const result = stuffStats.map((s, index) => {
      let primaryStatsSorted = [
        { name: 'strength', value: s.strengthMax ?? 0 },
        { name: 'intelligence', value: s.intelligenceMax ?? 0 },
        { name: 'agility', value: s.agilityMax ?? 0 },
      ]
        .filter((p) => p.value > 0)
        .sort(function (e, t) {
          return t.value - e.value;
        });
      let globalRepartition = ((s.count ?? 0) / dropRateData['Global'].totalItems) * 100;
      if (newRarityMinMaxRecords.minGlobal > globalRepartition) newRarityMinMaxRecords.minGlobal = globalRepartition;
      if (newRarityMinMaxRecords.maxGlobal < globalRepartition) newRarityMinMaxRecords.maxGlobal = globalRepartition;
      //let rarityDropRate = (globalDropRateData.rarityCounts[s.rarity as Rarity] / globalDropRateData.totalItems) * 100;
      let rarityDropRate = ((s.count ?? 0) / globalDropRateData.rarityCounts[s.rarity as Rarity]) * 100;
      if (newRarityMinMaxRecords.minRarity[s.rarity as Rarity] > rarityDropRate)
        newRarityMinMaxRecords.minRarity[s.rarity as Rarity] = rarityDropRate;
      if (newRarityMinMaxRecords.maxRarity[s.rarity as Rarity] < rarityDropRate)
        newRarityMinMaxRecords.maxRarity[s.rarity as Rarity] = rarityDropRate;
      return {
        item: s.name,
        rarity: RarityAsNumber[s.rarity as Rarity],
        statsPointRange: s.pointQtyMax,
        primaryStats: primaryStatsSorted,
        globalRepartition,
        rarityDropRate,
        count: s.count,
        id: index,
      };
    });
    setRarityMinMaxRecords(newRarityMinMaxRecords);
    return result;
  };

  const filterTypes = useMemo(() => ({}), []);

  const defaultColumn = useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
    }),
    []
  );

  const data = useMemo(() => compileData(props.stuffStats, props.dropRateData), [props.stuffStats, props.dropRateData]);

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data,
      defaultColumn, // Be sure to pass the defaultColumn option
      filterTypes,
    },
    useFilters, // useFilters!
    useSortBy
  );

  const widgetStyle: CSSProperties = {
    backgroundColor: colorPalette['mediumBlueGrey'],
    display: 'flex',
    flexDirection: 'column',
    borderRadius: '2px',
    padding: '5px 8px 5px 8px',
    fontWeight: '500',
    height: 'fit-content',
    width: 'fit-content',
    alignItems: 'center',
  };

  const overviewIconStyle: CSSProperties = {
    width: '50px',
    height: '50px',
    transform: 'scale(1)',
    imageRendering: 'pixelated',
    objectFit: 'contain',
    margin: '2px',
  };

  const textStyle: CSSProperties = {
    width: '100%',
    textAlign: 'center',
  };

  return (
    <div style={widgetStyle}>
      <div style={{ display: 'flex', marginBottom: '5px' }}>
        {Object.keys(baseDropRates).map((rarity, __index) => {
          return (
            <div
              key={rarity}
              style={{
                display: 'flex',
                flexDirection: 'column',
                height: '50px',
                width: '100px',
                color: colorPalette['whiteElf'],
              }}
            >
              <div
                style={{
                  backgroundColor: colorPalette[rarity],
                  height: '50%',
                  width: '100%',
                  textAlign: 'center',
                  color: 'black',
                }}
              >
                {rarity}
              </div>
              <div
                style={{
                  backgroundColor: colorPalette['darkBlueGrey'],
                  height: '50%',
                  width: '100%',
                  textAlign: 'center',
                }}
              >
                {baseDropRates[rarity as Rarity]?.toFixed(5) + '%'}
              </div>
            </div>
          );
        })}
      </div>
      <table {...getTableProps()}>
        <thead>
          <tr {...headerGroups[1].getHeaderGroupProps()}>
            {headerGroups[1].headers.map((column) => (
              // Add the sorting props to control sorting. For this example
              // we can add them into the header props
              <th
                {...column.getHeaderProps(column.getSortByToggleProps())}
                style={{ color: colorPalette['lightBlueGrey'] }}
              >
                <div style={{ margin: '0 5px' }}>
                  {column.render('Header')}
                  {/* Add a sort direction indicator */}
                  <span>{column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : ''}</span>
                </div>
                <div>{column.canFilter && filteredColumns.includes(column.id) ? column.render('Filter') : null}</div>
              </th>
            ))}
          </tr>
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            const stuffStats = props.stuffStats.find((s) => s.name === row.values.item)!;
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  switch (cell.column.id) {
                    case 'item':
                      return (
                        <td {...cell.getCellProps()}>
                          <div
                            style={{
                              display: 'flex',
                              flexDirection: 'column',
                              alignItems: 'center',
                              justifyContent: 'center',
                              position: 'relative',
                              width: '120px',
                            }}
                          >
                            <div
                              style={{
                                color: 'white',
                                textAlign: 'center',
                              }}
                            >
                              {stuffStats.name}
                            </div>
                            <img
                              style={{
                                ...overviewIconStyle,
                                filter: 'drop-shadow(-3px -3px 3px ' + colorPalette[stuffStats.rarity as Rarity] + ')',
                              }}
                              alt={stuffStats.name}
                              src={`https://crypto-raiders-assets.storage.googleapis.com/icons/${stuffStats.internalName}_ICON.png`}
                            />
                          </div>
                        </td>
                      );
                    case 'rarity':
                      return (
                        <td {...cell.getCellProps()}>
                          <p style={{ color: colorPalette[stuffStats.rarity ?? ''], ...textStyle }}>
                            {stuffStats.rarity}
                          </p>
                        </td>
                      );
                    case 'statsPointRange':
                      return (
                        <td {...cell.getCellProps()}>
                          <p style={{ color: colorPalette['average'], ...textStyle }}>
                            {stuffStats.pointQtyMin}
                            <span style={{ color: colorPalette['lightBlueGrey'] }}>{' - '}</span>
                            <span style={{ color: colorPalette['good'] }}>{stuffStats.pointQtyMax}</span>
                          </p>
                        </td>
                      );
                    case 'primaryStats':
                      return (
                        <td {...cell.getCellProps()}>
                          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                            {cell.value.map((p: any) => (
                              <div
                                key={p.name + p.value}
                                style={{
                                  display: 'flex',
                                  justifyContent: 'center',
                                  marginRight: '5px',
                                  fontSize: '15px',
                                  fontWeight: '500',
                                  borderRadius: '2px',
                                  backgroundColor: colorPalette[p.name],
                                  height: '25px',
                                  width: '45px',
                                }}
                              >
                                {p.name.substring(0, 3).toUpperCase()}
                              </div>
                            ))}
                          </div>
                        </td>
                      );
                    case 'globalRepartition':
                      const meta = getMetaScoreOutput(
                        cell.value - rarityMinMaxRecords.minGlobal,
                        rarityMinMaxRecords.maxGlobal - rarityMinMaxRecords.minGlobal
                      );
                      return (
                        <td {...cell.getCellProps()}>
                          <p
                            style={{
                              ...textStyle,
                              color: colorPalette[meta.toLowerCase()],
                            }}
                          >
                            {cell.value.toFixed(6)}
                            <span style={{ color: 'white' }}>{' %'}</span>
                          </p>
                        </td>
                      );
                    case 'rarityDropRate':
                      const rarityMeta = getMetaScoreOutput(
                        cell.value - rarityMinMaxRecords.minRarity[stuffStats.rarity as Rarity],
                        rarityMinMaxRecords.maxRarity[stuffStats.rarity as Rarity] -
                          rarityMinMaxRecords.minRarity[stuffStats.rarity as Rarity]
                      );
                      return (
                        <td {...cell.getCellProps()}>
                          <p
                            style={{
                              ...textStyle,
                              color: colorPalette[rarityMeta.toLowerCase()],
                            }}
                          >
                            {cell.value.toFixed(6)}
                            <span style={{ color: 'white' }}>{' %'}</span>
                          </p>
                        </td>
                      );
                    default:
                      return (
                        <td {...cell.getCellProps()}>
                          <p style={{ ...textStyle }}>{cell.value}</p>
                        </td>
                      );
                  }
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
