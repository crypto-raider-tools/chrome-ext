(async () => {
  const fs = await import('fs');

  // getting all the credentials and IDs from `gitlab-ci.yml` file
  let REFRESH_TOKEN = process.env.REFRESH_TOKEN;
  let EXTENSION_ID = process.env.EXTENSION_ID;
  let CLIENT_ID = process.env.CLIENT_ID;

  if (!process.argv[2]) {
    console.log('You must provide a filename');
    process.exit(1);
  }
  const filename = process.argv[2];

  const webStore = (await import('chrome-webstore-upload')).default({
    extensionId: EXTENSION_ID,
    clientId: CLIENT_ID,
    refreshToken: REFRESH_TOKEN,
  });

  const extensionSource = fs.createReadStream(filename);
  webStore
    .uploadExisting(extensionSource)
    .catch((error) => {
      console.log(`Error while uploading ZIP: ${error}`);
      process.exit(1);
    })
    .then((res) => {
      console.log('Successfully uploaded the ZIP');
      return webStore.publish('default');
    })
    .catch((error) => {
      console.log(`Error while publishing ZIP: ${error}`);
      process.exit(1);
    })
    .then((res) => {
      console.log('Successfully published the ZIP');
    });
})().catch((err) => console.error(err));
