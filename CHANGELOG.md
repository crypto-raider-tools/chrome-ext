# CHANGELOG

<!--- next entry here -->

## 2.10.0
2022-10-22

### Features

- add CRC Statistics as a Premium Feature (f903dced373b48d5c925e205b2256e9bca871e00)

### Fixes

- Added helmet (dddeb19a92c57159930443d9cb22e947c65340b5)
- Updated level caps (60ac0d697102f1e08331e15047e8ddad1f58a744)
- Ensure the beta version does not use the production server (6ec145b2fbae2d5980b8f5d731cf322e1173a016)

## 2.9.1
2022-09-11

### Fixes

- Get out of beta mode (42fedfd691c25d5712d9ef70fa86b9acd7569ca0)

## 2.9.0
2022-09-10

### Features

- New donation system, now 2 Matic equals 1 premium weeks (d86b8d843f992b7bd8131624ec4ed101df364373)
- You can now manage your questing raiders! (bb3f439d10423bd9a6c838d3e79144de9e1a5501)

### Fixes

- Removed quest data from main raider list (462c872eb93d21ef962761a74d742dc36ef4ec4f)
- Removed raids and duels button from main raider list (a49e33901144d597d53b40d28d801be134f3b5d3)
- Raiders in quest do not disapear over time anymore (ba6643e86d326fa0c30ba06963efc92a9581cca4)
- Stuff's selling price should be more accurate now (ca2b8e0faef29155b4d5129d481cc81822e0343b)
- Stuff Page, show selling error after all items sells (372bca73cbdd04c6ddb5dc0b7e7eff3734527e2d)

## 2.8.0
2022-05-13

### Features

- Stuff page, you can now filter by price and total stat points (28dc5a2a158a7f071238242243482e8db249cf4b)
- Stuff page, you can now order by level (fd6dfa8dee182e7a14eb11fc31e6233b91cc7032)
- Stuff page, you can now sell all current page's items at once (0d1c412f7b6a67b40f1320f471bb3ee0b49bb772)
- Stuff page, now displaying lvl and total stat points for each item (ca6d935821fff176d39b96ff64cbf0450e77bfea)

### Fixes

- Inventory should be openable even when CR change ability payloads (47b2514357d97224022a73d9695a082fc9f8e45c)
- Donation wallet has been updated (f43a4418a093a1c49551ecfd2d2c54a19a2a307d)
- APIs calls are now in dedicated files (fbd5b4bbdf1d8d89f444f6e52626f632bb445762)
- Stuff page, added update button (2f3c739070ebd35f200fd89af7280a61b7e8a0a6)
- Raider's level are now correctly display for lvl 9+ (f6d20bf312d9bfda929907e052f01d9a7c4e5d8e)

## 2.7.1
2022-05-03

### Fixes

- Order by selling price ASC should only display sellable items (bfeed37c9ee90d43d064e5806d68b4a890eaa6b3)
- Display errors if an item fails to be sold (bbb81e310bc346d3ef2796b0aaed96267c1036f2)
- Equipped items should not be selectable (3484a03cc1c303f5e62b51b361faf1e332354569)
- Total Aurum should count only selected items (a7ab8affa75952f9ee657cce553752902fa73e93)
- Items not in wallet should not be selectable (d70dc1a552351afd6eb3b0aa341196e28ed5877b)
- Original CR menu buttons should not freeze the page (deab2f496028bb72353b533857d9269332a8876e)
- Add equipped display and warning for non premium user (c9677c72c0380f63305ef6e08bc63bdec9308280)

## 2.7.0
2022-05-02

### Features

- You can now edit raider's name by clicking on it (b39d7a9a153fc0af4f87400136f6da3906866941)
- You can now edit your raider's abilities below the inventory (57a63c6a0dfb68a74a55e886b494799d77f847e2)
- Now displaying items drop rate in dungeon list (534081d63205acaddf0d77ff683409069b8284c7)
- New page "Stuff" where you can sell your items (ab131aa3985bb9429d0a5c34bcd02e2ca1be146e)

## 2.6.0
2022-04-29

### Features

- Display quest information on main raider card (35c3a4ad1c6f69d109ab2f0fb9eb5fd75db3cf5b)
- You can now order raider by quest status (5514e2c8f788e03bb1329d81fd32a4fdd46bea06)
- Display an information bar above raider list (1485c09eff78f92b1bc4b3e1e74c3f5082960e13)
- Order inventory items by total points (c7d3b9d304fe035c9e04677c3def680e7512ca67)

### Fixes

- Detect dungeon end to update raiders automatically (712a9d612ce6e2382b5b6249d4cccfea914b4754)
- Raid grid monitor is now properly persisted (bf6220ff4263c180d7a80bf8c57db941554dba43)
- Raid sound mute settings is now properly persisted (d04cc6958cea24bed702e157ca4492dd2a2561e9)

## 2.5.5
2022-04-22

### Fixes

- Levels are now displayed properly (7bf77298700d243efd1697080a45817e3f91b957)
- repair ws handlers (69c98d13d1d40f98f6db530acfe74d1f8a6569cf)

## 2.5.4
2022-04-17

### Fixes

- Extension should try to get data only after the user is connected (db9f4bb57ac36453e645156895c65326862e132e)

## 2.5.3
2022-04-17

### Fixes

- Companion button should be injected only once (a8d1eacb580f63326df6ea128077a2cb9d7ab2f4)
- Users with uninitialized filters server-side should not have a blank page (45804007ae61da84894556db6df8763a9ec4bd13)

## 2.5.2
2022-04-11

### Fixes

- Remove auto-sync on page load (ec8082b3707c77241eb95fc560ca30faeeb3a3d3)
- Added a button to sync on demand (bfbad410cc903b138e79758acd0195334df7f531)
- On action (dungeon is done / equip item) sync only the modified raider (65d1bcc8ba987a8adfc0981431dc42b683633f59)
- Level order now use xp instead of level (55a0b5376f00b0e678676fce6795114775175a93)

## 2.5.1
2022-04-09

### Fixes

- Equip items should work with default abilities data (060585c4dc70b8c8ba3325356b62366f387aaa74)

## 2.5.0
2022-04-06

### Features

- Added a toggle to show/hide the overlay (41bfeb67acf1b62d2cd9817a3d520dcc6affa564)
- Display dungeon winrate based on player and mob combat stats (9d426edef6fa9f150c85d7a9e9c90e6cffba8c39)

## 2.4.2
2022-04-02

### Fixes

- About popup should be wider (ab1f5e4400e4f661f4f1f53be5db6eb23bf10c05)
- Updated About popup content (2f4ee9d8e3b5e7302ad80b2b1585102a2e0c98ba)
- Better About popup display (858472d02587a84d5bf1d0ee3731a7f16187920b)
- Wallet errors should be more explicit (9c8513d3d7b92fe049b2513596185fe127e28988)

## 2.4.1
2022-04-02

### Fixes

- Better dungeon overlay (4fab86f5439b886edca060c5cb2da895f202f460)

## 2.4.0
2022-04-01

### Features

- add duels (5a6059752290623c55ac0b296049dd41e48cd5b2)
- Added an overlay in dungeons (47b65729f42f61845b87e4a46ecdb3785cac0898)

### Fixes

- Display pagination above AND below the raider list (b3399a893dbe674b13c05308c1deb651d1c725ae)
- Reuse advanced stats components with diff when hovering an inventory item (f5436653ddb077b6cd962505cdd20f7b8b990610)

## 2.3.0
2022-03-31

### Features

- Bases stats can now be edited manually (you can still use +/-) (386334949879a24b80cc22cfdbf83ebac4ff6f70)
- Show the next level XP limit (d7aaf6fcefc4168429f5b269781a881e40829e8f)
- Added item total point and main stats in inventory and dungeon (76f6324bdcf18e2a9227c2f66fbad3dbd1bf7fd3)
- Added raider comparator (cafc25102189291d2b98b37cf6bb326a0cf1cd4b)

### Fixes

- Updated combat stats caps (42a7a4a51de73ef315d05ba59c271052ec0deac1)
- Better raider list display on mobile (a56a0ec716cff142fa5e5a53aba3538517275282)
- Hide raid grid system on mobile devices (08efbdbf818f1a196cb0e951e59337b16583ab9c)
- Ensure raid grid return to default once the premium period ends (8c12bcee81785315c4ca8cc73ee361e58d19fcaf)
- Open quest info btn is not needed (5fbb9f81eb534a6849197d63801f192c8aac41e7)
- Moved paginator at the list's bottom (bf1681c7ab0bc9944fab391403f2644e9b9d5923)

## 2.2.0
2022-03-30

### Features

- Status indicator in dungeon (63d125944c0662c7e6ac1715b86082e5dfc88c83)

### Fixes

- Ensure total raider's base stats include neck slot (feb325d75896c53ea4dddf740679deff53da90d6)
- Ensure graph raider's advanced stats include neck slot (120a096733a36634412d188df2a6df9d2b55d2be)

## 2.1.1
2022-03-23

### Fixes

- Properly handle neck slot (6ca318c2313793d2963026217efa721d3eda7388)

## 2.1.0

2022-03-17

### Features

- Add endless dungeon remaining (b275559677badc6324b625cf4dfec9f2dbc6bccb)

### Fixes

- Search sidebar should be hideable (844b61fbfb4f729c5244a3479a9f764a03ac2113)
- Add companion btn to the mobile sidemenu (691c3be06b976e1815075ed553a1f443e5395603)
- Mobile menu opener should open sidebar mobile menu (9a951832e2bd408ad2ffb37f511c27f7ddfa81a9)
- Better raider display on mobile (03ae1649f4137f17ec5afe7175b32786b4274e7f)

## 2.0.2

2022-03-14

### Fixes

- We can't update base stats of raiders in quest (d54a71a4e558125af12ea5aafff40760c3cb1f4f)
- level up crash + remove cookie from manifest (17724559d1c9d9b4a076af561bf0e777df22a0eb)
- No more white screen with live reload once a dungeon is done (bb4182d7fa9adc444d23698582fb5d9f2963917a)
- No more white screen with live reload when a raider lvl up once a dungeon is done (a60eacf68d58a787d5fc518aa8e2cf105c213486)

## 2.0.1

2022-03-12

### Fixes

- Remove unused chrome permission (95e2495dbda20e0c41f38de2e7b950661d81572b)

## 2.0.0

2022-03-12

### Breaking changes

#### Move companion to a new tab (2e84515d7188ea81bde35b9373af0f96b429225b)

Everything has been moved to a new tab,

Nothing is injected in the original raider page now.

## 1.11.1

2022-02-06

### Fixes

- Update stats through compute package (e6e934ab4a13fd38ab11265353caf847507c06df)

## 1.11.0

2022-02-02

### Features

- Adding dungeon xp (81d05335acbd3b89100406ca535c791c0e4b91dd)

### Fixes

- Update stats through compute package (bfb9cee19c9b2b5a389587fd3cb1cba897c35b45)

## 1.10.1

2022-01-28

### Fixes

- Delivery process should update version in extension manifest (d24075bcadf028fac79998e5c6323033f421453d)

## 1.10.0

2022-01-28

### Features

- Add simulation on guild page (531d23ba81257243981ec87a085ac214b31f2d57)
- First premium feature, inventory simulations (35c346572b30e4539ed7fb4abc92f8d857cda22a)
- add new filters by date and recommendation (0cae332d4548e2733108cad2df3aa44396eb2034)

### Fixes

- Update extension popup (b3f239e29776f11f7dcc561aee0ece0b1e4c6da1)
- Wrong premium detection (54c03afd63751bf7d3ca038bce087a016729f632)

## 1.9.2

2022-01-26

### Fixes

- Delivery process is now less error-prone (76260671ca625a60d11d4a2035a5eff542cc812e)
- go-semrel-gitlab does not have yarn (80c0194a68debb400b863f9d819242211beebf20)
