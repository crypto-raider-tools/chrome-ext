[[_TOC_]]

## Workflow and release process

- Create a branch from master
- Create a PR
- See the pipeline to download the beta zipped extension
  1. ![PR's pipeline](/readme/PR-beta.png)
  2. ![job's artifact](/readme/PR-beta2.png)
  3. You're smart enough to guess the next steps
- Once the PR is merged, a new pipeline will allow you to deploy a new version
  ![PR's deploy pipeline](/readme/PR-prod.png)
- A new pipeline will be created to deploy the new version, see https://gitlab.com/crypto-raider-tools/chrome-ext/-/pipelines
  ![deploy pipeline](/readme/PR-prod2.png)
  **:warning:  There's a manual job to send the extension to the chrome store**
- Once the pipeline is done, see [the release page](https://gitlab.com/crypto-raider-tools/chrome-ext/-/releases) and [the chrome extension dashboard](https://chrome.google.com/u/1/webstore/devconsole/74be9bab-d5b6-40a4-8335-0dcb7773bb37/eamlgoifaclghociemekfnlanokhpleb/edit/package) to see the compliance state.
